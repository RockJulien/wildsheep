#ifndef DEF_MATHUTILITIES_H
#define DEF_MATHUTILITIES_H

// Includes
#include "Maths\Matrix4x4.h"

// Class definition
class MathUtilities
{
public:

	// Inverse-transpose is just applied to normals.  So zero out 
	// translation row so that it doesn't get into our inverse-transpose
	// calculation--we don't want the inverse-transpose of the translation.
	static Matrix4x4 InverseTranspose(const Matrix4x4& pMatrix)
	{
		Matrix4x4 lCopy = pMatrix;
		lCopy.set_41( 0.0f );
		lCopy.set_42( 0.0f );
		lCopy.set_43( 0.0f );
		lCopy.set_44( 1.0f );

		Matrix4x4 lInverse;
		lCopy.Matrix4x4Inverse( lInverse );

		Matrix4x4 lResult;
		lInverse.Matrix4x4Transpose( lResult );

		return lResult;
	}
};

#endif