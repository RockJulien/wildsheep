#include "AnimationClip.h"
#include "..\Maths\Utility.h"
#include "..\OSCheck.h"

// Namespaces used
using namespace std;

/*************************************************************************************/
AnimationClip::AnimationClip()
{

}

/*************************************************************************************/
AnimationClip::~AnimationClip()
{

}

/*************************************************************************************/
void AnimationClip::Interpolate(FFLOAT pTime, std::vector<Matrix4x4>& pBoneTransforms) const
{
	for(UBIGINT lCurr = 0; lCurr < this->mBoneAnimations.size(); lCurr++)
	{
		this->mBoneAnimations[lCurr].Interpolate( pTime, pBoneTransforms[lCurr] );
	}
}

/*************************************************************************************/
vector<BoneAnimation>& AnimationClip::BoneAnimations()
{
	return this->mBoneAnimations;
}

/*************************************************************************************/
FFLOAT						AnimationClip::ClipStartTime() const
{
	// Find smallest start time over all bones in this clip.
	FFLOAT lTime = MaxFloat;
	for(UBIGINT lCurr = 0; lCurr < this->mBoneAnimations.size(); lCurr++)
	{
		lTime = Smaller(lTime, this->mBoneAnimations[lCurr].StartTime());
	}

	return lTime;
}

/*************************************************************************************/
FFLOAT						AnimationClip::ClipEndTime() const
{
	// Find largest end time over all bones in this clip.
	FFLOAT lTime = 0.0f;
	for(UBIGINT lCurr = 0; lCurr < this->mBoneAnimations.size(); lCurr++)
	{
		lTime = Higher(lTime, this->mBoneAnimations[lCurr].EndTime());
	}

	return lTime;
}
