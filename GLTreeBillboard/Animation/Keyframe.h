#ifndef DEF_KEYFRAME_H
#define DEF_KEYFRAME_H

// Includes
#include "..\Maths\Vector3.h"
#include "..\Maths\Quaternion.h"

// Class definition
class Keyframe
{
public:

	// Attributes
	Vector3    Translation;
	Vector3    Scale;
	Quaternion RotationQuat;
	float      TimePos;

	// Constructor
	Keyframe();
	~Keyframe();

};

#endif