#ifndef DEF_ANIMATIONCLIP_H
#define DEF_ANIMATIONCLIP_H

// Includes
#include "BoneAnimation.h"
#include "..\Maths\Matrix4x4.h"

// Class definition
class AnimationClip
{
private:

	// Attributes
	std::vector<BoneAnimation> mBoneAnimations;

public:

	// Constructor
	AnimationClip();
	~AnimationClip();

	// Methods
	void Interpolate(float pTime, std::vector<Matrix4x4>& pBoneTransforms) const;

	// Accessors
	std::vector<BoneAnimation>& BoneAnimations();
	float						ClipStartTime() const;
	float						ClipEndTime() const;

};

#endif