#include "BoneAnimation.h"
#include "..\OSCheck.h"

// Namespaces used
using namespace std;

/************************************************************************************/
BoneAnimation::BoneAnimation()
{

}

/************************************************************************************/
BoneAnimation::~BoneAnimation()
{

}

/************************************************************************************/
VVOID BoneAnimation::Interpolate(FFLOAT pTime, Matrix4x4& pMatrix) const
{
	if( pTime <= this->mKeyframes.front().TimePos )
	{
		Vector3 lScaling     = this->mKeyframes.front().Scale;
		Vector3 lTranslation = this->mKeyframes.front().Translation;
		Quaternion lRotation = this->mKeyframes.front().RotationQuat;

		Vector3 lOrigin(0.0f, 0.0f, 0.0f);
		Matrix4x4::Matrix4x4AffineTransformation(pMatrix, lScaling, lOrigin, lRotation, lTranslation);
	}
	else if( pTime >= this->mKeyframes.back().TimePos )
	{
		Vector3 lScaling     = this->mKeyframes.back().Scale;
		Vector3 lTranslation = this->mKeyframes.back().Translation;
		Quaternion lRotation = this->mKeyframes.back().RotationQuat;

		Vector3 lOrigin(0.0f, 0.0f, 0.0f);
		Matrix4x4::Matrix4x4AffineTransformation(pMatrix, lScaling, lOrigin, lRotation, lTranslation);
	}
	else
	{
		for(UBIGINT lCurr = 0; lCurr < (UBIGINT)this->mKeyframes.size() - 1; lCurr++)
		{
			if( pTime >= this->mKeyframes[lCurr].TimePos && pTime <= this->mKeyframes[lCurr + 1].TimePos )
			{
				FFLOAT lLerpPercent = (pTime - this->mKeyframes[lCurr].TimePos) / (this->mKeyframes[lCurr + 1].TimePos - this->mKeyframes[lCurr].TimePos);

				Vector3 s0 = this->mKeyframes[lCurr].Scale;
				Vector3 s1 = this->mKeyframes[lCurr + 1].Scale;

				Vector3 p0 = this->mKeyframes[lCurr].Translation;
				Vector3 p1 = this->mKeyframes[lCurr + 1].Translation;

				Quaternion q0 = this->mKeyframes[lCurr].RotationQuat;
				Quaternion q1 = this->mKeyframes[lCurr + 1].RotationQuat;

				Vector3 lScaling     = s0.Vec3LinearInterpolation(s1, lLerpPercent);
				Vector3 lTranslation = p0.Vec3LinearInterpolation(p1, lLerpPercent);
				Quaternion lRotation = q0.QuaternionSLinearInterpolation(q1, lLerpPercent);

				Vector3 lOrigin(0.0f, 0.0f, 0.0f);
				Matrix4x4::Matrix4x4AffineTransformation(pMatrix, lScaling, lOrigin, lRotation, lTranslation);

				break;
			}
		}
	}
}

/************************************************************************************/
vector<Keyframe>& BoneAnimation::KeyFrames()
{
	return this->mKeyframes;
}

/************************************************************************************/
FFLOAT			  BoneAnimation::StartTime() const
{
	// this->mKeyframes are sorted by time, so first keyframe gives start time.
	return this->mKeyframes.front().TimePos;
}

/************************************************************************************/
FFLOAT			  BoneAnimation::EndTime() const
{
	// this->mKeyframes are sorted by time, so last keyframe gives end time.
	FFLOAT lEndTime = this->mKeyframes.back().TimePos;

	return lEndTime;
}
