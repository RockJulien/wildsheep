#ifndef DEF_BONEANIMATION_H
#define DEF_BONEANIMATION_H

// Includes
#include <vector>
#include "Keyframe.h"
#include "..\Maths\Matrix4x4.h"

// Class definition
class BoneAnimation
{
private:

	// Attributes
	std::vector<Keyframe> mKeyframes; 

public:

	// Constructor
	BoneAnimation();
	~BoneAnimation();

	// Methods
	void Interpolate(float pTime, Matrix4x4& pMatrix) const;

	// Accessors
	std::vector<Keyframe>& KeyFrames();
	float				   StartTime() const;
	float				   EndTime() const;

};

#endif