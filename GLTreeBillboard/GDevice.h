#ifndef DEF_GDEVICE_H
#define DEF_GDEVICE_H

#include "gl3w.h"
#include "OSCheck.h"
//#include <gl\GL.h>

#pragma comment(lib, "opengl32.lib")

/***********************
*
*  Extension Definition.
*
************************/
#define WGL_DRAW_TO_WINDOW_ARB         0x2001
#define WGL_ACCELERATION_ARB           0x2003
#define WGL_SWAP_METHOD_ARB            0x2007
#define WGL_SUPPORT_OPENGL_ARB         0x2010
#define WGL_DOUBLE_BUFFER_ARB          0x2011
#define WGL_PIXEL_TYPE_ARB             0x2013
#define WGL_COLOR_BITS_ARB             0x2014
#define WGL_DEPTH_BITS_ARB             0x2022
#define WGL_STENCIL_BITS_ARB           0x2023
#define WGL_FULL_ACCELERATION_ARB      0x2027
#define WGL_SWAP_EXCHANGE_ARB          0x2028
#define WGL_TYPE_RGBA_ARB              0x202B
#define WGL_CONTEXT_MAJOR_VERSION_ARB  0x2091
#define WGL_CONTEXT_MINOR_VERSION_ARB  0x2092
#define GL_ARRAY_BUFFER                0x8892
#define GL_STATIC_DRAW                 0x88E4
#define GL_DYNAMIC_DRAW				   0x88E8
#define GL_READ_ONLY				   0x88B8
#define GL_WRITE_ONLY				   0x88B9
#define GL_READ_WRITE				   0x88BA
#define GL_FRAGMENT_SHADER             0x8B30
#define GL_VERTEX_SHADER               0x8B31
#define TESS_EVALUATION_SHADER         0x8E87
#define TESS_CONTROL_SHADER            0x8E88
#define COMPUTE_SHADER                 0x91B9
#define GL_GEOMETRY_SHADER			   0x8DD9
#define GL_COMPILE_STATUS              0x8B81
#define GL_LINK_STATUS                 0x8B82
#define GL_INFO_LOG_LENGTH             0x8B84
//
//// Texture enum values
#define GL_CLAMP                          0x2900
#define GL_REPEAT                         0x2901
//#define GL_TEXTURE0                    0x84C0
//#define GL_TEXTURE_3D                  0x806F
//#define GL_TEXTURE_DEPTH               0x8071
//#define GL_TEXTURE_2D_ARRAY			   0x8C1A
//#define GL_TEXTURE_WRAP_R              0x8072
//#define GL_CLAMP_TO_EDGE               0x812F
//#define GL_CLAMP_TO_BORDER			   0x812D
//#define GL_GENERATE_MIPMAP_SGIS		   0x8191
//#define GL_MIRRORED_REPEAT             0x8370
//#define GL_TEXTURE_BORDER_COLOR		   0x1004
//#define GL_TEXTURE_CUBE_MAP            0x8513
//#define GL_TEXTURE_CUBE_MAP_POSITIVE_X 0x8515
//#define GL_TEXTURE_CUBE_MAP_NEGATIVE_X 0x8516
//#define GL_TEXTURE_CUBE_MAP_POSITIVE_Y 0x8517
//#define GL_TEXTURE_CUBE_MAP_NEGATIVE_Y 0x8518
//#define GL_TEXTURE_CUBE_MAP_POSITIVE_Z 0x8519
//#define GL_TEXTURE_CUBE_MAP_NEGATIVE_Z 0x851A
#define GL_COMPRESSED_RGB_S3TC_DXT1_EXT  0x83F0 
#define GL_COMPRESSED_RGBA_S3TC_DXT1_EXT 0x83F1 
#define GL_COMPRESSED_RGBA_S3TC_DXT3_EXT 0x83F2 
#define GL_COMPRESSED_RGBA_S3TC_DXT5_EXT 0x83F3
//#define GL_BGR                         0x80E0
//#define GL_BGRA                        0x80E1
//#define GL_DEPTH_COMPONENT16           0x81A5
//#define GL_DEPTH_COMPONENT24           0x81A6
//#define GL_DEPTH_COMPONENT32           0x81A7
//#define GL_ELEMENT_ARRAY_BUFFER        0x8893
//#define GL_MAX_ELEMENTS_INDICES        0x80E9
//#define GL_MAX_ELEMENTS_VERTICES       0x80E8
//#define GL_TEXTURE_MAX_LEVEL		   0x813D
//#define GL_TEXTURE_BASE_LEVEL		   0x813C
//#define GL_RENDERBUFFER				   0x8D41
//#define GL_FRAMEBUFFER				   0x8D40
//#define TESS_CONTROL_OUTPUT_VERTICES   0x8E75
//#define TESS_GEN_MODE                  0x8E76
//#define TESS_GEN_SPACING               0x8E77
//#define TESS_GEN_VERTEX_ORDER          0x8E78
//#define TESS_GEN_POINT_MODE            0x8E79
//#define ISOLINES                       0x8E7A
//#define FRACTIONAL_ODD                 0x8E7B
//#define FRACTIONAL_EVEN                0x8E7C
//#define MAX_PATCH_VERTICES                              0x8E7D
//#define MAX_TESS_GEN_LEVEL                              0x8E7E
//#define MAX_TESS_CONTROL_UNIFORM_COMPONENTS             0x8E7F
//#define MAX_TESS_EVALUATION_UNIFORM_COMPONENTS          0x8E80
//#define MAX_TESS_CONTROL_TEXTURE_IMAGE_UNITS            0x8E81
//#define MAX_TESS_EVALUATION_TEXTURE_IMAGE_UNITS         0x8E82
//#define MAX_TESS_CONTROL_OUTPUT_COMPONENTS              0x8E83
//#define MAX_TESS_PATCH_COMPONENTS                       0x8E84
//#define MAX_TESS_CONTROL_TOTAL_OUTPUT_COMPONENTS        0x8E85
//#define MAX_TESS_EVALUATION_OUTPUT_COMPONENTS           0x8E86
//#define MAX_TESS_CONTROL_UNIFORM_BLOCKS                 0x8E89
//#define MAX_TESS_EVALUATION_UNIFORM_BLOCKS              0x8E8A
//#define MAX_TESS_CONTROL_INPUT_COMPONENTS               0x886C
//#define MAX_TESS_EVALUATION_INPUT_COMPONENTS            0x886D
//#define MAX_COMBINED_TESS_CONTROL_UNIFORM_COMPONENTS    0x8E1E
//#define MAX_COMBINED_TESS_EVALUATION_UNIFORM_COMPONENTS 0x8E1F
//#define MAX_COMPUTE_UNIFORM_BLOCKS                      0x91BB
//#define MAX_COMPUTE_TEXTURE_IMAGE_UNITS                 0x91BC
//#define MAX_COMPUTE_IMAGE_UNIFORMS                      0x91BD
//#define MAX_COMPUTE_SHARED_MEMORY_SIZE                  0x8262
//#define MAX_COMPUTE_UNIFORM_COMPONENTS                  0x8263
//#define MAX_COMPUTE_ATOMIC_COUNTER_BUFFERS              0x8264
//#define MAX_COMPUTE_ATOMIC_COUNTERS                     0x8265
//#define MAX_COMBINED_COMPUTE_UNIFORM_COMPONENTS         0x8266
//#define MAX_COMPUTE_WORK_GROUP_INVOCATIONS              0x90EB
//#define MAX_COMPUTE_WORK_GROUP_COUNT                    0x91BE
//#define MAX_COMPUTE_WORK_GROUP_SIZE                     0x91BF
//#define COMPUTE_WORK_GROUP_SIZE                         0x8267
//#define UNIFORM_BLOCK_REFERENCED_BY_COMPUTE_SHADER      0x90EC
//#define ATOMIC_COUNTER_BUFFER_REFERENCED_BY_COMPUTE_SHADER  0x90ED
//#define DISPATCH_INDIRECT_BUFFER                        0x90EE
//#define DISPATCH_INDIRECT_BUFFER_BINDING                0x90EF
//#define COMPUTE_SHADER_BIT                              0x00000020
//
//// Frame buffer attachment enums
//#define GL_COLOR_ATTACHMENT0							0x8CE0
//#define GL_STENCIL_ATTACHMENT							0x8D20
//#define GL_DEPTH_ATTACHMENT								0x8D00
//#define GL_DEPTH_STENCIL_ATTACHMENT						0x821A
//
//#define GL_SAMPLE_ALPHA_TO_COVERAGE					    0x809E
//
//// Frame buffer debug
//#define GL_FRAMEBUFFER_COMPLETE							0x8CD5
//#define FRAMEBUFFER_INCOMPLETE_ATTACHMENT               0x8CD6
//#define FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT       0x8CD7
//#define FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER              0x8CDB
//#define FRAMEBUFFER_INCOMPLETE_READ_BUFFER              0x8CDC
//#define FRAMEBUFFER_UNSUPPORTED                         0x8CDD
//#define FRAMEBUFFER_INCOMPLETE_MULTISAMPLE              0x8D56
//#define FRAMEBUFFER_UNDEFINED                           0x8219
//
//// Texture extras
//#define GL_DEPTH24_STENCIL8                             0x88F0
//#define GL_TEXTURE_COMPARE_MODE							0x884C
//#define GL_TEXTURE_COMPARE_FUNC							0x884D
//#define GL_COMPARE_R_TO_TEXTURE							0x884E
//#define GL_TEXTURE_DEPTH_SIZE					        0x884A
//#define GL_DEPTH_TEXTURE_MODE					        0x884B
//#define GL_GENERATE_MIPMAP								0x8191
#define GL_TEXTURE_MAX_ANISOTROPY						0x84FE
//#define GL_MAX_TEXTURE_MAX_ANISOTROPY					0x84FF
//#define GL_TEXTURE_COMPRESSED							0x86A1
//
//// Blending
//#define GL_FUNC_ADD										0x8006
//
/*******************************
*
*  Extension type definition.
*
*******************************/
typedef BOOL (WINAPI * PFNWGLCHOOSEPIXELFORMATARBPROC) (HDC hdc, const int *piAttribIList, const FLOAT *pfAttribFList, UINT nMaxFormats, 
                                                        int *piFormats, UINT *nNumFormats);
typedef HGLRC (WINAPI * PFNWGLCREATECONTEXTATTRIBSARBPROC) (HDC hDC, HGLRC hShareContext, const int *attribList);
typedef BOOL (WINAPI * PFNWGLSWAPINTERVALEXTPROC) (int interval);
//typedef void (APIENTRY * PFNGLATTACHSHADERPROC) (GLuint program, GLuint shader);
//typedef void (APIENTRY * PFNGLBINDBUFFERPROC) (GLenum target, GLuint buffer);
//typedef void (APIENTRY * PFNGLBINDVERTEXARRAYPROC) (GLuint array);
//typedef void (APIENTRY * PFNGLBUFFERDATAPROC) (GLenum target, ptrdiff_t size, const GLvoid *data, GLenum usage);
//typedef void (APIENTRY * PFNGLCOMPILESHADERPROC) (GLuint shader);
//typedef GLuint (APIENTRY * PFNGLCREATEPROGRAMPROC) (void);
//typedef GLuint (APIENTRY * PFNGLCREATESHADERPROC) (GLenum type);
//typedef void (APIENTRY * PFNGLDELETEBUFFERSPROC) (GLsizei n, const GLuint *buffers);
//typedef void (APIENTRY * PFNGLDELETEPROGRAMPROC) (GLuint program);
//typedef void (APIENTRY * PFNGLDELETESHADERPROC) (GLuint shader);
//typedef void (APIENTRY * PFNGLDELETEVERTEXARRAYSPROC) (GLsizei n, const GLuint *arrays);
//typedef void (APIENTRY * PFNGLDETACHSHADERPROC) (GLuint program, GLuint shader);
//typedef void (APIENTRY * PFNGLENABLEVERTEXATTRIBARRAYPROC) (GLuint index);
//typedef void (APIENTRY * PFNGLGENBUFFERSPROC) (GLsizei n, GLuint *buffers);
//typedef void (APIENTRY * PFNGLGENVERTEXARRAYSPROC) (GLsizei n, GLuint *arrays);
//typedef GLint (APIENTRY * PFNGLGETATTRIBLOCATIONPROC) (GLuint program, const char *name);
//typedef void (APIENTRY * PFNGLGETPROGRAMINFOLOGPROC) (GLuint program, GLsizei bufSize, GLsizei *length, char *infoLog);
//typedef void (APIENTRY * PFNGLGETPROGRAMIVPROC) (GLuint program, GLenum pname, GLint *params);
//typedef void (APIENTRY * PFNGLGETSHADERINFOLOGPROC) (GLuint shader, GLsizei bufSize, GLsizei *length, char *infoLog);
//typedef void (APIENTRY * PFNGLGETSHADERIVPROC) (GLuint shader, GLenum pname, GLint *params);
//typedef void (APIENTRY * PFNGLLINKPROGRAMPROC) (GLuint program);
//typedef void (APIENTRY * PFNGLSHADERSOURCEPROC2) (GLuint shader, GLsizei count, const char* *string, const GLint *length);
//typedef void (APIENTRY * PFNGLUSEPROGRAMPROC) (GLuint program);
//typedef void (APIENTRY * PFNGLVERTEXATTRIBPOINTERPROC) (GLuint index, GLint size, GLenum type, GLboolean normalized, GLsizei stride, 
//                                                        const GLvoid *pointer);
//typedef void (APIENTRY * PFNGLBINDATTRIBLOCATIONPROC) (GLuint program, GLuint index, const char *name);
//typedef GLint (APIENTRY * PFNGLGETUNIFORMLOCATIONPROC) (GLuint program, const char *name);
//typedef void (APIENTRY * PFNGLUNIFORMMATRIX4FVPROC) (GLint location, GLsizei count, GLboolean transpose, const GLfloat *value);
//typedef void (APIENTRY * PFNGLACTIVETEXTUREPROC) (GLenum texture);
//typedef void (APIENTRY * PFNGLCOMPRESSEDTEXTIMAGE2DARB) (GLenum target, GLint level, GLenum internalformat, GLsizei width, GLsizei height, GLint border, GLsizei imageSize, const GLvoid *data ); 
//typedef void (APIENTRY * PFNGLTEXIMAGE3DPROC) (GLenum target, GLint level, GLint internalformat, GLsizei width, GLsizei height, GLsizei depth, GLint border, GLenum format, GLenum type, const GLvoid *data );
//typedef void (APIENTRY * PFNGLCOMPRESSEDTEXIMAGE3DPROC) (GLenum target, GLint level, GLenum internalformat, GLsizei width, GLsizei height, GLsizei depth, GLint border, GLsizei imageSize, const GLvoid *data );
//typedef void (APIENTRY * PFNGLUNIFORM1IPROC) (GLint location, GLint v0);
//typedef void (APIENTRY * PFNGLGENERATEMIPMAPPROC) (GLenum target);
//typedef void (APIENTRY * PFNGLDISABLEVERTEXATTRIBARRAYPROC) (GLuint index);
//typedef void (APIENTRY * PFNGLUNIFORM1FPROC) (GLint location, GLfloat v0);
//typedef void (APIENTRY * PFNGLUNIFORM3FVPROC) (GLint location, GLsizei count, const GLfloat *value);
//typedef void (APIENTRY * PFNGLUNIFORM4FVPROC) (GLint location, GLsizei count, const GLfloat *value);
//typedef void (APIENTRY * PFNGLDRAWRANGEELEMENTSPROC) (GLenum pMode, GLuint pStartIndex, GLuint pEndIndex, GLsizei pCount, GLenum pType, const GLvoid* pIndices);
//typedef void (APIENTRY * PFNGLBINDRENDERBUFFERPROC) (GLenum pTarget, GLuint pRenderBuffer);
//typedef void (APIENTRY * PFNGLRENDERBUFFERSTORAGEPROC) (GLenum pTarget, GLenum pInternalFormat, GLsizei pWidth, GLsizei pHeight);
//typedef void (APIENTRY * PFNGLFRAMEBUFFERRENDERBUFFERPROC) (GLenum pTarget, GLenum pAttachment, GLenum pRenderBufferTarget, GLuint pRenderBuffer);
//typedef void (APIENTRY * PFNGLGENFRAMEBUFFERSPROC) (GLsizei pCount, GLuint* pIds);
//typedef void (APIENTRY * PFNGLBINDFRAMEBUFFERPROC) (GLenum pTarget, GLuint pFrameBuffer);
//typedef void (APIENTRY * PFNGLFRAMEBUFFERTEXTUREPROC) (GLenum pTarget, GLenum pAttachment, GLuint pTexture, GLint pLevel);
//typedef void (APIENTRY * PFNGLFRAMEBUFFERTEXTURE1DPROC) (GLenum pTarget, GLenum pAttachment, GLenum pTexTarget, GLuint pTexture, GLint pLevel);
//typedef void (APIENTRY * PFNGLFRAMEBUFFERTEXTURE2DPROC) (GLenum pTarget, GLenum pAttachment, GLenum pTexTarget, GLuint pTexture, GLint pLevel);
//typedef void (APIENTRY * PFNGLFRAMEBUFFERTEXTURE3DPROC) (GLenum pTarget, GLenum pAttachment, GLenum pTexTarget, GLuint pTexture, GLint pLevel, GLint pLayer);
//typedef void (APIENTRY * PFNGLDELETEFRAMEBUFFERSPROC2) (GLsizei pCount, GLuint* pFramebuffers);
//typedef void (APIENTRY * PFNGLDELETERENDERBUFFERSPROC2) (GLsizei pCount, GLuint* pRenderbuffers);
//typedef void (APIENTRY * PFNGLDRAWBUFFERSPROC) (GLsizei pCount, const GLenum* pBuffers);
//typedef void (APIENTRY * PFNGLDRAWELEMENTSBASEVERTEXPROC2) (GLenum pMode, GLsizei pCount, GLenum pType, GLvoid* pIndices, GLint pBaseVertex);
//typedef void (APIENTRY * PFNGLGENRENDERBUFFERSPROC) (GLsizei pCount, GLuint* pRenderBuffers);
//typedef void (APIENTRY * PFNGLCLEARBUFFERIVPROC) (GLenum pBuffer, GLint pDrawBuffer, const GLint* pValue);
//typedef void (APIENTRY * PFNGLCLEARBUFFERUIVPROC) (GLenum pBuffer, GLint pDrawBuffer, const GLuint* pValue);
//typedef void (APIENTRY * PFNGLCLEARBUFFERFVPROC) (GLenum pBuffer, GLint pDrawBuffer, const GLfloat* pValue);
//typedef void (APIENTRY * PFNGLCLEARBUFFERFIPROC) (GLenum pBuffer, GLint pDrawBuffer, GLfloat pDepth, GLint pStencil);
//typedef void* (APIENTRY * PFNGLMAPBUFFERPROC) (GLenum pTarget, GLenum pAccess);
//typedef GLboolean (APIENTRY * PFNGLUNMAPBUFFERPROC) (GLenum pTarget);
//
//// Textures extensions
//typedef void (APIENTRY* PFNGLTEXSUBIMAGE3DPROC) (GLenum pTarget, GLint pLevel, GLint pXOffset, GLint pYOffset, GLint pZOffset, GLsizei pWidth, GLsizei pHeight, GLsizei pDepth, GLenum pFormat, GLenum pType, const GLvoid* pData);
//
//// Blending
//typedef void (APIENTRY * PFNGLBLENDEQUATIONSEPARATEPROC) (GLenum pModeRGB, GLenum pModeAlpha);
//typedef void (APIENTRY * PFNGLBLENDFUNCSEPARATEPROC) (GLenum pSrcRGB, GLenum pDstRGB, GLenum pSrcAlpha, GLenum pDstAlpha);
//typedef void (APIENTRY * PFNGLBLENDCOLORPROC) (GLclampf pRed, GLclampf pGreen, GLclampf pBlue, GLclampf pAlpha);
//
//// Frame buffer debug method
//typedef GLenum (APIENTRY * PFNGLCHECKFRAMEBUFFERSTATUSPROC) (GLenum pTarget);
//
//// Tesselation functions


class GDevice
{
private:

	// Attributes
	HDC   m_deviceContext;
	HGLRC m_renderContext;
	CCHAR m_cGPUDesc[128];

	// Interface function
	PFNWGLCHOOSEPIXELFORMATARBPROC    wglChoosePixFmtARB;
	PFNWGLCREATECONTEXTATTRIBSARBPROC wglCreateContextAttrARB;
	PFNWGLSWAPINTERVALEXTPROC         wglSwapIntervalExt;

	//
	BBOOL LoadExtension();

public:

	// Constructor & Destructor
	GDevice();
	~GDevice();

	// Methods
	BBOOL  InitializeExtensions(HWND winInst);
	BBOOL  InitializeDevice(HWND winInst, BIGINT width, BIGINT height, FFLOAT fFar, FFLOAT fNear, BBOOL vsync);
	BBOOL  StartRender(BBOOL clearPix, BBOOL clearDepthStencil); // Start Rendering, with a first call to clear buffer
	VVOID  StopRender();
	VVOID  Release(HWND winInst);

	// Interface function.
	PFNGLDELETEBUFFERSPROC       DeleteBuffers;
	PFNGLDELETEPROGRAMPROC       DeleteProgram;
	PFNGLDELETESHADERPROC        DeleteShader;
	PFNGLDELETEVERTEXARRAYSPROC  DeleteVertArrays;
	PFNGLATTACHSHADERPROC        AttachShader;
	PFNGLBINDBUFFERPROC          BindBuffer;
	PFNGLBINDVERTEXARRAYPROC     BindVertArray;
	PFNGLBUFFERDATAPROC          BufferData;
	PFNGLCOMPILESHADERPROC       CompileShader;
	PFNGLCREATEPROGRAMPROC       CreateProgram;
	PFNGLCREATESHADERPROC        CreateShader;
	PFNGLDETACHSHADERPROC        DetachShader;
	PFNGLENABLEVERTEXATTRIBARRAYPROC EnableVertAttrArrays;
	PFNGLGENBUFFERSPROC          GenBuffers;
	PFNGLGENVERTEXARRAYSPROC     GenVertArrays;
	PFNGLGETATTRIBLOCATIONPROC   GetAttrLocation;
	PFNGLGETPROGRAMINFOLOGPROC   GetProgramInfoLog;
	PFNGLGETPROGRAMIVPROC        GetProgramIV;
	PFNGLGETSHADERINFOLOGPROC    GetShaderInfoLog;
	PFNGLGETSHADERIVPROC         GetShaderIV;
	PFNGLLINKPROGRAMPROC         LinkProgram;
	PFNGLSHADERSOURCEPROC        ShaderSource;
	PFNGLUSEPROGRAMPROC          UseProgram;
	PFNGLVERTEXATTRIBPOINTERPROC VertAttrPointer;
	PFNGLBINDATTRIBLOCATIONPROC  BindAttrLocation;
	PFNGLGETUNIFORMLOCATIONPROC  GetUniformLocation;
	PFNGLUNIFORMMATRIX4FVPROC    UniformMat4fv; // Mat4x4.
	PFNGLACTIVETEXTUREPROC       ActiveTexture;
	PFNGLCOMPRESSEDTEXIMAGE2DPROC CompressTextImg2D;
	PFNGLTEXIMAGE3DPROC           TexImage3D;
	PFNGLCOMPRESSEDTEXIMAGE3DPROC CompressTextImg3D;
	PFNGLUNIFORM1IPROC            Uniform1i;     // int 1 (BBOOL as well)
	PFNGLGENERATEMIPMAPPROC       GenerateMipMap;
	PFNGLDISABLEVERTEXATTRIBARRAYPROC DisableVertAttrArray;
	PFNGLUNIFORM1FPROC           Uniform1f;     // only a single float.
	PFNGLUNIFORM3FVPROC          Uniform3fv;    // float 3
	PFNGLUNIFORM4FVPROC          Uniform4fv;    // float 4 (homogeneous coordinates).
	PFNGLBINDRENDERBUFFERPROC    BindRenderbuffer;
	PFNGLRENDERBUFFERSTORAGEPROC RenderbufferStorage;
	PFNGLFRAMEBUFFERRENDERBUFFERPROC FramebufferRenderbuffer;
	PFNGLGENFRAMEBUFFERSPROC	   GenFrameBuffers;
	PFNGLGENRENDERBUFFERSPROC      GenRenderBuffers;
	PFNGLBINDFRAMEBUFFERPROC       BindFramebuffer;
	PFNGLFRAMEBUFFERTEXTUREPROC    FrameBufferTexture;
	PFNGLFRAMEBUFFERTEXTURE1DPROC  FrameBufferTexture1D;
	PFNGLFRAMEBUFFERTEXTURE2DPROC  FrameBufferTexture2D;
	PFNGLFRAMEBUFFERTEXTURE3DPROC  FrameBufferTexture3D;
	PFNGLTEXSUBIMAGE3DPROC		   TexSubImage3D;
	PFNGLDELETEFRAMEBUFFERSPROC    DeleteFrameBuffers;
	PFNGLDELETERENDERBUFFERSPROC   DeleteRenderBuffers;
	PFNGLMAPBUFFERPROC			   MapBuffer;
	PFNGLUNMAPBUFFERPROC		   UnmapBuffer;

	// Blending
	PFNGLBLENDEQUATIONSEPARATEPROC BlendEquationSeparate;
	PFNGLBLENDFUNCSEPARATEPROC	   BlendFuncSeparate;
	PFNGLBLENDCOLORPROC			   BlendColor;

	// Frame buffer debug
	PFNGLCHECKFRAMEBUFFERSTATUSPROC CheckFrameBufferStatus;

	// Draw extensions
	PFNGLDRAWRANGEELEMENTSPROC      DrawRangeElements;
	PFNGLDRAWBUFFERSPROC		    DrawBuffers;
	PFNGLDRAWELEMENTSBASEVERTEXPROC DrawElementsBaseVertex;
	PFNGLCLEARBUFFERIVPROC			ClearBufferiv;
	PFNGLCLEARBUFFERUIVPROC			ClearBufferuiv;
	PFNGLCLEARBUFFERFVPROC			ClearBufferfv;
	PFNGLCLEARBUFFERFIPROC			ClearBufferfi;
};

typedef GDevice OpenGLClass;

#endif
