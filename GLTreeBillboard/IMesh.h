#ifndef DEF_IMESH_H
#define DEF_IMESH_H

#include "GDevice.h"

// GLOBAL Variables.
const BBOOL  g_bFullscreen = false;
const BBOOL  g_bVSync = true;
const FFLOAT g_fFar  = 1000.0f;
const FFLOAT g_fNear = 0.1f;

class IMesh
{
public:

		// Constructor & Destructor
		IMesh() { }
virtual ~IMesh() { }

		// Methods
virtual BBOOL Initialize(GDevice* device, CCHAR* textName, 
						 UBIGINT* textUnit, BBOOL wrap = true) = 0;
virtual BBOOL Update(float deltaTime)  = 0;
virtual BBOOL Render(GDevice* device)  = 0;
virtual VVOID Release(GDevice* device) = 0;

};

#endif