#ifndef DEF_VERTEXTYPES_H
#define DEF_VERTEXTYPES_H

/*****************************************************************
/  (c) Copyright 2012-2015 OcelotEngine All right reserved.
/
/  File name:   VertexTypes.h/cpp
/   
/  Description: This file provides multiple class for vertices 
/               of different kind of objects and effect intended.
/               Going from simple shape colored with simple Colors
/               to more complex ones with normal and tangent for 
/               Phong shading processing for example.
/               Particular structure for particles is provided
/               as well.
/
/  Author:      Larbi Julien
/  Version:     1.0
/  Creation:    17/02/2012
/*****************************************************************/

#include "Maths\Color.h"
#include "Maths\Vector3.h"
#include "Maths\Vector2.h"
#include "OSCheck.h"

namespace Ocelot
{
	// Special vertex class for CubeMap
	// which could serve as Base vertex
	// class.
	struct DomeVertex
	{
		// Constructors
		DomeVertex() :
		m_vPosition(0.0f) { }
		DomeVertex(Vector3 position) :
		m_vPosition(position) { }
		DomeVertex(FFLOAT posX, FFLOAT posY, FFLOAT posZ) :
		m_vPosition(posX, posY, posZ) { }

		// Attributes
		Vector3 m_vPosition;
	};

	struct TreePointSprite
	{
		// Constructors
		TreePointSprite() :
		Position(0.0f), Size(0.0f) { }
		TreePointSprite(Vector3 pPosition, Vector2 pSize) :
		Position(pPosition), Size(pSize) { }
		TreePointSprite(FFLOAT pPosX, FFLOAT pPosY, FFLOAT pPosZ, Vector2 pSize) :
		Position(pPosX, pPosY, pPosZ), Size(pSize) { }
		TreePointSprite(FFLOAT pPosX, FFLOAT pPosY, FFLOAT pPosZ, FFLOAT pSizeX, FFLOAT pSizeY) :
		Position(pPosX, pPosY, pPosZ), Size(pSizeX, pSizeY) { }

		// Attributes
		Vector3 Position;
		Vector2 Size;
	};


	// Basic vertex class with only 
	// position and color components.
	struct BasicVertex
	{
		// Constructors
		BasicVertex() :
		m_color(0.0f, 0.0f, 0.0f), m_vPosition(0.0f) { }
		BasicVertex(Vector3 position, Color color) :
		m_color(color), m_vPosition(position) { }
		BasicVertex(FFLOAT posX, FFLOAT posY, FFLOAT posZ, Color color) :
		m_color(color), m_vPosition(posX, posY, posZ) { }
		BasicVertex(FFLOAT posX, FFLOAT posY, FFLOAT posZ, FFLOAT red, FFLOAT green, FFLOAT blue, FFLOAT alpha) :
		m_color(red, green , blue, alpha), m_vPosition(posX, posY, posZ) { }

		// Attributes
		Color   m_color;
		Vector3 m_vPosition;
	};

	// Simple enhanced vertex class with position, 
	// normal and texture coordinates allowing 
	// basic lighting and texturization.
	struct SimpleVertex
	{
		// Constructors
		SimpleVertex() :
		Position(0.0f), Normal(0.0f), TexCoord(0.0f) { }
		SimpleVertex(Vector3 pPosition, Vector3 pNormal, Vector2 pTexCoord) :
		Position(pPosition), Normal(pNormal), TexCoord(pTexCoord) { }
		SimpleVertex(FFLOAT pPosX, FFLOAT pPosY, FFLOAT pPosZ, Vector3 pNormal, Vector2 pTexCoord) :
		Position(pPosX, pPosY, pPosZ), Normal(pNormal), TexCoord(pTexCoord) { }
		SimpleVertex(FFLOAT pPosX, FFLOAT pPosY, FFLOAT pPosZ, FFLOAT pNormX, FFLOAT pNormY, FFLOAT pNormZ, Vector2 pTexCoord) :
		Position(pPosX, pPosY, pPosZ), Normal(pNormX, pNormY, pNormZ), TexCoord(pTexCoord) { }
		SimpleVertex(FFLOAT pPosX, FFLOAT pPosY, FFLOAT pPosZ, FFLOAT pNormX, FFLOAT pNormY, FFLOAT pNormZ, FFLOAT pTexU, FFLOAT pTexV) :
		Position(pPosX, pPosY, pPosZ), Normal(pNormX, pNormY, pNormZ), TexCoord(pTexU, pTexV) { }
	
		// Attributes
		Vector3 Position;
		Vector3 Normal;
		Vector2 TexCoord;
	};

	// Simple volumetric line vertex declaration.
	struct VolumetricLineVertex
	{
		// Constructors
		VolumetricLineVertex() :
		Position(0.0f), OtherPosition(0.0f), OffsetAndUV(0.0f, 0.0f) { }
		VolumetricLineVertex(Vector3 pPosition, Vector3 pOtherPosition, Vector4 pOffsetAndUV) :
		Position(pPosition), OtherPosition(pOtherPosition), OffsetAndUV(pOffsetAndUV) { }
		VolumetricLineVertex(FFLOAT pPosX, FFLOAT pPosY, FFLOAT pPosZ, Vector3 pOtherPosition, Vector4 pTexCoord) :
		Position(pPosX, pPosY, pPosZ), OtherPosition(pOtherPosition), OffsetAndUV(pTexCoord) { }
		VolumetricLineVertex(FFLOAT pPosX, FFLOAT pPosY, FFLOAT pPosZ, FFLOAT pOtherPositionX, FFLOAT pOtherPositionY, FFLOAT pOtherPositionZ, Vector4 pTexCoord) :
		Position(pPosX, pPosY, pPosZ), OtherPosition(pOtherPositionX, pOtherPositionY, pOtherPositionZ), OffsetAndUV(pTexCoord) { }
		VolumetricLineVertex(FFLOAT pPosX, FFLOAT pPosY, FFLOAT pPosZ, FFLOAT pOtherPositionX, FFLOAT pOtherPositionY, FFLOAT pOtherPositionZ, FFLOAT pOffsetX, FFLOAT pOffsetY, FFLOAT pTexU, FFLOAT pTexV) :
		Position(pPosX, pPosY, pPosZ), OtherPosition(pOtherPositionX, pOtherPositionY, pOtherPositionZ), OffsetAndUV(pOffsetX, pOffsetY, pTexU, pTexV) { }
	
		// Attributes
		Vector4 OffsetAndUV;
		Vector3 Position;
		Vector3 OtherPosition;
	};

	// Particular vertex class for Particules.
	// Can be used for Fire, Rain, Snow, etc...
	// Extracted from the example of Franck LUNA.
	struct LunaPartVertex
	{

		// Constructors
		LunaPartVertex() :
		m_vPosition(0.0f), m_vVelocity(0.0f), m_vScale(0.0f), 
		m_fAge(0.0f), m_iType(0) {}
		LunaPartVertex(Vector3 position, Vector3 velocity, Vector2 scale, FFLOAT age, UBIGINT type) :
		m_vPosition(position), m_vVelocity(velocity), m_vScale(scale), 
		m_fAge(age), m_iType(type) {}
		LunaPartVertex(FFLOAT posX, FFLOAT posY, FFLOAT posZ, Vector3 velocity, Vector2 scale, FFLOAT age, UBIGINT type) :
		m_vPosition(posX, posY, posZ), m_vVelocity(velocity), m_vScale(scale), 
		m_fAge(age), m_iType(type) {}
		LunaPartVertex(FFLOAT posX, FFLOAT posY, FFLOAT posZ, FFLOAT velX, FFLOAT velY, FFLOAT velZ, Vector2 scale, FFLOAT age, UBIGINT type) :
		m_vPosition(posX, posY, posZ), m_vVelocity(velX, velY, velZ), m_vScale(scale), 
		m_fAge(age), m_iType(type) {}
		LunaPartVertex(FFLOAT posX, FFLOAT posY, FFLOAT posZ, FFLOAT velX, FFLOAT velY, FFLOAT velZ, FFLOAT scaleX, FFLOAT scaleY, FFLOAT age, UBIGINT type) :
		m_vPosition(posX, posY, posZ), m_vVelocity(velX, velY, velZ), m_vScale(scaleX, scaleY), 
		m_fAge(age), m_iType(type) {}

		// Attributes
		Vector3 m_vPosition;
		Vector3 m_vVelocity; // moving entity involving a velocity.
		Vector2 m_vScale;
		FFLOAT  m_fAge;      // the age of the particule at which it will die.
		UBIGINT m_iType;     // the particule type.
	};

	// Enhanced vertex structure with position, normal,
	// tangent and texture coordinates allowing
	// bump mapping enhancing lighting and texturing.
	struct EnhancedVertex
	{
		// constructors
		EnhancedVertex(){}
		EnhancedVertex(Vector3 pos, Vector3 norm, Vector4 tang, Vector2 tex) :
		Position(pos), Normal(norm), Tangent(tang), TexCoord(tex){}
		EnhancedVertex(float posX, float posY, float posZ, Vector3 norm, Vector4 tan, Vector2 tex) :
		Position(posX, posY, posZ), Normal(norm), Tangent(tan), TexCoord(tex){}
		EnhancedVertex(float posX, float posY, float posZ, float normX, float normY, float normZ, Vector4 tan, Vector2 tex) :
		Position(posX, posY, posZ), Normal(normX, normY, normZ), Tangent(tan), TexCoord(tex){}
		EnhancedVertex(float posX, float posY, float posZ, float normX, float normY, float normZ, float tanX, float tanY, float tanZ, float tanW, Vector2 tex) :
		Position(posX, posY, posZ), Normal(normX, normY, normZ), Tangent(tanX, tanY, tanZ, tanW), TexCoord(tex){}
		EnhancedVertex(float posX, float posY, float posZ, float normX, float normY, float normZ, float tanX, float tanY, float tanZ, float tanW, float texU, float texV) :
		Position(posX, posY, posZ), Normal(normX, normY, normZ), Tangent(tanX, tanY, tanZ, tanW), TexCoord(texU, texV){}

		// attributes
		Vector3 Position;
		Vector3 Normal;
		Vector4 Tangent;
		Vector2 TexCoord;
	};

	struct UVector4
	{
		UBIGINT X;
		UBIGINT Y;
		UBIGINT Z;
		UBIGINT W;

		UVector4() {}
		UVector4(UBIGINT pX, UBIGINT pY, UBIGINT pZ, UBIGINT pW) :
		X(pX), Y(pY), Z(pZ), W(pW)
		{

		}
	};

	// Skinned vertex owning a weight for skin at the vertex
	// and bone indices in addition of the enhanced vertex type.
	struct SkinnedVertex
	{
		SkinnedVertex(){}
		SkinnedVertex(Vector3 pos, Vector3 norm, Vector4 tang, Vector2 tex, Vector3 pWeight, UVector4 pBoneIndices) :
		Position(pos), Normal(norm), Tangent(tang), TexCoord(tex), Weights(pWeight), BoneIndices(pBoneIndices) { }
		SkinnedVertex(float posX, float posY, float posZ, Vector3 norm, Vector4 tan, Vector2 tex, Vector3 pWeight, UVector4 pBoneIndices) :
		Position(posX, posY, posZ), Normal(norm), Tangent(tan), TexCoord(tex), Weights(pWeight), BoneIndices(pBoneIndices) { }
		SkinnedVertex(float posX, float posY, float posZ, float normX, float normY, float normZ, Vector4 tan, Vector2 tex, Vector3 pWeight, UVector4 pBoneIndices) :
		Position(posX, posY, posZ), Normal(normX, normY, normZ), Tangent(tan), TexCoord(tex), Weights(pWeight), BoneIndices(pBoneIndices) { }
		SkinnedVertex(float posX, float posY, float posZ, float normX, float normY, float normZ, float tanX, float tanY, float tanZ, float tanW, Vector2 tex, Vector3 pWeight, UVector4 pBoneIndices) :
		Position(posX, posY, posZ), Normal(normX, normY, normZ), Tangent(tanX, tanY, tanZ, tanW), TexCoord(tex), Weights(pWeight), BoneIndices(pBoneIndices) { }
		SkinnedVertex(float posX, float posY, float posZ, float normX, float normY, float normZ, float tanX, float tanY, float tanZ, float tanW, float texU, float texV, Vector3 pWeight, UVector4 pBoneIndices) :
		Position(posX, posY, posZ), Normal(normX, normY, normZ), Tangent(tanX, tanY, tanZ, tanW), TexCoord(texU, texV), Weights(pWeight), BoneIndices(pBoneIndices) { }
		SkinnedVertex(float posX, float posY, float posZ, float normX, float normY, float normZ, float tanX, float tanY, float tanZ, float tanW, float texU, float texV, float pWeightX, float pWeightY, float pWeightZ, UVector4 pBoneIndices) :
		Position(posX, posY, posZ), Normal(normX, normY, normZ), Tangent(tanX, tanY, tanZ, tanW), TexCoord(texU, texV), Weights(pWeightX, pWeightY, pWeightZ), BoneIndices(pBoneIndices) { }
		SkinnedVertex(float posX, float posY, float posZ, float normX, float normY, float normZ, float tanX, float tanY, float tanZ, float tanW, float texU, float texV, float pWeightX, float pWeightY, float pWeightZ, UBIGINT pBoneIndice1, UBIGINT pBoneIndice2, UBIGINT pBoneIndice3, UBIGINT pBoneIndice4) :
		Position(posX, posY, posZ), Normal(normX, normY, normZ), Tangent(tanX, tanY, tanZ, tanW), TexCoord(texU, texV), Weights(pWeightX, pWeightY, pWeightZ), BoneIndices(pBoneIndice1, pBoneIndice2, pBoneIndice3, pBoneIndice4) { }

		// attributes
		Vector3 Position;
		Vector3 Normal;
		Vector4 Tangent;
		Vector2 TexCoord;
		Vector3 Weights;
		UVector4 BoneIndices;
	};

	// Particle vertex structure.
	struct ParticleVertex
	{
		// Constructors.
		ParticleVertex(){}
		ParticleVertex(Vector3 position, Vector2 tex, Vector2 lifeNRot, UBIGINT color) :
		Position(position), TexCoord(tex), LifeNRot(lifeNRot), Color(color) {}
		ParticleVertex(FFLOAT posX, FFLOAT posY, FFLOAT posZ, Vector2 tex, Vector2 lifeNRot, UBIGINT color) :
		Position(posX, posY, posZ), TexCoord(tex), LifeNRot(lifeNRot), Color(color) {}
		ParticleVertex(FFLOAT posX, FFLOAT posY, FFLOAT posZ, FFLOAT texU, FFLOAT texV, Vector2 lifeNRot, UBIGINT color) :
		Position(posX, posY, posZ), TexCoord(texU, texV), LifeNRot(lifeNRot), Color(color) {}
		ParticleVertex(FFLOAT posX, FFLOAT posY, FFLOAT posZ, FFLOAT texU, FFLOAT texV, FFLOAT life, FFLOAT rot, UBIGINT color) :
		Position(posX, posY, posZ), TexCoord(texU, texV), LifeNRot(life, rot), Color(color) {}

		// Attributes.
		Vector3 Position;
		Vector2 TexCoord;
		Vector2 LifeNRot;
		UBIGINT Color;
	};

	struct VoxelVertex
	{
		VoxelVertex() :
		m_vPosition(0.0f), m_vNormal(1.0f) {}
		VoxelVertex(Vector3 position, Vector3 normal) :
		m_vPosition(position), m_vNormal(normal) {}
		VoxelVertex(FFLOAT posX, FFLOAT posY, FFLOAT posZ, Vector3 normal) :
		m_vPosition(posX, posY, posZ), m_vNormal(normal) {}
		VoxelVertex(FFLOAT posX, FFLOAT posY, FFLOAT posZ, FFLOAT normX, FFLOAT normY, FFLOAT normZ) :
		m_vPosition(posX, posY, posZ), m_vNormal(normX, normY, normZ) {}

		Vector3 m_vPosition;
		Vector3 m_vNormal;
	};

	// Trail vertex
	struct TrailVertex
	{
		// Constructors
		TrailVertex() :
			Position(0.0f), Normal(0.0f), TexCoordNIndex(0.0f) { }
		TrailVertex(Vector3 pPosition, Vector3 pNormal, Vector3 pTexCoordNIndex) :
			Position(pPosition), Normal(pNormal), TexCoordNIndex(pTexCoordNIndex) { }
		TrailVertex(FFLOAT pPosX, FFLOAT pPosY, FFLOAT pPosZ, Vector3 pNormal, Vector3 pTexCoordNIndex) :
			Position(pPosX, pPosY, pPosZ), Normal(pNormal), TexCoordNIndex(pTexCoordNIndex) { }
		TrailVertex(FFLOAT pPosX, FFLOAT pPosY, FFLOAT pPosZ, FFLOAT pNormalX, FFLOAT pNormalY, FFLOAT pNormalZ, Vector3 pTexCoordNIndex) :
			Position(pPosX, pPosY, pPosZ), Normal(pNormalX, pNormalY, pNormalZ), TexCoordNIndex(pTexCoordNIndex) { }
		TrailVertex(FFLOAT pPosX, FFLOAT pPosY, FFLOAT pPosZ, FFLOAT pNormalX, FFLOAT pNormalY, FFLOAT pNormalZ, FFLOAT pTexU, FFLOAT pTexV, UBIGINT pIndex) :
			Position(pPosX, pPosY, pPosZ), Normal(pNormalX, pNormalY, pNormalZ), TexCoordNIndex(pTexU, pTexV, (FFLOAT)pIndex) { }

		// Attributes
		Vector3 Position;
		Vector3 Normal;
		Vector3 TexCoordNIndex;
	};
}

#endif