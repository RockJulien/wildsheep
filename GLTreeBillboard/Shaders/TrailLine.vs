
#define PI 3.1415926535897932384626433832795

/////////////////////
// INPUT VARIABLES //
/////////////////////
in vec3 PosL;		    // Local position
in vec3 NormalL;        // Local normal
in vec3 TexCoordNIndex; // Texture coordinates + quad index

//////////////////////
// OUTPUT VARIABLES //
//////////////////////
out vec2 TexOut1;     // Texture coordinates 1
out vec2 TexOut2;     // Texture coordinates 2
flat out int ColumnIndex;  // Column index

///////////////////////
// UNIFORM VARIABLES //
///////////////////////
uniform mat4  gWorld;
uniform mat4  gWorldViewProj;
uniform mat4  gTexTransform;
uniform vec3  gEyePosW;
uniform vec3  gLineAxis;
uniform float gTime;
uniform float gAmplitude;
uniform bool  gOndulate;

const float gFrequency = 30.0;

float angle_between_vector(vec3 v1, vec3 v2)
{
  return acos( dot(v1, v2)/ length(v1)*length(v2) );
}

vec3 project_vector_on_plane(vec3 v, vec3 normalized_plane_normal)
{
  return v - dot(v, normalized_plane_normal) * normalized_plane_normal;
}

vec4 quat_from_axis_angle(vec3 axis, float rad_angle)
{ 
	vec4 qr;
	float half_angle = 0.5f * rad_angle;
	qr.x = axis.x * sin(half_angle);
	qr.y = axis.y * sin(half_angle);
	qr.z = axis.z * sin(half_angle);
	qr.w = cos(half_angle);
	return qr;
}

vec3 rotate_vertex_position(vec3 position, vec3 axis, float rad_angle)
{ 
	vec4 q = quat_from_axis_angle(axis, rad_angle);
	vec3 v = position.xyz;
	return v + 2.0 * cross(q.xyz, cross(q.xyz, v) + q.w * v);
}

mat4 rotationMatrix(vec3 axis, float angle)
{
    axis = normalize(axis);
    float s = sin(angle);
    float c = cos(angle);
    float oc = 1.0 - c;
    
    return mat4(oc * axis.x * axis.x + c,           oc * axis.x * axis.y - axis.z * s,  oc * axis.z * axis.x + axis.y * s,  0.0,
                oc * axis.x * axis.y + axis.z * s,  oc * axis.y * axis.y + c,           oc * axis.y * axis.z - axis.x * s,  0.0,
                oc * axis.z * axis.x - axis.y * s,  oc * axis.y * axis.z + axis.x * s,  oc * axis.z * axis.z + c,           0.0,
                0.0,                                0.0,                                0.0,                                1.0);
}

////////////////////////////////////////////////////////////////////////////////
// Vertex Shader Stage
////////////////////////////////////////////////////////////////////////////////
void main(void)
{
	// Extract line column index the vertex is for.
	ColumnIndex = int( TexCoordNIndex.z );

	// Extract tex coords.
	vec2 lTexOut1 = TexCoordNIndex.xy;
	vec2 lTexOut2 = TexCoordNIndex.xy;

	// Scale tex coord. TO DO: allow user customization.
	lTexOut1.x *= 10;
	lTexOut2.x *= 10;
	
	// Translation using external transform parametrable from user(s) through the GUI.
	TexOut1 = vec4( gTexTransform * vec4( lTexOut1, 0.0, 1.0 ) ).xy;

	// In-shader coord translation using time.
	lTexOut2.x -= 0.05 * gTime;

	TexOut2 = lTexOut2;

	// Apply ondulation if needed.
	vec3 lPosL = PosL;
	if ( gOndulate &&
	     TexCoordNIndex.z > 0 && TexCoordNIndex.z < 10 )
	{
		lPosL += vec3(0.0, 1.0, 0.0) * cos( lPosL.x * gFrequency + gTime ) * gAmplitude;
	}

	vec3 lPosW  = (gWorld * vec4( lPosL, 1.0 )).xyz;

	vec3 lUp    = vec3( 0.0f, 1.0f, 0.0f );
	vec3 lLook  = normalize( project_vector_on_plane( gEyePosW - lPosW, normalize( gLineAxis ) ) );
	vec3 lRight = cross( lUp, lLook );

	// Angle from the eye to the Normal to line
	float lAngle = acos( dot( lLook, normalize( cross( gLineAxis, lUp ) ) ) );

	// Which way from the XZ plane.
	float lUpDot = dot( lLook, lUp );
	if (lUpDot < 0)
	{
		lAngle = -lAngle;
	}

	// Turns from -180/180 to 0/360;
	if (lAngle < 0.0 ) 
	{
		lAngle += 2.0f * PI;
	}

	// Rotate along the line axis.
	mat4 lRotation = rotationMatrix( gLineAxis, lAngle );

	vec3 lConstrainedPos = (lRotation * vec4( lPosL, 1.0 )).xyz;

	lPosL = lConstrainedPos;

	// Transform to homogeneous clip space.
	vec4 lClipPosition = gWorldViewProj * vec4( lPosL, 1.0 );

	gl_Position = lClipPosition;
}

