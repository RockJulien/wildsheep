////////////////////////////////////////////////////////////////////////////////
// Filename: TreeSprite.gs
////////////////////////////////////////////////////////////////////////////////

/////////////////////
// INPUT VARIABLES //
/////////////////////
in vec3 CenterW[1];   // World center position
in vec2 SizeWOut[1];  // World size

//////////////////////
// OUTPUT VARIABLES //
//////////////////////
out vec3 PosW;         // World position
out vec3 NormalW;      // World normal
out vec2 TexCoord;     // Texture coordinates

///////////////////////
// UNIFORM VARIABLES //
///////////////////////
uniform mat4	gViewProj;
uniform mat4    gTexTransform; // For inversing texture coordinates or not if needed.
uniform vec3	gEyePosW;

//
// Compute texture coordinates to stretch texture over quad.
//

vec2 gTexC[4] = { vec2( 0.0f, 0.0f ),
				  vec2( 0.0f, 1.0f ), 
				  vec2( 1.0f, 0.0f ),
				  vec2( 1.0f, 1.0f ) };

layout(points) in;
layout(triangle_strip, max_vertices = 4) out; // Point To Quad (whence max 4)

////////////////////////////////////////////////////////////////////////////////
// Geometry Shader Stage
////////////////////////////////////////////////////////////////////////////////
void main(void)
{
	//
	// Compute the local coordinate system of the sprite relative to the world
	// space such that the billboard is aligned with the y-axis and faces the eye.
	//

	vec3 lUp    = vec3( 0.0f, 1.0f, 0.0f );
	vec3 lLook  = gEyePosW - CenterW[0];
	lLook.y     = 0.0f; // y-axis aligned, so project to xz-plane
	lLook       = normalize( lLook );
	vec3 lRight = cross( lUp, lLook );

	//
	// Compute triangle strip vertices (quad) in world space.
	//
	float lHalfWidth  = 0.5f * SizeWOut[0].x;
	float lHalfHeight = 0.5f * SizeWOut[0].y;
	
	vec4 lQuad[4];
	lQuad[0] = vec4( CenterW[0] + lHalfWidth * lRight - lHalfHeight * lUp, 1.0f );
	lQuad[1] = vec4( CenterW[0] + lHalfWidth * lRight + lHalfHeight * lUp, 1.0f );
	lQuad[2] = vec4( CenterW[0] - lHalfWidth * lRight - lHalfHeight * lUp, 1.0f );
	lQuad[3] = vec4( CenterW[0] - lHalfWidth * lRight + lHalfHeight * lUp, 1.0f );

	//
	// Transform quad vertices to world space and output 
	// them as a triangle strip.
	//
	for( int i = 0; i < 4; i++ )
	{
		gl_Position = gViewProj * lQuad[ i ];
		PosW        = lQuad[ i ].xyz;
		NormalW     = lLook;
		TexCoord    = gTexC[ i ];
		gl_PrimitiveID = gl_PrimitiveIDIn;
		
		EmitVertex();
	}

	EndPrimitive();
}
