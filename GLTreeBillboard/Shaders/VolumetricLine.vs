
/////////////////////
// INPUT VARIABLES //
/////////////////////
in vec3 PosL;		   // Local position
in vec3 DuplicatePosL; // Another Local position for extrusion
in vec4 OffsetUV;      // Packed Extrusion + UVs.

//////////////////////
// OUTPUT VARIABLES //
//////////////////////
out vec2 TexOut;    // Texture coordinates

///////////////////////
// UNIFORM VARIABLES //
///////////////////////
uniform mat4  gWorldViewProj;
uniform mat4  gTexTransform;
uniform float gRadius; // TO DO: Combine the three float into a vec3
uniform float gInvScrRatio;

//const float gInvScrRatio = 1280.0 / 960.0;
const float gFrequency = 10.0;
const float gAmplitude = 0.1;

void main()
{
	TexOut = vec4( gTexTransform * vec4( OffsetUV.zw, 0.0, 1.0 ) ).xy; // Just pass UVs.

	vec3 lPosL = PosL;
	vec3 lDuplicatePosL = DuplicatePosL;

	// Compute vertices position in clip space
	vec4 lClipPosition = gWorldViewProj * vec4( lPosL, 1.0 );
	vec4 lOtherClipPosition = gWorldViewProj * vec4( lDuplicatePosL, 1.0 );

	// Compute line direction in screen space (perspective division required)
	vec2 lLineDirProj = gRadius * normalize( lClipPosition.xy / lClipPosition.ww - lOtherClipPosition.xy / lOtherClipPosition.ww );

	// Small trick to avoid inversed line condition when points are not on the same side of Z plane
	if ( sign( lOtherClipPosition.w ) != sign( lClipPosition.w ) )
	{
		lLineDirProj = -lLineDirProj;
	}

	// Offset the screen space position along the line direction 
	// and the orthogonal direction
	lClipPosition.xy += lLineDirProj.xy	* OffsetUV.xx * vec2( 1.0, gInvScrRatio );
	lClipPosition.xy += lLineDirProj.yx * vec2( 1.0, -1.0 )	* OffsetUV.yy * vec2( 1.0, gInvScrRatio );

	gl_Position = lClipPosition;
}

