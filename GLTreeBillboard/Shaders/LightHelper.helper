////////////////////////////////////////////////////////////////////////////////
// Filename: LightHelper.helper
////////////////////////////////////////////////////////////////////////////////

struct DirectionalLight
{
	vec4  Ambient;
	vec4  Diffuse;
	vec4  Specular;
	vec3  Direction;
};

struct PointLight
{ 
	vec4  Ambient;
	vec4  Diffuse;
	vec4  Specular;

	vec3  Position;
	vec3  Att;
	float Range;
};

struct SpotLight
{
	vec4  Ambient;
	vec4  Diffuse;
	vec4  Specular;

	vec3  Position;
	vec3  Direction;
	vec3  Att;

	float Range;
	float Spot;
};

struct Material
{
	vec4  Ambient;
	vec4  Diffuse;
	vec4  Specular; // w = SpecPower
	vec4  Reflect;
};

//---------------------------------------------------------------------------------------
// Computes the ambient, diffuse, and specular terms in the lighting equation
// from a directional light.  We need to output the terms separately because
// later we will modify the individual terms.
//---------------------------------------------------------------------------------------
void ComputeDirectionalLight(in Material pMat, in DirectionalLight pLight, 
                             in vec3 pNormal, in vec3 pToEye,
					         out vec4 pAmbient,
						     out vec4 pDiffuse,
						     out vec4 pSpec)
{
	// Initialize outputs.
	pAmbient = vec4(0.0, 0.0, 0.0, 0.0);
	pDiffuse = vec4(0.0, 0.0, 0.0, 0.0);
	pSpec    = vec4(0.0, 0.0, 0.0, 0.0);

	// The light vector aims opposite the direction the light rays travel.
	vec3 lLightVec = -pLight.Direction;

	// Add ambient term.
	pAmbient = pMat.Ambient * pLight.Ambient;	

	// Add diffuse and specular term, provided the surface is in 
	// the line of site of the light.
	float lDiffuseFactor = dot( lLightVec, pNormal );
	if( lDiffuseFactor > 0.0 )
	{
		vec3 lReflection  = reflect( -lLightVec, pNormal );
		float lSpecFactor = pow(max(dot( lReflection, pToEye ), 0.0), pMat.Specular.w);
					
		pDiffuse = lDiffuseFactor * pMat.Diffuse * pLight.Diffuse;
		pSpec    = lSpecFactor * pMat.Specular * pLight.Specular;
	}
}

//---------------------------------------------------------------------------------------
// Computes the ambient, diffuse, and specular terms in the lighting equation
// from a point light.  We need to output the terms separately because
// later we will modify the individual terms.
//---------------------------------------------------------------------------------------
void ComputePointLight(in Material pMat, in PointLight pLight, in vec3 pPos, 
					   in vec3 pNormal, in vec3 pToEye,
				       out vec4 pAmbient, 
					   out vec4 pDiffuse, 
					   out vec4 pSpec)
{
	// Initialize outputs.
	pAmbient = vec4(0.0, 0.0, 0.0, 0.0);
	pDiffuse = vec4(0.0, 0.0, 0.0, 0.0);
	pSpec    = vec4(0.0, 0.0, 0.0, 0.0);

	// The vector from the surface to the light.
	vec3 lLightVec = pLight.Position - pPos;
		
	// The distance from surface to light.
	float lDistance = length( lLightVec );
	
	// Range test.
	if( lDistance > pLight.Range )
	{
		return;
	}
		
	// Normalize the light vector.
	lLightVec /= lDistance; 
	
	// Ambient term.
	pAmbient = pMat.Ambient * pLight.Ambient;	

	// Add diffuse and specular term, provided the surface is in 
	// the line of site of the light.
	float lDiffuseFactor = dot( lLightVec, pNormal );
	if( lDiffuseFactor > 0.0 )
	{
		vec3 lReflection  = reflect( -lLightVec, pNormal );
		float lSpecFactor = pow(max(dot( lReflection, pToEye ), 0.0), pMat.Specular.w);
					
		pDiffuse = lDiffuseFactor * pMat.Diffuse * pLight.Diffuse;
		pSpec    = lSpecFactor * pMat.Specular * pLight.Specular;
	}

	// Attenuate
	float lAttenuation = 1.0 / dot( pLight.Att, vec3( 1.0, lDistance, lDistance * lDistance ));

	pDiffuse *= lAttenuation;
	pSpec    *= lAttenuation;
}

//---------------------------------------------------------------------------------------
// Computes the ambient, diffuse, and specular terms in the lighting equation
// from a spotlight.  We need to output the terms separately because
// later we will modify the individual terms.
//---------------------------------------------------------------------------------------
void ComputeSpotLight(in Material pMat, in SpotLight pLight, in vec3 pPos, 
					  in vec3 pNormal, in vec3 pToEye,
				      out vec4 pAmbient, 
					  out vec4 pDiffuse, 
					  out vec4 pSpec)
{
	// Initialize outputs.
	pAmbient = vec4(0.0, 0.0, 0.0, 0.0);
	pDiffuse = vec4(0.0, 0.0, 0.0, 0.0);
	pSpec    = vec4(0.0, 0.0, 0.0, 0.0);

	// The vector from the surface to the light.
	vec3 lLightVec = pLight.Position - pPos;
		
	// The distance from surface to light.
	float lDistance = length( lLightVec );
	
	// Range test.
	if( lDistance > pLight.Range )
	{
		return;
	}
		
	// Normalize the light vector.
	lLightVec /= lDistance; 
	
	// Ambient term.
	pAmbient = pMat.Ambient * pLight.Ambient;	

	// Add diffuse and specular term, provided the surface is in 
	// the line of site of the light.

	float lDiffuseFactor = dot( lLightVec, pNormal );
	if( lDiffuseFactor > 0.0 )
	{
		vec3 lReflection  = reflect( -lLightVec, pNormal );
		float lSpecFactor = pow(max(dot( lReflection, pToEye ), 0.0), pMat.Specular.w);
					
		pDiffuse = lDiffuseFactor * pMat.Diffuse * pLight.Diffuse;
		pSpec    = lSpecFactor * pMat.Specular * pLight.Specular;
	}
	
	// Scale by spotlight factor and attenuate.
	float lSpot = pow(max(dot( -lLightVec, pLight.Direction ), 0.0), pLight.Spot );

	// Scale by spotlight factor and attenuate.
	float lAttenuation = lSpot / dot( pLight.Att, vec3( 1.0, lDistance, lDistance * lDistance ) );

	pAmbient *= lSpot;
	pDiffuse *= lAttenuation;
	pSpec    *= lAttenuation;
}
