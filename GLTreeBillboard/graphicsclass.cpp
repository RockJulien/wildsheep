////////////////////////////////////////////////////////////////////////////////
// Filename: GraphicsClass.cpp
////////////////////////////////////////////////////////////////////////////////
#include "graphicsclass.h"
#include "Images\TGALoader.h"
#include <fstream>
#include "MathUtilities.h"
#include <time.h>
#include "systemclass.h"
#include "Meshes\MeshData.h"
#include "GUI\imgui.h"
#include "GUI\imgui_impl_win32.h"
#include "GUI\imgui_impl_opengl3.h"

#include "Meshes\VolumetricLine.h"
#include "Meshes\TrailLine.h"

using namespace std;
using namespace Ocelot;


bool mUseLightingOnly = false;
bool mUseLightAndTexture = false;
bool mUseLightAndTextureAndFog = false;

/*********************************************************************************************/
GraphicsClass::GraphicsClass() :
mOpenGL(NULL), mCamera(NULL), mCameraSpeed(60.0f), mController(NULL),
mLandVAO(0), mLandVB(0), mLandIB(0), mWaveVAO(0), mTrailLineTech(NULL), mTrailLineTexTech(NULL),
mWaveVB(0), mWaveIB(0), mBoxVAO(0), mBoxVB(0), mTreeSpritesVAO(0), mTreeSpritesVB(0), 
mBoxIB(0), mLight3Tech(NULL), mLight3TexTech(NULL), mTreeSpritesTech(NULL),
mLight3TexFogTech(NULL), mLight3TexAlphaClipTech(NULL), mTrailTech(NULL), mVolumetricLineTech(NULL),
mVolumetricLineTexTech(NULL), mTreeLight3Tech(NULL), mTreeLight3TexAlphaClipTech(NULL), mTreeLight3TexAlphaClipFogTech(NULL),
mLight3TexAlphaClipFogTech(NULL), mRenderOptions(RenderOptions::eTextures), mLineRadius(10.0f),
mTreeTexMapArrayRV(0), mAlphaToCoverageOn(false), mRequestWindowMaximize( false ),
mRequestWindowMinimize( false ), mRequestWindowClose( false ), mEndpointsVB(0),
mEndpointsIB( 0 ), mEndpointsVAO(0), mTrailOndulate(true), mTrailAmplitude(1.0f), mTrailSpeedFactor(0.4f)
{
	srand((UBIGINT)time(NULL));

	mUseLightAndTexture = true;

	this->mClearColor = Vector4( 0.75f, 0.75f, 0.75f, 1.0f );

	// Initialize the camera.
	OcelotCameraInfo lCamInfo( Vector3( 0.0f, 100.0f, -100.0f ), 
							   Vector3( 0.0f, -5.0f, 5.0f ), 
							   (FFLOAT)(1280 / 960),
							   1000.0f );
	lCamInfo.SetNear(1.0f);
	this->mCamera = new OcelotCamera( lCamInfo );

	// And create the controller
	this->mController = ControlFact->createController( this->mCamera, 
													   CAMERA_CONTROLLER_FLYER, 
													   this->mCameraSpeed );

	Matrix4x4 lBoxScale;
	Matrix4x4 lBoxOffset;
	lBoxScale.Matrix4x4Scaling( lBoxScale, 15.0f, 15.0f, 15.0f );
	lBoxOffset.Matrix4x4Translation( lBoxOffset, 8.0f, 5.0f, -15.0f );
	this->mBoxWorld.Matrix4x4Mutliply( this->mBoxWorld, lBoxScale, lBoxOffset );

	this->mLandTexTransform.Matrix4x4Scaling( this->mLandTexTransform, 5.0f, 5.0f, 0.0f );

	// Inverse tree images that are inverse (this either that or flip vertically image data)
	this->mTreeTexTransform.Matrix4x4Scaling( this->mTreeTexTransform, 1.0f, -1.0f, 1.0f );

	// Initialize the light object(s).
	this->mDirLights[0].SetAmbient( Color( 0.2f, 0.2f, 0.2f, 1.0f ) );
	this->mDirLights[0].SetDiffuse( Color( 0.5f, 0.5f, 0.5f, 1.0f ) );
	this->mDirLights[0].SetSpecular( Color( 0.5f, 0.5f, 0.5f, 1.0f ) );
	this->mDirLights[0].SetDirection( Vector3( 0.57735f, -0.57735f, 0.57735f ) );

	this->mDirLights[1].SetAmbient( Color( 0.0f, 0.0f, 0.0f, 1.0f ) );
	this->mDirLights[1].SetDiffuse( Color( 0.20f, 0.20f, 0.20f, 1.0f ) );
	this->mDirLights[1].SetSpecular( Color( 0.25f, 0.25f, 0.25f, 1.0f ) );
	this->mDirLights[1].SetDirection( Vector3( -0.57735f, -0.57735f, 0.57735f ) );

	this->mDirLights[2].SetAmbient( Color( 0.0f, 0.0f, 0.0f, 1.0f ) );
	this->mDirLights[2].SetDiffuse( Color( 0.2f, 0.2f, 0.2f, 1.0f ) );
	this->mDirLights[2].SetSpecular( Color( 0.0f, 0.0f, 0.0f, 1.0f ) );
	this->mDirLights[2].SetDirection( Vector3( 0.0f, -0.707f, -0.707f ) );

	// Initialize materials
	this->mLandMat.Ambient  = Color( 0.5f, 0.5f, 0.5f, 1.0f );
	this->mLandMat.Diffuse  = Color( 1.0f, 1.0f, 1.0f, 1.0f );
	this->mLandMat.Specular = Color( 0.2f, 0.2f, 0.2f, 16.0f );

	this->mWaveMat.Ambient  = Color( 0.5f, 0.5f, 0.5f, 1.0f );
	this->mWaveMat.Diffuse  = Color( 1.0f, 1.0f, 1.0f, 0.5f );
	this->mWaveMat.Specular = Color( 0.8f, 0.8f, 0.8f, 32.0f );

	this->mBoxMat.Ambient   = Color( 0.5f, 0.5f, 0.5f, 1.0f );
	this->mBoxMat.Diffuse   = Color( 1.0f, 1.0f, 1.0f, 1.0f );
	this->mBoxMat.Specular  = Color( 0.4f, 0.4f, 0.4f, 16.0f );

	this->mTreeMat.Ambient  = Color( 0.5f, 0.5f, 0.5f, 1.0f );
	this->mTreeMat.Diffuse  = Color( 1.0f, 1.0f, 1.0f, 1.0f );
	this->mTreeMat.Specular = Color( 0.2f, 0.2f, 0.2f, 16.0f );

	this->mTrailMat.Ambient  = Color( 0.0f, 0.0f, 1.0f, 1.0f );
	this->mTrailMat.Diffuse  = Color( 1.0f, 1.0f, 1.0f, 1.0f );
	this->mTrailMat.Specular = Color( 0.2f, 0.2f, 0.2f, 16.0f );

	this->mDiffuseTexUnit = 1;
}

/*********************************************************************************************/
GraphicsClass::~GraphicsClass()
{
	
}

/*********************************************************************************************/
BBOOL GraphicsClass::Initialize(OpenGLClass* pGDevice, BIGINT pClientWidth, BIGINT pClientHeight)
{
	BBOOL lResult = false;

	// Store a pointer to the OpenGL class object.
	this->mOpenGL = pGDevice;

	Effects::InitializeAll( this->mOpenGL );
	InputLayouts::InitializeAll( this->mOpenGL );

	// Initialize the waves
	this->mWaves.Init( 160, 160, 1.0f, 0.03f, 5.0f, 0.3f );

	// Initialize the texture manager
	this->mTexManager.Initialize( this->mOpenGL );

	// Init the diffuse map to apply to the mesh.
	ExtType lType = ExtType::eDDS;

	this->mLandMapRV = this->mTexManager.CreateTexture( "Textures/grass.dds",     true );
	this->mWaveMapRV = this->mTexManager.CreateTexture( "Textures/water2.dds",    true );
	this->mBoxMapRV  = this->mTexManager.CreateTexture( "Textures/WireFence.dds", true );

	// ImGui.
	this->mCloseRV    = this->mTexManager.CreateTexture( "Textures/Close.tga",    true );
	this->mMinimizeRV = this->mTexManager.CreateTexture( "Textures/Minimize.tga", true );
	this->mMaximizeRV = this->mTexManager.CreateTexture( "Textures/Maximize.tga", true );

	// Create the 2D Texture Array for trees
	vector<string> lTreeFilenames;
	lTreeFilenames.push_back( "Textures/tree0.tga" );
	lTreeFilenames.push_back( "Textures/tree1.tga" );
	lTreeFilenames.push_back( "Textures/tree2.tga" );
	lTreeFilenames.push_back( "Textures/tree3.tga" );

	this->mTreeTexMapArrayRV = this->mTexManager.CreateTextureArray( "TreeSprites", lTreeFilenames, false );

	// Initialize the technique(s).
	vector<string> lIncludes;
	lIncludes.push_back("Shaders/LightHelper.helper"); // Need light include
	StageDescription lDescription;
	lDescription.SetVertexStagePath("Shaders/Basic.vs");
	lDescription.SetPixelStagePath("Shaders/Basic.ps");
	this->mLight3Tech = Effects::BasicFX()->Light3Tech( lDescription, lIncludes );
	this->mLight3Tech->SetLayout( InputLayouts::PosNormalTexLayout() );
	this->mLight3TexTech = Effects::BasicFX()->Light3TexTech( lDescription, lIncludes );
	this->mLight3TexTech->SetLayout( InputLayouts::PosNormalTexLayout() );
	this->mLight3TexFogTech = Effects::BasicFX()->Light3TexFogTech( lDescription, lIncludes );
	this->mLight3TexFogTech->SetLayout( InputLayouts::PosNormalTexLayout() );
	this->mLight3TexAlphaClipTech = Effects::BasicFX()->Light3TexAlphaClipTech( lDescription, lIncludes );
	this->mLight3TexAlphaClipTech->SetLayout( InputLayouts::PosNormalTexLayout() );
	this->mLight3TexAlphaClipFogTech = Effects::BasicFX()->Light3TexAlphaClipFogTech( lDescription, lIncludes );
	this->mLight3TexAlphaClipFogTech->SetLayout( InputLayouts::PosNormalTexLayout() );

	// Tree techniques
	StageDescription lTreeDescription;
	lTreeDescription.SetVertexStagePath("Shaders/TreeSprite.vs");
	lTreeDescription.SetGeometryStagePath("Shaders/TreeSprite.gs");
	lTreeDescription.SetPixelStagePath("Shaders/TreeSprite.ps");
	this->mTreeLight3Tech = Effects::TreeSpriteFX()->Light3Tech( lTreeDescription, lIncludes );
	this->mTreeLight3Tech->SetLayout( InputLayouts::TreePointSpriteLayout() );
	this->mTreeLight3TexAlphaClipTech = Effects::TreeSpriteFX()->Light3TexAlphaClipTech( lTreeDescription, lIncludes );
	this->mTreeLight3TexAlphaClipTech->SetLayout( InputLayouts::TreePointSpriteLayout() );
	this->mTreeLight3TexAlphaClipFogTech = Effects::TreeSpriteFX()->Light3TexAlphaClipFogTech( lTreeDescription, lIncludes );
	this->mTreeLight3TexAlphaClipFogTech->SetLayout( InputLayouts::TreePointSpriteLayout() );

	// Wild sheep.
	StageDescription lTrailDescription;
	lTrailDescription.SetVertexStagePath("Shaders/TrailLine.vs");
	lTrailDescription.SetPixelStagePath("Shaders/TrailLine.ps");
	this->mTrailLineTech = Effects::TrailLineFX()->TrailLineTech( lTrailDescription, lIncludes );
	this->mTrailLineTexTech = Effects::TrailLineFX()->TrailLineTexTech( lTrailDescription, lIncludes );
	this->mTrailLineTech->SetLayout( InputLayouts::TrailLineLayout() );
	this->mTrailLineTexTech->SetLayout( InputLayouts::TrailLineLayout() );

	// Volumetric test.
	StageDescription lVolumetricDescription;
	lVolumetricDescription.SetVertexStagePath("Shaders/VolumetricLine.vs");
	lVolumetricDescription.SetPixelStagePath("Shaders/VolumetricLine.ps");
	this->mVolumetricLineTech = Effects::VolumetricLineFX()->VolumetricTech( lVolumetricDescription, std::vector<string>() );
	this->mVolumetricLineTexTech = Effects::VolumetricLineFX()->VolumetricTexTech( lVolumetricDescription, std::vector<string>() );
	this->mVolumetricLineTech->SetLayout( InputLayouts::VolumetricLineLayout() );
	this->mVolumetricLineTexTech->SetLayout( InputLayouts::VolumetricLineLayout() );

	this->BuildLandGeometryBuffers();
	this->BuildWaveGeometryBuffers();
	this->BuildCrateGeometryBuffers();
	this->BuildTreeSpritesBuffer();
	this->BuildTrailEndpointsGeometryBuffers( Vector3( -25.0, 50.0, -20 ), Vector3( 25.0, 50.0, -20 ) );
	this->BuildTrailGeometryBuffers( Vector3( -25.0, 50.0, -20 ), Vector3( 25.0, 50.0, -20 ) );

	// Volumetric test.
	this->mLine = new VolumetricLine( this->mOpenGL, &this->mTexManager );
	this->mLine->Initialize( Vector3(-30.0, 20.0, -50.0), Vector3( 30.0, 20.0, -50.0 ), "Textures/part3.bmp" ); // Try part1.bmp or part3.bmp
	this->mLine->SetCamera( this->mCamera );


	// Init Gui stuff.

	// Setup style.
	ImGui::StyleColorsDark();

	return true;
}

/*********************************************************************************************/
VVOID GraphicsClass::OnResize(BIGINT pClientWidth, BIGINT pClientHeight)
{
	FFLOAT lAspect = (FFLOAT)pClientWidth / pClientHeight;

	// refresh the aspect of the camera if the
	// window is resized.
	OcelotCameraInfo lInfo = this->mCamera->Info();
	if( this->mCamera != NULL )
	{
		lInfo.SetAspect( lAspect );  // aspect ratio
		this->mCamera->SetInfo( lInfo );
	}

	glViewport( 0, 0, pClientWidth, pClientHeight );
}

/*********************************************************************************************/
VVOID GraphicsClass::Shutdown()
{
	DeletePointer( this->mCamera );
	DeletePointer( this->mController );
	DeletePointer( this->mLight3Tech );
	DeletePointer( this->mLight3TexTech );
	DeletePointer( this->mLight3TexFogTech );
	DeletePointer( this->mLight3TexAlphaClipTech );
	DeletePointer( this->mLight3TexAlphaClipFogTech );
	DeletePointer( this->mTreeLight3Tech );
	DeletePointer( this->mTreeLight3TexAlphaClipTech );
	DeletePointer( this->mTreeLight3TexAlphaClipFogTech );

	// Wild sheep
	DeletePointer( this->mTrailLineTech );
	DeletePointer( this->mTrailLineTexTech );

	// Volumetric test
	DeletePointer( this->mVolumetricLineTech );
	DeletePointer( this->mVolumetricLineTexTech );

	glDeleteTextures( 1, &this->mLandMapRV );
	glDeleteTextures( 1, &this->mWaveMapRV );
	glDeleteTextures( 1, &this->mBoxMapRV );
	glDeleteTextures( 1, &this->mTreeTexMapArrayRV );

	glDeleteTextures( 1, &this->mCloseRV );
	glDeleteTextures( 1, &this->mMinimizeRV );
	glDeleteTextures( 1, &this->mMaximizeRV );

	// Release VAOs, VBOs and IBOs
	// Release Land buffers
	this->mOpenGL->BindBuffer( GL_ARRAY_BUFFER, 0 );
	this->mOpenGL->DeleteBuffers( 1, &this->mLandVB );
	this->mOpenGL->BindBuffer( GL_ELEMENT_ARRAY_BUFFER, 0 );
	this->mOpenGL->DeleteBuffers( 1, &this->mLandIB );
	this->mOpenGL->BindVertArray( 0 );
	this->mOpenGL->DeleteVertArrays( 1, &this->mLandVAO );

	// Release Wave buffers
	this->mOpenGL->DeleteBuffers( 1, &this->mWaveVB );
	this->mOpenGL->DeleteBuffers( 1, &this->mWaveIB );
	this->mOpenGL->DeleteVertArrays( 1, &this->mWaveVAO );

	// Release Box buffers
	this->mOpenGL->DeleteBuffers( 1, &this->mBoxVB );
	this->mOpenGL->DeleteBuffers( 1, &this->mBoxIB );
	this->mOpenGL->DeleteVertArrays( 1, &this->mBoxVAO );

	// Release Trees buffers
	this->mOpenGL->DeleteBuffers( 1, &this->mTreeSpritesVB );
	this->mOpenGL->DeleteVertArrays( 1, &this->mTreeSpritesVAO );

	// Release Wild sheep stuff...
	this->mOpenGL->DeleteBuffers( 1, &this->mEndpointsVB );
	this->mOpenGL->DeleteBuffers( 1, &this->mEndpointsIB );
	this->mOpenGL->DeleteVertArrays( 1, &this->mEndpointsVAO );

	mTrail->CleanUp();
	DeletePointer( mTrail );

	mLine->CleanUp();
	DeletePointer( mLine );

	Effects::DestroyAll();
	InputLayouts::DestroyAll();
}

/*********************************************************************************************/
BBOOL GraphicsClass::Update(FFLOAT pDeltaTime)
{
	// Update the camera.
	this->mController->update( pDeltaTime );

	GLenum lError = glGetError();
	if ( lError != GL_NO_ERROR )
	{
		throw exception(" Start Update Failure !!! ");
	}

	//
	// Every quarter second, generate a random wave.
	//
	static FFLOAT sBase = 0.0f;
	FFLOAT lTotalTime = SystemClass::GetTotalTime();
	if( (lTotalTime - sBase) >= 0.25f )
	{
		sBase += 0.25f;
 
		ULINT lRow = 5 + rand() % (this->mWaves.RowCount() - 10 );
		ULINT lCol = 5 + rand() % (this->mWaves.ColumnCount() - 10 );

		FFLOAT lMagnitude = RandBetween( 1.0f, 2.0f );

		this->mWaves.Disturb( lRow, lCol, lMagnitude );
	}

	this->mWaves.Update( pDeltaTime );

	//
	// Update the wave vertex buffer with the new solution.
	//
	this->mOpenGL->BindBuffer( GL_ARRAY_BUFFER, this->mWaveVB );
	SimpleVertex* lVBO = reinterpret_cast<SimpleVertex*>( this->mOpenGL->MapBuffer( GL_ARRAY_BUFFER, GL_WRITE_ONLY ) );
	if 
		( lVBO )
	{
		lError = glGetError();
		if
			( lError != GL_NO_ERROR )
		{
			throw exception(" Map Waves VBO Failure !!! ");
		}

		// Update waves VBO
		for( UBIGINT lCurrVertex = 0; lCurrVertex < this->mWaves.VertexCount(); lCurrVertex++ )
		{
			lVBO[ lCurrVertex ].Position = this->mWaves[ lCurrVertex ];
			lVBO[ lCurrVertex ].Normal   = this->mWaves.Normal( lCurrVertex );

			// Derive tex-coords in [0,1] from position.
			lVBO[ lCurrVertex ].TexCoord.x( 0.5f + this->mWaves[ lCurrVertex ].getX() / this->mWaves.Width() );
			lVBO[ lCurrVertex ].TexCoord.y( 0.5f - this->mWaves[ lCurrVertex ].getZ() / this->mWaves.Depth() );
		}

		this->mOpenGL->UnmapBuffer( GL_ARRAY_BUFFER );

		lError = glGetError();
		if
			( lError != GL_NO_ERROR )
		{
			throw exception(" Unmap Waves VBO Failure !!! ");
		}
	}

	//
	// Animate water texture coordinates.
	//

	// Tile water texture.
	Matrix4x4 lWavesScale;
	lWavesScale.Matrix4x4Scaling( lWavesScale, 5.0f, 5.0f, 0.0f );

	// Translate texture over time.
	this->mWaterTexOffset.y( this->mWaterTexOffset.getY() + 0.05f * pDeltaTime );
	this->mWaterTexOffset.x( this->mWaterTexOffset.getX() + 0.1f * pDeltaTime );	
	Matrix4x4 lWavesOffset;
	lWavesOffset.Matrix4x4Translation( lWavesOffset, this->mWaterTexOffset.getX(), this->mWaterTexOffset.getY(), 0.0f );

	// Combine scale and translation.
	this->mWaveTexTransform = lWavesScale * lWavesOffset;

	//
	// Update the trail.
	//
	this->mTrail->Update( pDeltaTime );

	//
	// Update the Volumetric test.
	//
	this->mLine->Update( pDeltaTime );

	return true;
}

/*********************************************************************************************/
BBOOL GraphicsClass::Render()
{
	// Clear the buffers to begin the scene.
	this->mOpenGL->StartRender( true, true );

	glClearColor( this->mClearColor.getX(), 
				  this->mClearColor.getY(), 
				  this->mClearColor.getZ(), 
				  this->mClearColor.getW() ); // Silver
	glClearDepth( 1.0f );
	glClearStencil( 0 );
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT );

	GLenum lError = glGetError();
	if ( lError != GL_NO_ERROR )
	{
		throw exception(" Before rendering FAILURE !!! ");
	}

	GLuint lStatus = this->mOpenGL->CheckFrameBufferStatus( GL_FRAMEBUFFER );
	if ( lStatus != GL_FRAMEBUFFER_COMPLETE )
	{
		throw exception(" Frame buffer failure !!!");
	}

	Matrix4x4 lView  = this->mCamera->View();
	Matrix4x4 lProj  = this->mCamera->Proj();
	Matrix4x4 lViewProj = lView * lProj;
	OcelotCameraInfo lInfo = this->mCamera->Info();


	// Draw trees.
	this->DrawTreeSprites( lViewProj );

	// Draw Hills and wave.
	this->DrawHillAndWave( lViewProj );

	// Wild sheep.
	this->DrawTrail( lViewProj );

	// Volumetric line test.
	this->VolumetricTest();

	lStatus = this->mOpenGL->CheckFrameBufferStatus( GL_FRAMEBUFFER );
	if ( lStatus != GL_FRAMEBUFFER_COMPLETE )
	{
		throw exception(" Frame buffer failure !!!");
	}

	// Render Gui.
	this->RenderGui();

	// Present the rendered scene to the screen.
	this->mOpenGL->StopRender();

	return true;
}

/*********************************************************************************************/
BBOOL GraphicsClass::RenderGui()
{
	// Start the Dear ImGui frame
	ImGui_ImplOpenGL3_NewFrame();
	ImGui_ImplWin32_NewFrame();
	ImGui::NewFrame();

	// Debug stuff.
	{
		ImGui::Text(" - Gui average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);

		ImGui::Text(" - Rendering : ");

		ImGui::Checkbox("Use lighting only.", &mUseLightingOnly);

		if ( mUseLightingOnly )
		{
			mUseLightAndTexture = false;
			mUseLightAndTextureAndFog = false;
		}

		ImGui::Checkbox("Use lighting and texture.", &mUseLightAndTexture);

		if ( mUseLightAndTexture )
		{
			mUseLightingOnly = false;
			mUseLightAndTextureAndFog = false;
		}

		ImGui::Checkbox("Use lighting, texture and fog.", &mUseLightAndTextureAndFog);

		if ( mUseLightAndTextureAndFog )
		{
			mUseLightingOnly = false;
			mUseLightAndTexture = false;
		}

		if ( mUseLightingOnly )
		{
			this->mRenderOptions = RenderOptions::eLighting;
		}
	
		else if ( mUseLightAndTexture )
		{
			this->mRenderOptions = RenderOptions::eTextures;
		}
	
		else if ( mUseLightAndTextureAndFog )
		{
			this->mRenderOptions = RenderOptions::eTexturesAndFog;
		}

		ImGui::ColorEdit3("Clear color", (float*)&this->mClearColor); // Edit 3 floats representing a color
		if (ImGui::IsItemHovered())
			ImGui::SetTooltip("Modify the clear color.");

		ImGui::Text(" - Camera : ");

		ImGui::SliderFloat("Camera Speed", &this->mCameraSpeed, 20.0f, 200.0f);
		if (ImGui::IsItemHovered())
			ImGui::SetTooltip("Modify how fast the camera is moving.");

		this->mController->SetSpeed( this->mCameraSpeed );

		ImGui::Text(" - Trail : ");

		ImGui::Checkbox("Ondulate", &mTrailOndulate);
		if (ImGui::IsItemHovered())
			ImGui::SetTooltip("Trigger the line ondulation.");

		ImGui::SliderFloat("Speed Factor", &this->mTrailSpeedFactor, 0.1f, 10.0f);
		if (ImGui::IsItemHovered())
			ImGui::SetTooltip("Modify how fast the flow is moving.");

		ImGui::SliderFloat("Amplitude", &this->mTrailAmplitude, 0.1f, 5.0f);
		if (ImGui::IsItemHovered())
			ImGui::SetTooltip("Modify the trail amplitude.");

		ImGui::Text(" - Volumetric test : ");
		ImGui::SliderFloat("Line Radius", &this->mLineRadius, 0.1f, 50.0f);
		if (ImGui::IsItemHovered())
			ImGui::SetTooltip("Modify the line radius.");
	}

	// Splash screen window like window control options
	{
		ImGui::Begin("Window options", false, ImGuiWindowFlags_::ImGuiWindowFlags_NoMove | ImGuiWindowFlags_::ImGuiWindowFlags_NoResize );
	
		UBIGINT lResult = glGetError();
		if ( lResult != GL_NO_ERROR )
		{
			throw exception( "Error." );
		}

		float my_tex_w = 32;
		float my_tex_h = 32;

		if ( ImGui::ImageButton( (void*)(intptr_t)this->mMinimizeRV, ImVec2(32, 32), ImVec2(0, 0), ImVec2(32.0f / my_tex_w, 32 / my_tex_h), -1, ImColor(0, 0, 0, 255) ) )
		{
			this->mRequestWindowMaximize = false;
			this->mRequestWindowMinimize = true;
		}
		if (ImGui::IsItemHovered())
			ImGui::SetTooltip("Minimize the window.");

		ImGui::SameLine();
		if ( ImGui::ImageButton( (void*)(intptr_t)this->mMaximizeRV, ImVec2(32, 32), ImVec2(0, 0), ImVec2(32.0f / my_tex_w, 32 / my_tex_h), -1, ImColor(0, 0, 0, 255) ) )
		{
			this->mRequestWindowMaximize = true;
			this->mRequestWindowMinimize = false;
		}
		if (ImGui::IsItemHovered())
			ImGui::SetTooltip("Maximize the window.");

		ImGui::SameLine();
		if ( ImGui::ImageButton( (void*)(intptr_t)this->mCloseRV, ImVec2(32, 32), ImVec2(0, 0), ImVec2(32.0f / my_tex_w, 32 / my_tex_h), -1, ImColor(0, 0, 0, 255) ) )
		{
			this->mRequestWindowClose = true;
			this->mRequestWindowMaximize = false;
			this->mRequestWindowMinimize = false;
		}
		if (ImGui::IsItemHovered())
			ImGui::SetTooltip("Close the window.");

		ImGui::End();
	}

	// Rendering
	ImGui::Render();

	ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

	return true;
}

/*********************************************************************************************/
FFLOAT  GraphicsClass::GetHillHeight( FFLOAT pX, FFLOAT pZ ) const
{
	return 0.3f * ( pZ * sinf( 0.1f * pX ) + pX * cosf( 0.1f * pZ ) );
}

/*********************************************************************************************/
Vector3 GraphicsClass::GetHillNormal( FFLOAT pX, FFLOAT pZ ) const
{
	// Normal = (-df / dx, 1, -df / dz)
	Vector3 lNormal( -0.03f * pZ * cosf( 0.1f * pX ) - 0.3f * cosf( 0.1f * pZ ),
					 1.0f,
					 -0.3f * sinf( 0.1f * pX ) + 0.03f * pX * sinf( 0.1f * pZ ) );
	
	Vector3 lUnitNormal = lNormal.Vec3Normalise();

	return lUnitNormal;
}

/*********************************************************************************************/
VVOID   GraphicsClass::BuildLandGeometryBuffers()
{
	MeshData lGrid;

	GeometryHelper::CreateGrid( 160.0f, 160.0f, 50, 50, lGrid );

	this->mLandIndexCount = (UBIGINT)lGrid.Indices.size();

	//
	// Extract the vertex elements we are interested and apply the height function to
	// each vertex.  
	//
	std::vector<SimpleVertex> lVertices( lGrid.Vertices.size() );
	for( UBIGINT lCurrVert = 0; lCurrVert < (UBIGINT)lGrid.Vertices.size(); lCurrVert++ )
	{
		Vector3 lPosition = lGrid.Vertices[ lCurrVert ].Position;

		lPosition.y( this->GetHillHeight( lPosition.getX(), lPosition.getZ() ) );
		
		lVertices[ lCurrVert ].Position = lPosition;
		lVertices[ lCurrVert ].Normal   = this->GetHillNormal( lPosition.getX(), lPosition.getZ() );
		lVertices[ lCurrVert ].TexCoord = lGrid.Vertices[ lCurrVert ].TexCoord;
	}

	GLenum lError = glGetError();
	if( lError != GL_NO_ERROR )
	{
		throw exception(" Build Land Geometry Failure !!! ");
	}

	// Allocate an OpenGL vertex array object.
	this->mOpenGL->GenVertArrays( 1, &this->mLandVAO );

	// Bind the vertex array object to store all the buffers and vertex attributes we create here.
	this->mOpenGL->BindVertArray( this->mLandVAO );

	// Generate a vertex buffer object
	this->mOpenGL->GenBuffers( 1, &this->mLandVB );

	// Bind the vertex buffer and load the vertex (position, and normal) data into the vertex buffer.
	this->mOpenGL->BindBuffer( GL_ARRAY_BUFFER, this->mLandVB );
	this->mOpenGL->BufferData( GL_ARRAY_BUFFER, lVertices.size() * sizeof(SimpleVertex), &lVertices[0], GL_STATIC_DRAW );

	// Set the layout for the vertex stage
	InputLayouts::PosNormalTexLayout()->SetVertexLayout( this->mLandVB );

	// Generate an Index buffer object
	this->mOpenGL->GenBuffers( 1, &this->mLandIB );

	// Bind the index buffer and load the index data into it.
	this->mOpenGL->BindBuffer( GL_ELEMENT_ARRAY_BUFFER, this->mLandIB );
	this->mOpenGL->BufferData( GL_ELEMENT_ARRAY_BUFFER, this->mLandIndexCount * sizeof(UBIGINT), &lGrid.Indices[0], GL_STATIC_DRAW );

	lError = glGetError();
	if( lError != GL_NO_ERROR )
	{
		throw exception(" Build Land Geometry Failure !!! ");
	}

	this->mOpenGL->BindVertArray( 0 );
}

/*********************************************************************************************/
VVOID   GraphicsClass::BuildWaveGeometryBuffers()
{
	GLenum lError = glGetError();
	if( lError != GL_NO_ERROR )
	{
		throw exception(" Build Wave Geometry Failure !!! ");
	}

	// Create the vertex buffer.  Note that we allocate space only, as
	// we will be updating the data every time step of the simulation.

	// Allocate an OpenGL vertex array object.
	this->mOpenGL->GenVertArrays( 1, &this->mWaveVAO );

	// Bind the vertex array object to store all the buffers and vertex attributes we create here.
	this->mOpenGL->BindVertArray( this->mWaveVAO );

	// Generate a vertex buffer object
	this->mOpenGL->GenBuffers( 1, &this->mWaveVB );

	// Bind the vertex buffer and load the vertex (position, and normal) data into the vertex buffer.
	this->mOpenGL->BindBuffer( GL_ARRAY_BUFFER, this->mWaveVB );
	this->mOpenGL->BufferData( GL_ARRAY_BUFFER, this->mWaves.VertexCount() * sizeof(SimpleVertex), NULL, GL_DYNAMIC_DRAW );

	lError = glGetError();
	if( lError != GL_NO_ERROR )
	{
		throw exception(" Build Wave Geometry Failure !!! ");
	}

	// Set the vertex stage layout.
	InputLayouts::PosNormalTexLayout()->SetVertexLayout( this->mWaveVB );

	// Create the index buffer.  The index buffer is fixed, so we only 
	// need to create and set once.

	vector<UBIGINT> lIndices( 3 * this->mWaves.TriangleCount() ); // 3 indices per face

	// Iterate over each quad.
	UBIGINT lRowCount = this->mWaves.RowCount();
	UBIGINT lColCount = this->mWaves.ColumnCount();
	BIGINT lCounter = 0;
	for( UBIGINT lCurrRow = 0; lCurrRow < lRowCount - 1; lCurrRow++ )
	{
		for( UBIGINT lCurrCol = 0; lCurrCol < lColCount - 1; lCurrCol++ )
		{
			lIndices[ lCounter ]   = lCurrRow * lColCount + lCurrCol;
			lIndices[ lCounter + 1 ] = lCurrRow * lColCount + lCurrCol + 1;
			lIndices[ lCounter + 2 ] = (lCurrRow + 1) * lColCount + lCurrCol;

			lIndices[ lCounter + 3 ] = (lCurrRow + 1) * lColCount + lCurrCol;
			lIndices[ lCounter + 4 ] = lCurrRow * lColCount + lCurrCol + 1;
			lIndices[ lCounter + 5 ] = (lCurrRow + 1) * lColCount + lCurrCol + 1;

			lCounter += 6; // next quad
		}
	}

	// Generate a Index buffer object
	this->mOpenGL->GenBuffers( 1, &this->mWaveIB );

	// Bind the index buffer and load the index data into it.
	this->mOpenGL->BindBuffer( GL_ELEMENT_ARRAY_BUFFER, this->mWaveIB );
	this->mOpenGL->BufferData( GL_ELEMENT_ARRAY_BUFFER, lIndices.size() * sizeof(UBIGINT), &lIndices[0], GL_STATIC_DRAW );

	lError = glGetError();
	if( lError != GL_NO_ERROR )
	{
		throw exception(" Build Wave Geometry Failure !!! ");
	}

	// Unbind the VAO
	this->mOpenGL->BindVertArray( 0 );
}

/*********************************************************************************************/
VVOID   GraphicsClass::BuildCrateGeometryBuffers()
{
	MeshData lBox;

	GeometryHelper::CreateBox( 1.0f, 1.0f, 1.0f, lBox );

	//
	// Extract the vertex elements we are interested in and pack the
	// vertices of all the meshes into one vertex buffer.
	//
	vector<SimpleVertex> lVertices( (UBIGINT)lBox.Vertices.size() );

	UBIGINT lCounter = 0;
	for( UBIGINT lCurr = 0; lCurr < (UBIGINT)lBox.Vertices.size(); lCurr++, lCounter++ )
	{
		lVertices[ lCounter ].Position = lBox.Vertices[ lCurr ].Position;
		lVertices[ lCounter ].Normal   = lBox.Vertices[ lCurr ].Normal;
		lVertices[ lCounter ].TexCoord = lBox.Vertices[ lCurr ].TexCoord;
	}

	// Allocate an OpenGL vertex array object.
	this->mOpenGL->GenVertArrays( 1, &this->mBoxVAO );

	// Bind the vertex array object to store all the buffers and vertex attributes we create here.
	this->mOpenGL->BindVertArray( this->mBoxVAO );

	// Generate a vertex buffer object
	this->mOpenGL->GenBuffers( 1, &this->mBoxVB );

	// Bind the vertex buffer and load the vertex (position, texture, and normal) data into the vertex buffer.
	this->mOpenGL->BindBuffer( GL_ARRAY_BUFFER, this->mBoxVB );
	this->mOpenGL->BufferData( GL_ARRAY_BUFFER, (UBIGINT)lBox.Vertices.size() * sizeof(SimpleVertex), &lVertices[0], GL_STATIC_DRAW );

	// Set the layout for the vertex stage
	InputLayouts::PosNormalTexLayout()->SetVertexLayout( this->mBoxVB );

	vector<UBIGINT> lIndices;
	lIndices.insert( lIndices.end(), lBox.Indices.begin(), lBox.Indices.end() );

	// Generate a index buffer object
	this->mOpenGL->GenBuffers( 1, &this->mBoxIB );

	// Bind the index buffer and load the index data into it.
	this->mOpenGL->BindBuffer( GL_ELEMENT_ARRAY_BUFFER, this->mBoxIB );
	this->mOpenGL->BufferData( GL_ELEMENT_ARRAY_BUFFER, (UBIGINT)lBox.Indices.size() * sizeof(UBIGINT), &lIndices[0], GL_STATIC_DRAW );

	GLenum lError = glGetError();
	if( lError != GL_NO_ERROR )
	{
		throw exception(" Box geometry Failed !!! ");
	}

	this->mOpenGL->BindVertArray( 0 );
}

/*********************************************************************************************/
VVOID   GraphicsClass::BuildTreeSpritesBuffer()
{
	TreePointSprite lVertices[ GraphicsClass::sTreeCount ];

	for( UBIGINT lCurrTree = 0; lCurrTree < GraphicsClass::sTreeCount; lCurrTree++ )
	{
		FFLOAT lX = RandBetween( -50.0f, 50.0f );
		FFLOAT lZ = RandBetween( -50.0f, 50.0f );
		FFLOAT lY = this->GetHillHeight( lX, lZ );

		// Move tree slightly above land height.
		lY += 10.0f;

		lVertices[ lCurrTree ].Position = Vector3( lX, lY, lZ );
		lVertices[ lCurrTree ].Size     = Vector2( 24.0f, 24.0f );
	}

	GLenum lError = glGetError();
	if( lError != GL_NO_ERROR )
	{
		throw exception(" Build Room Geometry Failure !!! ");
	}

	// Allocate an OpenGL vertex array object.
	this->mOpenGL->GenVertArrays( 1, &this->mTreeSpritesVAO );

	// Bind the vertex array object to store all the buffers and vertex attributes we create here.
	this->mOpenGL->BindVertArray( this->mTreeSpritesVAO );

	// Generate a vertex buffer object
	this->mOpenGL->GenBuffers( 1, &this->mTreeSpritesVB );

	// Bind the vertex buffer and load the vertex (position, and normal) data into the vertex buffer.
	this->mOpenGL->BindBuffer( GL_ARRAY_BUFFER, this->mTreeSpritesVB );
	this->mOpenGL->BufferData( GL_ARRAY_BUFFER, GraphicsClass::sTreeCount * sizeof(TreePointSprite), lVertices, GL_STATIC_DRAW );

	// Set the layout for the vertex stage
	InputLayouts::TreePointSpriteLayout()->SetVertexLayout( this->mTreeSpritesVB );

	lError = glGetError();
	if( lError != GL_NO_ERROR )
	{
		throw exception(" Build Room Geometry Failure !!! ");
	}

	this->mOpenGL->BindVertArray( 0 );
}

/*********************************************************************************************/
VVOID	GraphicsClass::BuildTrailEndpointsGeometryBuffers(Vector3 pStart, Vector3 pEnd)
{
	MeshData lSphere1;
	//MeshData lSphere2;

	this->mEndpoint1World.Matrix4x4Translation( this->mEndpoint1World, pStart.getX(), pStart.getY(), pStart.getZ() );
	this->mEndpoint2World.Matrix4x4Translation( this->mEndpoint2World, pEnd.getX(), pEnd.getY(), pEnd.getZ() );

	GeometryHelper::CreateSphere( 0.5f, 10, 10, lSphere1 );
	
	// Geometry helpers give EnhancedVertex but we need simpler ones for our VBO.
	std::vector<SimpleVertex> lVertices( lSphere1.Vertices.size() );
	for ( UBIGINT lCurrVert = 0; lCurrVert < (UBIGINT)lSphere1.Vertices.size(); lCurrVert++ )
	{
		EnhancedVertex lVertex = lSphere1.Vertices[ lCurrVert ];
		
		lVertices[ lCurrVert ].Position = lVertex.Position;
		lVertices[ lCurrVert ].Normal   = lVertex.Normal;
		lVertices[ lCurrVert ].TexCoord = lVertex.TexCoord;
	}

	this->mEndpointIndexCount = (UBIGINT)lSphere1.Indices.size();

	GLenum lError = glGetError();
	if ( lError != GL_NO_ERROR )
	{
		throw exception(" Build Land Geometry Failure !!! ");
	}

	// Allocate an OpenGL vertex array object.
	this->mOpenGL->GenVertArrays( 1, &this->mEndpointsVAO );

	// Bind the vertex array object to store all the buffers and vertex attributes we create here.
	this->mOpenGL->BindVertArray( this->mEndpointsVAO );

	// Generate a vertex buffer object
	this->mOpenGL->GenBuffers( 1, &this->mEndpointsVB );

	// Bind the vertex buffer and load the vertex (position, and normal) data into the vertex buffer.
	this->mOpenGL->BindBuffer( GL_ARRAY_BUFFER, this->mEndpointsVB );
	this->mOpenGL->BufferData( GL_ARRAY_BUFFER, lSphere1.Vertices.size() * sizeof(SimpleVertex), &lVertices[0], GL_STATIC_DRAW );

	// Set the layout for the vertex stage
	InputLayouts::PosNormalTexLayout()->SetVertexLayout( this->mEndpointsVB );

	// Generate an Index buffer object
	this->mOpenGL->GenBuffers( 1, &this->mEndpointsIB );

	// Bind the index buffer and load the index data into it.
	this->mOpenGL->BindBuffer( GL_ELEMENT_ARRAY_BUFFER, this->mEndpointsIB );
	this->mOpenGL->BufferData( GL_ELEMENT_ARRAY_BUFFER, this->mEndpointIndexCount * sizeof(UBIGINT), &lSphere1.Indices[0], GL_STATIC_DRAW );

	lError = glGetError();
	if( lError != GL_NO_ERROR )
	{
		throw exception(" Build Land Geometry Failure !!! ");
	}

	this->mOpenGL->BindVertArray( 0 );
}

/*********************************************************************************************/
VVOID   GraphicsClass::BuildTrailGeometryBuffers(Vector3 pStart, Vector3 pEnd)
{
	Vector3 lLineAxis = pEnd - pStart;
	FFLOAT lLength = lLineAxis.Vec3Length();
	vector<string> lTrailFilenames;
	lTrailFilenames.push_back("Textures/wild_mask1.DDS");
	lTrailFilenames.push_back("Textures/wild_mask2.DDS");

	this->mTrail = new TrailLine( this->mOpenGL, &this->mTexManager );
	this->mTrail->Initialize( lLength, lTrailFilenames );

	Vector3 lCenter = (pStart + pEnd) * 0.5f;
	this->mTrailWorld.Matrix4x4Translation( this->mTrailWorld, lCenter.getX(), lCenter.getY(), lCenter.getZ() );

	this->mTrail->SetCamera( this->mCamera );
	this->mTrail->SetWorld( this->mTrailWorld );
	this->mTrail->SetMaterial( this->mTrailMat );
	this->mTrail->SetLineAxis( lLineAxis.Vec3Normalise() );
}

/*********************************************************************************************/
VVOID   GraphicsClass::DrawHillAndWave(const Matrix4x4& pViewProj)
{
	FFLOAT lBlendFactor[] = { 0.0f, 0.0f, 0.0f, 0.0f };
	OcelotCameraInfo lInfo = this->mCamera->Info();

	if ( Effects::BasicFX() != NULL )
	{
		switch ( this->mRenderOptions )
		{
		case eLighting:
			this->mBoxTech = this->mLight3Tech;
			this->mLandNWaveTech = this->mLight3Tech;
			break;
		case eTextures:
			this->mBoxTech = this->mLight3TexAlphaClipTech;
			this->mLandNWaveTech = this->mLight3TexTech;
			break;
		case eTexturesAndFog:
			this->mBoxTech = this->mLight3TexAlphaClipFogTech;
			this->mLandNWaveTech = this->mLight3TexFogTech;
			break;
		}

		//
		// Draw the box with alpha clipping.
		// 

		// Make the box technique current
		Effects::BasicFX()->MakeCurrent( this->mBoxTech );

		Effects::BasicFX()->SetDirLights( this->mDirLights, 3 );
		Effects::BasicFX()->SetEyePosW( lInfo.Eye() );
		Effects::BasicFX()->SetFogColor( Color( 0.75f, 0.75f, 0.75f, 1.0f ) );
		Effects::BasicFX()->SetFogStart( 15.0f );
		Effects::BasicFX()->SetFogRange( 175.0f );

		// Active the first texture unit
		this->mOpenGL->ActiveTexture( GL_TEXTURE0 + this->mDiffuseTexUnit );
		// Set it to the texture unit.
		glBindTexture( GL_TEXTURE_2D, this->mBoxMapRV );

		//
		// Draw the box
		// 
		Matrix4x4 lWorld = this->mBoxWorld;
		Matrix4x4 lWorldInvTranspose = MathUtilities::InverseTranspose( lWorld );
		Matrix4x4 lWorldViewProj = lWorld * pViewProj;

		Effects::BasicFX()->SetWorld( lWorld );
		Effects::BasicFX()->SetWorldInvTranspose( lWorldInvTranspose );
		Effects::BasicFX()->SetWorldViewProj( lWorldViewProj );
		Effects::BasicFX()->SetTexTransform( Matrix4x4() );
		Effects::BasicFX()->SetMaterial( this->mBoxMat );
		Effects::BasicFX()->SetDiffuseMap( this->mDiffuseTexUnit );

		this->mOpenGL->BindVertArray( this->mBoxVAO );

		UBIGINT lError = glGetError();
		if ( lError != GL_NO_ERROR )
		{
			throw exception(" Draw box Failure !!! ");
		}

		glDisable( GL_CULL_FACE );
		glDrawElements( GL_TRIANGLES, 36, GL_UNSIGNED_INT, NULL );
		glEnable( GL_CULL_FACE );

		// Unbind Land VAO
		this->mOpenGL->BindVertArray( 0 );

		//
		// Draw the hills and water with texture and fog (no alpha clipping needed).
		//

		// Active Land and Wave technique
		Effects::BasicFX()->MakeCurrent( this->mLandNWaveTech );

		Effects::BasicFX()->SetDirLights( this->mDirLights, 3 );
		Effects::BasicFX()->SetEyePosW( lInfo.Eye() );
		Effects::BasicFX()->SetFogColor( Color( 0.75f, 0.75f, 0.75f, 1.0f ) );
		Effects::BasicFX()->SetFogStart( 15.0f );
		Effects::BasicFX()->SetFogRange( 175.0f );

		// Active the first texture unit
		this->mOpenGL->ActiveTexture( GL_TEXTURE0 + this->mDiffuseTexUnit );
		// Set it to the texture unit.
		glBindTexture( GL_TEXTURE_2D, this->mLandMapRV );

		//
		// Draw the hills
		//
		lWorld = this->mLandWorld;
		lWorldInvTranspose = MathUtilities::InverseTranspose( lWorld );
		lWorldViewProj = lWorld * pViewProj;

		Effects::BasicFX()->SetWorld( lWorld );
		Effects::BasicFX()->SetWorldInvTranspose( lWorldInvTranspose );
		Effects::BasicFX()->SetWorldViewProj( lWorldViewProj );
		Effects::BasicFX()->SetTexTransform( this->mLandTexTransform );
		Effects::BasicFX()->SetMaterial( this->mLandMat );
		Effects::BasicFX()->SetDiffuseMap( this->mDiffuseTexUnit );

		this->mOpenGL->BindVertArray( this->mLandVAO );

		lError = glGetError();
		if
			( lError != GL_NO_ERROR )
		{
			throw exception(" Draw Land Failure !!! ");
		}

		glDrawElements( GL_TRIANGLES, this->mLandIndexCount, GL_UNSIGNED_INT, NULL );
		
		// Unbind Land VAO
		this->mOpenGL->BindVertArray( 0 );

		// Active the first texture unit
		this->mOpenGL->ActiveTexture( GL_TEXTURE0 + this->mDiffuseTexUnit );
		// Set it to the texture unit.
		glBindTexture( GL_TEXTURE_2D, this->mWaveMapRV );
		
		lError = glGetError();
		if
			( lError != GL_NO_ERROR )
		{
			throw exception(" Draw Waves Failure !!! ");
		}

		//
		// Draw the waves.
		//
		lWorld = this->mWaveWorld;
		lWorldInvTranspose = MathUtilities::InverseTranspose( lWorld );
		lWorldViewProj = lWorld * pViewProj;

		Effects::BasicFX()->SetWorld( lWorld );
		Effects::BasicFX()->SetWorldInvTranspose( lWorldInvTranspose );
		Effects::BasicFX()->SetWorldViewProj( lWorldViewProj );
		Effects::BasicFX()->SetTexTransform( this->mWaveTexTransform );
		Effects::BasicFX()->SetMaterial( this->mWaveMat );
		Effects::BasicFX()->SetDiffuseMap( this->mDiffuseTexUnit );

		this->mOpenGL->BindVertArray( this->mWaveVAO );

		// Enable blending
		glEnable( GL_BLEND );
		this->mOpenGL->BlendColor( lBlendFactor[0], lBlendFactor[1], lBlendFactor[2], lBlendFactor[3] );
		this->mOpenGL->BlendEquationSeparate( GL_FUNC_ADD, GL_FUNC_ADD );
		this->mOpenGL->BlendFuncSeparate( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA, GL_ONE, GL_ZERO );
		
		lError = glGetError();
		if ( lError != GL_NO_ERROR )
		{
			throw exception(" Draw Waves Failure !!! ");
		}

		glDrawElements( GL_TRIANGLES, 3 * this->mWaves.TriangleCount(), GL_UNSIGNED_INT, NULL );

		glDisable( GL_BLEND );

		// Unbind Waves VAO
		this->mOpenGL->BindVertArray( 0 );
		
		lError = glGetError();
		if ( lError != GL_NO_ERROR )
		{
			throw exception(" Draw Waves Failure !!! ");
		}
	}
}

/*********************************************************************************************/
VVOID   GraphicsClass::DrawTreeSprites(const Matrix4x4& pViewProj)
{
	OcelotCameraInfo lInfo = this->mCamera->Info();

	if ( Effects::TreeSpriteFX() != NULL )
	{
		switch ( this->mRenderOptions )
		{
		case eLighting:
			this->mTreeSpritesTech = this->mTreeLight3Tech;
			break;
		case eTextures:
			this->mTreeSpritesTech = this->mTreeLight3TexAlphaClipTech;
			break;
		case eTexturesAndFog:
			this->mTreeSpritesTech = this->mTreeLight3TexAlphaClipFogTech;
			break;
		}

		// Make the box technique current
		Effects::TreeSpriteFX()->MakeCurrent( this->mTreeSpritesTech );

		Effects::TreeSpriteFX()->SetDirLights( this->mDirLights, 3 );
		Effects::TreeSpriteFX()->SetEyePosW( lInfo.Eye() );
		Effects::TreeSpriteFX()->SetFogColor( Color( 0.75f, 0.75f, 0.75f, 1.0f ) );
		Effects::TreeSpriteFX()->SetFogStart( 15.0f );
		Effects::TreeSpriteFX()->SetFogRange( 175.0f );
		Effects::TreeSpriteFX()->SetViewProj( pViewProj );
		Effects::TreeSpriteFX()->SetMaterial( this->mTreeMat );

		GLenum lError = glGetError();
		if
			( lError != GL_NO_ERROR )
		{
			throw exception(" Draw Tree sprites Failure !!! ");
		}

		//
		// Draw the Tree Sprites
		// 
		this->mOpenGL->BindVertArray( this->mTreeSpritesVAO );

		// Active the first texture unit
		this->mOpenGL->ActiveTexture( GL_TEXTURE0 + this->mDiffuseTexUnit );
		// Set it to the texture unit.
		glBindTexture( GL_TEXTURE_2D_ARRAY, this->mTreeTexMapArrayRV );

		lError = glGetError();
		if ( lError != GL_NO_ERROR )
		{
			throw exception(" Draw Tree sprites Failure !!! ");
		}

		Effects::TreeSpriteFX()->SetTreeTextureMapArray( this->mDiffuseTexUnit );

		if ( this->mAlphaToCoverageOn )
		{
			glEnable( GL_SAMPLE_ALPHA_TO_COVERAGE );

			lError = glGetError();
			if
				( lError != GL_NO_ERROR )
			{
				throw exception(" Draw Tree sprites Failure !!! ");
			}
		}

		glDrawArrays( GL_POINTS, 0, GraphicsClass::sTreeCount );

		if
			( this->mAlphaToCoverageOn )
		{
			glDisable( GL_SAMPLE_ALPHA_TO_COVERAGE );
		}

		lError = glGetError();
		if
			( lError != GL_NO_ERROR )
		{
			throw exception(" Draw Tree sprites Failure !!! ");
		}
	}
}

/*********************************************************************************************/
VVOID   GraphicsClass::DrawTrail(const Matrix4x4& pViewProj)
{
	OcelotCameraInfo lInfo = this->mCamera->Info();

	// Draw endpoints.
	if ( Effects::BasicFX() != NULL )
	{
		switch ( this->mRenderOptions )
		{
		case eLighting:
			this->mEndpointsTech = this->mLight3Tech;
			break;
		case eTextures:
			this->mEndpointsTech = this->mLight3TexTech;
			break;
		case eTexturesAndFog:
			this->mEndpointsTech = this->mLight3TexFogTech;
			break;
		}

		// Make the box technique current
		Effects::BasicFX()->MakeCurrent( this->mEndpointsTech );

		// Active the first texture unit
		this->mOpenGL->ActiveTexture( GL_TEXTURE0 + this->mDiffuseTexUnit );
		// Set it to the texture unit.
		glBindTexture( GL_TEXTURE_2D, this->mWaveMapRV );

		//
		// Draw the endpoints (spheres).
		//
		Matrix4x4 lWorld = this->mEndpoint1World;
		Matrix4x4 lWorldInvTranspose = MathUtilities::InverseTranspose(lWorld);
		Matrix4x4 lWorldViewProj = lWorld * pViewProj;

		Effects::BasicFX()->SetWorld( lWorld );
		Effects::BasicFX()->SetWorldInvTranspose( lWorldInvTranspose );
		Effects::BasicFX()->SetWorldViewProj( lWorldViewProj );
		Effects::BasicFX()->SetTexTransform( this->mWaveTexTransform );
		Effects::BasicFX()->SetMaterial( this->mBoxMat );
		Effects::BasicFX()->SetDiffuseMap( this->mDiffuseTexUnit );

		this->mOpenGL->BindVertArray( this->mEndpointsVAO );

		UBIGINT lError = glGetError();
		if ( lError != GL_NO_ERROR )
		{
			throw exception(" Draw sphere Failure !!! ");
		}

		// First sphere.
		glDrawElements( GL_TRIANGLES, this->mEndpointIndexCount, GL_UNSIGNED_INT, NULL );
		
		// Just update quickly relevant uniforms
		lWorld = this->mEndpoint2World;
		lWorldInvTranspose = MathUtilities::InverseTranspose( lWorld );
		lWorldViewProj = lWorld * pViewProj;

		Effects::BasicFX()->SetWorld( lWorld );
		Effects::BasicFX()->SetWorldInvTranspose( lWorldInvTranspose );
		Effects::BasicFX()->SetWorldViewProj( lWorldViewProj );

		// Second sphere.
		glDrawElements( GL_TRIANGLES, this->mEndpointIndexCount, GL_UNSIGNED_INT, NULL );

		// OPTIMIZE OPTIONS IF MANY ( > 50 for instance ) SAME OBJECT ONLY WITH DIFF LOCATION or so:

		// Create an offset buffer.
		//UBIGINT lSphereTBO;
		//glGenBuffers( 1, &lSphereTBO );
		//glBindBuffer( GL_ARRAY_BUFFER, lSphereTBO );

		//// N offsets with 3 components each
		//FFLOAT lTranslationData[] = 
		//{
		//			 pStart.getX(), pStart.getY(), pStart.getZ(),  // sphere 0
		//			 pEnd.getX(), pEnd.getY(), pEnd.getZ(),        // sphere 1
		//			 // ...
		//			 // sphere N
		//};

		//// Other stuff...

		//// Provides data
		//glBufferData( GL_ARRAY_BUFFER, sizeof(GLfloat) * 3 * 8, lTranslationData, GL_STATIC_DRAW );

		//// Then for drawing.......

		//// - The shader will use gl_InstanceID to select wich offset to apply for the current mesh position.

		//// - Draw in one call.
		//// NOTE: the additional parameter indicates how many instances to render
		//glDrawElementsInstanced( GL_TRIANGLES, Count, GL_UNSIGNED_INT, 0, N );

		// Unbind Land VAO
		this->mOpenGL->BindVertArray( 0 );
	}
	
	// Draw the trail.
	switch (this->mRenderOptions)
	{
	case eLighting:
		this->mTrailTech = this->mTrailLineTech;
		break;
	case eTextures:
	case eTexturesAndFog:
		this->mTrailTech = this->mTrailLineTexTech;
		break;
	}

	this->mTrail->SetTechnique( this->mTrailTech );
	this->mTrail->SetTextureUnit( this->mDiffuseTexUnit );
	this->mTrail->SetOndulate( this->mTrailOndulate );
	this->mTrail->SetAmplitude( this->mTrailAmplitude );
	this->mTrail->SetSpeedFactor( this->mTrailSpeedFactor );

	this->mTrail->Render();
}

/*********************************************************************************************/
VVOID   GraphicsClass::VolumetricTest()
{
	switch (this->mRenderOptions)
	{
	case eLighting:
		this->mVolumetricTech = this->mVolumetricLineTech;
		break;
	case eTextures:
	case eTexturesAndFog:
		this->mVolumetricTech = this->mVolumetricLineTexTech;
		break;
	}

	this->mLine->SetTechnique( this->mVolumetricTech );
	this->mLine->SetLineRadius( this->mLineRadius );
	this->mLine->SetTextureUnit( this->mDiffuseTexUnit );

	this->mLine->Render();
}

/*********************************************************************************************/
IOcelotCameraController* GraphicsClass::Controller()
{
	return this->mController;
}

/*********************************************************************************************/
VVOID GraphicsClass::SetOption(RenderOptions pOption)
{
	this->mRenderOptions = pOption;
}

/*********************************************************************************************/
VVOID GraphicsClass::SwitchAlphaToCoverageOn()
{
	this->mAlphaToCoverageOn = true;
}

/*********************************************************************************************/
VVOID GraphicsClass::SwitchAlphaToCoverageOff()
{
	this->mAlphaToCoverageOn = false;
}

