#include "VolumetricLine.h"
#include "..\Maths\Vector3.h"
#include "..\GDevice.h"
#include "..\TextureManager\TextureManager.h"
#include "..\Effects\InputLayouts.h"
#include "..\Effects\Effects.h"
#include "..\Effects\EffectTechnique.h"


using namespace std;
using namespace Ocelot;

#define END_OF_PRIMITIVE_ID 		999999

/*********************************************************************************************/
VolumetricLine::VolumetricLine(GDevice* pDevice, TextureManager* pTextureManager) :
mDevice(pDevice), mTextureManager( pTextureManager ), mVAO(0), mMainVBO(0), mDuplicateVBO(0), mOffsetUVVBO(0),
mIBO(0), mTextureRV(0), mLineCount( 1)
{

}

/*********************************************************************************************/
VolumetricLine:: ~VolumetricLine()
{

}

/*********************************************************************************************/
VVOID VolumetricLine::Initialize(const Vector3& pStart, const Vector3& pEnd, const string& pTexturePath, const UBIGINT& pCount /*= 1*/)
{
	this->mLineCount = pCount;
	this->mTextureRV = this->mTextureManager->CreateTexture( pTexturePath, true );

	GLenum lError = glGetError();
	if ( lError != GL_NO_ERROR )
	{
		throw exception(" Build Line Geometry Failure !!! ");
	}

	// Create the trail, using the overall distance and the unit factor to determine points.
	const UBIGINT lLineCount    = pCount;
	const UBIGINT lPointPerLine = 2;
	const UBIGINT lVertexSize   = 3;
	const UBIGINT lOffsetUVSize = 4;
	const UBIGINT lTriangleStripVertexCount = 8; // The amount of vertex to extrude a line using triangle strips.

	Vector3 lToEnd = pEnd - pStart;
	const FFLOAT lLength = lToEnd.Vec3Length();
	const FFLOAT lLineLength = lLength / lLineCount;

	Vector3 lToEndUnit = lToEnd.Vec3Normalise();

	FFLOAT* lPositions = new FFLOAT[ lLineCount * lPointPerLine * lVertexSize ];

	{
		UBIGINT lCurr = 0;
		lPositions[ lCurr++ ] = pStart.getX();
		lPositions[ lCurr++ ] = pStart.getY();
		lPositions[ lCurr++ ] = pStart.getZ();

		Vector3 lPrevious = pStart;
		for ( UBIGINT lIter = 0; lIter < lLineCount - 1; lIter++, lCurr+= 6 )
		{
			Vector3 lCurrent = lPrevious + lToEndUnit * lLineLength;
			lPositions[ lCurr ]     = lCurrent.getX();
			lPositions[ lCurr + 1 ] = lCurrent.getY();
			lPositions[ lCurr + 2 ] = lCurrent.getZ();

			lPositions[ lCurr + 3 ] = lPrevious.getX();
			lPositions[ lCurr + 4 ] = lPrevious.getY();
			lPositions[ lCurr + 5 ] = lPrevious.getZ();

			lPrevious = lCurrent;
		}

		lPositions[ lCurr++ ] = pEnd.getX();
		lPositions[ lCurr++ ] = pEnd.getY();
		lPositions[ lCurr++ ] = pEnd.getZ();
	}

	// Allocate an OpenGL vertex array object.
	this->mDevice->GenVertArrays( 1, &this->mVAO );

	// Bind the vertex array object to store all the buffers and vertex attributes we create here.
	this->mDevice->BindVertArray( this->mVAO );

	// Generate a vertex buffer object
	this->mDevice->GenBuffers( 1, &this->mMainVBO );

	FFLOAT* lMainVertices = new FFLOAT[ lLineCount * lTriangleStripVertexCount * lVertexSize ];
	for ( UBIGINT lCurr = 0; lCurr < lLineCount; lCurr++ )
	{
		int lFrom  = lCurr * 6;
		int lTo    = lCurr * 6 + 3;
		int l24    = lCurr * 24;
		lMainVertices[l24]		= lPositions[lFrom];		lMainVertices[l24 + 1]  = lPositions[lFrom + 1];		lMainVertices[l24 + 2]  = lPositions[lFrom + 2];
		lMainVertices[l24 + 3]  = lPositions[lFrom];		lMainVertices[l24 + 4]  = lPositions[lFrom + 1];		lMainVertices[l24 + 5]  = lPositions[lFrom + 2];
		lMainVertices[l24 + 6]  = lPositions[lFrom];		lMainVertices[l24 + 7]  = lPositions[lFrom + 1];		lMainVertices[l24 + 8]  = lPositions[lFrom + 2];
		lMainVertices[l24 + 9]  = lPositions[lFrom];		lMainVertices[l24 + 10] = lPositions[lFrom + 1];		lMainVertices[l24 + 11] = lPositions[lFrom + 2];

		lMainVertices[l24 + 12] = lPositions[lTo];		lMainVertices[l24 + 13] = lPositions[lTo + 1];		lMainVertices[l24 + 14] = lPositions[lTo + 2];
		lMainVertices[l24 + 15] = lPositions[lTo];		lMainVertices[l24 + 16] = lPositions[lTo + 1];		lMainVertices[l24 + 17] = lPositions[lTo + 2];
		lMainVertices[l24 + 18] = lPositions[lTo];		lMainVertices[l24 + 19] = lPositions[lTo + 1];		lMainVertices[l24 + 20] = lPositions[lTo + 2];
		lMainVertices[l24 + 21] = lPositions[lTo];		lMainVertices[l24 + 22] = lPositions[lTo + 1];		lMainVertices[l24 + 23] = lPositions[lTo + 2];
	}

	// Bind the vertex buffer and load the vertex data into the vertex buffer.
	this->mDevice->BindBuffer( GL_ARRAY_BUFFER, this->mMainVBO );
	this->mDevice->BufferData( GL_ARRAY_BUFFER, lLineCount * lTriangleStripVertexCount * lVertexSize * sizeof(FFLOAT), lMainVertices, GL_STATIC_DRAW ); // Use stream draw only if you intend to modify it once only later on and then never change.

	// Only bind the main vertex pos buffer to the PosL vertex component.
	InputLayouts::VolumetricLineLayout()->SetVertexLayout( this->mMainVBO, "PosL" );

	FFLOAT* lDuplicatedVertices = new FFLOAT[ lLineCount * lTriangleStripVertexCount * lVertexSize ];
	for ( UBIGINT lCurr = 0; lCurr < lLineCount; lCurr++ )
	{
		int lFrom  = lCurr * 6;
		int lTo    = lCurr * 6 + 3;
		int l24    = lCurr * 24;
		lDuplicatedVertices[l24]      = lPositions[lTo];		lDuplicatedVertices[l24 + 1]  = lPositions[lTo + 1];		lDuplicatedVertices[l24 + 2]  = lPositions[lTo + 2];
		lDuplicatedVertices[l24 + 3]  = lPositions[lTo];		lDuplicatedVertices[l24 + 4]  = lPositions[lTo + 1];		lDuplicatedVertices[l24 + 5]  = lPositions[lTo + 2];
		lDuplicatedVertices[l24 + 6]  = lPositions[lTo];		lDuplicatedVertices[l24 + 7]  = lPositions[lTo + 1];		lDuplicatedVertices[l24 + 8]  = lPositions[lTo + 2];
		lDuplicatedVertices[l24 + 9]  = lPositions[lTo];		lDuplicatedVertices[l24 + 10] = lPositions[lTo + 1];		lDuplicatedVertices[l24 + 11] = lPositions[lTo + 2];

		lDuplicatedVertices[l24 + 12] = lPositions[lFrom];		lDuplicatedVertices[l24 + 13] = lPositions[lFrom + 1];		lDuplicatedVertices[l24 + 14] = lPositions[lFrom + 2];
		lDuplicatedVertices[l24 + 15] = lPositions[lFrom];		lDuplicatedVertices[l24 + 16] = lPositions[lFrom + 1];		lDuplicatedVertices[l24 + 17] = lPositions[lFrom + 2];
		lDuplicatedVertices[l24 + 18] = lPositions[lFrom];		lDuplicatedVertices[l24 + 19] = lPositions[lFrom + 1];		lDuplicatedVertices[l24 + 20] = lPositions[lFrom + 2];
		lDuplicatedVertices[l24 + 21] = lPositions[lFrom];		lDuplicatedVertices[l24 + 22] = lPositions[lFrom + 1];		lDuplicatedVertices[l24 + 23] = lPositions[lFrom + 2];
	}

	// Generate a duplicate vertex buffer object
	this->mDevice->GenBuffers( 1, &this->mDuplicateVBO );

	// Bind the vertex buffer and load the vertex data into the vertex buffer.
	this->mDevice->BindBuffer( GL_ARRAY_BUFFER, this->mDuplicateVBO );
	this->mDevice->BufferData( GL_ARRAY_BUFFER, lLineCount * lTriangleStripVertexCount * lVertexSize * sizeof(FFLOAT), lDuplicatedVertices, GL_STATIC_DRAW ); // Use stream draw only if you intend to modify it once only later on and then never change.

	// Only bind the duplicated pos buffer to the DuplicatePosL vertex component.
	InputLayouts::VolumetricLineLayout()->SetVertexLayout( this->mDuplicateVBO, "DuplicatePosL" );

	FFLOAT* lOffsetUVs = new FFLOAT[ lLineCount * lOffsetUVSize * lTriangleStripVertexCount ];
	for ( UBIGINT lCurr = 0; 
		  lCurr < lLineCount * lOffsetUVSize * lTriangleStripVertexCount;
		  lCurr += lOffsetUVSize * lTriangleStripVertexCount )
	{
		lOffsetUVs[ lCurr ]	     = 1.0f;	lOffsetUVs[lCurr + 1]  = 1.0f;		lOffsetUVs[lCurr + 2]  = 1.0f;		lOffsetUVs[lCurr + 3]  = 0.0f;
		lOffsetUVs[ lCurr + 4 ]  = 1.0f;	lOffsetUVs[lCurr + 5]  = -1.0f;		lOffsetUVs[lCurr + 6]  = 1.0f;		lOffsetUVs[lCurr + 7]  = 1.0f;
		lOffsetUVs[ lCurr + 8 ]  = 0.0f;	lOffsetUVs[lCurr + 9]  = 1.0f;		lOffsetUVs[lCurr + 10] = 0.5f;		lOffsetUVs[lCurr + 11] = 0.0f;
		lOffsetUVs[ lCurr + 12 ] = 0.0f;	lOffsetUVs[lCurr + 13] = -1.0f;		lOffsetUVs[lCurr + 14] = 0.5f;		lOffsetUVs[lCurr + 15] = 1.0f;
		lOffsetUVs[ lCurr + 16 ] = 0.0f;	lOffsetUVs[lCurr + 17] = -1.0f;		lOffsetUVs[lCurr + 18] = 0.5f;		lOffsetUVs[lCurr + 19] = 0.0f;
		lOffsetUVs[ lCurr + 20 ] = 0.0f;	lOffsetUVs[lCurr + 21] = 1.0f;		lOffsetUVs[lCurr + 22] = 0.5f;		lOffsetUVs[lCurr + 23] = 1.0f;
		lOffsetUVs[ lCurr + 24 ] = 1.0f;	lOffsetUVs[lCurr + 25] = -1.0f;		lOffsetUVs[lCurr + 26] = 0.0f;		lOffsetUVs[lCurr + 27] = 0.0f;
		lOffsetUVs[ lCurr + 28 ] = 1.0f;	lOffsetUVs[lCurr + 29] = 1.0f;		lOffsetUVs[lCurr + 30] = 0.0f;		lOffsetUVs[lCurr + 31] = 1.0f;
	}

	// Generate the Offsets+UVs buffer object
	this->mDevice->GenBuffers( 1, &this->mOffsetUVVBO );

	this->mDevice->BindBuffer( GL_ARRAY_BUFFER, this->mOffsetUVVBO );
	this->mDevice->BufferData( GL_ARRAY_BUFFER, lLineCount * lTriangleStripVertexCount * lOffsetUVSize * sizeof(FFLOAT), lOffsetUVs, GL_STATIC_DRAW );

	// Only bind the vertex Offset+UV buffer to the OffsetUV vertex component.
	InputLayouts::VolumetricLineLayout()->SetVertexLayout( this->mOffsetUVVBO, "OffsetUV" );

	// Now create the indices buffer to render lines using primitive restart.
	const UBIGINT lElementCount = lLineCount * lTriangleStripVertexCount + lLineCount;
	UBIGINT* lStripIndices = new UBIGINT[ lElementCount ];
	UBIGINT lLineId = 0;
	for ( UBIGINT lCurr = 0; lCurr < lElementCount; lCurr+= lTriangleStripVertexCount + 1 )
	{
		lStripIndices[ lCurr ]     = lLineId;
		lStripIndices[ lCurr + 1 ] = lLineId + 1;
		lStripIndices[ lCurr + 2 ] = lLineId + 2;
		lStripIndices[ lCurr + 3 ] = lLineId + 3;
		lStripIndices[ lCurr + 4 ] = lLineId + 4;
		lStripIndices[ lCurr + 5 ] = lLineId + 5;
		lStripIndices[ lCurr + 6 ] = lLineId + 6;
		lStripIndices[ lCurr + 7 ] = lLineId + 7;
		lStripIndices[ lCurr + 8 ] = END_OF_PRIMITIVE_ID;

		lLineId += lTriangleStripVertexCount;
	}

	// Generate a indices buffer object
	this->mDevice->GenBuffers( 1, &this->mIBO );

	this->mDevice->BindBuffer( GL_ELEMENT_ARRAY_BUFFER, this->mIBO );
	this->mDevice->BufferData( GL_ELEMENT_ARRAY_BUFFER, lElementCount * sizeof(FFLOAT), lStripIndices, GL_STATIC_DRAW );

	delete[] lPositions;
	delete[] lMainVertices;
	delete[] lDuplicatedVertices;
	delete[] lOffsetUVs;
	delete[] lStripIndices;

	this->mDevice->BindVertArray(0);

	lError = glGetError();
	if ( lError != GL_NO_ERROR )
	{
		throw exception(" Build Line Geometry Failure !!! ");
	}
}

/*********************************************************************************************/
VVOID VolumetricLine::Update(const FFLOAT& pDeltaTime)
{

}

/*********************************************************************************************/
VVOID VolumetricLine::Render()
{
	// Draw Line.
	if ( Effects::VolumetricLineFX() != NULL )
	{
		// Make the volumetric line technique current
		Effects::VolumetricLineFX()->MakeCurrent( this->mTechnique );

		UBIGINT lError = glGetError();
		if ( lError != GL_NO_ERROR )
		{
			throw exception(" Draw line Failure !!! ");
		}

		// Uses additive blending
		glDisable(GL_DEPTH_TEST);
		glDepthMask(GL_FALSE);
		glEnable(GL_BLEND);
		glBlendFunc(GL_ONE, GL_ONE);
		glDisable(GL_CULL_FACE);

		lError = glGetError();
		if ( lError != GL_NO_ERROR )
		{
			throw exception(" Draw line Failure !!! ");
		}

		Matrix4x4 lView = this->mCamera->View();
		Matrix4x4 lProj = this->mCamera->Proj();
		Matrix4x4 lViewProj = lView * lProj;

		Matrix4x4 lWorldViewProj = this->mWorld * lViewProj;
		OcelotCameraInfo lInfo = this->mCamera->Info();
		float lAspectRatio = (1280 / 960);// lInfo.Aspect();

		// Set uniforms.
		Effects::VolumetricLineFX()->SetWorldViewProj( lWorldViewProj );
		Effects::VolumetricLineFX()->SetTexTransform( this->mTexTransform );
		Effects::VolumetricLineFX()->SetRadius( this->mLineRadius );
		Effects::VolumetricLineFX()->SetInvSrcRatio( 1.0f / lAspectRatio );
		Effects::VolumetricLineFX()->SetEyePosW( lInfo.Eye() );

		// Active the first texture unit
		this->mDevice->ActiveTexture( GL_TEXTURE0 + this->mTextureUnit );
		// Set it to the texture unit.
		glBindTexture( GL_TEXTURE_2D, mTextureRV );

		Effects::VolumetricLineFX()->SetDiffuseMap( this->mTextureUnit );

		// Enable primitive restart and pass the identifier.
		glEnable(GL_PRIMITIVE_RESTART);
		glPrimitiveRestartIndex(END_OF_PRIMITIVE_ID);

		this->mDevice->BindVertArray( this->mVAO );
		this->mDevice->BindBuffer( GL_ELEMENT_ARRAY_BUFFER, this->mIBO );

		lError = glGetError();
		if ( lError != GL_NO_ERROR )
		{
			throw exception(" Draw line Failure !!! ");
		}

		// Draw...
		const UBIGINT lTriangleStripVertexCount = 8;
		glDrawElements( GL_TRIANGLE_STRIP, this->mLineCount * (lTriangleStripVertexCount + 1), GL_UNSIGNED_INT, NULL );

		// Unbind Land VAO
		this->mDevice->BindVertArray( 0 );

		lError = glGetError();
		if ( lError != GL_NO_ERROR )
		{
			throw exception(" Draw line Failure !!! ");
		}

		// Reset the previous state(s)
		glDisable(GL_PRIMITIVE_RESTART);

		glEnable(GL_CULL_FACE);
		glDisable(GL_BLEND);
		glDepthMask(GL_TRUE);
		glEnable(GL_DEPTH_TEST);

		lError = glGetError();
		if ( lError != GL_NO_ERROR )
		{
			throw exception(" Draw line Failure !!! ");
		}
	}
}

/*********************************************************************************************/
VVOID VolumetricLine::CleanUp()
{
	glDeleteTextures( 1, &this->mTextureRV );

	this->mDevice->DeleteBuffers( 1, &this->mMainVBO );
	this->mDevice->DeleteBuffers( 1, &this->mDuplicateVBO );
	this->mDevice->DeleteBuffers( 1, &this->mOffsetUVVBO );
	this->mDevice->DeleteBuffers( 1, &this->mIBO );
	this->mDevice->DeleteVertArrays( 1, &this->mVAO );

	this->mDevice = NULL;
	this->mCamera = NULL;
	this->mTechnique = NULL;
}
