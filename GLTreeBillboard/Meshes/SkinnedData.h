#ifndef DEF_SKINNEDDATA_H
#define DEF_SKINNEDDATA_H

// Includes
#include <map>
#include <string>
#include "..\OSCheck.h"
#include "..\Maths\Matrix4x4.h"
#include "..\Animation\AnimationClip.h"

// Class definition
class SkinnedData
{
private:

	// Attributes

	// Gives parentIndex of ith bone.
	std::vector<BIGINT>					 mBoneHierarchy;
	std::vector<Matrix4x4>				 mBoneOffsets;
	std::map<std::string, AnimationClip> mAnimations;

public:

	// Constructor
	SkinnedData();
	~SkinnedData();

	// Methods
	void Initialize(std::vector<int>& pBoneHierarchy, std::vector<Matrix4x4>& pBoneOffsets, std::map<std::string, AnimationClip>& pAnimations);

	// In a real project, you'd want to cache the result if there was a chance
	// that you were calling this several times with the same clipName at 
	// the same timePos.
    void ComputeFinalTransforms(const std::string& pClipName, float pTimePos, std::vector<Matrix4x4>& pFinalTransforms);

	// Accessors
	UBIGINT	BoneCount() const;
	FFLOAT  ClipStartTime(const std::string& pClipName) const;
	FFLOAT  ClipEndTime(const std::string& pClipName) const;

};

#endif