#ifndef DEF_SUBSET_H
#define DEF_SUBSET_H

// Includes
#include "..\OSCheck.h"

// Class definition
class Subset
{
private:

	// Attributes
	UBIGINT mId;
	UBIGINT mVertexStart;
	UBIGINT mVertexCount;
	UBIGINT mFaceStart;
	UBIGINT mFaceCount;

public:

	// Constructor
	Subset();
	Subset(const UBIGINT& pId, const UBIGINT& pVertexStart, const UBIGINT& pVertexCount, const UBIGINT& pFaceStart, const UBIGINT& pFaceCount);
	~Subset();

	// Accessors
	UBIGINT Id() const;
	UBIGINT VertexStart() const;
	UBIGINT VertexCount() const;
	UBIGINT FaceStart() const;
	UBIGINT FaceCount() const;
	VVOID   SetId(const UBIGINT& pId);
	VVOID   SetVertexStart(const UBIGINT& pVertexStart);
	VVOID   SetVertexCount(const UBIGINT& pVertexCount);
	VVOID   SetFaceStart(const UBIGINT& pFaceStart);
	VVOID   SetFaceCount(const UBIGINT& pFaceCount);

};

#endif