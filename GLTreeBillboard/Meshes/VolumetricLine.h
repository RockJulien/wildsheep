#pragma once

#include "..\OSCheck.h"
#include "..\Camera\OcelotCamera.h"
#include <string>

class GDevice;
class TextureManager;
class Vector3;
class EffectTechnique;

class VolumetricLine
{
private:

	GDevice* mDevice;
	Ocelot::OcelotCamera* mCamera;
	TextureManager* mTextureManager;

	Matrix4x4 mWorld;
	Matrix4x4 mTexTransform;
	FFLOAT    mLineRadius;
	UBIGINT   mLineCount;

	UBIGINT mVAO;
	UBIGINT mMainVBO;
	UBIGINT mDuplicateVBO;
	UBIGINT mOffsetUVVBO;
	UBIGINT mIBO;

	UBIGINT mTextureRV;
	UBIGINT mTextureUnit;

	EffectTechnique* mTechnique;

public:

	VolumetricLine(GDevice* pDevice, TextureManager* pTextureManager);
	~VolumetricLine();

	VVOID Initialize(const Vector3& pStart, const Vector3& pEnd, const std::string& pTexturePath, const UBIGINT& pCount = 1);
	VVOID Update(const FFLOAT& pDeltaTime);
	VVOID Render();
	VVOID CleanUp();

	inline VVOID SetCamera(Ocelot::OcelotCamera* pCamera)
	{
		this->mCamera = pCamera;
	}
	inline VVOID SetTechnique(EffectTechnique* pTechnique)
	{
		this->mTechnique = pTechnique;
	}
	inline VVOID SetTextureUnit(const UBIGINT& pUnit)
	{
		this->mTextureUnit = pUnit;
	}
	inline VVOID SetLineRadius(const FFLOAT& pRadius)
	{
		this->mLineRadius = pRadius;
	}
};