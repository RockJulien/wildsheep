#pragma once

#include "..\OSCheck.h"
#include "..\Materials\Material.h"
#include "..\Camera\OcelotCamera.h"
#include <string>

class GDevice;
class TextureManager;
class Vector3;
class EffectTechnique;

class TrailLine
{
private:

	GDevice* mDevice;
	Ocelot::OcelotCamera* mCamera;
	TextureManager* mTextureManager;

	Matrix4x4 mWorld;
	Matrix4x4 mTexTransform;
	Vector3   mLineAxis;
	FFLOAT	  mTexOffset;
	FFLOAT    mLineRadius;
	UBIGINT   mLineCount;
	UBIGINT   mIndexCount;
	FFLOAT	  mTime;
	FFLOAT    mSpeed;
	FFLOAT    mAmplitude;
	BBOOL	  mOndulate;

	UBIGINT   mVAO;
	UBIGINT   mVBO;
	UBIGINT   mIBO;

	Material  mMaterial;
	UBIGINT   mTextureRV1;
	UBIGINT   mTextureRV2;
	UBIGINT   mTextureUnit;

	EffectTechnique* mTechnique;

public:

	TrailLine(GDevice* pDevice, TextureManager* pTextureManager);
	~TrailLine();

	VVOID Initialize(const FFLOAT& pLength, const std::vector<std::string>& pTexturePath);
	VVOID Update(const FFLOAT& pDeltaTime);
	VVOID Render();
	VVOID CleanUp();

	inline VVOID SetCamera(Ocelot::OcelotCamera* pCamera)
	{
		this->mCamera = pCamera;
	}
	inline VVOID SetTechnique(EffectTechnique* pTechnique)
	{
		this->mTechnique = pTechnique;
	}
	inline VVOID SetTextureUnit(const UBIGINT& pUnit)
	{
		this->mTextureUnit = pUnit;
	}
	inline VVOID SetMaterial(const Material& pMaterial)
	{
		this->mMaterial = pMaterial;
	}
	inline VVOID SetWorld(const Matrix4x4& pWorld)
	{
		this->mWorld = pWorld;
	}
	inline VVOID SetLineAxis(const Vector3& pAxis)
	{
		this->mLineAxis = pAxis;
	}
	inline VVOID SetSpeedFactor(const FFLOAT& pSpeedFactor)
	{
		this->mSpeed = pSpeedFactor;
	}
	inline VVOID SetAmplitude(const FFLOAT& pAmplitude)
	{
		this->mAmplitude = pAmplitude;
	}
	inline VVOID SetOndulate(const BBOOL& pOndulate)
	{
		this->mOndulate = pOndulate;
	}
};