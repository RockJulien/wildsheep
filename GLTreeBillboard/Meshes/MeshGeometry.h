#ifndef DEF_MESHGEOMETRY_H
#define DEF_MESHGEOMETRY_H

// Includes
#include <vector>
#include "Subset.h"
#include "..\GDevice.h"
#include "..\VertexTypes.h"
#include "..\Effects\InputLayouts.h"

// Class definition
template<typename VertexType>
class MeshGeometry
{
private:

	// Attributes
	std::vector<Subset> mSubsetTable;
	GDevice*            mDevice;
	UBIGINT				mVertArrayID;
	UBIGINT				mVertBuffID;
	UBIGINT				mIndBuffID;
	UBIGINT				mVertCount;
	UBIGINT				mIndCount;

	UBIGINT             mVertexStride;

	// Private Methods
	MeshGeometry(const MeshGeometry& pToCopy);
	MeshGeometry& operator = (const MeshGeometry& pToAssmt);

public:

	// Constructor
	MeshGeometry( GDevice* pDevice );
	~MeshGeometry();

	// Methods
	VVOID SetGeometry(const VertexType* pVertices, UBIGINT pVertCount, const UBIGINT* pIndices, UBIGINT pIndCount, InputLayout* pLayout);
	VVOID SetSubsetTable(std::vector<Subset>& pSubsetTable);
	VVOID Render(UBIGINT pSubsetId);
};

/*************************************************************************************************/
template<typename VertexType>
MeshGeometry<VertexType>::MeshGeometry( GDevice* pDevice ) :
mVertArrayID(0), mVertBuffID(0),
mIndBuffID(0), mVertCount(0), 
mIndCount(0), mVertexStride(0),
mDevice(pDevice)
{
	
}

/*************************************************************************************************/
template<typename VertexType>
MeshGeometry<VertexType>::~MeshGeometry()
{
	// Release the vertex buffer.
	this->mDevice->glBindBuffer(GL_ARRAY_BUFFER, 0);
	this->mDevice->glDeleteBuffers(1, &this->mVertBuffID);

	// Release the index buffer.
	this->mDevice->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	this->mDevice->glDeleteBuffers(1, &this->mIndBuffID);

	// Release the vertex array object.
	this->mDevice->glBindVertArray(0);
	this->mDevice->glDeleteVertArrays(1, &this->mVertArrayID);
}

/**********************************************************************************************/
template<typename VertexType>
VVOID MeshGeometry<VertexType>::SetGeometry(const VertexType* pVertices, UBIGINT pVertCount, const UBIGINT* pIndices, UBIGINT pIndCount, InputLayout* pLayout)
{
	if
		( pVertices == NULL || pIndices == NULL )
	{
		return;
	}

	// Allocate an OpenGL vertex array object.
	this->mDevice->glGenVertArrays( 1, &this->mVertArrayID );

	this->mVertexStride = sizeof(VertexType);
	this->mVertCount    = pVertCount;

	// Bind the vertex array object to store all the buffers and vertex attributes we create here.
	this->mDevice->glBindVertArray( this->mVertArrayID );

	// (Re)Create the vertex buffer
	// Generate an ID for the vertex buffer.
	this->mDevice->glGenBuffers( 1, &this->mVertBuffID );

	// Bind the vertex buffer and load the vertex (position, texture, and normal) data into the vertex buffer.
	this->mDevice->glBindBuffer( GL_ARRAY_BUFFER, this->mVertBuffID );
	this->mDevice->glBufferData( GL_ARRAY_BUFFER, this->mVertCount * this->mVertexStride, pVertices, GL_STATIC_DRAW );

	GLenum lError = glGetError();
	if
		( lError != GL_NO_ERROR )
	{
		throw exception(" Mesh Geometry Set Vertices Failure !!! ");
	}

	// Set the layout.
	pLayout->SetVertexLayout( this->mVertBuffID );

	// Store useful data
	this->mIndCount = pIndCount;

	// Generate an ID for the index buffer.
	this->mDevice->glGenBuffers( 1, &this->mIndBuffID );

	// Bind the index buffer and load the index data into it.
	this->mDevice->glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, this->mIndBuffID );
	this->mDevice->glBufferData( GL_ELEMENT_ARRAY_BUFFER, this->mIndCount * sizeof(UBIGINT), pIndices, GL_STATIC_DRAW );

	lError = glGetError();
	if
		( lError != GL_NO_ERROR )
	{
		throw exception(" Mesh Geometry Set Indices Failure !!! ");
	}

	// Unbind the VAO
	this->mDevice->glBindVertArray( 0 );
}

/*************************************************************************************************/
template<typename VertexType>
VVOID MeshGeometry<VertexType>::SetSubsetTable(std::vector<Subset>& pSubsetTable)
{
	this->mSubsetTable = pSubsetTable;
}

/*************************************************************************************************/
template<typename VertexType>
VVOID MeshGeometry<VertexType>::Render(UBIGINT pSubsetId)
{
	// Bind the vertex array object containing the geometry info.
	this->mDevice->glBindVertArray( this->mVertArrayID );

	GLenum lError = glGetError();
	if
		( lError != GL_NO_ERROR )
	{
		throw exception(" Mesh Before Rendering FAILURE !!! ");
	}

	// Draw geometry.
	UBIGINT lStartIndex   = this->mSubsetTable[pSubsetId].FaceStart() * 3;
	GLsizei lCountIndices = (BIGINT)this->mSubsetTable[pSubsetId].FaceCount() * 3;
	/*this->mDevice->glDrawElementsBaseVertex( GL_TRIANGLES, 
											 lCountIndices, 
											 GL_UNSIGNED_INT, 
											 (VVOID*)(lStartIndex * sizeof(UBIGINT)), 
											 0 );*/
	this->mDevice->glDrawRangeElements( GL_TRIANGLES, lStartIndex, lStartIndex + lCountIndices, lCountIndices, GL_UNSIGNED_INT, NULL );

	GLenum lStatus = this->mDevice->glCheckFrameBufferStatus( GL_FRAMEBUFFER );
	if( lStatus != GL_FRAMEBUFFER_COMPLETE )
	{
		throw exception(" Frame buffer failure !!!");
	}

	// Unbind the VAO.
	this->mDevice->glBindVertArray( 0 );
}

#endif