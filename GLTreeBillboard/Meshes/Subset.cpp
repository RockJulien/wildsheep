#include "Subset.h"

// Namespaces used

/*********************************************************************************************/
Subset::Subset() :
mId(0), mVertexStart(0), 
mVertexCount(0), mFaceStart(0), 
mFaceCount(0)
{

}

/*********************************************************************************************/
Subset::Subset(const UBIGINT& pId, const UBIGINT& pVertexStart, const UBIGINT& pVertexCount, const UBIGINT& pFaceStart, const UBIGINT& pFaceCount) :
mId(pId), mVertexStart(pVertexStart), 
mVertexCount(pVertexCount), mFaceStart(pFaceStart), 
mFaceCount(pFaceCount)
{

}

/*********************************************************************************************/
Subset::~Subset()
{

}

/*********************************************************************************************/
UBIGINT Subset::Id() const
{
	return this->mId;
}

/*********************************************************************************************/
UBIGINT Subset::VertexStart() const
{
	return this->mVertexStart;
}

/*********************************************************************************************/
UBIGINT Subset::VertexCount() const
{
	return this->mVertexCount;
}

/*********************************************************************************************/
UBIGINT Subset::FaceStart() const
{
	return this->mFaceStart;
}

/*********************************************************************************************/
UBIGINT Subset::FaceCount() const
{
	return this->mFaceCount;
}

/*********************************************************************************************/
VVOID   Subset::SetId(const UBIGINT& pId)
{
	this->mId = pId;
}

/*********************************************************************************************/
VVOID   Subset::SetVertexStart(const UBIGINT& pVertexStart)
{
	this->mVertexStart = pVertexStart;
}

/*********************************************************************************************/
VVOID   Subset::SetVertexCount(const UBIGINT& pVertexCount)
{
	this->mVertexCount = pVertexCount;
}

/*********************************************************************************************/
VVOID   Subset::SetFaceStart(const UBIGINT& pFaceStart)
{
	this->mFaceStart = pFaceStart;
}

/*********************************************************************************************/
VVOID   Subset::SetFaceCount(const UBIGINT& pFaceCount)
{
	this->mFaceCount = pFaceCount;
}
