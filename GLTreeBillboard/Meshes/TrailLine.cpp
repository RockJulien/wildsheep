#include "TrailLine.h"
#include "..\Maths\Vector3.h"
#include "..\Maths\Vector4.h"
#include "..\GDevice.h"
#include "..\TextureManager\TextureManager.h"
#include "..\VertexTypes.h"
#include "..\Effects\InputLayouts.h"
#include "..\Effects\Effects.h"
#include "..\Effects\EffectTechnique.h"
#include "..\Meshes\GeometryHelper.h"

#include <iostream>
#include <sstream>

using namespace std;
using namespace Ocelot;

/*********************************************************************************************/
TrailLine::TrailLine(GDevice* pDevice, TextureManager* pTextureManager) :
mDevice(pDevice), mTextureManager( pTextureManager ), mVAO(0), mVBO(0),
mIBO(0), mTextureRV1(0), mTextureRV2(0), mLineCount(1), mLineRadius(2.0f), mTime(0)
{

}

/*********************************************************************************************/
TrailLine::~TrailLine()
{

}

/*********************************************************************************************/
VVOID TrailLine::Initialize(const FFLOAT& pLength, const vector<string>& pTexturePath)
{
	this->mTextureRV1 = this->mTextureManager->CreateTexture( pTexturePath[ 0 ], true );
	if ( pTexturePath.size() > 1 )
	{
		this->mTextureRV2 = this->mTextureManager->CreateTexture( pTexturePath[ 1 ], true );
	}

	GLenum lError = glGetError();
	if ( lError != GL_NO_ERROR )
	{
		throw exception(" Build Trail Geometry Failure !!! ");
	}
	
	const UBIGINT lSubdivideCount = 10;
	const FFLOAT  lChunkLength = pLength / lSubdivideCount;

	MeshData lPolyline;
	GeometryHelper::CreatePolyline( pLength, this->mLineRadius, lSubdivideCount + 1, lPolyline );

	this->mIndexCount = (UBIGINT)lPolyline.Indices.size();

	//
	// Extract the vertex elements we are interested and apply the height function to
	// each vertex.  
	//
	UBIGINT lIndex = 0;
	std::vector<TrailVertex> lVertices( lPolyline.Vertices.size() );
	for( UBIGINT lCurrVert = 0; lCurrVert < (UBIGINT)lPolyline.Vertices.size(); lCurrVert++ )
	{
		Vector3 lPosition = lPolyline.Vertices[ lCurrVert ].Position;
		Vector3 lNormal   = lPolyline.Vertices[ lCurrVert ].Normal;
		Vector2 lCoord    = lPolyline.Vertices[ lCurrVert ].TexCoord;

		if ( lCurrVert == lSubdivideCount )
		{
			lIndex = 0;
		}

		lVertices[ lCurrVert ].Position = lPosition;
		lVertices[ lCurrVert ].Normal   = lNormal;
		lVertices[ lCurrVert ].TexCoordNIndex = Vector3( lCoord.getX(), lCoord.getY(), (FFLOAT)(lCurrVert % (lSubdivideCount + 1)) );

		if ( lCurrVert % 2 )
		{
			lIndex++;
		}
	}

	lError = glGetError();
	if( lError != GL_NO_ERROR )
	{
		throw exception(" Build Land Geometry Failure !!! ");
	}

	// Allocate an OpenGL vertex array object.
	this->mDevice->GenVertArrays( 1, &this->mVAO );

	// Bind the vertex array object to store all the buffers and vertex attributes we create here.
	this->mDevice->BindVertArray( this->mVAO );

	// Generate a vertex buffer object
	this->mDevice->GenBuffers( 1, &this->mVBO );

	// Bind the vertex buffer and load the vertex data into it.
	this->mDevice->BindBuffer( GL_ARRAY_BUFFER, this->mVBO );
	this->mDevice->BufferData( GL_ARRAY_BUFFER, lVertices.size() * sizeof(TrailVertex), &lVertices[0], GL_STATIC_DRAW );

	// Set the layout for the vertex stage
	InputLayouts::TrailLineLayout()->SetVertexLayout( this->mVBO );

	// Generate an Index buffer object
	this->mDevice->GenBuffers( 1, &this->mIBO );

	// Bind the index buffer and load the index data into it.
	this->mDevice->BindBuffer( GL_ELEMENT_ARRAY_BUFFER, this->mIBO );
	this->mDevice->BufferData( GL_ELEMENT_ARRAY_BUFFER, this->mIndexCount * sizeof(UBIGINT), &lPolyline.Indices[0], GL_STATIC_DRAW );

	lError = glGetError();
	if( lError != GL_NO_ERROR )
	{
		throw exception(" Build Land Geometry Failure !!! ");
	}

	this->mDevice->BindVertArray( 0 );
	
}

/*********************************************************************************************/
VVOID TrailLine::Update(const FFLOAT& pDeltaTime)
{
	//
	// Animate the trail texture coordinates.
	//
	this->mTexOffset -= this->mSpeed * pDeltaTime;
	Matrix4x4 lTexCoordOffset;
	lTexCoordOffset.Matrix4x4Translation( lTexCoordOffset, this->mTexOffset, 0.0f, 0.0f );

	this->mTexTransform = lTexCoordOffset;

	this->mTime += pDeltaTime;
}

/*********************************************************************************************/
VVOID TrailLine::Render()
{
	// Draw Line.
	if ( Effects::TrailLineFX() != NULL )
	{
		// Make the trail line technique current
		Effects::TrailLineFX()->MakeCurrent( this->mTechnique );

		UBIGINT lError = glGetError();
		if ( lError != GL_NO_ERROR )
		{
			throw exception(" Draw line Failure !!! ");
		}

		OcelotCameraInfo lInfo = this->mCamera->Info();
		Matrix4x4 lView = this->mCamera->View();
		Matrix4x4 lProj = this->mCamera->Proj();
		Matrix4x4 lViewProj = lView * lProj;

		// TO DO: Pass a single texture array.

		// Active the first texture unit
		this->mDevice->ActiveTexture( GL_TEXTURE0 + this->mTextureUnit );
		// Set it to the texture unit.
		glBindTexture( GL_TEXTURE_2D, this->mTextureRV1 );

		// Active the second texture unit
		this->mDevice->ActiveTexture( GL_TEXTURE0 + this->mTextureUnit + 1 );
		// Set it to the texture unit.
		glBindTexture( GL_TEXTURE_2D, this->mTextureRV2 );

		lError = glGetError();
		if ( lError != GL_NO_ERROR )
		{
			throw exception(" Draw line Failure !!! ");
		}

		Matrix4x4 lWorldViewProj = this->mWorld * lViewProj;
		
		Effects::TrailLineFX()->SetWorld( this->mWorld );
		Effects::TrailLineFX()->SetWorldViewProj( lWorldViewProj );
		Effects::TrailLineFX()->SetTexTransform( this->mTexTransform );
		Effects::TrailLineFX()->SetMaterial( this->mMaterial );
		Effects::TrailLineFX()->SetEyePosW( lInfo.Eye() );
		Effects::TrailLineFX()->SetLineAxis( this->mLineAxis );
		Effects::TrailLineFX()->SetTime( this->mTime );
		Effects::TrailLineFX()->SetOndulate( this->mOndulate );
		Effects::TrailLineFX()->SetAmplitude( this->mAmplitude );
		Effects::TrailLineFX()->SetMask1( this->mTextureUnit );
		Effects::TrailLineFX()->SetMask2( this->mTextureUnit + 1 );

		this->mDevice->BindVertArray( this->mVAO );

		glDisable(GL_CULL_FACE);

		glDrawElements( GL_TRIANGLES, this->mIndexCount, GL_UNSIGNED_INT, NULL );

		glEnable(GL_CULL_FACE);

		// Unbind Land VAO
		this->mDevice->BindVertArray( 0 );

		lError = glGetError();
		if ( lError != GL_NO_ERROR )
		{
			throw exception(" Draw line Failure !!! ");
		}
	}
}

/*********************************************************************************************/
VVOID TrailLine::CleanUp()
{
	glDeleteTextures( 1, &this->mTextureRV1 );
	glDeleteTextures( 1, &this->mTextureRV2 );

	this->mDevice->DeleteBuffers( 1, &this->mVBO );
	this->mDevice->DeleteBuffers( 1, &this->mIBO );
	this->mDevice->DeleteVertArrays( 1, &this->mVAO );

	this->mDevice = NULL;
	this->mCamera = NULL;
	this->mTechnique = NULL;
}