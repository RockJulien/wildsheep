#include "SkinnedData.h"
#include <string.h>
#include <string>
#include <algorithm>

// Namespaces used
using namespace std;

/******************************************************************************************************/
SkinnedData::SkinnedData()
{

}

/******************************************************************************************************/
SkinnedData::~SkinnedData()
{

}

/******************************************************************************************************/
VVOID SkinnedData::Initialize(std::vector<int>& pBoneHierarchy, std::vector<Matrix4x4>& pBoneOffsets, std::map<std::string, AnimationClip>& pAnimations)
{
	this->mBoneHierarchy = pBoneHierarchy;
	this->mBoneOffsets   = pBoneOffsets;
	this->mAnimations    = pAnimations;
}

/******************************************************************************************************/
VVOID SkinnedData::ComputeFinalTransforms(const std::string& pClipName, float pTimePos, std::vector<Matrix4x4>& pFinalTransforms)
{
	UBIGINT lNumBones = (UBIGINT)this->mBoneOffsets.size();

	vector<Matrix4x4> lToParentTransforms(lNumBones);

	// Interpolate all the bones of this Clip at the given time instance.
	map<string, AnimationClip>::const_iterator lClip = this->mAnimations.find(pClipName);
	lClip->second.Interpolate(pTimePos, lToParentTransforms);

	//
	// Traverse the hierarchy and transform all the bones to the root space.
	//
	vector<Matrix4x4> lToRootTransforms(lNumBones);

	// The root bone has index 0.  The root bone has no parent, so its toRootTransform
	// is just its local bone transform.
	lToRootTransforms[0] = lToParentTransforms[0];

	// Now find the toRootTransform of the children.
	for(UBIGINT lCurr = 1; lCurr < lNumBones; lCurr++)
	{
		Matrix4x4 lToParent     = lToParentTransforms[lCurr];
		BIGINT lParentIndex     = this->mBoneHierarchy[lCurr];
		Matrix4x4 lParentToRoot = lToRootTransforms[lParentIndex];

		Matrix4x4 lToRoot;
		lToParent.Matrix4x4Mutliply(lToRoot, lParentToRoot);

		lToRootTransforms[lCurr] = lToRoot;
	}

	// Premultiply by the bone offset transform to get the final transform.
	for(UBIGINT lCurr = 0; lCurr < lNumBones; lCurr++)
	{
		Matrix4x4 lOffset = this->mBoneOffsets[lCurr];
		Matrix4x4 lToRoot = lToRootTransforms[lCurr];
		Matrix4x4 lResult;
		lOffset.Matrix4x4Mutliply(lResult, lToRoot);
		pFinalTransforms[lCurr] = lResult;
	}
}

/******************************************************************************************************/
UBIGINT	SkinnedData::BoneCount() const
{
	return (UBIGINT)this->mBoneHierarchy.size();
}

/******************************************************************************************************/
FFLOAT	SkinnedData::ClipStartTime(const string& pClipName) const
{
	std::map<string, AnimationClip>::const_iterator lClip = this->mAnimations.find(pClipName);
	return lClip->second.ClipStartTime();
}

/******************************************************************************************************/
FFLOAT	SkinnedData::ClipEndTime(const string& pClipName) const
{
	std::map<string, AnimationClip>::const_iterator lClip = this->mAnimations.find(pClipName);
	return lClip->second.ClipEndTime();
}
