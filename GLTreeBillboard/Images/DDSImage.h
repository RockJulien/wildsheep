#ifndef DEF_DDSIMAGE_H
#define DEF_DDSIMAGE_H

// Includes
#include "..\OSCheck.h"

// Namespaces


// Class definition
class DDSImage
{
public:

	enum Max
	{
		MAX_MIPMAP_LEVEL = 16 // For 65536x65536 images (2^16)
	};

private:

	UCCHAR* mPixels[ MAX_MIPMAP_LEVEL ]; // Texture data
	BIGINT  mBytes[MAX_MIPMAP_LEVEL];

	// Info extracted from file
	BIGINT mWidth;
	BIGINT mHeight;
	BIGINT mMipmapLevels;
	BIGINT mFormat;
	BIGINT mInternalFormat;
	BIGINT mCompressionType;
	BIGINT mBlockSize; // Used in passing compressed textures to OpenGL

public:

	// Constructor & Destructor
	DDSImage();
	~DDSImage();

	// Methods


	// Accessors
	inline BIGINT GetWidth(){ return mWidth; }
	inline BIGINT GetHeight(){ return mHeight; }
	inline BIGINT GetMipMapLevels(){ return mMipmapLevels; }
	inline BIGINT GetFormat(){ return mFormat; }
	inline BIGINT GetInternalFormat(){ return mInternalFormat; }
	inline BIGINT GetCompressionType(){ return mCompressionType; }
	inline BIGINT GetBlockSize(){ return mBlockSize; }
	inline UCCHAR* GetPixels(BIGINT pLevel){ return mPixels[pLevel]; }
	inline BIGINT  GetBytes(BIGINT pLevel){ return mBytes[pLevel]; }
	inline VVOID  SetWidth(const BIGINT& pWidth) { mWidth = pWidth; }
	inline VVOID  SetHeight(const BIGINT& pHeight) { mHeight = pHeight; }
	inline VVOID  SetMipMapLevels(const BIGINT& pMipMapLevels) { mMipmapLevels = pMipMapLevels; }
	inline VVOID  SetFormat(const BIGINT& pFormat) { mFormat = pFormat; }
	inline VVOID  SetInternalFormat(const BIGINT& pInternalFormat) { mInternalFormat = pInternalFormat; }
	inline VVOID  SetCompressionType(const BIGINT& pCompressionType) { mCompressionType = pCompressionType; }
	inline VVOID  SetBlockSize(const BIGINT& pBlockSize) { mBlockSize = pBlockSize; }
	inline VVOID  SetPixels(UCCHAR* pPixels, BIGINT pLevel){ mPixels[pLevel] = pPixels; }
	inline VVOID  SetBytes(BIGINT pBytes, BIGINT pLevel){ mBytes[pLevel] = pBytes; }
};

#endif