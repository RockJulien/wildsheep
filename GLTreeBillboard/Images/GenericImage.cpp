#include "GenericImage.h"

// Namespaces used

/***************************************************************************************/
GenericImage::GenericImage() :
mPixels(NULL), mMap(NULL), mWidth(0), mHeight(0), 
mMapEntries(0), mFormat(0), mMapFormat(0), 
mComponents(-1)
{

}

/***************************************************************************************/
GenericImage::~GenericImage()
{

}

/***************************************************************************************/
VVOID GenericImage::Release()
{
	DeletePointer( this->mPixels );
	DeletePointer( this->mMap );
}
