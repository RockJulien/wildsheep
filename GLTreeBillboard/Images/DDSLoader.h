#ifndef DEF_DDSLOADER_H
#define DEF_DDSLOADER_H

// Includes
#include "DDSImage.h"

// Class definition
class DDSLoader
{
public:

	// Constructor & Destructor
	DDSLoader();
	~DDSLoader();

	// Methods
	DDSImage* LoadDDS(const CCHAR* pName);

};

#endif