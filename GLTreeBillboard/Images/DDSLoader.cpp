#include "DDSLoader.h"
#include "..\GDevice.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "..\TextureEXType.h"

#define FOURCC_DXT1	MakeFourCC('D', 'X', 'T', '1')
#define FOURCC_DXT3	MakeFourCC('D', 'X', 'T', '3')
#define FOURCC_DXT5	MakeFourCC('D', 'X', 'T', '5')

// Magic
#define MAGIC_DDS 0x20534444 // DDS<space>

// Namespaces used
using namespace std;

// Utilities
static VVOID ConvertARGB2RGBA(UCCHAR* pArray, BIGINT pCount)
{
   // In hex dump: BGRA -> RGBA
   UCCHAR lTemp;
   pCount /= 4;
   for( BIGINT lCurr = 0; lCurr < pCount; lCurr++ )
   {
	   lTemp	 = pArray[2];
	   pArray[2] = pArray[0];
	   pArray[0] = lTemp;
	   pArray += 4;
   }
}

// A DXT1 block layout is:
// [0-1] color0.
// [2-3] color1.
// [4-7] color bitmap, 2 bits per pixel.
// So each of the 4-7 bytes represents one line, flipping a block is just
// flipping those bytes.
// Note that http://src.chromium.org/viewvc/chrome/trunk/src/o3d/core/cross/bitmap_dds.cc?view=markup&pathrev=21227
// contains an error in the last line: data[6]=data[5] is a bug!
static VVOID FlipDXT1BlockFull( UCCHAR* pData )
{
   UCCHAR lTemp;

   lTemp    = pData[4];
   pData[4] = pData[7];
   pData[7] = lTemp;

   lTemp    = pData[5];
   pData[5] = pData[6];
   pData[6] = lTemp;
}

// Flips a full DXT3 block in the y direction.
static VVOID FlipDXT3BlockFull( UCCHAR* pBlock )
{
   // A DXT3 block layout is:
   // [0-7]  alpha bitmap, 4 bits per pixel.
   // [8-15] a DXT1 block.

   // We can flip the alpha bits at the byte level (2 bytes per line).
   UCCHAR lTemp = pBlock[0];
   pBlock[0] = pBlock[6];
   pBlock[6] = lTemp;
   lTemp     = pBlock[1];
   pBlock[1] = pBlock[7];
   pBlock[7] = lTemp;
   lTemp     = pBlock[2];
   pBlock[2] = pBlock[4];
   pBlock[4] = lTemp;
   lTemp     = pBlock[3];
   pBlock[3] = pBlock[5];
   pBlock[5] = lTemp;

   // And flip the DXT1 block using the above function.
   FlipDXT1BlockFull( pBlock + 8 );
}

// 
static VVOID QBitDump( UCCHAR* pArray, BIGINT pCount )
{
	BIGINT lBits;
	CCHAR lBuffer[1024],*d;

	//qdbg("Bitdump at %p",a);
	lBits = 0;
	d = lBuffer;

	for( BIGINT i = 0; i < pCount; i++ )
	{
		for( BIGINT j = 0; j < 8; j++ )
		{
			if( pArray[ i ] & ( 1 << j ) )
			{
				*d++='1';
			}
			else
			{
				*d++='0';
			}

			if( ( lBits % 12 ) == 11 )
			{
				*d++='.';
			}

			lBits++;
		}
	}
   *d = 0;

   //qdbg("%s\n",buf);
}

// From http://src.chromium.org/viewvc/chrome/trunk/src/o3d/core/cross/bitmap_dds.cc?view=markup&pathrev=21227
// Original source contained bugs; fixed here.
static VVOID FlipDXT5BlockFull( UCCHAR* pBlock )
{
	// A DXT5 block layout is:
	// [0]    alpha0.
	// [1]    alpha1.
	// [2-7]  alpha bitmap, 3 bits per pixel.
	// [8-15] a DXT1 block.

	// The alpha bitmap doesn't easily map lines to bytes, so we have to
	// interpret it correctly.  Extracted from
	// http://www.opengl.org/registry/specs/EXT/texture_compression_s3tc.txt :
	//
	//   The 6 "bits" bytes of the block are decoded into one 48-bit integer:
	//
	//     bits = bits_0 + 256 * (bits_1 + 256 * (bits_2 + 256 * (bits_3 +
	//                   256 * (bits_4 + 256 * bits_5))))
	//
	//   bits is a 48-bit unsigned integer, from which a three-bit control code
	//   is extracted for a texel at location (x,y) in the block using:
	//
	//       code(x,y) = bits[3*(4*y+x)+1..3*(4*y+x)+0]
	//
	//   where bit 47 is the most significant and bit 0 is the least
	//   significant bit.
	//QBitDump(block+2,6);

	// From Chromium (source was buggy)
	UBIGINT line_0_1 = pBlock[2] + 256 * ( pBlock[3] + 256 * pBlock[4] );
	UBIGINT line_2_3 = pBlock[5] + 256 * ( pBlock[6] + 256 * pBlock[7] );
	// swap lines 0 and 1 in line_0_1.
	UBIGINT line_1_0 = (( line_0_1 & 0x000fff ) << 12) | (( line_0_1 & 0xfff000 ) >> 12);
	// swap lines 2 and 3 in line_2_3.
	UBIGINT line_3_2 = (( line_2_3 & 0x000fff ) << 12) | (( line_2_3 & 0xfff000 ) >> 12);
	pBlock[2] = line_3_2 & 0xff;
	pBlock[3] = ( line_3_2 & 0xff00 ) >> 8;
	pBlock[4] = ( line_3_2 & 0xff0000 ) >> 16;
	pBlock[5] = line_1_0 & 0xff;
	pBlock[6] = ( line_1_0 & 0xff00 ) >> 8;
	pBlock[7] = ( line_1_0 & 0xff0000 ) >> 16;

	// And flip the DXT1 block using the above function.
	FlipDXT1BlockFull( pBlock + 8 );
}

/***************************************************************************************************/
DDSLoader::DDSLoader()
{

}

/***************************************************************************************************/
DDSLoader::~DDSLoader()
{

}

/***************************************************************************************************/
DDSImage* DDSLoader::LoadDDS(const CCHAR* pName)
{
    FILE* lFile = NULL;
	BIGINT lMagic;
	UCCHAR* lTemp = NULL;
	DDSImage* lImage = new DDSImage();
 
    /* try to open the file */
    fopen_s(&lFile, pName, "rb");
    if ( lFile == NULL )
	{
        return NULL;
	}
 
    /* verify the type of file */
	fread( &lMagic, 1, sizeof(lMagic), lFile );

	if( lMagic != MAGIC_DDS ) 
	{
		goto FAILURE;
	}
	else
	{
		// Direct3D 9 format
		D3D_SurfaceDesc2 lHeader;
		fread( &lHeader , 1, sizeof(lHeader), lFile );
		
		// Remember info for users of this object
		lImage->SetWidth( lHeader.dwWidth );
		lImage->SetHeight( lHeader.dwHeight );
		lImage->SetMipMapLevels( lHeader.dwMipMapCount == 0 ? 1 : lHeader.dwMipMapCount ); // Insure if no mipmaps, the process is done once at least.

		BIGINT lByteCount;
		// Number of pixels
		lByteCount = lHeader.dwHeight * lHeader.dwWidth;
		// Block size default
		lImage->SetBlockSize( 16 );

		// DXT5?
		if
			( lHeader.ddpfPixelFormat.dwFlags & DDPF_FOURCC )
		{
			// Compressed
			lImage->SetFormat( DDS_FORMAT_YCOCG );      // ???????? Not right
			UBIGINT lFourCC;
			lFourCC = lHeader.ddpfPixelFormat.dwFourCC;
			if
				( lFourCC == FOURCC_DXT1 )
			{
				lImage->SetBlockSize( 8 );
				lImage->SetInternalFormat( GL_COMPRESSED_RGBA_S3TC_DXT1_EXT );
			} 
			else if
				( lFourCC == FOURCC_DXT3 )
			{
				lImage->SetInternalFormat( GL_COMPRESSED_RGBA_S3TC_DXT3_EXT );
			} 
			else if
				( lFourCC == FOURCC_DXT5 )
			{
				// DXT5
				lImage->SetInternalFormat( GL_COMPRESSED_RGBA_S3TC_DXT5_EXT );
			} 
			else
			{
				CCHAR lBuffer[5];
				lBuffer[0] = lFourCC & 255;
				lBuffer[1] = (lFourCC >> 8) & 255;
				lBuffer[2] = (lFourCC >> 16) & 255;
				lBuffer[3] = (lFourCC >> 24) & 255;
				lBuffer[4] = 0;
				printf_s("DDDS:Load(%s); unknown compressed format (%s)", pName, lBuffer );
				goto FAILURE;
			}

			// DXT5 compression
			//bytes/=4;
		} 
		else if
			( lHeader.ddpfPixelFormat.dwRGBBitCount == 32 )
		{
			lImage->SetFormat( DDS_FORMAT_RGBA8 );
			// Calculate bytes for highest mipmap level
			lByteCount = lByteCount * lHeader.ddpfPixelFormat.dwRGBBitCount / 8;
		} 
		else //if( lHeader.ddpfPixelFormat.dwRGBBitCount!=32)
		{
			printf_s( "DDS:Load(%s); unknown DDS format (rgb bitcount not 32, not DXT5)", pName );
			goto FAILURE;
		}

		// Read all mipmap levels
		BIGINT lWidth  = lImage->GetWidth();
		BIGINT lHeight = lImage->GetHeight();
		BIGINT lMipMapCount = lImage->GetMipMapLevels();

		for( BIGINT lCurrLevel = 0; 
					lCurrLevel < lMipMapCount && lCurrLevel < DDSImage::MAX_MIPMAP_LEVEL; 
					lCurrLevel++ )
		{
			// Deduce # of bytes
			// Close to the higher mipmap levels, wid or hgt may become 0; keep things at 1
			if( lWidth == 0 )
			{
				lWidth = 1;
			}

			if( lHeight == 0 )
			{
				lHeight = 1;
			}

			if
				( lImage->GetFormat() == DDS_FORMAT_RGBA8 )
			{
				// RGBA8
				lByteCount = lWidth * lHeight * 4;
			} 
			else
			{
				// DXTx
				lByteCount = ((lWidth + 3) / 4) * ((lHeight + 3) / 4) * lImage->GetBlockSize();
				if
					( lCurrLevel == 0 )
				{
					// Create temp buffer to flip DDS
					lTemp = new UCCHAR[ lByteCount ];
				}
			}

			lImage->SetBytes( lByteCount, lCurrLevel );
			lImage->SetPixels( new UCCHAR[ lByteCount ], lCurrLevel );
			if
				( lImage->GetPixels(lCurrLevel) == NULL )
			{
				printf_s( "DDS:Load(%s); out of memory for mipmap level %d", pName, lCurrLevel );
				goto FAILURE;
			}

			if
				( lImage->GetFormat() != DDS_FORMAT_RGBA8 )
			{
				// First read in temp buffer
				fread( lTemp, 1, lByteCount, lFile );

				// Flip & copy to actual pixel buffer
				BIGINT widBytes;
				UCCHAR* s,* d;
				widBytes = ( ( lWidth + 3 ) / 4 ) * lImage->GetBlockSize();
				s = lTemp;
				d = lImage->GetPixels( lCurrLevel ) + ( ( lHeight + 3 ) / 4 - 1 ) * widBytes;
				
				for( BIGINT j = 0; j < (lHeight + 3) / 4; j++ )
				{
					memcpy( d, s, widBytes );
					if( lImage->GetBlockSize() == 8 )
					{
						for( BIGINT lK = 0; lK < widBytes / lImage->GetBlockSize(); lK++ )
						{
							FlipDXT1BlockFull( d + lK * lImage->GetBlockSize() );
						}
					} 
					else
					{
						// DXT3, DXT5
						if( lImage->GetCompressionType() == DDS_COMPRESS_BC2)
						{
							// DXT3
							for( BIGINT lK = 0; lK < widBytes / lImage->GetBlockSize(); lK++ )
							{
								FlipDXT3BlockFull( (UCCHAR*)d + lK * lImage->GetBlockSize() );
							}
						} 
						else
						{
							// DXT5
							for( BIGINT lK = 0; lK < widBytes / lImage->GetBlockSize(); lK++ )
							{
								FlipDXT5BlockFull( (UCCHAR*)d + lK * lImage->GetBlockSize() );
							}
						}
					}

					s += widBytes;
					d -= widBytes;
				}
			} 
			else
			{
				// RGBA8
				if
					( lImage->GetFormat() == DDS_FORMAT_RGBA8 )
				{
					ConvertARGB2RGBA( lImage->GetPixels( lCurrLevel ), lByteCount );
				}
			}

			// Next level is smaller
			lWidth /= 2;
			lHeight /= 2;
		}

		// Release temp buffer
		DeletePointer( lTemp );
	}

	fclose( lFile );
	return lImage;

	FAILURE:
	fclose( lFile );
	DeletePointer( lImage );
	return NULL;
}
