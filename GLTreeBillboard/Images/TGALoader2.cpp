#include "TGALoader.h"
#include "../GDevice.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Namespaces used
using namespace std;

// Static variables
static CCHAR   sError[256];
static UBIGINT sVerbose  = 0;
static BIGINT  sTotbytes = 0;

#define MIN( a, b ) ((( a ) < ( b )) ? ( a ) : ( b ))

#define RLE_PACKETSIZE 0x80

// fixed header values for the subset of TGA we use for writing
UCCHAR TGAHeaderColor[12] = 
{ 
	0,     // 0 ID length = no id
	0,     // 1 color map type = no color map
	2,     // 2 image type = uncompressed true color
	0, 0, 0, 0, 0,// color map spec = empty
	0, 0,  // x origin of image 
	0, 0   // y origin of image
};

UCCHAR TGAHeaderBW[12] = 
{ 
	0,       // 0 ID length = no id
	0,       // 1 color map type = no color map
	3,       // 3 image type = uncompressed black and white
	0, 0, 0, 0, 0,// color map spec = empty
	0, 0,    // x origin of image 
	0, 0     // y origin of image
};

// this makes sure that 
// image size is written in correct format 
// and byte order (least first)
VVOID write16bit(BIGINT pCount, FILE* pFile) 
{ 
	UCCHAR lBytes[] = { pCount % 256, pCount / 256 };
	fwrite( lBytes, 2, sizeof(UCCHAR), pFile );
}

/************************************************************************************/
BIGINT STDRead(RLEState* pRLEInfo, UCCHAR* pBuffer, BIGINT pDataSize, BIGINT pNelems, FILE* pFile)
{
	if 
		( sVerbose > 1 ) 
	{
		sTotbytes += pNelems * pDataSize;
		printf( "TGA: standard fread %d (total %d)\n", pNelems * pDataSize, sTotbytes );
	}

	return (BIGINT)fread( pBuffer, pDataSize, pNelems, pFile );
}

/************************************************************************************/
BIGINT RLERead(RLEState* pRLEInfo, UCCHAR* pBuffer, BIGINT pDataSize, BIGINT pNelems, FILE* pFile)
{
	UCCHAR* lBuffer = pBuffer;
	BIGINT j, k;
	BIGINT lBufLen;
	BIGINT lCount;
	BIGINT lBytes;
	BIGINT lCurrbytes;
	UCCHAR *p;

	/* Scale the buffer length. */
	lBufLen = pNelems * pDataSize;

	j = 0;
	lCurrbytes = sTotbytes;
	while 
		( j < lBufLen ) 
	{
		if 
			( pRLEInfo->LastState < pRLEInfo->StateLen ) 
		{
			/* Copy bytes from our previously decoded buffer. */
			lBytes = MIN( lBufLen - j, pRLEInfo->StateLen - pRLEInfo->LastState );
			memcpy( lBuffer + j, pRLEInfo->StateBuf + pRLEInfo->LastState, lBytes );
			j += lBytes;
			pRLEInfo->LastState += lBytes;

			/* If we used up all of our state bytes, then reset them. */
			if 
				( pRLEInfo->LastState >= pRLEInfo->StateLen ) 
			{
				pRLEInfo->LastState = 0;
				pRLEInfo->StateLen  = 0;
			}

			/* If we filled the buffer, then exit the loop. */
			if ( j >= lBufLen ) 
			{
				break;
			}
		}

		/* Decode the next packet. */
		lCount = fgetc( pFile );
		if 
			( lCount == EOF ) 
		{
			if ( sVerbose ) 
			{
				printf( "TGA: hit EOF while looking for count\n" );
			}

			return j / pDataSize;
		}

		/* Scale the byte length to the size of the data. */
		lBytes = ((lCount & ~RLE_PACKETSIZE) + 1) * pDataSize;

		if 
			( j + lBytes <= lBufLen ) 
		{
			/* We can copy directly into the image buffer. */
			p = lBuffer + j;
		} 
		else 
		{
	#ifdef PROFILE
			printf("TGA: needed to use statebuf for %d bytes\n", lBufLen - j);
	#endif
			/* Allocate the state buffer if we haven't already. */
			if 
				( pRLEInfo->StateBuf == NULL ) 
			{
				pRLEInfo->StateBuf = (UCCHAR *)malloc( RLE_PACKETSIZE * pDataSize );
			}

			p = pRLEInfo->StateBuf;
		}

		if 
			( lCount & RLE_PACKETSIZE ) 
		{
			/* Fill the buffer with the next value. */
			if 
				( fread( p, pDataSize, 1, pFile ) != 1 ) 
			{
				if 
					( sVerbose ) 
				{
					printf( "TGA: EOF while reading %d/%d element RLE packet\n", lBytes, pDataSize );
				}

				return j / pDataSize;
			}

			/* Optimized case for single-byte encoded data. */
			if ( pDataSize == 1 ) 
			{
				memset( p + 1, *p, lBytes - 1 );
			} 
			else 
			{
				for ( k = pDataSize; k < lBytes; k += pDataSize ) 
				{
					memcpy( p + k, p, pDataSize );
				}
			}
		} 
		else 
		{
			/* Read in the buffer. */
			if 
				( fread( p, lBytes, 1, pFile ) != 1 ) 
			{
				if 
					( sVerbose ) 
				{
					printf( "TGA: EOF while reading %d/%d element raw packet\n", lBytes, pDataSize );
				}

				return j / pDataSize;
			}
		}

		if 
			( sVerbose > 1 ) 
		{
			sTotbytes += lBytes;
			if 
				( sVerbose > 2 ) 
			{
				printf( "TGA: %s packet %d/%d\n", (lCount & RLE_PACKETSIZE) ? "RLE" : "raw", lBytes, sTotbytes );
			}
		}

		/* We may need to copy bytes from the state buffer. */
		if 
			( p == pRLEInfo->StateBuf ) 
		{
			pRLEInfo->StateLen = lBytes;
		} 
		else 
		{
			j += lBytes;
		}
	}

	if 
		( sVerbose > 1 ) 
	{
		printf( "TGA: rle_fread %d/%d (total %d)\n", pNelems * pDataSize, sTotbytes - lCurrbytes, sTotbytes );
	}

	return pNelems;
}

/************************************************************************************/
TGALoader::TGALoader()
{

}

/************************************************************************************/
TGALoader::~TGALoader()
{

}

/************************************************************************************/
GenericImage* TGALoader::ReadTGA(const CCHAR* pName)
{
	FILE* lInput = NULL;
	TGAHeader lTgaHeader;
	TGAFooter lTgaFooter;
	CCHAR lHorzrev, lVertrev;
	BIGINT lWidth, lHeight, lBpp;
	BIGINT lStart, lEnd, lDir;
	BIGINT i, j, k;
	BIGINT lPelbytes, lWBytes;
	UBIGINT lFormat;
	BIGINT lComponents;
	RLEState lRleRec;
	RLEState* lRleInfo;
	BIGINT lRle;
	BIGINT lIndex, lColors, lLength;
	UCCHAR* lMap;
	UCCHAR* lPixels;
	UCCHAR* lData;
	BIGINT (*ReadCallback)( RLEState* pRleInfo, UCCHAR* pBuffer, BIGINT pDataSize, BIGINT pNelems, FILE* pFile );
	GenericImage* lImage;

	// Open the texture file (read binary mode).
	BIGINT lError = fopen_s( &lInput, pName, "rb" );
	if
		( lError != 0 )
	{
		return NULL;
	}

	/* Check the footer. */
	if 
		( fseek( lInput, 0L - sizeof(lTgaFooter), SEEK_END ) || fread( &lTgaFooter, sizeof(lTgaFooter), 1, lInput ) != 1 ) 
	{
		sprintf_s( sError, "TGA: Cannot read footer from \"%s\"", pName );
		if 
			( sVerbose ) 
		{
			printf( "%s\n", sError );
		}

		return NULL;
	}  

	/* Check the Signature. */
	if 
		( memcmp( lTgaFooter.Signature, TGA_SIGNATURE, sizeof(lTgaFooter.Signature) ) == 0 ) 
	{
		if 
			( sVerbose ) 
		{
			printf( "TGA: found New TGA\n" );
		}
	} 
	else 
	{
		if 
			( sVerbose ) 
		{
			printf( "TGA: found Original TGA\n" );
		}
	}

	if 
		( fseek( lInput, 0, SEEK_SET ) || fread( &lTgaHeader, sizeof(lTgaHeader), 1, lInput ) != 1 ) 
	{
		sprintf_s( sError, "TGA: Cannot read header from \"%s\"", pName );
		if 
			( sVerbose ) 
		{
			printf( "%s\n", sError );
		}

		return NULL;
	}

	if 
		( sVerbose && lTgaHeader.IdLength ) 
	{
		CCHAR* lIdString = (CCHAR*)malloc( lTgaHeader.IdLength );
    
		if 
			( fread( lIdString, lTgaHeader.IdLength, 1, lInput ) != 1 ) 
		{
			sprintf_s( sError, "TGA: Cannot read ID field in \"%s\"", pName );
			printf( "%s\n", sError );
		} 
		else 
		{
			printf( "TGA: ID field: \"%*s\"\n", lTgaHeader.IdLength, lIdString );
		}

		free( lIdString );
	} 
	else 
	{
		/* Skip the image ID field. */
		if 
			( lTgaHeader.IdLength && fseek( lInput, lTgaHeader.IdLength, SEEK_CUR ) ) 
		{
			sprintf_s( sError, "TGA: Cannot skip ID field in \"%s\"", pName );
			if 
				( sVerbose ) 
			{
				printf("%s\n", sError);
			}

			return NULL;
		}
	}
  
	/* Reassemble the multi-byte values correctly, regardless of
		host endianness. */
	lWidth  = ( lTgaHeader.WidthMSB << 8 ) | lTgaHeader.WidthLSB;
	lHeight = ( lTgaHeader.HeightMSB << 8 ) | lTgaHeader.HeightLBS;
	lBpp    = lTgaHeader.Bpp;
	if 
		( sVerbose ) 
	{
		printf( "TGA: Width=%d, Height=%d, Bpp=%d\n", lWidth, lHeight, lBpp );
	}

	lHorzrev = lTgaHeader.Descriptor & TGA_DESC_HORIZONTAL;
	lVertrev = lTgaHeader.Descriptor & TGA_DESC_VERTICAL;
	if (sVerbose && lHorzrev) printf("TGA: horizontal reversed\n");
	if (sVerbose && lVertrev) printf("TGA: vertical reversed\n");

	lRle = 0;
	switch (lTgaHeader.ImageType) 
	{
	case TGA_TYPE_MAPPED_RLE:
		lRle = 1;
		if 
			( sVerbose ) 
		{
			printf( "TGA: run-lLength encoded\n" );
		}
	case TGA_TYPE_MAPPED:
		/* Test for alpha channel. */
		lFormat = GL_COLOR_INDEX;
		lComponents = 1;
		if 
			( sVerbose ) 
		{
			printf( "TGA: %d bit indexed image (%d bit palette)\n", lTgaHeader.ColorMapSize, lBpp );
		}
	break;
	case TGA_TYPE_GRAY_RLE:
		lRle = 1;
		if 
			( sVerbose ) 
		{
			printf( "TGA: run-lLength encoded\n" );
		}
	case TGA_TYPE_GRAY:
		lFormat = GL_LUMINANCE;
		lComponents = 1;
		if 
			( sVerbose ) 
		{
			printf( "TGA: %d bit grayscale image\n", lBpp );
		}
	break;

	case TGA_TYPE_COLOR_RLE:
		lRle = 1;
		if 
			( sVerbose ) 
		{
			printf( "TGA: run-lLength encoded\n" );
		}
	case TGA_TYPE_COLOR:
		/* Test for alpha channel. */
		if 
			( lBpp == 32 ) 
		{
			lFormat = GL_BGRA_EXT;
			lComponents = 4;
			if 
				( sVerbose ) 
			{
				printf( "TGA: %d bit color image with alpha channel\n", lBpp );
			}
		} 
		else 
		{
			lFormat = GL_BGR_EXT;
			lComponents = 3;
			if 
				( sVerbose ) 
			{
				printf( "TGA: %d bit color image\n", lBpp );
			}
		}
	break;

	default:
		sprintf_s( sError, "TGA: unrecognized image type %d\n", lTgaHeader.ImageType );
		if 
			( sVerbose ) 
		{
			printf( "%s\n", sError );
		}

		return NULL;
	}

	if 
		( (lFormat == GL_BGRA_EXT && lBpp != 32) ||
		  (lFormat == GL_BGR_EXT && lBpp != 24) ||
		  ((lFormat == GL_LUMINANCE || lFormat == GL_COLOR_INDEX) && lBpp != 8) ) 
	{
		/* FIXME: We haven't implemented bit-packed fields yet. */
		sprintf_s( sError, "TGA: channel sizes other than 8 bits are unimplemented" );
		if 
			( sVerbose ) 
		{
			printf( "%s\n", sError );
		}

		return NULL;
	}

	/* Check that we have a color map only when we need it. */
	if 
		( lFormat == GL_COLOR_INDEX ) 
	{
		if 
			( lTgaHeader.ColorMapType != 1) 
		{
			sprintf_s( sError, "TGA: indexed image has invalid color map type %d\n", lTgaHeader.ColorMapType );
			if 
				( sVerbose ) 
			{
				printf( "%s\n", sError );
			}

			return NULL;
		}
	} 
	else if 
		( lTgaHeader.ColorMapType != 0 ) 
	{
		sprintf_s( sError, "TGA: non-indexed image has invalid color map type %d\n", lTgaHeader.ColorMapType );
		if 
			( sVerbose ) 
		{
			printf( "%s\n", sError );
		}

		return NULL;
	}

	if 
		( lTgaHeader.ColorMapType == 1 ) 
	{
		/* We need to read in the colormap. */
		lIndex  = ( lTgaHeader.ColorMapIndexMSB << 8 ) | lTgaHeader.ColorMapIndexLSB;
		lLength = ( lTgaHeader.ColorMapLengthMSB << 8 ) | lTgaHeader.ColorMapLengthLSB;

		if 
			( sVerbose ) 
		{
			printf( "TGA: reading color map (%d + %d) * (%d / 8)\n", lIndex, lLength, lTgaHeader.ColorMapSize );
		}

		if 
			( lLength == 0 ) 
		{
			sprintf_s( sError, "TGA: invalid color map Length %d", lLength );
			if 
				( sVerbose ) 
			{
				printf( "%s\n", sError );
			}

			return NULL;
		}

		if 
			( lTgaHeader.ColorMapSize != 24 ) 
		{
			/* We haven't implemented bit-packed fields yet. */
			sprintf_s( sError, "TGA: channel sizes other than 8 bits are unimplemented" );
			if 
				( sVerbose ) 
			{
				printf( "%s\n", sError );
			}

			return NULL;
		}

		lPelbytes = lTgaHeader.ColorMapSize / 8;
		lColors   = lLength + lIndex;
		lMap      = new UCCHAR[ lColors * lPelbytes ];

		/* Zero the entries up to the beginning of the map. */
		memset( lMap, 0, lIndex * lPelbytes );

		/* Read in the rest of the colormap. */
		if 
			( fread( lMap, lPelbytes, lLength, lInput ) != (size_t)lLength ) 
		{
			sprintf_s( sError, "TGA: sError reading colormap (ftell == %ld)\n", ftell( lInput ) );
			if 
				( sVerbose ) 
			{
				printf( "%s\n", sError );
			}

			return NULL;
		}

		if 
			( lPelbytes >= 3 ) 
		{
			/* Rearrange the lColors from BGR to RGB. */
			BIGINT lTemp;
			for 
				( j = lIndex; j < lLength * lPelbytes; j += lPelbytes ) 
			{
				lTemp   = lMap[ j ];
				lMap[ j ] = lMap[ j + 2 ];
				lMap[ j + 2 ] = lTemp;
			}
		}
	} 
	else 
	{
		lColors = 0;
		lMap = NULL;
	}

	/* Allocate the lData. */
	lPelbytes = lBpp / 8;
	lPixels   = new UCCHAR[ lWidth * lHeight * lPelbytes ];

	if 
		( lRle ) 
	{
		lRleRec.StateBuf  = 0;
		lRleRec.StateLen  = 0;
		lRleRec.LastState = 0;
		lRleInfo = &lRleRec;
		ReadCallback = RLERead;
	} 
	else 
	{
		lRleInfo = NULL;
		ReadCallback = STDRead;
	}

	lWBytes = lWidth * lPelbytes;

	if 
		( lVertrev )
	{
		lStart = 0;
		lEnd   = lHeight;
		lDir   = 1;
	} 
	else 
	{
		/* We need to reverse loading order of rows. */
		lStart = lHeight-1;
		lEnd   = -1;
		lDir   = -1;
	}

	for ( i = lStart; i != lEnd; i += lDir ) 
	{
		lData = lPixels + i * lWBytes;

		/* Suck in the lData one row at a time. */
		if 
			( ReadCallback( lRleInfo, lData, lPelbytes, lWidth, lInput ) != lWidth ) 
		{
			/* Probably premature End of file. */
			if 
				( sVerbose ) 
			{
				printf ( "TGA: sError reading (ftell == %ld, lWidth=%d)\n", ftell(lInput), lWidth );
			}

			return NULL;
		}  

		if 
			( lHorzrev ) 
		{
			/* We need to mirror row horizontally. */
			for ( j = 0; j < lWidth / 2; j++ ) 
			{
				GLubyte lTemp;
				for ( k = 0; k < lPelbytes; k++ ) 
				{
					lTemp = lData[ j * lPelbytes + k ];
					lData[ j * lPelbytes + k ] = lData[( lWidth - j - 1 ) * lPelbytes + k ];
					lData[ ( lWidth - j - 1 ) * lPelbytes + k ] = lTemp;
				}
			}
		}
	}

	if 
		( lRle ) 
	{
		free( lRleInfo->StateBuf );
	}

	if 
		( fgetc( lInput ) != EOF ) 
	{
		if 
			( sVerbose ) 
		{
			printf ( "TGA: too much input lData, ignoring extra...\n" );
		}
	}

	lImage = new GenericImage();
	lImage->SetWidth( lWidth );
	lImage->SetHeight( lHeight );
	lImage->SetFormat( lFormat );
	lImage->SetComponents( lComponents );
	lImage->SetMapEntries( lColors );
	lImage->SetMapFormat( GL_BGR_EXT );  // XXX fix me
	lImage->SetMap( lMap );
	lImage->SetPixels( lPixels );

	// Close the file.
	lError = fclose( lInput );
	if( lError != 0 )
	{
		lImage->Release();
		DeletePointer( lImage );
		return NULL;
	}

	return lImage;
}

/************************************************************************************/
BIGINT		  TGALoader::Verbose(BIGINT pNewVerbose)
{
	BIGINT lOldVerbose = sVerbose;
	sVerbose = pNewVerbose;
	return sVerbose;
}

/************************************************************************************/
VVOID		  TGALoader::WriteTGA(GenericImage* pImage, const CCHAR* pName)
{
	if( pImage == NULL )
	{
		return;
	}

	FILE* lOutput = NULL;
	// Open the texture file (write binary mode).
	BIGINT lError = fopen_s( &lOutput, pName, "wb" );
	if
		( lError != 0 )
	{
		return;
	}

	if
		( pImage->Components() == 3 || pImage->Components() == 4 )
	{
		fwrite(TGAHeaderColor, 12, sizeof(UCCHAR), lOutput );
	}
	else 
	{ 
		if
			( pImage->Components() == 1 )
		{
			fwrite( TGAHeaderBW, 12, sizeof(UCCHAR), lOutput );    
		}
		else 
		{ 
			fprintf( stderr, "Supported component number: 1,3 or 4\n" ); 
			exit(1); 
		}
	}

	write16bit( pImage->Width(), lOutput );  
	write16bit( pImage->Height(), lOutput );  
	switch ( pImage->Components() ) 
	{ 
	case 1: 
		putc( 8, lOutput );
		break;
	case 3: 
		putc( 24, lOutput ); 
		break;
	case 4:
		putc( 32, lOutput );
		break;
	default: 
		fprintf( stderr, "Supported component number: 1,3 or 4\n" ); 
		exit(1);
		break;
	};
  
	if
		( pImage->Components() == 4 )
	{
		putc( 0x24, lOutput ); // bottom left image (0x00) + 8 bit alpha (0x4)
	}
	else
	{
		putc( 0x00, lOutput );
	}

	fwrite( pImage->Pixels(), pImage->Height() * pImage->Width() * pImage->Components(), sizeof(CCHAR), lOutput );

	// Close the file.
	lError = fclose( lOutput );
	if( lError != 0 )
	{
		return;
	}
}
