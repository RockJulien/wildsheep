#include "TGALoader.h"
#include "..\GDevice.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Namespaces used
using namespace std;

// Static variables
static CCHAR   sError[256];
static UBIGINT sVerbose  = 0;
static BIGINT  sTotbytes = 0;

#define MIN( a, b ) ((( a ) < ( b )) ? ( a ) : ( b ))

#define RLE_PACKETSIZE 0x80

// Uncompressed TGA Header
UCCHAR uTGAcompare[12]  = { 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
UCCHAR uTGAcompare2[12] = { 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 2 };
// Compressed TGA Header
UCCHAR cTGAcompare[12] = { 0, 0, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

// this makes sure that 
// image size is written in correct format 
// and byte order (least first)
VVOID write16bit(BIGINT pCount, FILE* pFile) 
{ 
	UCCHAR lBytes[] = { (UCCHAR)(pCount % 256), (UCCHAR)(pCount / 256) };
	fwrite( lBytes, 2, sizeof(UCCHAR), pFile );
}

/************************************************************************************/
BIGINT STDRead(RLEState* pRLEInfo, UCCHAR* pBuffer, BIGINT pDataSize, BIGINT pNelems, FILE* pFile)
{
	if 
		( sVerbose > 1 ) 
	{
		sTotbytes += pNelems * pDataSize;
		printf( "TGA: standard fread %d (total %d)\n", pNelems * pDataSize, sTotbytes );
	}

	return (BIGINT)fread( pBuffer, pDataSize, pNelems, pFile );
}

/************************************************************************************/
BIGINT RLERead(RLEState* pRLEInfo, UCCHAR* pBuffer, BIGINT pDataSize, BIGINT pNelems, FILE* pFile)
{
	UCCHAR* lBuffer = pBuffer;
	BIGINT j, k;
	BIGINT lBufLen;
	BIGINT lCount;
	BIGINT lBytes;
	BIGINT lCurrbytes;
	UCCHAR *p;

	/* Scale the buffer length. */
	lBufLen = pNelems * pDataSize;

	j = 0;
	lCurrbytes = sTotbytes;
	while 
		( j < lBufLen ) 
	{
		if 
			( pRLEInfo->LastState < pRLEInfo->StateLen ) 
		{
			/* Copy bytes from our previously decoded buffer. */
			lBytes = MIN( lBufLen - j, pRLEInfo->StateLen - pRLEInfo->LastState );
			memcpy( lBuffer + j, pRLEInfo->StateBuf + pRLEInfo->LastState, lBytes );
			j += lBytes;
			pRLEInfo->LastState += lBytes;

			/* If we used up all of our state bytes, then reset them. */
			if 
				( pRLEInfo->LastState >= pRLEInfo->StateLen ) 
			{
				pRLEInfo->LastState = 0;
				pRLEInfo->StateLen  = 0;
			}

			/* If we filled the buffer, then exit the loop. */
			if ( j >= lBufLen ) 
			{
				break;
			}
		}

		/* Decode the next packet. */
		lCount = fgetc( pFile );
		if 
			( lCount == EOF ) 
		{
			if ( sVerbose ) 
			{
				printf( "TGA: hit EOF while looking for count\n" );
			}

			return j / pDataSize;
		}

		/* Scale the byte length to the size of the data. */
		lBytes = ((lCount & ~RLE_PACKETSIZE) + 1) * pDataSize;

		if 
			( j + lBytes <= lBufLen ) 
		{
			/* We can copy directly into the image buffer. */
			p = lBuffer + j;
		} 
		else 
		{
	#ifdef PROFILE
			printf("TGA: needed to use statebuf for %d bytes\n", lBufLen - j);
	#endif
			/* Allocate the state buffer if we haven't already. */
			if 
				( pRLEInfo->StateBuf == NULL ) 
			{
				pRLEInfo->StateBuf = (UCCHAR *)malloc( RLE_PACKETSIZE * pDataSize );
			}

			p = pRLEInfo->StateBuf;
		}

		if 
			( lCount & RLE_PACKETSIZE ) 
		{
			/* Fill the buffer with the next value. */
			if 
				( fread( p, pDataSize, 1, pFile ) != 1 ) 
			{
				if 
					( sVerbose ) 
				{
					printf( "TGA: EOF while reading %d/%d element RLE packet\n", lBytes, pDataSize );
				}

				return j / pDataSize;
			}

			/* Optimized case for single-byte encoded data. */
			if ( pDataSize == 1 ) 
			{
				memset( p + 1, *p, lBytes - 1 );
			} 
			else 
			{
				for ( k = pDataSize; k < lBytes; k += pDataSize ) 
				{
					memcpy( p + k, p, pDataSize );
				}
			}
		} 
		else 
		{
			/* Read in the buffer. */
			if 
				( fread( p, lBytes, 1, pFile ) != 1 ) 
			{
				if 
					( sVerbose ) 
				{
					printf( "TGA: EOF while reading %d/%d element raw packet\n", lBytes, pDataSize );
				}

				return j / pDataSize;
			}
		}

		if 
			( sVerbose > 1 ) 
		{
			sTotbytes += lBytes;
			if 
				( sVerbose > 2 ) 
			{
				printf( "TGA: %s packet %d/%d\n", (lCount & RLE_PACKETSIZE) ? "RLE" : "raw", lBytes, sTotbytes );
			}
		}

		/* We may need to copy bytes from the state buffer. */
		if 
			( p == pRLEInfo->StateBuf ) 
		{
			pRLEInfo->StateLen = lBytes;
		} 
		else 
		{
			j += lBytes;
		}
	}

	if 
		( sVerbose > 1 ) 
	{
		printf( "TGA: rle_fread %d/%d (total %d)\n", pNelems * pDataSize, sTotbytes - lCurrbytes, sTotbytes );
	}

	return pNelems;
}

// Load An Uncompressed File
BBOOL LoadUncompressedTGA( GenericImage* lImage, const CCHAR* pFileName, FILE* pFile )
{
	TGA lTga;             // Used To Store File Information

	// Attempt To Read Next 6 Bytes
	if
		( fread( lTga.Header, sizeof(lTga.Header), 1, pFile ) == 0 )
	{                                      
		printf_s( "LoadUncompressedTGA Failed !!! The file %s cannot be read", pFileName );
		return false;               // Return False
	}

	lTga.Width  = lTga.Header[1] * 256 + lTga.Header[0];   // Calculate The Width 
	lTga.Height = lTga.Header[3] * 256 + lTga.Header[2];   // Calculate The Height
	lTga.Bpp    = lTga.Header[4];						   // Calculate Bits Per Pixel

	lImage->SetWidth( lTga.Width );    // Copy Width Into the image
	lImage->SetHeight( lTga.Height );  // Copy Height Into the image
	lImage->SetBpp( lTga.Bpp );		   // Copy Bpp Into the image

	// Make Sure All Information Is Valid
	if( ( lTga.Width <= 0 ) || 
		( lTga.Height <= 0 ) || 
		( (lTga.Bpp != 24) && (lTga.Bpp !=32) ) )
	{
		printf_s( "LoadUncompressedTGA Failed !!! The file %s has a bad size or bpp count", pFileName );
		return false;  // Return False
	}

	if
		( lTga.Bpp == 24 )  // Is It A 24bpp Image?
	{
		lImage->SetFormat( GL_RGB );       // If So, Set Type To GL_RGB
	}
	else  // If It's Not 24, It Must Be 32
	{
		lImage->SetFormat( GL_RGBA );      // So Set The Type To GL_RGBA
	}

	// Calculate The BYTES Per Pixel
	lTga.BytesPerPixel = (lTga.Bpp / 8);

	// Calculate Memory Needed To Store Image
	lTga.ImageSize = (lTga.BytesPerPixel * lTga.Width * lTga.Height);

	// Allocate Memory
	UCCHAR* lBuffer = new UCCHAR[ lTga.ImageSize ];
	if( lBuffer == NULL ) // Make Sure It Was Allocated Ok
	{
		printf_s( "LoadUncompressedTGA Failed !!! The file %s cannot allocate needed memory", pFileName );
		return false; // If Not, Return False
	}

	// Attempt To Read All The Image Data
	if( fread( lBuffer, 1, lTga.ImageSize, pFile ) != lTga.ImageSize )
	{
		printf_s( "LoadUncompressedTGA Failed !!! The overall file %s cannot be read", pFileName );
		return false;               // If We Cant, Return False
	}

	// Start The Loop switching BGR into RGB
    for( UBIGINT lSwap = 0; lSwap < lTga.ImageSize; lSwap += lTga.BytesPerPixel )
    {
        // 1st Byte XOR 3rd Byte XOR 1st Byte XOR 3rd Byte
        lBuffer[ lSwap ] ^= lBuffer[ lSwap + 2 ] ^=
        lBuffer[ lSwap ] ^= lBuffer[ lSwap + 2 ];
    }

	// Set the buffer.
	lImage->SetPixels( lBuffer );
 
	fclose( pFile ); // Close The File

	return true;
}

// Load A Compressed File
BBOOL LoadCompressedTGA( GenericImage* lImage, const CCHAR* pFileName, FILE* pFile )
{
	TGA lTga;             // Used To Store File Information
	
	if
		( fread( lTga.Header, sizeof(lTga.Header), 1, pFile ) == 0 )
	{                                      
		printf_s( "LoadCompressedTGA Failed !!! The file %s cannot be read", pFileName );
		return false;               // Return False
	}

	lTga.Width  = lTga.Header[1] * 256 + lTga.Header[0];   // Calculate The Width 
	lTga.Height = lTga.Header[3] * 256 + lTga.Header[2];   // Calculate The Height
	lTga.Bpp    = lTga.Header[4];						   // Calculate Bits Per Pixel

	lImage->SetWidth( lTga.Width );    // Copy Width Into the image
	lImage->SetHeight( lTga.Height );  // Copy Height Into the image
	lImage->SetBpp( lTga.Bpp );		   // Copy Bpp Into the image

    // Make Sure All Information Is Valid
	if( ( lTga.Width <= 0 ) || 
		( lTga.Height <= 0 ) || 
		( (lTga.Bpp != 24) && (lTga.Bpp !=32) ) )
	{
		printf_s( "LoadCompressedTGA Failed !!! The file %s has a bad size or bpp count", pFileName );
		return false;  // Return False
	}
 
    if
		( lTga.Bpp == 24 )  // Is It A 24bpp Image?
	{
		lImage->SetFormat( GL_RGB );       // If So, Set Type To GL_RGB
	}
	else  // If It's Not 24, It Must Be 32
	{
		lImage->SetFormat( GL_RGBA );      // So Set The Type To GL_RGBA
	}

	// Calculate The BYTES Per Pixel
	lTga.BytesPerPixel = (lTga.Bpp / 8);

	// Calculate Memory Needed To Store Image
	lTga.ImageSize = (lTga.BytesPerPixel * lTga.Width * lTga.Height);

	// Allocate Memory
	UCCHAR* lBuffer = new UCCHAR[ lTga.ImageSize ];
	if( lBuffer == NULL ) // Make Sure It Was Allocated Ok
	{
		printf_s( "LoadCompressedTGA Failed !!! The file %s cannot allocate needed memory", pFileName );
		fclose( pFile );
		return false; // If Not, Return False
	}

	// Determine how many pixels make up the image
	UBIGINT lPixelCount   = lTga.Height * lTga.Width; // Number Of Pixels In The Image
	UBIGINT lCurrentpixel = 0;            // Current Pixel We Are Reading From Data
	UBIGINT lCurrentbyte  = 0;            // Current Byte We Are Writing Into Imagedata
	// Storage For 1 Pixel
	UCCHAR* lColorBuffer  = new UCCHAR[ lTga.BytesPerPixel ];

	do // Start Loop
	{
		UCCHAR lChunkHeader = 0; // Variable To Store The Value Of The Id Chunk
		if
			( fread( &lChunkHeader, sizeof(UCCHAR), 1, pFile ) == 0 )  // Attempt To Read The Chunk's Header
		{
			printf_s( "LoadCompressedTGA Failed !!! The file %s cannot be read", pFileName );
			
			if
				( pFile != NULL )
			{
				fclose( pFile );
			}

			if
				( lBuffer != NULL )
			{
				DeletePointer( lBuffer );
			}

			return false; // If It Fails, Return False
		}

		if
			( lChunkHeader < 128 )  // If The Chunk Is A 'RAW' Chunk
		{                                                  
			lChunkHeader++; // Add 1 To The Value To Get Total Number Of Raw Pixels
			// Start Pixel Reading Loop
			for
				( SMALLINT lCounter = 0; lCounter < lChunkHeader; lCounter++ )
			{
				// Try To Read 1 Pixel
				if
					( fread( lColorBuffer, 1, lTga.BytesPerPixel, pFile ) != lTga.BytesPerPixel )
				{
					printf_s( "LoadCompressedTGA Failed !!! The file %s cannot be read", pFileName );
					
					if
						( pFile != NULL )
					{
						fclose( pFile );
					}

					if
						( lColorBuffer != NULL )
					{
						DeletePointer( lColorBuffer );
					}

					if
						( lBuffer != NULL )
					{
						DeletePointer( lBuffer );
					}
					
					return false; // If It Fails, Return False
				}

				lBuffer[ lCurrentbyte ]     = lColorBuffer[ 2 ]; // Write The 'R' Byte
				lBuffer[ lCurrentbyte + 1 ] = lColorBuffer[ 1 ]; // Write The 'G' Byte
				lBuffer[ lCurrentbyte + 2 ] = lColorBuffer[ 0 ]; // Write The 'B' Byte

				if
					( lTga.BytesPerPixel == 4 ) // If It's A 32bpp Image...
				{
					lBuffer[ lCurrentbyte + 3 ] = lColorBuffer[ 3 ];    // Write The 'A' Byte
				}

				// Increment The Byte Counter By The Number Of Bytes In A Pixel
				lCurrentbyte += lTga.BytesPerPixel;
				lCurrentpixel++; // Increment The Number Of Pixels By 1

				if
					( lCurrentpixel > lPixelCount )											// Make sure we havent written too many pixels
				{
					printf_s( "Too many pixels read" );	// if there is too many... Display an error!

					if
						( pFile != NULL )	// If there is a file open
					{
						fclose( pFile );  // Close file
					}	

					if
						( lColorBuffer != NULL )	// If there is data in colorbuffer
					{
						DeletePointer( lColorBuffer );	// Delete it
					}

					if
						( lBuffer != NULL )	// If there is Image data
					{
						DeletePointer( lBuffer ); // delete it
					}

					return false; // Return failed
				}
			}
		}
		else // If It's An RLE Header
		{
			lChunkHeader -= 127; // Subtract 127 To Get Rid Of The ID Bit

			// Read The Next Pixel
			if
				( fread( lColorBuffer, 1, lTga.BytesPerPixel, pFile ) != lTga.BytesPerPixel )
			{  
				printf_s( "LoadCompressedTGA Failed !!! The file %s cannot be read", pFileName );
				
				if
					( pFile != NULL )
				{
					fclose( pFile );
				}

				if
					( lColorBuffer != NULL )
				{
					DeletePointer( lColorBuffer );
				}

				if
					( lBuffer != NULL )
				{
					DeletePointer( lBuffer );
				}

				return false;  // If It Fails, Return False
			}

			// Start The Loop
			for
				( SMALLINT lCounter = 0; lCounter < lChunkHeader; lCounter++ )
			{
				// Copy The 'R' Byte
				lBuffer[ lCurrentbyte ]     = lColorBuffer[ 2 ];
				// Copy The 'G' Byte
				lBuffer[ lCurrentbyte + 1 ] = lColorBuffer[ 1 ];
				// Copy The 'B' Byte
				lBuffer[ lCurrentbyte + 2 ] = lColorBuffer[ 0 ];

				if
					( lTga.BytesPerPixel == 4 ) // If It's A 32bpp Image
				{
					// Copy The 'A' Byte
					lBuffer[ lCurrentbyte + 3 ] = lColorBuffer[ 3 ];
				}

				lCurrentbyte += lTga.BytesPerPixel; // Increment The Byte Counter
				lCurrentpixel++;  // Increment The Pixel Counter

				if
					( lCurrentpixel > lPixelCount )											// Make sure we havent written too many pixels
				{
					printf_s( "Too many pixels read" );	// if there is too many... Display an error!

					if
						( pFile != NULL )	// If there is a file open
					{
						fclose( pFile );  // Close file
					}	

					if
						( lColorBuffer != NULL )	// If there is data in colorbuffer
					{
						DeletePointer( lColorBuffer );	// Delete it
					}

					if
						( lBuffer != NULL )	// If there is Image data
					{
						DeletePointer( lBuffer ); // delete it
					}

					return false; // Return failed
				}
			}
		}
	}
	while( lCurrentpixel < lPixelCount ); // More Pixels To Read? ... Start Loop Over

	// Set the pixels
	lImage->SetPixels( lBuffer );

	fclose( pFile ); // Close File

    return true;   // Return Success
}

/************************************************************************************/
TGALoader::TGALoader()
{

}

/************************************************************************************/
TGALoader::~TGALoader()
{

}

/************************************************************************************/
GenericImage* TGALoader::ReadTGA(const CCHAR* pName)
{
	FILE* lInput = NULL;
	TGAHeader lTgaHeader; // Used To Store Our File Header
	GenericImage* lImage = new GenericImage();

	// Open the texture file (read binary mode).
	BIGINT lError = fopen_s( &lInput, pName, "rb" );
	if
		( lError != 0 )
	{
		lImage->Release();
		DeletePointer( lImage );
		return NULL;
	}

	// Attempt To Read The File Header
	if( fread( &lTgaHeader, sizeof(TGAHeader), 1, lInput ) == 0 )
	{
		printf_s( "TGALoad Failed !!! The file %s cannot be read", pName );

		fclose( lInput );

		lImage->Release();
		DeletePointer( lImage );

		return NULL; // Return If It Fails
	}

	// If The File Header Matches The Uncompressed Header
	if
		( memcmp( uTGAcompare, &lTgaHeader, sizeof(TGAHeader))  == 0 ||
		  memcmp( uTGAcompare2, &lTgaHeader, sizeof(TGAHeader)) == 0 )
	{
		// Load An Uncompressed TGA
		LoadUncompressedTGA( lImage, pName, lInput );
	}
	// If The File Header Matches The Compressed Header
	else if
		( memcmp( cTGAcompare, &lTgaHeader, sizeof(TGAHeader) ) == 0 )
	{                                          
		// Load A Compressed TGA
		LoadCompressedTGA( lImage, pName, lInput );
	}
	else // If It Doesn't Match Either One
	{
		printf_s( "TGALoad Failed !!! The file %s has a type not supported", pName );                  
		
		fclose( lInput );

		lImage->Release();
		DeletePointer( lImage );

		return NULL;  // Return
	}    

	return lImage;
}

/************************************************************************************/
BIGINT		  TGALoader::Verbose(BIGINT pNewVerbose)
{
	BIGINT lOldVerbose = sVerbose;
	sVerbose = pNewVerbose;
	return sVerbose;
}

/************************************************************************************/
VVOID		  TGALoader::WriteTGA(GenericImage* pImage, const CCHAR* pName)
{
	//if( pImage == NULL )
	//{
	//	return;
	//}

	//FILE* lOutput = NULL;
	//// Open the texture file (write binary mode).
	//BIGINT lError = fopen_s( &lOutput, pName, "wb" );
	//if
	//	( lError != 0 )
	//{
	//	return;
	//}

	//if
	//	( pImage->Components() == 3 || pImage->Components() == 4 )
	//{
	//	fwrite(TGAHeaderColor, 12, sizeof(UCCHAR), lOutput );
	//}
	//else 
	//{ 
	//	if
	//		( pImage->Components() == 1 )
	//	{
	//		fwrite( TGAHeaderBW, 12, sizeof(UCCHAR), lOutput );    
	//	}
	//	else 
	//	{ 
	//		fprintf( stderr, "Supported component number: 1,3 or 4\n" ); 
	//		exit(1); 
	//	}
	//}

	//write16bit( pImage->Width(), lOutput );  
	//write16bit( pImage->Height(), lOutput );  
	//switch ( pImage->Components() ) 
	//{ 
	//case 1: 
	//	putc( 8, lOutput );
	//	break;
	//case 3: 
	//	putc( 24, lOutput ); 
	//	break;
	//case 4:
	//	putc( 32, lOutput );
	//	break;
	//default: 
	//	fprintf( stderr, "Supported component number: 1,3 or 4\n" ); 
	//	exit(1);
	//	break;
	//};
 // 
	//if
	//	( pImage->Components() == 4 )
	//{
	//	putc( 0x24, lOutput ); // bottom left image (0x00) + 8 bit alpha (0x4)
	//}
	//else
	//{
	//	putc( 0x00, lOutput );
	//}

	//fwrite( pImage->Pixels(), pImage->Height() * pImage->Width() * pImage->Components(), sizeof(CCHAR), lOutput );

	//// Close the file.
	//lError = fclose( lOutput );
	//if( lError != 0 )
	//{
	//	return;
	//}
}
