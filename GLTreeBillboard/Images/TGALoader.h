#ifndef DEF_TGALOADER_H
#define DEF_TGALOADER_H

// Includes
#include "GenericImage.h"
#include "..\TextureEXType.h"

// Class definition
class TGALoader
{
private:

	// Attributes

public:

	// Constructor
	TGALoader();
	~TGALoader();

	// Methods
	GenericImage* ReadTGA(const CCHAR* pName);
	BIGINT		  Verbose(BIGINT pNewVerbose);
	VVOID		  WriteTGA(GenericImage* pImage, const CCHAR* pName);

};

#endif