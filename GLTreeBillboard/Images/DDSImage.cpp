#include "DDSImage.h"

/*****************************************************************************************/
DDSImage::DDSImage() :
mWidth(0), mHeight(0), mMipmapLevels(0), 
mFormat(0), mInternalFormat(0), mCompressionType(0),
mBlockSize(0)
{
	for( BIGINT lCurr = 0; lCurr < MAX_MIPMAP_LEVEL; lCurr++ )
	{
		mPixels[ lCurr ] = 0;
		mBytes[ lCurr ] = 0;
	}
}

/*****************************************************************************************/
DDSImage::~DDSImage()
{
	for( BIGINT lCurr = 0; lCurr < MAX_MIPMAP_LEVEL; lCurr++ )
	{
		DeletePointer( mPixels[ lCurr ] );
	}
}
