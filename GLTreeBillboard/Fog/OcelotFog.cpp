#include "OcelotFog.h"

using namespace Ocelot;
using namespace Ocelot::OcelotColor;

OcelotFog::OcelotFog() : 
m_cColor(GREY), m_fStart(2000.0f), m_fRange(1000.0f)
{

}

OcelotFog::OcelotFog(FFLOAT from, FFLOAT range, Color col) : 
m_cColor(col), m_fStart(from), m_fRange(range)
{

}

OcelotFog::~OcelotFog()
{

}

OcelotFog::operator float* ()
{
	FFLOAT* matVec3[6];
	FFLOAT* col = (FFLOAT*)m_cColor;

	matVec3[0] = col;   // R
	matVec3[1] = col+1; // G
	matVec3[2] = col+2; // B
	matVec3[3] = col+3; // A
	matVec3[4] = &m_fStart;
	matVec3[5] = &m_fRange;

	return matVec3[0];
}

OcelotFog::operator const float* () const
{
	const FFLOAT* matVec3[6];
	const FFLOAT* col = (const FFLOAT*)m_cColor;

	matVec3[0] = col;   // R
	matVec3[1] = col+1; // G
	matVec3[2] = col+2; // B
	matVec3[3] = col+3; // A
	matVec3[4] = &m_fStart;
	matVec3[5] = &m_fRange;

	return matVec3[0];
}
