#ifndef DEF_OCELOTFOG_H
#define DEF_OCELOTFOG_H

#include "..\Maths\Color.h"
#include "..\OSCheck.h"

namespace Ocelot
{
	class OcelotFog
	{
	private:

		// Attributes
		Color  m_cColor; // Color of the fog to merge with the shader color.
		FFLOAT m_fStart; // starting point of the Fog.
		FFLOAT m_fRange; // the range of the fog until it will be applied.

	public:

		// Constructor & Destructor
		OcelotFog();
		OcelotFog(FFLOAT from, FFLOAT range, Color col = OcelotColor::GREY);
		~OcelotFog();

		// Methods


		// Accessors
		inline Color  FogColor() const { return m_cColor;}
		inline FFLOAT Start() const { return m_fStart;}
		inline FFLOAT Range() const { return m_fRange;}
		inline VVOID  SetFogColor(Color fogColor) { m_cColor = fogColor;}
		inline VVOID  SetStart(FFLOAT start) { m_fStart = start;}
		inline VVOID  SetRange(FFLOAT range) { m_fRange = range;}
	
		// operators overload
		/*********************Cast Operators**************************/
		operator float* ();              // for being able to pass it through the shader as a (float*)
		operator const float* () const;  // constant version
	
	};
}

#endif