////////////////////////////////////////////////////////////////////////////////
// Filename: systemclass.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _SYSTEMCLASS_H_
#define _SYSTEMCLASS_H_


///////////////////////////////
// PRE-PROCESSING DIRECTIVES //
///////////////////////////////
//#define WIN32_LEAN_AND_MEAN


//////////////
// INCLUDES //
//////////////
#include <windows.h>

///////////////////////
// MY CLASS INCLUDES //
///////////////////////
#include "graphicsclass.h"
#include "Time\Timer.h"

enum MouseButton
{
	eMOUSEBUTTON_NA = 0,
	eMOUSEBUTTON_LEFT,
	eMOUSEBUTTON_RIGHT,
	eMOUSEBUTTON_MIDDLE
};

////////////////////////////////////////////////////////////////////////////////
// Class name: SystemClass
////////////////////////////////////////////////////////////////////////////////
class SystemClass
{
public:

	// Constructor & Destructor
	SystemClass();
	~SystemClass();

	// Methods
	BBOOL Initialize();
	VVOID OnResize();
	VVOID Shutdown();
	VVOID Run();

	static FFLOAT GetTotalTime();

	// Inputs callback
	LRESULT CALLBACK MessageHandler(HWND, UINT, WPARAM, LPARAM);

private:

	SystemClass(const SystemClass&);

	// Private Methods
	BBOOL Update(FFLOAT pDeltaTime);
	BBOOL InitializeWindows(OpenGLClass*, BIGINT&, BIGINT&);
	BBOOL InitializeGui();
	BBOOL IsHoveringAnyGui();
	VVOID HandleInput();
	VVOID ShutdownWindows();

	// Inputs events
	VVOID OnMouseDown(WPARAM pButtonState, BIGINT pX, BIGINT pY);
	VVOID OnMouseUp(WPARAM pButtonState, BIGINT pX, BIGINT pY);
	VVOID OnMouseMove(WPARAM pButtonState, BIGINT pX, BIGINT pY);
	VVOID OnMouseWheel(WPARAM pButtonState);
	VVOID OnKeyDown(WPARAM pButtonState);
	VVOID OnKeyUp(WPARAM pButtonState);

private:

	// Attributes
	LPCWSTR   mApplicationName;
	HINSTANCE mHinstance;
	HWND      mHwnd;

	BIGINT    mClientWidth;
	BIGINT    mClientHeight;

	OpenGLClass*   mOpenGL;
	GraphicsClass* mGraphics;
	Timer          mTimer;

	// Flags
	BBOOL      mAppPaused;
	BBOOL      mMinimized;
	BBOOL      mMaximized;
	BBOOL      mResizing;

	// Mouse
	LINT       mLastMouseX;
	LINT       mLastMouseY;

	// Total Time
	static FFLOAT sTotalTime;
};

/////////////////////////
// FUNCTION PROTOTYPES //
/////////////////////////
static LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

/////////////
// GLOBALS //
/////////////
static SystemClass* ApplicationHandle = 0;

#endif