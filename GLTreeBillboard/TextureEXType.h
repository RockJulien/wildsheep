#ifndef DEF_TEXTUREEXTYPE_H
#define DEF_TEXTUREEXTYPE_H

#include "OSCheck.h"
#include <gli.hpp>

enum ExtType
{
	eBMP,
	ePNG,
	eJPG,
	eDDS,
	eTGA
};

struct RLEState
{
	UCCHAR* StateBuf;
	BIGINT  StateLen;
	BIGINT  LastState;
};

typedef struct
{
    UCCHAR Header[12];         // File Header To Determine File Type
} TGAHeader;
 
typedef struct
{
    UCCHAR  Header[6];       // Holds The First 6 Useful Bytes Of The File
    UBIGINT BytesPerPixel;   // Number Of BYTES Per Pixel (3 Or 4)
    UBIGINT ImageSize;       // Amount Of Memory Needed To Hold The Image
    UBIGINT type;            // The Type Of Image, GL_RGB Or GL_RGBA
    UBIGINT Height;          // Height Of Image                 
    UBIGINT Width;           // Width Of Image              
    UBIGINT Bpp;             // Number Of BITS Per Pixel (24 Or 32)
} TGA;

//
// Structures from Direct3D 9
//
struct D3D_PixelFormat // DDPIXELFORMAT
{
	int dwSize;
	int dwFlags;
	int dwFourCC;
	int dwRGBBitCount;
	int dwRBitMask, dwGBitMask, dwBBitMask;
	int dwRGBAlphaBitMask;
};

struct D3D_Caps2
{
	int dwCaps1;
	int dwCaps2;
	int Reserved[2];
};

struct D3D_SurfaceDesc2
{
	int dwSize;
	int dwFlags;
	int dwHeight;
	int dwWidth;
	int dwPitchOrLinearSize;
	int dwDepth;
	int dwMipMapCount;
	int dwReserved1[11];
	D3D_PixelFormat ddpfPixelFormat;
	D3D_Caps2 ddsCaps;
	int dwReserved2;
};

//
// Enums
//
#define MakeFourCC(ch0, ch1, ch2, ch3)                              \
				  ((ULINT)(UCCHAR)(ch0) | ((ULINT)(UCCHAR)(ch1) << 8) |   \
                  ((ULINT)(UCCHAR)(ch2) << 16) | ((ULINT)(UCCHAR)(ch3) << 24 ))

typedef enum
{
	DDS_COMPRESS_NONE = 0,
	DDS_COMPRESS_BC1, /* DXT1 */
	DDS_COMPRESS_BC2, /* DXT3 */
	DDS_COMPRESS_BC3, /* DXT5 */
	DDS_COMPRESS_BC3N, /* DXT5n */
	DDS_COMPRESS_BC4, /* ATI1 */
	DDS_COMPRESS_BC5, /* ATI2 */
	DDS_COMPRESS_AEXP, /* DXT5 */
	DDS_COMPRESS_YCOCG, /* DXT5 */
	DDS_COMPRESS_YCOCGS, /* DXT5 */
	DDS_COMPRESS_MAX
} DDS_COMPRESSION_TYPE;

typedef enum
{
	DDS_SAVE_SELECTED_LAYER = 0,
	DDS_SAVE_CUBEMAP,
	DDS_SAVE_VOLUMEMAP,
	DDS_SAVE_MAX
} DDS_SAVE_TYPE;

typedef enum
{
	DDS_FORMAT_DEFAULT = 0,
	DDS_FORMAT_RGB8,
	DDS_FORMAT_RGBA8,
	DDS_FORMAT_BGR8,
	DDS_FORMAT_ABGR8,
	DDS_FORMAT_R5G6B5,
	DDS_FORMAT_RGBA4,
	DDS_FORMAT_RGB5A1,
	DDS_FORMAT_RGB10A2,
	DDS_FORMAT_R3G3B2,
	DDS_FORMAT_A8,
	DDS_FORMAT_L8,
	DDS_FORMAT_L8A8,
	DDS_FORMAT_AEXP,
	DDS_FORMAT_YCOCG,
	DDS_FORMAT_MAX
} DDS_FORMAT_TYPE;

typedef enum
{
	DDS_COLOR_DEFAULT = 0,
	DDS_COLOR_DISTANCE,
	DDS_COLOR_LUMINANCE,
	DDS_COLOR_INSET_BBOX,
	DDS_COLOR_MAX
} DDS_COLOR_TYPE;

typedef enum
{
	DDS_MIPMAP_DEFAULT = 0,
	DDS_MIPMAP_NEAREST,
	DDS_MIPMAP_BOX,
	DDS_MIPMAP_BILINEAR,
	DDS_MIPMAP_BICUBIC,
	DDS_MIPMAP_LANCZOS,
	DDS_MIPMAP_MAX
} DDS_MIPMAP_TYPE;

#if !defined(DDS_HEADERSIZE)
#define DDS_HEADERSIZE 128
#endif

#if !defined(DDSD_CAPS)
#define DDSD_CAPS 0x00000001
#endif

#if !defined(DDSD_HEIGHT)
#define DDSD_HEIGHT 0x00000002
#endif

#if !defined(DDSD_WIDTH)
#define DDSD_WIDTH 0x00000004
#endif

#if !defined(DDSD_PITCH)
#define DDSD_PITCH 0x00000008
#endif

#if !defined(DDSD_PIXELFORMAT)
#define DDSD_PIXELFORMAT 0x00001000
#endif

#if !defined(DDSD_MIPMAPCOUNT)
#define DDSD_MIPMAPCOUNT 0x00020000
#endif

#if !defined(DDSD_LINEARSIZE)
#define DDSD_LINEARSIZE 0x00080000
#endif

#if !defined(DDSD_DEPTH)
#define DDSD_DEPTH 0x00800000
#endif

#if !defined(DDPF_ALPHAPIXELS)
#define DDPF_ALPHAPIXELS 0x00000001
#endif

#if !defined(DDPF_ALPHA)
#define DDPF_ALPHA 0x00000002
#endif

#if !defined(DDPF_FOURCC)
#define DDPF_FOURCC 0x00000004
#endif

#if !defined(DDPF_PALETTEINDEXED8)
#define DDPF_PALETTEINDEXED8 0x00000020
#endif

#if !defined(DDPF_RGB)
#define DDPF_RGB 0x00000040
#endif

#if !defined(DDPF_LUMINANCE)
#define DDPF_LUMINANCE 0x00020000
#endif

#if !defined(DDSCAPS_COMPLEX)
#define DDSCAPS_COMPLEX 0x00000008
#endif

#if !defined(DDSCAPS_TEXTURE)
#define DDSCAPS_TEXTURE 0x00001000
#endif

#if !defined(DDSCAPS_MIPMAP)
#define DDSCAPS_MIPMAP 0x00400000
#endif

#if !defined(DDSCAPS2_CUBEMAP)
#define DDSCAPS2_CUBEMAP 0x00000200
#endif

#if !defined(DDSCAPS2_CUBEMAP_POSITIVEX)
#define DDSCAPS2_CUBEMAP_POSITIVEX 0x00000400
#endif

#if !defined(DDSCAPS2_CUBEMAP_NEGATIVEX)
#define DDSCAPS2_CUBEMAP_NEGATIVEX 0x00000800
#endif

#if !defined(DDSCAPS2_CUBEMAP_POSITIVEY)
#define DDSCAPS2_CUBEMAP_POSITIVEY 0x00001000
#endif

#if !defined(DDSCAPS2_CUBEMAP_NEGATIVEY)
#define DDSCAPS2_CUBEMAP_NEGATIVEY 0x00002000
#endif

#if !defined(DDSCAPS2_CUBEMAP_POSITIVEZ)
#define DDSCAPS2_CUBEMAP_POSITIVEZ 0x00004000
#endif

#if !defined(DDSCAPS2_CUBEMAP_NEGATIVEZ)
#define DDSCAPS2_CUBEMAP_NEGATIVEZ 0x00008000
#endif

#if !defined(DDSCAPS2_VOLUME)
#define DDSCAPS2_VOLUME 0x00200000
#endif

#endif