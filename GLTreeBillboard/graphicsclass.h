////////////////////////////////////////////////////////////////////////////////
// Filename: GraphicsClass.h
////////////////////////////////////////////////////////////////////////////////
#ifndef _GRAPHICSCLASS_H_
#define _GRAPHICSCLASS_H_

///////////////////////
// INCLUDES //
///////////////////////
#include "GDevice.h"
#include "Camera\OcelotCamera.h"
#include "Camera\OcelotCameraFactory.h"
#include "Meshes\GeometryHelper.h"
#include "Lights\OcelotLight.h"
#include "Materials\Material.h"
#include "Meshes\Wave.h"
#include "Effects\Effects.h"
#include "Effects\InputLayouts.h"
#include "TextureManager\TextureManager.h"

using namespace Ocelot;

class TrailLine;
class VolumetricLine;

/////////////
// GLOBALS //
/////////////
const BBOOL  FULL_SCREEN   = false;
const BBOOL  VSYNC_ENABLED = true;
const FFLOAT SCREEN_DEPTH  = 1.0f;
const FFLOAT SCREEN_NEAR   = 0.1f;

///////////////////////
// Rendering Options //
///////////////////////
enum RenderOptions
{
	eLighting = 0,
	eTextures = 1,
	eTexturesAndFog = 2
};

////////////////////////////////////////////////////////////////////////////////
// Class name: GraphicsClass
////////////////////////////////////////////////////////////////////////////////
class GraphicsClass
{
public:

	// Constructor & Destructor
	GraphicsClass();
	~GraphicsClass();

	// Methods
	BBOOL Initialize(OpenGLClass* pGDevice, BIGINT pClientWidth, BIGINT pClientHeight);
	VVOID OnResize(BIGINT pClientWidth, BIGINT pClientHeight);
	VVOID Shutdown();
	BBOOL Update(FFLOAT pDeltaTime);
	BBOOL Render();
	BBOOL RenderGui();

	// Accessors
	IOcelotCameraController* Controller();
	VVOID SetOption(RenderOptions pOption);
	VVOID SwitchAlphaToCoverageOn();
	VVOID SwitchAlphaToCoverageOff();

	inline BBOOL IsCloseRequested() const { return this->mRequestWindowClose; }
	inline BBOOL IsMinimizeRequested() const { return this->mRequestWindowMinimize; }
	inline BBOOL IsMaximizeRequested() const { return this->mRequestWindowMaximize; }
	inline VVOID MinimizedDone() { this->mRequestWindowMinimize = false; }
	inline VVOID MaximizedDone() { this->mRequestWindowMaximize = false; }

private:

	// Forbidden
	GraphicsClass(const GraphicsClass&);

	// Private Methods
	FFLOAT  GetHillHeight( FFLOAT pX, FFLOAT pZ ) const;
	Vector3 GetHillNormal( FFLOAT pX, FFLOAT pZ ) const;
	VVOID   BuildLandGeometryBuffers();
	VVOID   BuildWaveGeometryBuffers();
	VVOID   BuildCrateGeometryBuffers();
	VVOID   BuildTreeSpritesBuffer();

	// New...
	VVOID	BuildTrailEndpointsGeometryBuffers(Vector3 pStart, Vector3 pEnd);
	VVOID   BuildTrailGeometryBuffers(Vector3 pStart, Vector3 pEnd);

	// Draw methods.
	VVOID   DrawTreeSprites(const Matrix4x4& pViewProj);
	VVOID   DrawHillAndWave(const Matrix4x4& pViewProj);

	// Wild sheep...
	VVOID   DrawTrail(const Matrix4x4& pViewProj);

	VVOID   VolumetricTest();

private:

	static const UBIGINT sTreeCount = 16;

	// Attributes
	// Graphic device
	OpenGLClass*             mOpenGL;

	// Camera
	OcelotCamera*            mCamera;
	FFLOAT					 mCameraSpeed;
	IOcelotCameraController* mController;

	// Clear color.
	Vector4					 mClearColor;
	
	// Buffers
	UBIGINT					 mLandVAO;
	UBIGINT					 mLandVB;
	UBIGINT					 mLandIB;
	UBIGINT					 mWaveVAO;
	UBIGINT					 mWaveVB;
	UBIGINT					 mWaveIB;
	UBIGINT					 mBoxVAO;
	UBIGINT					 mBoxVB;
	UBIGINT					 mBoxIB;
	UBIGINT					 mTreeSpritesVAO;
	UBIGINT					 mTreeSpritesVB;

	// Wild sheep.
	UBIGINT					 mEndpointsVAO;
	UBIGINT					 mEndpointsVB;
	UBIGINT					 mEndpointsIB;

	VolumetricLine*			 mLine;
	TrailLine*				 mTrail;

	// Wave
	Wave					 mWaves;

	// Lights
	Ocelot::OcelotParallelLight mDirLights[3];
	Material				    mLandMat;
	Material				    mWaveMat;
	Material					mBoxMat;
	Material					mTreeMat;
	Material					mTrailMat;

	// Transforms
	// Define transformations from local spaces to world space.
	Matrix4x4		 mLandWorld;
	Matrix4x4		 mWaveWorld;
	Matrix4x4		 mBoxWorld;
	Matrix4x4		 mLandTexTransform;
	Matrix4x4		 mWaveTexTransform;
	Matrix4x4		 mTreeTexTransform;

	// Wild sheep.
	Matrix4x4		 mEndpoint1World;
	Matrix4x4		 mEndpoint2World;
	Matrix4x4		 mTrailWorld;
	Matrix4x4		 mTrailTexTransform;
	FFLOAT			 mTrailSpeedFactor;
	FFLOAT			 mTrailAmplitude;
	BBOOL			 mTrailOndulate;

	// Index and Offset Count
	UBIGINT			 mLandIndexCount;
	Vector2			 mWaterTexOffset;

	// Wild sheep.
	UBIGINT			 mEndpointIndexCount;

	// Volumetric test.
	FFLOAT			 mLineRadius;

	// Texture Manager
	TextureManager   mTexManager;
	UBIGINT			 mLandMapRV;
	UBIGINT			 mWaveMapRV;
	UBIGINT			 mBoxMapRV;
	UBIGINT			 mTreeTexMapArrayRV;

	// ImGui.
	UBIGINT			 mCloseRV;
	UBIGINT			 mMinimizeRV;
	UBIGINT			 mMaximizeRV;

	// Texture unit.
	UBIGINT			 mDiffuseTexUnit;

	// Effects
	EffectTechnique* mLight3Tech;
	EffectTechnique* mLight3TexTech;
	EffectTechnique* mLight3TexFogTech;
	EffectTechnique* mLight3TexAlphaClipTech;
	EffectTechnique* mLight3TexAlphaClipFogTech;

	// Tree billboard
	EffectTechnique* mTreeLight3Tech;
	EffectTechnique* mTreeLight3TexAlphaClipTech;
	EffectTechnique* mTreeLight3TexAlphaClipFogTech;

	// Trail line
	EffectTechnique* mTrailLineTech;
	EffectTechnique* mTrailLineTexTech;

	// Volumetric line
	EffectTechnique* mVolumetricLineTech;
	EffectTechnique* mVolumetricLineTexTech;

	EffectTechnique* mBoxTech;
	EffectTechnique* mLandNWaveTech;
	EffectTechnique* mTreeSpritesTech;

	// Wild sheep.
	EffectTechnique* mEndpointsTech;
	EffectTechnique* mTrailTech;

	// Volumetric test
	EffectTechnique* mVolumetricTech;

	// Options
	RenderOptions    mRenderOptions;

	// 
	BBOOL			 mAlphaToCoverageOn;

	// Requests.
	BBOOL			 mRequestWindowMaximize;
	BBOOL			 mRequestWindowMinimize;
	BBOOL			 mRequestWindowClose;
};

#endif