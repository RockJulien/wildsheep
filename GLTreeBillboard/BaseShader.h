#ifndef DEF_BASESHADER_H
#define DEF_BASESHADER_H

#include <fstream>
#include "IShader.h"

class BaseShader : public IShader
{
protected:

		// Attributes
		USMALLINT m_iVertShaderID;  // vertex shader stage.
		USMALLINT m_iPixShaderID;   // pixel shader stage
		USMALLINT m_iMeshShader;    // overall glsl shader

		// Private Methods
		CCHAR* LoadShaderFromFile(CCHAR* shaderPath);

		// Shader debug functions
virtual VVOID  ShaderErrorReport(GDevice* device, HWND winInst, USMALLINT shaderID, CCHAR* shaderPath) = 0;
virtual VVOID  LinkerErrorReport(GDevice* device, HWND winInst, USMALLINT shaderID) = 0;

public:

		// Constructor & Destructor
		BaseShader();
virtual ~BaseShader();

		// Methods
virtual BBOOL Initialize(GDevice* device, HWND winInst) = 0;
virtual BBOOL UpdateShader(GDevice* device, FFLOAT* world, FFLOAT* view, FFLOAT* proj,
					       FFLOAT* worldInv, OcelotSpotLight* light, OcelotFog* fog, 
					       FFLOAT* eye, BIGINT* textUnit = NULL) = 0;
virtual VVOID Release(GDevice* device) = 0;
};

#endif