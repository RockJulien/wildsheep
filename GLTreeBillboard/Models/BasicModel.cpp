#include "BasicModel.h"
#include "..\Effects\InputLayouts.h"
#include "..\M3DPotentialPlugin\M3DLoader.h"
#include "..\M3DPotentialPlugin\M3DMaterial.h"

// Namespaces used
using namespace std;
using namespace Ocelot;

/****************************************************************************************************/
BasicModel::BasicModel(GDevice* pDevice, TextureManager& pTextureService, const string& pModelFilename, const string& pTexturePath, ExtType pTextType) :
mMesh( pDevice )
{
	std::vector<M3DMaterial> lMaterials;
	M3DLoader lM3dLoader;
	lM3dLoader.LoadM3d( pModelFilename, this->mVertices, this->mIndices, this->mSubsets, lMaterials );

	this->mMesh.SetGeometry( &this->mVertices[0], 
							 (UBIGINT)this->mVertices.size(), 
							 &this->mIndices[0], 
							 (UBIGINT)this->mIndices.size(), 
							 InputLayouts::PosNormTanTexLayout() );

	this->mMesh.SetSubsetTable( this->mSubsets );

	this->mSubsetCount = (UBIGINT)lMaterials.size();

	for(UBIGINT lCurr = 0; lCurr < this->mSubsetCount; lCurr++)
	{
		this->mMaterials.push_back(lMaterials[lCurr].Mat());

		BIGINT lDiffuseMapRV = pTextureService.CreateTexture(pTexturePath + lMaterials[lCurr].DiffuseMapName(), pTextType);
		if( lDiffuseMapRV != -1 )
		{
			this->mDiffuseMap.push_back( lDiffuseMapRV );
		}

		BIGINT lNormalMapRV  = pTextureService.CreateTexture(pTexturePath + lMaterials[lCurr].NormalMapName(), pTextType);
		if( lNormalMapRV != -1 )
		{
			this->mNormalMap.push_back( lNormalMapRV );
		}
	}
}

/****************************************************************************************************/
BasicModel::~BasicModel()
{

}

/****************************************************************************************************/
vector<Material>&				 BasicModel::Materials()
{
	return this->mMaterials;
}

/****************************************************************************************************/
vector<UBIGINT>&				 BasicModel::DiffuseMap()
{
	return this->mDiffuseMap;
}

/****************************************************************************************************/
vector<UBIGINT>&				 BasicModel::NormalMap()
{
	return this->mNormalMap;
}

/****************************************************************************************************/
vector<EnhancedVertex>&			 BasicModel::Vertices()
{
	return this->mVertices;
}

/****************************************************************************************************/
vector<UBIGINT>&				 BasicModel::Indices()
{
	return this->mIndices;
}

/****************************************************************************************************/
vector<Subset>&					 BasicModel::Subsets()
{
	return this->mSubsets;
}

/****************************************************************************************************/
MeshGeometry<EnhancedVertex>&	 BasicModel::Mesh()
{
	return this->mMesh;
}

/****************************************************************************************************/
UBIGINT							 BasicModel::SubsetCount() const
{
	return this->mSubsetCount;
}
