#include "SkinnedModel.h"
#include "..\Effects\InputLayouts.h"
#include "..\M3DPotentialPlugin\M3DLoader.h"
#include "..\M3DPotentialPlugin\M3DMaterial.h"

// Namespaces used
using namespace std;
using namespace Ocelot;

/***************************************************************************************/
SkinnedModel::SkinnedModel(GDevice* pDevice, TextureManager& pTextureService, const string& pModelFilename, const string& pTexturePath, ExtType pTextType) :
mMesh( pDevice )
{
	vector<M3DMaterial> lMaterials;
	M3DLoader lM3dLoader;
	lM3dLoader.LoadM3d(pModelFilename, this->mVertices, this->mIndices, this->mSubsets, lMaterials, this->mSkinnedData);

	this->mMesh.SetGeometry( &this->mVertices[0], 
							 (UBIGINT)this->mVertices.size(), 
							 &this->mIndices[0], 
							 (UBIGINT)this->mIndices.size(), 
							 InputLayouts::PosNormTanTexSkinnedLayout() );

	this->mMesh.SetSubsetTable( this->mSubsets );

	this->mSubsetCount = (UBIGINT)lMaterials.size();

	for(UBIGINT lCurr = 0; lCurr < this->mSubsetCount; lCurr++)
	{
		this->mMaterial.push_back(lMaterials[lCurr].Mat());

		BIGINT lDiffuseMapRV = pTextureService.CreateTexture(pTexturePath + lMaterials[lCurr].DiffuseMapName(), pTextType);
		if( lDiffuseMapRV != -1 )
		{
			this->mDiffuseMap.push_back( lDiffuseMapRV );
		}

		BIGINT lNormalMapRV  = pTextureService.CreateTexture(pTexturePath + lMaterials[lCurr].NormalMapName(), pTextType);
		if( lNormalMapRV != -1 )
		{
			this->mNormalMap.push_back( lNormalMapRV );
		}
	}
}

/***************************************************************************************/
SkinnedModel::~SkinnedModel()
{

}

/***************************************************************************************/
vector<Material>&					SkinnedModel::Material()
{
	return this->mMaterial;
}

/***************************************************************************************/
vector<UBIGINT>&					SkinnedModel::DiffuseMap()
{
	return this->mDiffuseMap;
}

/***************************************************************************************/
vector<UBIGINT>&					SkinnedModel::NormalMap()
{
	return this->mNormalMap;
}

/***************************************************************************************/
vector<SkinnedVertex>&				SkinnedModel::Vertices()
{
	return this->mVertices;
}

/***************************************************************************************/
vector<UBIGINT>&					SkinnedModel::Indices()
{
	return this->mIndices;
}

/***************************************************************************************/
vector<Subset>&						SkinnedModel::Subsets()
{
	return this->mSubsets;
}

/***************************************************************************************/
MeshGeometry<SkinnedVertex>&		SkinnedModel::Mesh()
{
	return this->mMesh;
}

/***************************************************************************************/
SkinnedData&					    SkinnedModel::SkinnedData()
{
	return this->mSkinnedData;
}

/***************************************************************************************/
UBIGINT							    SkinnedModel::SubsetCount() const
{
	return this->mSubsetCount;
}
