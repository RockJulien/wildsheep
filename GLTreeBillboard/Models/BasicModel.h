#ifndef DEF_BASICMODEL_H
#define DEF_BASICMODEL_H

// Includes
#include <string>
#include <vector>
#include "..\Meshes\Subset.h"
#include "..\GDevice.h"
#include "..\Materials\Material.h"
#include "..\VertexTypes.h"
#include "..\Meshes\MeshGeometry.h"
#include "..\TextureManager\TextureManager.h"

// Class definition
class BasicModel
{
private:

	// Attributes
	std::vector<Material>				 mMaterials;
	std::vector<UBIGINT>				 mDiffuseMap;
	std::vector<UBIGINT>				 mNormalMap;
	std::vector<Ocelot::EnhancedVertex>  mVertices;
	std::vector<UBIGINT>				 mIndices;
	std::vector<Subset>					 mSubsets;
	MeshGeometry<Ocelot::EnhancedVertex> mMesh;
	UBIGINT								 mSubsetCount;

public:

	// Constructor
	BasicModel(GDevice* pDevice, TextureManager& pTextureService, const std::string& pModelFilename, const std::string& pTexturePath, ExtType pTextType);
	~BasicModel();

	// Methods


	// Accessors
	std::vector<Material>&				  Materials();
	std::vector<UBIGINT>&				  DiffuseMap();
	std::vector<UBIGINT>&				  NormalMap();
	std::vector<Ocelot::EnhancedVertex>&  Vertices();
	std::vector<UBIGINT>&				  Indices();
	std::vector<Subset>&				  Subsets();
	MeshGeometry<Ocelot::EnhancedVertex>& Mesh();
	UBIGINT								  SubsetCount() const;

};

#endif