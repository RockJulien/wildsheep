#ifndef DEF_SKINNEDMODELINSTANCE_H
#define DEF_SKINNEDMODELINSTANCE_H

// Includes
#include <string>
#include <vector>
#include "SkinnedModel.h"
#include "..\Maths\Matrix4x4.h"

// Class definition
class SkinnedModelInstance
{
public:

	// Constructor
	SkinnedModelInstance();
	~SkinnedModelInstance();

	// Attributes
	SkinnedModel* Model;
	float		  TimePos;
	std::string   ClipName;
	Matrix4x4	  World;
	std::vector<Matrix4x4> FinalTransforms;

	// Methods
	void Update(FFLOAT pDeltaTime);

};

#endif