#include "OcelotCameraInfo.h"

using namespace Ocelot;

OcelotCameraInfo::OcelotCameraInfo() :
m_vEyePos(0.0f, 80.0f, -10.0f), m_vLAtPos(0.0f), m_vUpVect(0.0f, 1.0f, 0.0f), m_fFovy(PI * 0.25f), m_fAspect(0.75f), m_fNearPlane(0.1f), m_fFarPlane(2000.0f)
{

}

OcelotCameraInfo::OcelotCameraInfo(Vector3 eyePos, Vector3 lookAt, FFLOAT aspect, FFLOAT fFar, Vector3 up, FFLOAT fov, FFLOAT fNear) :
m_vEyePos(eyePos), m_vLAtPos(lookAt), m_vUpVect(up), m_fFovy(fov), m_fAspect(aspect), m_fNearPlane(fNear), m_fFarPlane(fFar)
{

}

OcelotCameraInfo::~OcelotCameraInfo()
{

}
