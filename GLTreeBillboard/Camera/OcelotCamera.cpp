#include "OcelotCamera.h"

using namespace Ocelot;

OcelotCamera::OcelotCamera() :
IOcelotCamera(), m_info()
{
	m_matView.Matrix4x4Identity();
	m_matProj.Matrix4x4Identity();
	m_frustrumPlanes.resize(6);
}

OcelotCamera::OcelotCamera(OcelotCameraInfo info) :
IOcelotCamera(), m_info(info)
{
	m_matView.Matrix4x4Identity();
	m_matProj.Matrix4x4Identity();
	m_frustrumPlanes.resize(6);
}

OcelotCamera::~OcelotCamera()
{

}

VVOID OcelotCamera::update(FFLOAT deltaTime)
{
	// update view matrix
	m_matView = m_matView.MatrixViewLookAtLH(m_info.Eye(), m_info.LookAt(), m_info.Up());

	// update projection matrix
	m_matProj = m_matProj.MatrixProjectionPerspectiveFOVLH(m_info.Fov(), m_info.Aspect(), m_info.Near(), m_info.Far());
}

VVOID OcelotCamera::computeFrustumPlane()
{
	// compute the world matrix
	Matrix4x4 world;
	m_matView.Matrix4x4Mutliply(world, m_matProj);

	// near plane
	Plane nearPlane(world.get_14() + world.get_13(), world.get_24() + world.get_23(), world.get_34() + world.get_33(), world.get_44() + world.get_43());
	// normalize near plane
	nearPlane.PlaneNormalize();
	// far plane
	Plane farPlane(world.get_14() - world.get_13(), world.get_24() - world.get_23(), world.get_34() - world.get_33(), world.get_44() - world.get_43());
	// normalize far plane
	farPlane.PlaneNormalize();
	// left plane
	Plane leftPlane(world.get_14() + world.get_11(), world.get_24() + world.get_21(), world.get_34() + world.get_31(), world.get_44() + world.get_41());
	// normalize left plane
	leftPlane.PlaneNormalize();
	// right plane
	Plane rightPlane(world.get_14() - world.get_11(), world.get_24() - world.get_21(), world.get_34() - world.get_31(), world.get_44() - world.get_41());
	// normalize right plane
	rightPlane.PlaneNormalize();
	// top plane
	Plane topPlane(world.get_14() - world.get_12(), world.get_24() - world.get_22(), world.get_34() - world.get_32(), world.get_44() - world.get_42());
	// normalize top plane
	topPlane.PlaneNormalize();
	// bottom plane
	Plane bottomPlane(world.get_14() + world.get_12(), world.get_24() + world.get_22(), world.get_34() + world.get_32(), world.get_44() + world.get_42());
	// normalize bottom plane
	bottomPlane.PlaneNormalize();

	// then, store planes
	m_frustrumPlanes[0] = nearPlane;
	m_frustrumPlanes[1] = farPlane;
	m_frustrumPlanes[2] = leftPlane;
	m_frustrumPlanes[3] = rightPlane;
	m_frustrumPlanes[4] = topPlane;
	m_frustrumPlanes[5] = bottomPlane;
}

BBOOL OcelotCamera::cullMesh(const FFLOAT& posX, const FFLOAT& posY, const FFLOAT& posZ, const FFLOAT& radius)
{
	Vector3 meshPos(posX, posY, posZ);

	for(USMALLINT currPlane = 0; currPlane < (USMALLINT)m_frustrumPlanes.size(); currPlane++)
	{
		// Compute distance between mesh center position and each plane.
		FFLOAT dist = distanceToPlane(meshPos, m_frustrumPlanes[currPlane]);

		// Check if the distance is greater than the mesh's radius.
		if(dist < -radius)
		{
			// Draw
			return false;
		}
	}

	// Do not draw.
	return true;
}

FFLOAT OcelotCamera::distanceToPlane(const Vector3& position, Plane plane)
{
	return plane.PlaneDotCoord(position);
}
