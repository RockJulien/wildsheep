#include "OcelotFlyerController.h"

using namespace Ocelot;

OcelotFlyerController::OcelotFlyerController() : 
OcelotOrbiterController(), m_fSpeed(20.0f)
{

}

OcelotFlyerController::OcelotFlyerController(OcelotCamera* camera, FFLOAT speed) : 
OcelotOrbiterController(camera), m_fSpeed(speed)
{

}

OcelotFlyerController::~OcelotFlyerController()
{

}

VVOID OcelotFlyerController::update(FFLOAT deltaTime)
{
	m_fDeltaTime = deltaTime;

	// update the camera.
	m_instance->update(deltaTime);

	// apply movements
	applyMovements();
}

VVOID OcelotFlyerController::pitch(FFLOAT angle)
{
	OcelotOrbiterController::pitch(angle);
}

VVOID OcelotFlyerController::yaw(FFLOAT angle)
{
	OcelotOrbiterController::yaw(angle);
}

VVOID OcelotFlyerController::roll(FFLOAT angle)
{
	// only for flying simulator camera.
	// very special.
}

VVOID OcelotFlyerController::move(Direction dir)
{
	// grab useful attributes.
	OcelotCameraInfo tempInfo = m_instance->Info();

	Vector3 eye    = tempInfo.Eye();
	Vector3 lookAt = tempInfo.LookAt();
	Vector3 lUp    = tempInfo.Up();

	switch(dir)
	{
	case Forward:
		eye += (m_fSpeed * m_fDeltaTime) * lookAt;
		break;
	case Backward:
		eye += -(m_fSpeed * m_fDeltaTime) * lookAt;
		break;
	case LeftSide:
		eye += -(m_fSpeed * m_fDeltaTime) * m_vRight;
		break;
	case RightSide:
		eye += (m_fSpeed * m_fDeltaTime) * m_vRight;
		break;
	case Up:
		eye += (m_fSpeed * m_fDeltaTime) * lUp;
		break;
	case Down:
		eye += -(m_fSpeed * m_fDeltaTime) * lUp;
		break;
	}

	// reset camera's attributes.
	tempInfo.SetEye(eye);
	m_instance->SetInfo(tempInfo);
}
