#ifndef DEF_OCELOTORBITERCONTROLLER_H
#define DEF_OCELOTORBITERCONTROLLER_H

#include "OcelotCamera.h"
#include "..\Maths\Vector3.h"
#include "..\Maths\MatrixView.h"
#include "IOcelotCameraController.h"

namespace Ocelot
{
	class OcelotOrbiterController : public IOcelotCameraController
	{
	protected:

			// Attributes
			OcelotCamera*  m_instance;
			Vector3        m_vRight;

			// Private methods
			Vector3 toVec3(const Vector4& toCast);
	virtual VVOID   applyMovements();

	public:

			// Constructor & Destructor
			OcelotOrbiterController();
			OcelotOrbiterController(OcelotCamera* camera);
	virtual ~OcelotOrbiterController();

			// Methods
	virtual VVOID update(FFLOAT deltaTime);
	virtual VVOID move(Direction dir);
	virtual VVOID pitch(FFLOAT angle);
	virtual VVOID yaw(FFLOAT angle);
	virtual VVOID roll(FFLOAT angle);

	// Accessors
	virtual FFLOAT Speed() const override { return 0.0; }
	virtual VVOID  SetSpeed(FFLOAT speed) override { }

			// Accessors
	inline  OcelotCamera* GetCamera() const { return m_instance;}
	};
}

#endif