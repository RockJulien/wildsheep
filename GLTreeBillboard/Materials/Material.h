#ifndef DEF_MATERIAL_H
#define DEF_MATERIAL_H

// Includes
#include "..\Maths\Color.h"

// Class definition
class Material
{
public:

	Ocelot::Color Ambient;
	Ocelot::Color Diffuse;
	Ocelot::Color Specular; // w = SpecPower
	Ocelot::Color Reflect;

	// Constructor
	Material();
	~Material();
};

#endif