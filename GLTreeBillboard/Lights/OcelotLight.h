#ifndef DEF_OCELOTLIGHT_H
#define DEF_OCELOTLIGHT_H

/*****************************************************************
/  (c) Copyright 2012-2015 OcelotEngine All right reserved.
/
/  File name:   OcelotLight.h/cpp
/   
/  Description: This file provides a classes for storing light's
/               data needed for 3D proper lighting.
/               Three classes for representing the three different
/               types of light.
/
/  Author:      Larbi Julien
/  Version:     1.0
/  Creation:    03/10/2012
/*****************************************************************/

#include "..\OSCheck.h"
#include "..\Maths\Vector3.h"
#include "..\Maths\Color.h"

namespace Ocelot
{
								enum OcelotLightType
								{
									LIGHTTYPE_PARALLEL,
									LIGHTTYPE_POINT,
									LIGHTTYPE_SPOT
								};

	class OcelotParallelLight
	{
	private:

		// Attributes
		Color    m_cAmbient;
		Color    m_cDiffuse;
		Color    m_cSpecular;
		Vector3  m_vDirection;
		FFLOAT   m_fSpecularPower;

	public:

		// Constructor & Destructor
		OcelotParallelLight();
		OcelotParallelLight(Vector3 direction, Color ambient, Color diffuse, Color specular, FFLOAT power = 5.0f);
		~OcelotParallelLight();

		// Methods


		// Accessors
		inline Vector3 Direction() const { return m_vDirection;}
		inline Color   Ambient() const { return m_cAmbient;}
		inline Color   Diffuse() const { return m_cDiffuse;}
		inline Color   Specular() const { return m_cSpecular;}
		inline FFLOAT  SpecPower() const { return m_fSpecularPower;}
		inline VVOID   SetDirection(Vector3 direction) { m_vDirection = direction;}
		inline VVOID   SetAmbient(Color ambient) { m_cAmbient = ambient;}
		inline VVOID   SetDiffuse(Color diffuse) { m_cDiffuse = diffuse;}
		inline VVOID   SetSpecular(Color specular) { m_cSpecular = specular;}
		inline VVOID   SetSpecPower(FFLOAT power) { m_fSpecularPower = power;}
	
		// operators overload
		/*********************Cast Operators**************************/
		operator float* ();              // for being able to pass it through the shader as a (float*)
		operator const float* () const;  // constant version
	
	};

	class OcelotPointLight
	{
	private:

		// Attributes
		Color    m_cAmbient;
		Color    m_cDiffuse;
		Color    m_cSpecular;
		Vector3  m_vPosition;
		Vector3  m_vAttenuation;
		FFLOAT   m_fRange;
		FFLOAT   m_fSpecularPower;

	public:

		// Constructor & Destructor
		OcelotPointLight();
		OcelotPointLight(Vector3 position, Vector3 attenuation, Color ambient, Color diffuse, Color specular, FFLOAT range = 50.0f, FFLOAT power = 5.0f);
		~OcelotPointLight();

		// Methods


		// Accessors
		inline Vector3 Position() const { return m_vPosition;}
		inline Vector3 Attenuation() const { return m_vAttenuation;}
		inline Color   Ambient() const { return m_cAmbient;}
		inline Color   Diffuse() const { return m_cDiffuse;}
		inline Color   Specular() const { return m_cSpecular;}
		inline FFLOAT  Range() const { return m_fRange;}
		inline FFLOAT  SpecPower() const { return m_fSpecularPower;}
		inline VVOID   SetPosition(Vector3 position) { m_vPosition = position;}
		inline VVOID   SetAttenuation(Vector3 attenuation) { m_vAttenuation = attenuation;}
		inline VVOID   SetAmbient(Color ambient) { m_cAmbient = ambient;}
		inline VVOID   SetDiffuse(Color diffuse) { m_cDiffuse = diffuse;}
		inline VVOID   SetSpecular(Color specular) { m_cSpecular = specular;}
		inline VVOID   SetRange(FFLOAT range) { m_fRange = range;}
		inline VVOID   SetSpecPower(FFLOAT power) { m_fSpecularPower = power;}
	
		// operators overload
		/*********************Cast Operators**************************/
		operator float* ();              // for being able to pass it through the shader as a (float*)
		operator const float* () const;  // constant version
	
	};

	class OcelotSpotLight
	{
	private:

		// Attributes
		Color    m_cAmbient;
		Color    m_cDiffuse;
		Color    m_cSpecular;
		Vector3  m_vPosition;
		Vector3  m_vDirection;
		Vector3  m_vAttenuation;
		FFLOAT   m_fRange;
		FFLOAT   m_fSpotPower;
		FFLOAT   m_fSpecularPower;

	public:

		// Constructor & Destructor
		OcelotSpotLight();
		OcelotSpotLight(Vector3 position, Vector3 direction, Vector3 attenuation, Color ambient, Color diffuse, Color specular, FFLOAT range = 50.0f, FFLOAT spotPower = 50.0f, FFLOAT specPower = 5.0f);
		~OcelotSpotLight();

		// Methods


		// Accessors
		inline Vector3 Position() const { return m_vPosition;}
		inline Vector3 Direction() const { return m_vDirection;}
		inline Vector3 Attenuation() const { return m_vAttenuation;}
		inline Color   Ambient() const { return m_cAmbient;}
		inline Color   Diffuse() const { return m_cDiffuse;}
		inline Color   Specular() const { return m_cSpecular;}
		inline FFLOAT  Range() const { return m_fRange;}
		inline FFLOAT  SpotPower() const { return m_fSpotPower;}
		inline FFLOAT  SpecPower() const { return m_fSpecularPower;}
		inline VVOID   SetPosition(Vector3 position) { m_vPosition = position;}
		inline VVOID   SetDirection(Vector3 direction) { m_vDirection = direction;}
		inline VVOID   SetAttenuation(Vector3 attenuation) { m_vAttenuation = attenuation;}
		inline VVOID   SetAmbient(Color ambient) { m_cAmbient = ambient;}
		inline VVOID   SetDiffuse(Color diffuse) { m_cDiffuse = diffuse;}
		inline VVOID   SetSpecular(Color specular) { m_cSpecular = specular;}
		inline VVOID   SetRange(FFLOAT range) { m_fRange = range;}
		inline VVOID   SetSpotPower(FFLOAT power) { m_fSpotPower = power;}
		inline VVOID   SetSpecPower(FFLOAT power) { m_fSpecularPower = power;}
	
		// operators overload
		/*********************Cast Operators**************************/
		operator float* ();              // for being able to pass it through the shader as a (float*)
		operator const float* () const;  // constant version
	
	};

	// general structure for the light
	// created due to padding probs
	// when an attribute has odd number
	// of components.
	struct OcelotLight
	{
		// Attributes
		Color    m_cAmbient;
		Color    m_cDiffuse;
		Color    m_cSpecular;
		Vector3  m_vPosition;
		FFLOAT   m_pad1; // padding (not used)
		Vector3  m_vDirection;
		FFLOAT   m_pad2; // padding (not used)
		Vector3  m_vAttenuation;
		FFLOAT   m_fRange;
		FFLOAT   m_fSpotPower;
		FFLOAT   m_fSpecularPower;

		OcelotLight() :
		m_cAmbient(OcelotColor::GREY), m_cDiffuse(OcelotColor::LIGHTGREY), m_cSpecular(OcelotColor::DARKGREY), m_vPosition(0.0f, 50.0f, 50.0f), m_vDirection(Vector3(0.0f, 0.0f, 0.0f) - m_vPosition), 
		m_vAttenuation(0.0f, 1.0f, 0.0f), m_fRange(50.0f), m_fSpotPower(50.0f), m_fSpecularPower(5.0f) {}
		OcelotLight(Vector3 position, Vector3 direction, Vector3 attenuation, Color ambient, Color diffuse, Color specular, FFLOAT range = 50.0f, FFLOAT spotPower = 50.0f, FFLOAT specPower = 5.0f) :
		m_cAmbient(ambient), m_cDiffuse(diffuse), m_cSpecular(specular), m_vPosition(position), m_vDirection(direction), 
		m_vAttenuation(attenuation), m_fRange(range), m_fSpotPower(spotPower), m_fSpecularPower(specPower) {}
	};
}

#endif