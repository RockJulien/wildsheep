////////////////////////////////////////////////////////////////////////////////
// Filename: Main.cpp
////////////////////////////////////////////////////////////////////////////////
#include "systemclass.h"

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PSTR pScmdline, int iCmdshow)
{
//#if defined (DEBUG) || defined (_DEBUG)
//	//_CrtSetBreakAlloc( 192 );
//#endif

	// Enable run-time memory check for debug builds.
#if defined(DEBUG) | defined(_DEBUG)
	_CrtSetDbgFlag( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );
#endif

	SystemClass* lSystem;
	BBOOL lResult;
	
	// Create the system object.
	lSystem = new SystemClass;
	if ( lSystem == NULL )
	{
		return 0;
	}

	// Initialize and run the system object.
	lResult = lSystem->Initialize();
	if ( lResult )
	{
		lSystem->Run();
	}

	// Shutdown and release the system object.
	lSystem->Shutdown();
	DeletePointer( lSystem );
	
//#if defined (DEBUG) || defined (_DEBUG)
//	// Check for memory leaks when destroying the application.
//	_CrtMemDumpAllObjectsSince( NULL );
//#endif

	return 0;
}
