#ifndef DEF_OSCHECK_H
#define DEF_OSCHECK_H

#if defined (WIN32)
#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif
#include <Windows.h>
// Other win-based stuff.
#if defined(DEBUG) || defined(_DEBUG)
	#ifndef D3D_DEBUG_INFO
	#define D3D_DEBUG_INFO
	#endif
#endif

#if defined(DEBUG) || defined(_DEBUG)
#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>
// Will provide the line and the file when memory leaks occur
// because of a "new".
//#define DEBUG_NEW new(_CLIENT_BLOCK, __FILE__, __LINE__)
//#define new DEBUG_NEW
#else
#define DEBUG_NEW 
#endif

#if defined(DXAPI)
#define ReleaseCOM(x) {if(x){x->Release(); x = NULL;}}
#endif

#define DeletePointer(p) {if(p){delete p; p = NULL;}}
#define KEY_DOWN(vk_code) ((GetAsyncKeyState(vk_code) & 0x8000) ? 1 : 0)
#define KEY_UP(vk_code) ((GetAsyncKeyState(vk_code) & 0x8000) ? 0 : 1)
#define WAS_PRESSED(vk_code) ((GetKeyState(vk_code) & 0x8000) != 0)

typedef long long      BIGLINT;
typedef long           LINT;
typedef int            BIGINT;
typedef short          SMALLINT;
typedef float          FFLOAT;
typedef bool           BBOOL;
typedef double         DFLOAT;
typedef char           CCHAR;
typedef wchar_t        WCHAR;
typedef unsigned long  ULINT;
typedef unsigned int   UBIGINT;
typedef unsigned short USMALLINT;
typedef unsigned char  UCCHAR;
typedef void           VVOID;
typedef void*          UNKNOWN;

#if defined(DEBUG) | defined(_DEBUG)
	/*#ifndef HR
	#define HR(x){HRESULT hr = (x); if(FAILED(hr)) DXTrace(__FILE__, (DWORD)__LINE__, hr, L#x, true);}
	#endif*/
	#ifndef HR
	#define HR(x) (x)
	#endif
#else
	#ifndef HR
	#define HR(x) (x)
	#endif
#endif

#endif

#if defined (MACOS)
// Same for Mac.
#endif

#if defined (LINUX)
// Same for Linux.
#endif

class IOSManager
{
public:

		// Constructor & Destructor
		IOSManager(){}
virtual ~IOSManager(){}

		// Purely Virtual Methods.
virtual BBOOL  Initialize() = 0;
virtual VVOID  Release()    = 0;
virtual BIGINT RunLoop()    = 0;
virtual VVOID  Resize()     = 0;

};



#endif
