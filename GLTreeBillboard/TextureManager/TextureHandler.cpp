#include "TextureHandler.h"
#include "..\GDevice.h"
#include "..\Images\TGALoader.h"
#include "..\Images\DDSLoader.h"

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>
#include <gli.hpp>
#include <glm/glm.hpp>

// Namespaces used
using namespace std;

#define MakeFourCC(ch0, ch1, ch2, ch3)                              \
				  ((ULINT)(UCCHAR)(ch0) | ((ULINT)(UCCHAR)(ch1) << 8) |   \
                  ((ULINT)(UCCHAR)(ch2) << 16) | ((ULINT)(UCCHAR)(ch3) << 24 ))

#define FOURCC_DXT1	MakeFourCC('D', 'X', 'T', '1')
#define FOURCC_DXT3	MakeFourCC('D', 'X', 'T', '3')
#define FOURCC_DXT5	MakeFourCC('D', 'X', 'T', '5')

/****************************************************************************************************/
BIGINT TextureHandler::LoadBMP(const string& pTextName, BBOOL pWrap)
{
	UBIGINT lTextureId = -1;

	BIGINT lWidth;
	BIGINT lHeight;
	BIGINT lChannelCount;

	//stbi_set_flip_vertically_on_load(true); Vertical flip if needed.

	UCCHAR* lImage = stbi_load(pTextName.c_str(), &lWidth, &lHeight, &lChannelCount, STBI_rgb );

	if (lImage == nullptr)
	{
		return -1;
	}

	lTextureId = this->CreateSingleTexture( lWidth, lHeight, lChannelCount, pWrap, lImage );

	stbi_image_free(lImage);

	return lTextureId;
}

/****************************************************************************************************/
BIGINT TextureHandler::LoadPNG(const string& pTextName, BBOOL pWrap)
{
	UBIGINT lTextureId = -1;

	BIGINT lWidth;
	BIGINT lHeight;
	BIGINT lChannelCount;

	//stbi_set_flip_vertically_on_load(true); Vertical flip if needed.

	UCCHAR* lImage = stbi_load(pTextName.c_str(), &lWidth, &lHeight, &lChannelCount, STBI_rgb_alpha );

	if (lImage == nullptr)
	{
		return -1;
	}

	lTextureId = this->CreateSingleTexture( lWidth, lHeight, lChannelCount, pWrap, lImage );

	stbi_image_free(lImage);

	return lTextureId;
}

/****************************************************************************************************/
BIGINT TextureHandler::LoadJPG(const string& pTextName, BBOOL pWrap)
{
	UBIGINT lTextureId = -1;

	BIGINT lWidth;
	BIGINT lHeight;
	BIGINT lChannelCount;

	//stbi_set_flip_vertically_on_load(true); Vertical flip if needed.

	UCCHAR* lImage = stbi_load(pTextName.c_str(), &lWidth, &lHeight, &lChannelCount, STBI_rgb );

	if (lImage == nullptr)
	{
		return -1;
	}

	lTextureId = this->CreateSingleTexture( lWidth, lHeight, lChannelCount, pWrap, lImage );

	stbi_image_free(lImage);

	return lTextureId;
}

/****************************************************************************************************/
BIGINT TextureHandler::LoadDDS(const string& pFilename, BBOOL pWrap)
{
	UBIGINT lTextureId;

	// Check if a name is passed.
	if(pFilename.empty())
	{
		return -1;
	}

	DDSLoader lLoader;
	DDSImage* lImage = lLoader.LoadDDS( pFilename.c_str() );

	if( lImage == NULL )
	{
		return -1;
	}

	// Generate an ID for the texture.
	glGenTextures(1, &lTextureId);

	// Bind the texture as a 2D texture.
	glBindTexture( GL_TEXTURE_2D, lTextureId );

	// Set the texture color to either pWrap around or clamp to the edge.
	if(pWrap)
	{
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	}
	else
	{
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	}

	// Set the texture filtering.
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

	BIGINT lMipMapLevels = lImage->GetMipMapLevels();
	BIGINT lWidth  = lImage->GetWidth();
	BIGINT lHeight = lImage->GetHeight();

	if
		( lMipMapLevels > 0 )
	{
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0 );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, lMipMapLevels - 1 );
	}

	glPixelStorei( GL_UNPACK_ROW_LENGTH, 0 );
	glPixelStorei( GL_UNPACK_SKIP_ROWS, 0 );
	glPixelStorei( GL_UNPACK_SKIP_PIXELS, 0 );

	/* load the mipmaps */
	for( BIGINT lCurrLevel = 0; lCurrLevel < lMipMapLevels; lCurrLevel++ )
	{
		// Keep size at a minimum
		if( lWidth == 0 ) 
		{
			lWidth = 1;
		}
		if( lHeight == 0 ) 
		{
			lHeight = 1;
		}

		if
			( lImage->GetPixels( lCurrLevel ) )
		{
			if
				( lImage->GetFormat() == DDS_FORMAT_RGBA8 )
			{
				glTexImage2D( GL_TEXTURE_2D, 
							  lCurrLevel, 
							  GL_RGBA, 
							  lWidth, 
							  lHeight, 
							  0, 
							  GL_RGBA, 
							  GL_UNSIGNED_BYTE, 
							  lImage->GetPixels( lCurrLevel ) );
			} 
			else
			{
				// Compressed formats
				if
					( lImage->GetFormat() == DDS_FORMAT_YCOCG )
				{
					// DXT1/3/5
					this->mDevice->CompressTextImg2D( GL_TEXTURE_2D, 
														lCurrLevel, 
														lImage->GetInternalFormat(), 
														lWidth, 
														lHeight, 
														0, 
														lImage->GetBytes( lCurrLevel ), 
														lImage->GetPixels( lCurrLevel ) );
				} 
				else
				{
					printf_s( "DDS:CreateTexture; not DDS_FORMAT_YCOCG for mipmap level %d", lCurrLevel );
				}

				// Verify that compression took place
				GLenum lError = glGetError();
				if( lError != GL_NO_ERROR )
				{
					throw exception(" Create compressed texture Failure !!! ");
				}

				GLint lParam = 0;
				glGetTexLevelParameteriv( GL_TEXTURE_2D, lCurrLevel, GL_TEXTURE_COMPRESSED, &lParam );
				if
					( lParam == 0 )
				{
					printf_s( "DDS:LoadDDS(); mipmap level %d indicated compression failed", lCurrLevel );
				}
			}

			GLint param=0;
			glGetTexLevelParameteriv( GL_TEXTURE_2D, lCurrLevel, GL_TEXTURE_INTERNAL_FORMAT, &param);
			
		} 
		else
		{
			printf_s( "DDS:LoadDDS(); missing pixels for mipmap level %d", lCurrLevel );
		}

		// Next level uses less levels
		lWidth /= 2;
		lHeight /= 2;
	}

	// Release the dds image data.
	DeletePointer( lImage );

	GLenum lError = glGetError();
	if( lError != GL_NO_ERROR )
	{
		throw exception(" Create compressed texture Failure !!! ");
	}

	return lTextureId;
}

/****************************************************************************************************/
BIGINT TextureHandler::LoadTGA(const string& pTextName, BBOOL pWrap)
{
	UBIGINT lTextureId = -1;

	BIGINT lWidth;
	BIGINT lHeight;
	BIGINT lChannelCount;

	UCCHAR* lImage = stbi_load(pTextName.c_str(), &lWidth, &lHeight, &lChannelCount, STBI_rgb_alpha );

	if (lImage == nullptr)
	{
		return -1;
	}

	lTextureId = this->CreateSingleTexture( lWidth, lHeight, lChannelCount, pWrap, lImage );

	stbi_image_free(lImage);

	return lTextureId;

	//UBIGINT lTextureId;

	//TGALoader lLoader;
	//GenericImage* lImage = lLoader.ReadTGA( pTextName );

	//// Generate an ID for the texture.
	//glGenTextures(1, &lTextureId);

	//// Bind the texture as a 2D texture.
	//glBindTexture(GL_TEXTURE_2D, lTextureId);

	//// Load the image data into the texture.
	//glTexImage2D( GL_TEXTURE_2D, 
	//			  0, 
	//			  lImage->Format(), 
	//			  lImage->Width(), 
	//			  lImage->Height(), 
	//			  0, 
	//			  lImage->Format(), 
	//			  GL_UNSIGNED_BYTE, 
	//			  lImage->Pixels() );

	//// Set the texture color to either pWrap around or clamp to the edge.
	//if(pWrap)
	//{
	//	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	//	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	//}
	//else
	//{
	//	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	//	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	//}

	//// Set the texture filtering.
	//glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY, 4.0f);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

	//// Generate mipmaps for the texture.
	//this->mDevice->GenerateMipMap(GL_TEXTURE_2D);

	//GLenum lError = glGetError();
	//if( lError != GL_NO_ERROR )
	//{
	//	throw exception(" Build Random vector texture Failure !!! ");
	//}

	//// Release the targa image data.
	//lImage->Release();
	//DeletePointer( lImage );

	//return lTextureId;
}

/****************************************************************************************************/
TextureHandler::TextureHandler(GDevice* pDevice) :
mDevice(pDevice)
{
	
}

/****************************************************************************************************/
TextureHandler::~TextureHandler()
{

}

/****************************************************************************************************/
BIGINT TextureHandler::Load(const string& pTextName)
{
	gli::texture Texture = gli::load(pTextName);
	if (Texture.empty())
		return 0;

	gli::gl GL(gli::gl::PROFILE_GL33);
	gli::gl::format const Format = GL.translate(Texture.format(), Texture.swizzles());
	GLenum Target = GL.translate(Texture.target());

	GLuint TextureName = 0;
	glGenTextures(1, &TextureName);
	glBindTexture(Target, TextureName);
	glTexParameteri(Target, GL_TEXTURE_BASE_LEVEL, 0);
	glTexParameteri(Target, GL_TEXTURE_MAX_LEVEL, static_cast<GLint>(Texture.levels() - 1));
	glTexParameteri(Target, GL_TEXTURE_SWIZZLE_R, Format.Swizzles[0]);
	glTexParameteri(Target, GL_TEXTURE_SWIZZLE_G, Format.Swizzles[1]);
	glTexParameteri(Target, GL_TEXTURE_SWIZZLE_B, Format.Swizzles[2]);
	glTexParameteri(Target, GL_TEXTURE_SWIZZLE_A, Format.Swizzles[3]);

	glTexParameterf(Target, GL_TEXTURE_MAX_ANISOTROPY, 4.0f );
	glTexParameteri(Target, GL_TEXTURE_MIN_FILTER, GL_NEAREST/*GL_LINEAR*/ );
	glTexParameteri(Target, GL_TEXTURE_MAG_FILTER, GL_NEAREST/*GL_LINEAR*/ );

	glm::tvec3<GLsizei> const Extent(Texture.extent());
	GLsizei const FaceTotal = static_cast<GLsizei>(Texture.layers() * Texture.faces());

	switch (Texture.target())
	{
	case gli::TARGET_1D:
		glTexStorage1D(
			Target, static_cast<GLint>(Texture.levels()), Format.Internal, Extent.x);
		break;
	case gli::TARGET_1D_ARRAY:
	case gli::TARGET_2D:
	case gli::TARGET_CUBE:
		glTexStorage2D(
			Target, static_cast<GLint>(Texture.levels()), Format.Internal,
			Extent.x, Texture.target() == gli::TARGET_2D ? Extent.y : FaceTotal);
		break;
	case gli::TARGET_2D_ARRAY:
	case gli::TARGET_3D:
	case gli::TARGET_CUBE_ARRAY:
		glTexStorage3D(
			Target, static_cast<GLint>(Texture.levels()), Format.Internal,
			Extent.x, Extent.y,
			Texture.target() == gli::TARGET_3D ? Extent.z : FaceTotal);
		break;
	default:
		assert(0);
		break;
	}

	for (std::size_t Layer = 0; Layer < Texture.layers(); ++Layer)
		for (std::size_t Face = 0; Face < Texture.faces(); ++Face)
			for (std::size_t Level = 0; Level < Texture.levels(); ++Level)
			{
				GLsizei const LayerGL = static_cast<GLsizei>(Layer);
				glm::tvec3<GLsizei> Extent(Texture.extent(Level));
				Target = gli::is_target_cube(Texture.target())
					? static_cast<GLenum>(GL_TEXTURE_CUBE_MAP_POSITIVE_X + Face)
					: Target;

				switch (Texture.target())
				{
				case gli::TARGET_1D:
					if (gli::is_compressed(Texture.format()))
						glCompressedTexSubImage1D(
							Target, static_cast<GLint>(Level), 0, Extent.x,
							Format.Internal, static_cast<GLsizei>(Texture.size(Level)),
							Texture.data(Layer, Face, Level));
					else
						glTexSubImage1D(
							Target, static_cast<GLint>(Level), 0, Extent.x,
							Format.External, Format.Type,
							Texture.data(Layer, Face, Level));
					break;
				case gli::TARGET_1D_ARRAY:
				case gli::TARGET_2D:
				case gli::TARGET_CUBE:
					if (gli::is_compressed(Texture.format()))
						glCompressedTexSubImage2D(
							Target, static_cast<GLint>(Level),
							0, 0,
							Extent.x,
							Texture.target() == gli::TARGET_1D_ARRAY ? LayerGL : Extent.y,
							Format.Internal, static_cast<GLsizei>(Texture.size(Level)),
							Texture.data(Layer, Face, Level));
					else
						glTexSubImage2D(
							Target, static_cast<GLint>(Level),
							0, 0,
							Extent.x,
							Texture.target() == gli::TARGET_1D_ARRAY ? LayerGL : Extent.y,
							Format.External, Format.Type,
							Texture.data(Layer, Face, Level));
					break;
				case gli::TARGET_2D_ARRAY:
				case gli::TARGET_3D:
				case gli::TARGET_CUBE_ARRAY:
					if (gli::is_compressed(Texture.format()))
						glCompressedTexSubImage3D(
							Target, static_cast<GLint>(Level),
							0, 0, 0,
							Extent.x, Extent.y,
							Texture.target() == gli::TARGET_3D ? Extent.z : LayerGL,
							Format.Internal, static_cast<GLsizei>(Texture.size(Level)),
							Texture.data(Layer, Face, Level));
					else
						glTexSubImage3D(
							Target, static_cast<GLint>(Level),
							0, 0, 0,
							Extent.x, Extent.y,
							Texture.target() == gli::TARGET_3D ? Extent.z : LayerGL,
							Format.External, Format.Type,
							Texture.data(Layer, Face, Level));
					break;
				default: assert(0); break;
				}
			}

	return TextureName;
}

/****************************************************************************************************/
BIGINT TextureHandler::Load(const string& pTextName, const BBOOL& pWrap )
{
	BIGINT lId = -1;

	string lExtension = this->GetFileExt( pTextName );

	if ( _stricmp( lExtension.c_str(), "bmp" ) == 0 )
	{
		lId = LoadBMP( pTextName, pWrap );
	}
	else if ( _stricmp( lExtension.c_str(), "png" ) == 0 )
	{
		lId = LoadPNG( pTextName, pWrap );
	}
	else if ( _stricmp( lExtension.c_str(), "jpg" ) == 0 )
	{
		lId = LoadJPG( pTextName, pWrap );
	}
	else if ( _stricmp( lExtension.c_str(), "dds" ) == 0 )
	{
		lId = LoadDDS( pTextName, pWrap );
	}
	else if ( _stricmp( lExtension.c_str(), "tga" ) == 0 )
	{
		lId = LoadTGA( pTextName, pWrap );
	}
	
	// Last chance uses, Gli.
	if ( lId < 0 )
	{
		lId = this->Load( pTextName );
	}

	return lId;
}

/****************************************************************************************************/
BIGINT TextureHandler::LoadArray(const vector<string>& pFilenames, const BBOOL& pWrap )
{
	UBIGINT lTextureId = -1;
	const UBIGINT lLayerCount = (UBIGINT)pFilenames.size();
	
	// Read all needed data for creating array layers
	DDSLoader lDDSLoader;
	TGALoader lTGALoader;
	vector<DDSImage*> lDDSToDelete;
	vector<GenericImage*> lGenericToDelete;
	vector<UCCHAR*> lSTBIToDelete;
	UCCHAR** lDatas  = new UCCHAR*[ lLayerCount ];
	BIGINT* lWidths  = new BIGINT[ lLayerCount ];
	BIGINT* lHeights = new BIGINT[ lLayerCount ];
	BIGINT* lChannelCounts = new BIGINT[ lLayerCount ];
	BIGINT* lInternalFormats = new BIGINT[ lLayerCount ];
	for( UBIGINT lCurrLayer = 0; lCurrLayer < lLayerCount; lCurrLayer++ )
	{
		string lFileName = pFilenames[ lCurrLayer ];
		string lExtension = this->GetFileExt( lFileName );

		BIGINT lRegComp = STBI_rgb_alpha;
		BIGINT lInternalFormat = GL_RGBA8;
		if ( _stricmp( lExtension.c_str(), "bmp" ) == 0 ||
			 _stricmp( lExtension.c_str(), "jpg" ) == 0 )
		{
			lRegComp = STBI_rgb;
		}

		BIGINT lWidth;
		BIGINT lHeight;
		BIGINT lChannelCount;

		UCCHAR* lImage = NULL;
		if ( _stricmp( lExtension.c_str(), "dds" ) == 0 )
		{
			DDSImage* lDDSImage = lDDSLoader.LoadDDS( pFilenames[ lCurrLayer ].c_str() );
			if ( lDDSImage != NULL )
			{
				lImage  = lDDSImage->GetPixels( 0 );
				lWidth  = lDDSImage->GetWidth();
				lHeight = lDDSImage->GetHeight();
				lInternalFormat = lDDSImage->GetInternalFormat();
				lChannelCount   = 4;

				lDDSToDelete.push_back( lDDSImage );
			}
			//else
			//{
			//	// TO DO: Last chance, uses GDI.
			//	gli::texture Texture = gli::load( pFilenames[ lCurrLayer ] );
			//	if (Texture.empty())
			//		continue;

			//	gli::gl GL(gli::gl::PROFILE_GL33);
			//	gli::gl::format const Format = GL.translate(Texture.format(), Texture.swizzles());
			//	GLenum Target = GL.translate(Texture.target());

			//	glm::tvec3<GLsizei> const lExtent(Texture.extent());

			//	UCCHAR* lData = Texture.data<UCCHAR>();
			//	lImage  = new UCCHAR[ Texture.size() ];
			//	copy( lData, lData + Texture.size(), lImage );
			//	lWidth  = lExtent.x;
			//	lHeight = lExtent.y;
			//	lInternalFormat = Format.Internal;
			//	lChannelCount   = 4;
			//}
		}
		/*else if ( _stricmp( lExtension.c_str(), "tga" ) == 0 )
		{
			GenericImage* lTGAImage = lTGALoader.ReadTGA( pFilenames[ lCurrLayer ].c_str() );
			lImage  = lTGAImage->Pixels();
			lWidth  = lTGAImage->Width();
			lHeight = lTGAImage->Height();
			lInternalFormat = GL_RGBA8;
			lChannelCount   = 4;

			lGenericToDelete.push_back( lTGAImage );
		}*/
		else 
		{
			stbi_set_flip_vertically_on_load(true);
			lImage = stbi_load( lFileName.c_str(), &lWidth, &lHeight, &lChannelCount, lRegComp );
			lInternalFormat = lChannelCount == 3 ? GL_RGB8 : GL_RGBA8;

			lSTBIToDelete.push_back( lImage );
		}

		if (lImage == nullptr)
		{
			return -1;
		}

		lDatas[ lCurrLayer ]   = lImage;
		lWidths[ lCurrLayer ]  = lWidth;
		lHeights[ lCurrLayer ] = lHeight;
		lChannelCounts[ lCurrLayer ] = lChannelCount;
		lInternalFormats[ lCurrLayer ] = lInternalFormat;
	}

	lTextureId = this->CreateArrayTexture( lLayerCount, lWidths, lHeights, lChannelCounts, lInternalFormats, pWrap, lDatas );

	for (UBIGINT lCurr = 0; lCurr < lDDSToDelete.size(); lCurr++)
	{
		DeletePointer( lDDSToDelete[ lCurr ] );
	}
	lDDSToDelete.clear();

	for (UBIGINT lCurr = 0; lCurr < lGenericToDelete.size(); lCurr++)
	{
		GenericImage* lToDelete = lGenericToDelete[ lCurr ];
		lToDelete->Release();
		DeletePointer( lToDelete );
	}
	lGenericToDelete.clear();

	for (UBIGINT lCurr = 0; lCurr < lSTBIToDelete.size(); lCurr++)
	{
		stbi_image_free( lSTBIToDelete[ lCurr ] );
	}
	lSTBIToDelete.clear();

	delete[] lDatas;
	delete[] lWidths;
	delete[] lHeights;
	delete[] lChannelCounts;
	delete[] lInternalFormats;

	return lTextureId;
}

/***********************************************************************************************/
string TextureHandler::GetFileExt(const string& pFileName)
{
	size_t i = pFileName.rfind('.', pFileName.length());
	if (i != string::npos)
	{
		return(pFileName.substr(i + 1, pFileName.length() - i));
	}

	return("");
}

/****************************************************************************************************/
BIGINT TextureHandler::CreateSingleTexture(BIGINT pWidth, BIGINT pHeight, BIGINT pChannelCount, bool pWrap, UCCHAR* pData)
{
	UBIGINT lTextureId = -1;

	glGenTextures(1, &lTextureId);

	glBindTexture(GL_TEXTURE_2D, lTextureId);

	if (pChannelCount == 3)
	{
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, pWidth, pHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, pData);
	}
	else if (pChannelCount == 4)
	{
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, pWidth, pHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, pData);
	}

	// Set the texture color to either pWrap around or clamp to the edge.
	if(pWrap)
	{
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	}
	else
	{
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	}

	// Filtering
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY, 4.0f);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST/*GL_LINEAR*/);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	// Generate mipmaps for the texture.
	this->mDevice->GenerateMipMap(GL_TEXTURE_2D);

	glBindTexture(GL_TEXTURE_2D, 0);

	GLenum lError = glGetError();
	if( lError != GL_NO_ERROR )
	{
		throw exception(" Load Bmp texture Failure !!! ");
	}

	return lTextureId;
}

/****************************************************************************************************/
BIGINT TextureHandler::CreateArrayTexture(UBIGINT pCount, BIGINT pWidth[], BIGINT pHeight[], BIGINT pChannelCount[], BIGINT pInternalFormats[], bool pWrap, UCCHAR* pDatas[])
{
	UBIGINT lTextureId  = -1;
	UBIGINT lLayerCount = pCount;

	glGenTextures( 1, &lTextureId );

	glBindTexture( GL_TEXTURE_2D_ARRAY, lTextureId );

	if ( pWrap )
	{
		glTexParameteri( GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_REPEAT );
		glTexParameteri( GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_REPEAT );
	}
	else
	{
		glTexParameteri( GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
		glTexParameteri( GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
	}

	glTexParameterf( GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAX_ANISOTROPY, 4.0f );
	glTexParameteri( GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
	glTexParameteri( GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_LINEAR );

	GLenum lError = glGetError();
	if ( lError != GL_NO_ERROR )
	{
		throw exception(" Texture Load Failure !!! ");
	}

	// 2D Texture arrays a loaded just like 3D textures
	BIGINT lChannelCount = pChannelCount[ 0 ];
	this->mDevice->TexImage3D( GL_TEXTURE_2D_ARRAY, 0, pInternalFormats[0], pWidth[0], pHeight[0], lLayerCount, 0, lChannelCount == 3 ? GL_RGB : GL_RGBA, GL_UNSIGNED_BYTE, NULL );

	lError = glGetError();
	if ( lError != GL_NO_ERROR )
	{
		throw exception(" Texture Load Failure !!! ");
	}

	for ( UBIGINT lCurrLayer = 0; lCurrLayer < lLayerCount; lCurrLayer++ )
	{
		this->mDevice->TexSubImage3D( GL_TEXTURE_2D_ARRAY, 0, 0, 0, lCurrLayer, pWidth[ lCurrLayer ], pHeight[ lCurrLayer ], 1, pChannelCount[ lCurrLayer ] == 3 ? GL_RGB : GL_RGBA, GL_UNSIGNED_BYTE, pDatas[ lCurrLayer ] );
	}

	glBindTexture(GL_TEXTURE_2D, 0);

	lError = glGetError();
	if( lError != GL_NO_ERROR )
	{
		throw exception(" Load Bmp texture Failure !!! ");
	}

	return lTextureId;
}