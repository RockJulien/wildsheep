#include "TextureManager.h"
#include "TextureHandler.h"

// Namespaces used
using namespace std;

/***********************************************************************************************/
TextureManager::TextureManager() :
mUnitCount(0)
{

}

/***********************************************************************************************/
TextureManager::~TextureManager()
{
	this->mTextures.clear();
}

/***********************************************************************************************/
VVOID     TextureManager::Initialize(GDevice* pDevice)
{
	this->mDevice = pDevice;
}

/***********************************************************************************************/
BIGINT    TextureManager::CreateTexture(const string& pFilename, BBOOL pWrap)
{
	BIGINT lNewTexture = -1; // Default meaning failure.

	// If already exists
	if(this->mTextures.find(pFilename) != this->mTextures.end())
	{
		lNewTexture = this->mTextures[pFilename];
	}
	// Else create it.
	else
	{
		TextureHandler lHandler( this->mDevice );
		lNewTexture = lHandler.Load( pFilename.c_str(), pWrap );
		
		if ( lNewTexture != -1 )
		{
			this->mTextures.insert( pair<string, UBIGINT>( pFilename, lNewTexture ) );
		}
	}

	return lNewTexture;
}

/***********************************************************************************************/
BIGINT    TextureManager::CreateTextureArray(const string& pRetrieverName, const vector<string>& pFilenames, BBOOL pWrap)
{
	BIGINT lNewTexture = -1; // Default meaning failure.

	// If already exists
	if(this->mTextures.find( pRetrieverName ) != this->mTextures.end())
	{
		lNewTexture = this->mTextures[ pRetrieverName ];
	}
	// Else create it.
	else
	{
		TextureHandler lHandler( this->mDevice );
		lNewTexture = lHandler.LoadArray( pFilenames, pWrap );

		if ( lNewTexture != -1 )
		{
			this->mTextures.insert( pair<string, UBIGINT>( pRetrieverName, lNewTexture ) );
		}
	}

	return lNewTexture;
}

/***********************************************************************************************/
UBIGINT   TextureManager::UnitCount() const
{
	return this->mUnitCount;
}

/***********************************************************************************************/
VVOID     TextureManager::SetUnitCount(UBIGINT pCount)
{
	this->mUnitCount = pCount;
}
