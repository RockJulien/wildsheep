#ifndef DEF_TEXTUREHANDLER_H
#define DEF_TEXTUREHANDLER_H

#include <stdio.h>
#include <vector>
#include <string>
#include "..\OSCheck.h"

class GDevice;

class TextureHandler
{
private:

	// Attributes
	GDevice* mDevice;

	// Private Methods
	BIGINT Load(const std::string& pTextName);
	BIGINT LoadBMP( const std::string& pTextName, BBOOL pWrap );
	BIGINT LoadPNG( const std::string& pTextName, BBOOL pWrap );
	BIGINT LoadJPG( const std::string& pTextName, BBOOL pWrap );
	BIGINT LoadDDS( const std::string& pTextName, BBOOL pWrap );
	BIGINT LoadTGA( const std::string& pTextName, BBOOL pWrap );

	BIGINT CreateSingleTexture(BIGINT pWidth, BIGINT pHeight, BIGINT pChannelCount, bool pWrap, UCCHAR* pData);
	BIGINT CreateArrayTexture(UBIGINT pCount, BIGINT pWidth[], BIGINT pHeight[], BIGINT pChannelCount[], BIGINT pInternalFormats[], bool pWrap, UCCHAR* pDatas[]);
	std::string GetFileExt(const std::string& pFileName);

public:

	// Constructor & Destructor
	TextureHandler(GDevice* pDevice);
	~TextureHandler();

	// Methods
	BIGINT Load(const std::string& pTextName, const BBOOL& pWrap);
	BIGINT LoadArray(const std::vector<std::string>& pFilenames, const BBOOL& pWrap);

};

#endif