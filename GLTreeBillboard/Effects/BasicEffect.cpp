#include "BasicEffect.h"
#include "Constants.h"
#include <sstream>

// Namespaces used
using namespace std;
using namespace Ocelot;

/*************************************************************************************************/
BasicEffect::BasicEffect(GDevice* pDevice) :
mCurrentTechnique(NULL), mDevice(pDevice)
{
	// Init the flags map.
	this->mFlagMap.insert(pair<string, USMALLINT>( Constants::cLight1TechName, 0 ));
	this->mFlagMap.insert(pair<string, USMALLINT>( Constants::cLight2TechName, 0 ));
	this->mFlagMap.insert(pair<string, USMALLINT>( Constants::cLight3TechName, 0 ));

	this->mFlagMap.insert(pair<string, USMALLINT>( Constants::cLight0TexTechName, Constants::cUseTextureMask ));
	this->mFlagMap.insert(pair<string, USMALLINT>( Constants::cLight1TexTechName, Constants::cUseTextureMask ));
	this->mFlagMap.insert(pair<string, USMALLINT>( Constants::cLight2TexTechName, Constants::cUseTextureMask ));
	this->mFlagMap.insert(pair<string, USMALLINT>( Constants::cLight3TexTechName, Constants::cUseTextureMask ));

	this->mFlagMap.insert(pair<string, USMALLINT>( Constants::cLight0TexAlphaClipTechName, Constants::cUseTextureMask | Constants::cAlphaClipMask ));
	this->mFlagMap.insert(pair<string, USMALLINT>( Constants::cLight1TexAlphaClipTechName, Constants::cUseTextureMask | Constants::cAlphaClipMask ));
	this->mFlagMap.insert(pair<string, USMALLINT>( Constants::cLight2TexAlphaClipTechName, Constants::cUseTextureMask | Constants::cAlphaClipMask ));
	this->mFlagMap.insert(pair<string, USMALLINT>( Constants::cLight3TexAlphaClipTechName, Constants::cUseTextureMask | Constants::cAlphaClipMask ));

	this->mFlagMap.insert(pair<string, USMALLINT>( Constants::cLight1FogTechName, Constants::cFogEnabledMask ));
	this->mFlagMap.insert(pair<string, USMALLINT>( Constants::cLight2FogTechName, Constants::cFogEnabledMask ));
	this->mFlagMap.insert(pair<string, USMALLINT>( Constants::cLight3FogTechName, Constants::cFogEnabledMask ));

	this->mFlagMap.insert(pair<string, USMALLINT>( Constants::cLight0TexFogTechName, Constants::cUseTextureMask | Constants::cFogEnabledMask ));
	this->mFlagMap.insert(pair<string, USMALLINT>( Constants::cLight1TexFogTechName, Constants::cUseTextureMask | Constants::cFogEnabledMask ));
	this->mFlagMap.insert(pair<string, USMALLINT>( Constants::cLight2TexFogTechName, Constants::cUseTextureMask | Constants::cFogEnabledMask ));
	this->mFlagMap.insert(pair<string, USMALLINT>( Constants::cLight3TexFogTechName, Constants::cUseTextureMask | Constants::cFogEnabledMask ));

	this->mFlagMap.insert(pair<string, USMALLINT>( Constants::cLight0TexAlphaClipFogTechName, Constants::cUseTextureMask | Constants::cAlphaClipMask | Constants::cFogEnabledMask ));
	this->mFlagMap.insert(pair<string, USMALLINT>( Constants::cLight1TexAlphaClipFogTechName, Constants::cUseTextureMask | Constants::cAlphaClipMask | Constants::cFogEnabledMask ));
	this->mFlagMap.insert(pair<string, USMALLINT>( Constants::cLight2TexAlphaClipFogTechName, Constants::cUseTextureMask | Constants::cAlphaClipMask | Constants::cFogEnabledMask ));
	this->mFlagMap.insert(pair<string, USMALLINT>( Constants::cLight3TexAlphaClipFogTechName, Constants::cUseTextureMask | Constants::cAlphaClipMask | Constants::cFogEnabledMask ));
}

/*************************************************************************************************/
BasicEffect::~BasicEffect()
{

}

/*************************************************************************************************/
VVOID BasicEffect::SetWorldViewProj(const Matrix4x4& pMatrix)
{
	UBIGINT lShaderLocation;
	lShaderLocation = this->mDevice->GetUniformLocation(this->mCurrentTechnique->Handle(), "gWorldViewProj");
	if(lShaderLocation == -1)
	{
		return;
	}

	this->mDevice->UniformMat4fv(lShaderLocation, 1, false, pMatrix);
}

/*************************************************************************************************/
VVOID BasicEffect::SetWorld(const Matrix4x4& pMatrix)
{
	UBIGINT lShaderLocation;
	lShaderLocation = this->mDevice->GetUniformLocation(this->mCurrentTechnique->Handle(), "gWorld");
	if(lShaderLocation == -1)
	{
		return;
	}

	this->mDevice->UniformMat4fv(lShaderLocation, 1, false, pMatrix);
}

/*************************************************************************************************/
VVOID BasicEffect::SetWorldInvTranspose(const Matrix4x4& pMatrix)
{
	UBIGINT lShaderLocation;
	lShaderLocation = this->mDevice->GetUniformLocation(this->mCurrentTechnique->Handle(), "gWorldInvTranspose");
	if(lShaderLocation == -1)
	{
		return;
	}

	this->mDevice->UniformMat4fv(lShaderLocation, 1, false, pMatrix);
}

/***********************************************************************************************/
VVOID BasicEffect::SetTexTransform(const Matrix4x4& pMatrix)
{
	UBIGINT lShaderLocation;
	lShaderLocation = this->mDevice->GetUniformLocation(this->mCurrentTechnique->Handle(), "gTexTransform");
	if(lShaderLocation == -1)
	{
		return;
	}

	this->mDevice->UniformMat4fv(lShaderLocation, 1, false, pMatrix);
}

/*************************************************************************************************/
VVOID BasicEffect::SetEyePosW(const Vector3& pEyePos)
{
	UBIGINT lShaderLocation;
	lShaderLocation = this->mDevice->GetUniformLocation(this->mCurrentTechnique->Handle(), "gEyePosW");
	if(lShaderLocation == -1)
	{
		return;
	}

	this->mDevice->Uniform3fv( lShaderLocation, 1, pEyePos );
}

/*************************************************************************************************/
VVOID BasicEffect::SetFogColor(const Color& pFogColor)
{
	UBIGINT lShaderLocation;
	lShaderLocation = this->mDevice->GetUniformLocation(this->mCurrentTechnique->Handle(), "gFogColor");
	if(lShaderLocation == -1)
	{
		return;
	}

	this->mDevice->Uniform4fv( lShaderLocation, 1, pFogColor );
}

/*************************************************************************************************/
VVOID BasicEffect::SetFogStart(const FFLOAT& pFogStart)
{
	UBIGINT lShaderLocation;
	lShaderLocation = this->mDevice->GetUniformLocation(this->mCurrentTechnique->Handle(), "gFogStart");
	if(lShaderLocation == -1)
	{
		return;
	}

	this->mDevice->Uniform1f(lShaderLocation, pFogStart);
}

/*************************************************************************************************/
VVOID BasicEffect::SetFogRange(const FFLOAT& pFogRange)
{
	UBIGINT lShaderLocation;
	lShaderLocation = this->mDevice->GetUniformLocation(this->mCurrentTechnique->Handle(), "gFogRange");
	if(lShaderLocation == -1)
	{
		return;
	}

	this->mDevice->Uniform1f(lShaderLocation, pFogRange);
}

/*************************************************************************************************/
VVOID BasicEffect::SetDirLights(const OcelotParallelLight* pLights, UBIGINT pCount)
{
	if(pLights == NULL)
	{
		return;
	}

	UBIGINT lShaderLocation;
	lShaderLocation = this->mDevice->GetUniformLocation(this->mCurrentTechnique->Handle(), "gLightCount");
	if(lShaderLocation == -1)
	{
		return;
	}

	this->mDevice->Uniform1i(lShaderLocation, pCount);

	for( UBIGINT lCurrLight = 0; lCurrLight < pCount; lCurrLight++ )
	{
		stringstream lAmbientShaderVarName;
		lAmbientShaderVarName << "gDirLights[" << lCurrLight << "].Ambient";
		lShaderLocation = this->mDevice->GetUniformLocation( this->mCurrentTechnique->Handle(), lAmbientShaderVarName.str().c_str() );
		if(lShaderLocation == -1)
		{
			return;
		}

		this->mDevice->Uniform4fv(lShaderLocation, 1, pLights[lCurrLight].Ambient());

		stringstream lDiffuseShaderVarName;
		lDiffuseShaderVarName << "gDirLights[" << lCurrLight << "].Diffuse";
		lShaderLocation = this->mDevice->GetUniformLocation( this->mCurrentTechnique->Handle(), lDiffuseShaderVarName.str().c_str() );
		if(lShaderLocation == -1)
		{
			return;
		}

		this->mDevice->Uniform4fv(lShaderLocation, 1, pLights[lCurrLight].Diffuse());

		stringstream lSpecularShaderVarName;
		lSpecularShaderVarName << "gDirLights[" << lCurrLight << "].Specular";
		lShaderLocation = this->mDevice->GetUniformLocation( this->mCurrentTechnique->Handle(), lSpecularShaderVarName.str().c_str() );
		if(lShaderLocation == -1)
		{
			return;
		}

		this->mDevice->Uniform4fv(lShaderLocation, 1, pLights[lCurrLight].Specular());

		stringstream lDirectionShaderVarName;
		lDirectionShaderVarName << "gDirLights[" << lCurrLight << "].Direction";
		lShaderLocation = this->mDevice->GetUniformLocation(this->mCurrentTechnique->Handle(), lDirectionShaderVarName.str().c_str() );
		if(lShaderLocation == -1)
		{
			return;
		}

		this->mDevice->Uniform3fv(lShaderLocation, 1, pLights[lCurrLight].Direction());

		GLenum lError = glGetError();
		if
			( lError != GL_NO_ERROR )
		{
			throw exception(" Set Effect Light FAILURE !!! ");
		}
	}
}

/*************************************************************************************************/
VVOID BasicEffect::SetMaterial(const Material& pMaterial)
{
	UBIGINT lShaderLocation;

	// Now, pass material object
	// Obviously opengl does not allow to pass an entire struct as DirectX does
	// so, we ll pass elements one by one. It could avoid padding issues that appear
	// in DirectX managing data by one or four. (e.g: vec3 need an extra float as padding).
	lShaderLocation = this->mDevice->GetUniformLocation(this->mCurrentTechnique->Handle(), "gMaterial.Ambient");
	if(lShaderLocation == -1)
	{
		return;
	}

	this->mDevice->Uniform4fv(lShaderLocation, 1, pMaterial.Ambient);

	lShaderLocation = this->mDevice->GetUniformLocation(this->mCurrentTechnique->Handle(), "gMaterial.Diffuse");
	if(lShaderLocation == -1)
	{
		return;
	}

	this->mDevice->Uniform4fv(lShaderLocation, 1, pMaterial.Diffuse);

	lShaderLocation = this->mDevice->GetUniformLocation(this->mCurrentTechnique->Handle(), "gMaterial.Specular");
	if(lShaderLocation == -1)
	{
		return;
	}

	this->mDevice->Uniform4fv(lShaderLocation, 1, pMaterial.Specular);

	lShaderLocation = this->mDevice->GetUniformLocation(this->mCurrentTechnique->Handle(), "gMaterial.Reflect");
	if(lShaderLocation == -1)
	{
		return;
	}

	this->mDevice->Uniform4fv(lShaderLocation, 1, pMaterial.Reflect);

	GLenum lError = glGetError();
	if ( lError != GL_NO_ERROR )
	{
		throw exception(" Set Material Failure !!! ");
	}
}

/***********************************************************************************************/
VVOID BasicEffect::SetDiffuseMap(const UBIGINT& pUnit)
{
	UBIGINT lShaderLocation;

	lShaderLocation = this->mDevice->GetUniformLocation(this->mCurrentTechnique->Handle(), "gDiffuseMap");
	if(lShaderLocation == -1)
	{
		return;
	}

	this->mDevice->Uniform1i(lShaderLocation, pUnit );
}

/*************************************************************************************************/
VVOID BasicEffect::MakeCurrent(EffectTechnique* pCurrent)
{
	if
		( pCurrent == NULL )
	{
		return;
	}

	pCurrent->MakeCurrent();
	this->mCurrentTechnique = pCurrent;

	// Check technique's particularities ans set proper flags
	map<string, USMALLINT>::const_iterator lIter = this->mFlagMap.find( this->mCurrentTechnique->Name() );

	if
		( lIter != this->mFlagMap.end() )
	{
		BBOOL lUseTexture = (lIter->second & Constants::cUseTextureMask) == Constants::cUseTextureMask;
		BBOOL lAlphaClip  = (lIter->second & Constants::cAlphaClipMask)  == Constants::cAlphaClipMask;
		BBOOL lUseFog     = (lIter->second & Constants::cFogEnabledMask) == Constants::cFogEnabledMask;

		UBIGINT lShaderLocation = this->mDevice->GetUniformLocation( this->mCurrentTechnique->Handle(), "gUseTexure" );
		if
			( lShaderLocation == -1 )
		{
			return;
		}

		this->mDevice->Uniform1i( lShaderLocation, lUseTexture );

		lShaderLocation = this->mDevice->GetUniformLocation( this->mCurrentTechnique->Handle(), "gAlphaClip" );
		if
			( lShaderLocation == -1 )
		{
			return;
		}

		this->mDevice->Uniform1i( lShaderLocation, lAlphaClip );

		lShaderLocation = this->mDevice->GetUniformLocation( this->mCurrentTechnique->Handle(), "gFogEnabled" );
		if
			( lShaderLocation == -1 )
		{
			return;
		}

		this->mDevice->Uniform1i( lShaderLocation, lUseFog );
	}
}

/*************************************************************************************************/
EffectTechnique* BasicEffect::Light1Tech(const StageDescription& pStagePaths, const vector<string>& pIncludes)
{
	return new EffectTechnique( this->mDevice, Constants::cLight1TechName, pStagePaths, pIncludes );
}

/*************************************************************************************************/
EffectTechnique* BasicEffect::Light2Tech(const StageDescription& pStagePaths, const vector<string>& pIncludes)
{
	return new EffectTechnique( this->mDevice, Constants::cLight2TechName, pStagePaths, pIncludes );
}

/*************************************************************************************************/
EffectTechnique* BasicEffect::Light3Tech(const StageDescription& pStagePaths, const vector<string>& pIncludes)
{
	return new EffectTechnique( this->mDevice, Constants::cLight3TechName, pStagePaths, pIncludes );
}

/*************************************************************************************************/
EffectTechnique* BasicEffect::Light0TexTech(const StageDescription& pStagePaths, const vector<string>& pIncludes)
{
	return new EffectTechnique( this->mDevice, Constants::cLight0TexTechName, pStagePaths, pIncludes );
}

/*************************************************************************************************/
EffectTechnique* BasicEffect::Light1TexTech(const StageDescription& pStagePaths, const vector<string>& pIncludes)
{
	return new EffectTechnique( this->mDevice, Constants::cLight1TexTechName, pStagePaths, pIncludes );
}

/*************************************************************************************************/
EffectTechnique* BasicEffect::Light2TexTech(const StageDescription& pStagePaths, const vector<string>& pIncludes)
{
	return new EffectTechnique( this->mDevice, Constants::cLight2TexTechName, pStagePaths, pIncludes );
}

/*************************************************************************************************/
EffectTechnique* BasicEffect::Light3TexTech(const StageDescription& pStagePaths, const vector<string>& pIncludes)
{
	return new EffectTechnique( this->mDevice, Constants::cLight3TexTechName, pStagePaths, pIncludes );
}

/***********************************************************************************************/
EffectTechnique* BasicEffect::Light0TexAlphaClipTech(const StageDescription& pStagePaths, const std::vector<std::string>& pIncludes)
{
	return new EffectTechnique( this->mDevice, Constants::cLight0TexAlphaClipTechName, pStagePaths, pIncludes );
}

/***********************************************************************************************/
EffectTechnique* BasicEffect::Light1TexAlphaClipTech(const StageDescription& pStagePaths, const std::vector<std::string>& pIncludes)
{
	return new EffectTechnique( this->mDevice, Constants::cLight1TexAlphaClipTechName, pStagePaths, pIncludes );
}

/***********************************************************************************************/
EffectTechnique* BasicEffect::Light2TexAlphaClipTech(const StageDescription& pStagePaths, const std::vector<std::string>& pIncludes)
{
	return new EffectTechnique( this->mDevice, Constants::cLight2TexAlphaClipTechName, pStagePaths, pIncludes );
}

/***********************************************************************************************/
EffectTechnique* BasicEffect::Light3TexAlphaClipTech(const StageDescription& pStagePaths, const std::vector<std::string>& pIncludes)
{
	return new EffectTechnique( this->mDevice, Constants::cLight3TexAlphaClipTechName, pStagePaths, pIncludes );
}

/***********************************************************************************************/
EffectTechnique* BasicEffect::Light1FogTech(const StageDescription& pStagePaths, const std::vector<std::string>& pIncludes)
{
	return new EffectTechnique( this->mDevice, Constants::cLight1FogTechName, pStagePaths, pIncludes );
}

/***********************************************************************************************/
EffectTechnique* BasicEffect::Light2FogTech(const StageDescription& pStagePaths, const std::vector<std::string>& pIncludes)
{
	return new EffectTechnique( this->mDevice, Constants::cLight2FogTechName, pStagePaths, pIncludes );
}

/***********************************************************************************************/
EffectTechnique* BasicEffect::Light3FogTech(const StageDescription& pStagePaths, const std::vector<std::string>& pIncludes)
{
	return new EffectTechnique( this->mDevice, Constants::cLight3FogTechName, pStagePaths, pIncludes );
}

/***********************************************************************************************/
EffectTechnique* BasicEffect::Light0TexFogTech(const StageDescription& pStagePaths, const std::vector<std::string>& pIncludes)
{
	return new EffectTechnique( this->mDevice, Constants::cLight0TexFogTechName, pStagePaths, pIncludes );
}

/***********************************************************************************************/
EffectTechnique* BasicEffect::Light1TexFogTech(const StageDescription& pStagePaths, const std::vector<std::string>& pIncludes)
{
	return new EffectTechnique( this->mDevice, Constants::cLight1TexFogTechName, pStagePaths, pIncludes );
}

/***********************************************************************************************/
EffectTechnique* BasicEffect::Light2TexFogTech(const StageDescription& pStagePaths, const std::vector<std::string>& pIncludes)
{
	return new EffectTechnique( this->mDevice, Constants::cLight2TexFogTechName, pStagePaths, pIncludes );
}

/***********************************************************************************************/
EffectTechnique* BasicEffect::Light3TexFogTech(const StageDescription& pStagePaths, const std::vector<std::string>& pIncludes)
{
	return new EffectTechnique( this->mDevice, Constants::cLight3TexFogTechName, pStagePaths, pIncludes );
}

/***********************************************************************************************/
EffectTechnique* BasicEffect::Light0TexAlphaClipFogTech(const StageDescription& pStagePaths, const std::vector<std::string>& pIncludes)
{
	return new EffectTechnique( this->mDevice, Constants::cLight0TexAlphaClipFogTechName, pStagePaths, pIncludes );
}

/***********************************************************************************************/
EffectTechnique* BasicEffect::Light1TexAlphaClipFogTech(const StageDescription& pStagePaths, const std::vector<std::string>& pIncludes)
{
	return new EffectTechnique( this->mDevice, Constants::cLight1TexAlphaClipFogTechName, pStagePaths, pIncludes );
}

/***********************************************************************************************/
EffectTechnique* BasicEffect::Light2TexAlphaClipFogTech(const StageDescription& pStagePaths, const std::vector<std::string>& pIncludes)
{
	return new EffectTechnique( this->mDevice, Constants::cLight2TexAlphaClipFogTechName, pStagePaths, pIncludes );
}

/***********************************************************************************************/
EffectTechnique* BasicEffect::Light3TexAlphaClipFogTech(const StageDescription& pStagePaths, const std::vector<std::string>& pIncludes)
{
	return new EffectTechnique( this->mDevice, Constants::cLight3TexAlphaClipFogTechName, pStagePaths, pIncludes );
}
