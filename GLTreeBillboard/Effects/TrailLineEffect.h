#pragma once

// Includes
#include <map>
#include <string>
#include "..\GDevice.h"
#include "..\Materials\Material.h"
#include "..\Maths\Color.h"
#include "..\Maths\Matrix4x4.h"
#include "..\Lights\OcelotLight.h"
#include "EffectTechnique.h"

// Namespaces

// Class declaration
class TrailLineEffect
{
private:

	std::map<std::string, USMALLINT> mFlagMap;

	// Attributes
	EffectTechnique* mCurrentTechnique;
	GDevice*		 mDevice;

public:

	// Constructor & destructor
	TrailLineEffect(GDevice* pDevice);
	~TrailLineEffect();

	// Methods
	VVOID SetWorld(const Matrix4x4& pMatrix);
	VVOID SetWorldViewProj(const Matrix4x4& pMatrix);
	VVOID SetTexTransform(const Matrix4x4& pMatrix);
	VVOID SetMaterial(const Material& pMaterial);
	VVOID SetEyePosW(const Vector3& pEyePos);
	VVOID SetLineAxis(const Vector3& pAxis);
	VVOID SetMask1(const UBIGINT& pUnit);
	VVOID SetMask2(const UBIGINT& pUnit);

	VVOID SetTime(const FFLOAT& pTime);
	VVOID SetAmplitude(const FFLOAT& pAmplitude);
	VVOID SetOndulate(const BBOOL& pOndulate);

	VVOID MakeCurrent(EffectTechnique* pCurrent);

	// Technique getters
	EffectTechnique* TrailLineTech(const StageDescription& pStagePaths, const std::vector<std::string>& pIncludes);
	EffectTechnique* TrailLineTexTech(const StageDescription& pStagePaths, const std::vector<std::string>& pIncludes);
};