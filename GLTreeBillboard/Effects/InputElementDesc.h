#ifndef DEF_INPUTELEMENTDESC_H
#define DEF_INPUTELEMENTDESC_H

// Includes
#include <string>
#include "..\OSCheck.h"

// Class definition
class InputElementDesc
{
private:

	// Attributes
	std::string  mSemanticName;
	UBIGINT      mSemanticIndex;
	UBIGINT      mStride;
	const VVOID* mOffset; // If Any.
	USMALLINT	 mDataCount;
	USMALLINT	 mDataType;
	BBOOL		 mNeedNormalization;

public:

	// Constructor
	InputElementDesc();
	InputElementDesc(const std::string& pSemanticName, const UBIGINT& pSemanticIndex, const UBIGINT& pStride, const USMALLINT& pDataCount, const USMALLINT& pDataType, const VVOID* pOffset = NULL, BBOOL pNeedNormalization = false);
	~InputElementDesc();

	// Accessors
	inline const std::string& SemanticName() const;
	inline const VVOID* Offset() const;
	inline UBIGINT		SemanticIndex() const;
	inline UBIGINT		Stride() const;
	inline USMALLINT	DataCount() const;
	inline USMALLINT    DataType() const;
	inline BBOOL		NeedNormalization() const;
	inline VVOID        SetSemanticName(const std::string& pSemanticName);
	inline VVOID        SetOffset(const VVOID* pOffset);
	inline VVOID		SetSemanticIndex(const UBIGINT& pSemanticIndex);
	inline VVOID		SetStride(const UBIGINT& pStride);
	inline VVOID		SetDataCount(const USMALLINT& pDataCount);
	inline VVOID        SetDataType(const USMALLINT& pDataType);
	inline VVOID		SetNeedNormalization(BBOOL pNormalize);

};

// Inlines
#include "InputElementDesc.inl"

#endif