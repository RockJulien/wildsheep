#ifndef DEF_EFFECTTECHNIQUE_H
#define DEF_EFFECTTECHNIQUE_H

// Includes
#include <string>
#include <vector>
#include "..\GDevice.h"
#include "InputLayout.h"

enum PipelineStages
{
	// Vertex stage
	ePIPELINESTAGES_VERTEX = 0,

	// Tesselation control stage
	ePIPELINESTAGES_HULL,

	// Tesselation evaluation stage
	ePIPELINESTAGES_DOMAIN,

	// Geometry stage
	ePIPELINESTAGES_GEOMETRY,

	// Pixel stage
	ePIPELINESTAGES_PIXEL,

	// Compute stage
	ePIPELINESTAGES_COMPUTE
};

class StageDescription
{
private:

	std::string mVertexStagePath;
	std::string mHullStagePath;
	std::string mDomainStagePath;
	std::string mGeometryStagePath;
	std::string mPixelStagePath;
	std::string mComputeStagePath;
	BBOOL		mHasVertexStage;
	BBOOL		mHasHullStage;
	BBOOL		mHasDomainStage;
	BBOOL		mHasGeometryStage;
	BBOOL		mHasPixelStage;
	BBOOL		mHasComputeStage;

public:

	StageDescription();

	// Vertex stage path
	std::string VertexStagePath() const;
	VVOID       SetVertexStagePath(const std::string& pVertexStagePath);
	// Tesselation control stage path
	std::string HullStagePath() const;
	VVOID       SetHullStagePath(const std::string& pHullStagePath);
	// Tesselation evaluation stage path
	std::string DomainStagePath() const;
	VVOID       SetDomainStagePath(const std::string& pDomainStagePath);
	// Geometry stage path
	std::string GeometryStagePath() const;
	VVOID       SetGeometryStagePath(const std::string& pGeometryStagePath);
	// Pixel stage path
	std::string PixelStagePath() const;
	VVOID       SetPixelStagePath(const std::string& pPixelStagePath);
	// Compute stage path
	std::string ComputeStagePath() const;
	VVOID       SetComputeStagePath(const std::string& pComputeStagePath);

	BBOOL     HasVertexStage() const;
	BBOOL     HasHullStage() const;
	BBOOL     HasDomainStage() const;
	BBOOL     HasGeometryStage() const;
	BBOOL     HasPixelStage() const;
	BBOOL     HasComputeStage() const;

};

// Class definition
class EffectTechnique
{
private:

			// Attributes
			std::string  mTechniqueName;
			UBIGINT      mVertShaderID;		   // Vertex shader ID
			UBIGINT      mTessControlShaderID; // Tessellation control shader ID
			UBIGINT      mTessEvalShaderID;    // Tessellation evaluate shader ID
			UBIGINT      mGeometryShaderID;    // Geometry shader ID
			UBIGINT      mPixShaderID;         // Pixel shader ID
			UBIGINT      mComputeShaderID;     // Compute shader ID
			UBIGINT      mEffectShader;        // EffectTechnique handle
			GDevice*     mDevice;
			InputLayout* mLayout;              // The input layout, that is, the vertex input desc.

			// Private Methods
			EffectTechnique(const EffectTechnique& pToCopy);
			EffectTechnique& operator = (const EffectTechnique& pToAssgm);

			// Private Methods (API dependant)
			std::string LoadShaderFromFile(const std::string& pShaderPath);
			BBOOL   InitializeShaders(const StageDescription& pStagePaths, const std::vector<std::string>& pIncludes);
			BBOOL   Link();
			VVOID   ReleaseShader();

			// Shader debug functions (API dependant)
			VVOID  ShaderErrorReport(USMALLINT pShaderID, const std::string& pShaderPath);
			VVOID  LinkerErrorReport(USMALLINT pShaderID);

public:

			// Constructor
			EffectTechnique(GDevice* pDevice, std::string pTechniqueName, const StageDescription& pStagePaths, const std::vector<std::string>& pIncludes);
			~EffectTechnique();

			// Methods
			VVOID MakeCurrent();
			VVOID SetLayout(InputLayout* pLayout);

			// Accessors
			std::string Name() const;
			UBIGINT		Handle() const;

};

#endif