#include "TrailLineEffect.h"
#include "Constants.h"

// Namespaces used
using namespace std;
using namespace Ocelot;

/*****************************************************************************************************/
TrailLineEffect::TrailLineEffect(GDevice* pDevice) :
mCurrentTechnique(NULL), mDevice(pDevice)
{
	this->mFlagMap.insert(pair<string, USMALLINT>( Constants::cTrailLineTechName, 0 ));
	this->mFlagMap.insert(pair<string, USMALLINT>( Constants::cTrailLineTexTechName, Constants::cUseTextureMask ));
}

/*****************************************************************************************************/
TrailLineEffect::~TrailLineEffect()
{

}

/*****************************************************************************************************/
VVOID TrailLineEffect::SetWorld(const Matrix4x4& pMatrix)
{
	UBIGINT lShaderLocation;
	lShaderLocation = this->mDevice->GetUniformLocation(this->mCurrentTechnique->Handle(), "gWorld");
	if(lShaderLocation == -1)
	{
		return;
	}

	this->mDevice->UniformMat4fv(lShaderLocation, 1, false, pMatrix);
}

/*****************************************************************************************************/
VVOID TrailLineEffect::SetWorldViewProj(const Matrix4x4& pMatrix)
{
	UBIGINT lShaderLocation;
	lShaderLocation = this->mDevice->GetUniformLocation( this->mCurrentTechnique->Handle(), "gWorldViewProj" );
	if ( lShaderLocation == -1 )
	{
		return;
	}

	this->mDevice->UniformMat4fv( lShaderLocation, 1, false, pMatrix );
}

/*****************************************************************************************************/
VVOID TrailLineEffect::SetTexTransform(const Matrix4x4& pMatrix)
{
	UBIGINT lShaderLocation;
	lShaderLocation = this->mDevice->GetUniformLocation( this->mCurrentTechnique->Handle(), "gTexTransform" );
	if ( lShaderLocation == -1 )
	{
		return;
	}

	this->mDevice->UniformMat4fv( lShaderLocation, 1, false, pMatrix );
}

/*****************************************************************************************************/
VVOID TrailLineEffect::SetMaterial(const Material& pMaterial)
{
	UBIGINT lShaderLocation;

	// Now, pass material object
	// Obviously opengl does not allow to pass an entire struct as DirectX does
	// so, we ll pass elements one by one. It could avoid padding issues that appear
	// in DirectX managing data by one or four. (e.g: vec3 need an extra float as padding).
	lShaderLocation = this->mDevice->GetUniformLocation(this->mCurrentTechnique->Handle(), "gMaterial.Ambient");
	if(lShaderLocation == -1)
	{
		return;
	}

	this->mDevice->Uniform4fv(lShaderLocation, 1, pMaterial.Ambient);

	lShaderLocation = this->mDevice->GetUniformLocation(this->mCurrentTechnique->Handle(), "gMaterial.Diffuse");
	if(lShaderLocation == -1)
	{
		return;
	}

	this->mDevice->Uniform4fv(lShaderLocation, 1, pMaterial.Diffuse);

	lShaderLocation = this->mDevice->GetUniformLocation(this->mCurrentTechnique->Handle(), "gMaterial.Specular");
	if(lShaderLocation == -1)
	{
		return;
	}

	this->mDevice->Uniform4fv(lShaderLocation, 1, pMaterial.Specular);

	lShaderLocation = this->mDevice->GetUniformLocation(this->mCurrentTechnique->Handle(), "gMaterial.Reflect");
	if(lShaderLocation == -1)
	{
		return;
	}

	this->mDevice->Uniform4fv(lShaderLocation, 1, pMaterial.Reflect);

	GLenum lError = glGetError();
	if ( lError != GL_NO_ERROR )
	{
		throw exception(" Set Material Failure !!! ");
	}
}

/*****************************************************************************************************/
VVOID TrailLineEffect::SetEyePosW(const Vector3& pEyePos)
{
	UBIGINT lShaderLocation;
	lShaderLocation = this->mDevice->GetUniformLocation(this->mCurrentTechnique->Handle(), "gEyePosW");
	if(lShaderLocation == -1)
	{
		return;
	}

	this->mDevice->Uniform3fv( lShaderLocation, 1, pEyePos );
}

/*****************************************************************************************************/
VVOID TrailLineEffect::SetLineAxis(const Vector3& pAxis)
{
	UBIGINT lShaderLocation;
	lShaderLocation = this->mDevice->GetUniformLocation(this->mCurrentTechnique->Handle(), "gLineAxis");
	if(lShaderLocation == -1)
	{
		return;
	}

	this->mDevice->Uniform3fv( lShaderLocation, 1, pAxis );
}

/*****************************************************************************************************/
VVOID TrailLineEffect::SetMask1(const UBIGINT& pUnit)
{
	UBIGINT lShaderLocation;

	lShaderLocation = this->mDevice->GetUniformLocation( this->mCurrentTechnique->Handle(), "gMask1" );
	if ( lShaderLocation == -1 )
	{
		return;
	}

	this->mDevice->Uniform1i( lShaderLocation, pUnit );
}

/*****************************************************************************************************/
VVOID TrailLineEffect::SetMask2(const UBIGINT& pUnit)
{
	BIGINT lShaderLocation;

	lShaderLocation = this->mDevice->GetUniformLocation( this->mCurrentTechnique->Handle(), "gMask2" );
	if ( lShaderLocation == -1 )
	{
		return;
	}

	this->mDevice->Uniform1i( lShaderLocation, pUnit );
}

/*************************************************************************************************/
VVOID TrailLineEffect::SetTime(const FFLOAT& pTime)
{
	UBIGINT lShaderLocation;
	lShaderLocation = this->mDevice->GetUniformLocation( this->mCurrentTechnique->Handle(), "gTime" );
	if ( lShaderLocation == -1 )
	{
		return;
	}

	this->mDevice->Uniform1f( lShaderLocation, pTime );
}

/*************************************************************************************************/
VVOID TrailLineEffect::SetAmplitude(const FFLOAT& pAmplitude)
{
	UBIGINT lShaderLocation;
	lShaderLocation = this->mDevice->GetUniformLocation( this->mCurrentTechnique->Handle(), "gAmplitude" );
	if ( lShaderLocation == -1 )
	{
		return;
	}

	this->mDevice->Uniform1f( lShaderLocation, pAmplitude );
}

/*************************************************************************************************/
VVOID TrailLineEffect::SetOndulate(const BBOOL& pOndulate)
{
	UBIGINT lShaderLocation = this->mDevice->GetUniformLocation( this->mCurrentTechnique->Handle(), "gOndulate" );
	if ( lShaderLocation == -1 )
	{
		return;
	}

	this->mDevice->Uniform1i( lShaderLocation, pOndulate );
}

/*****************************************************************************************************/
VVOID TrailLineEffect::MakeCurrent(EffectTechnique* pCurrent)
{
	if ( pCurrent == NULL )
	{
		return;
	}

	pCurrent->MakeCurrent();
	this->mCurrentTechnique = pCurrent;

	// Check technique's particularities and set proper flags
	map<string, USMALLINT>::const_iterator lIter = this->mFlagMap.find( this->mCurrentTechnique->Name() );

	if ( lIter != this->mFlagMap.end() )
	{
		BBOOL lUseTexture = (lIter->second & Constants::cUseTextureMask) == Constants::cUseTextureMask;

		UBIGINT lShaderLocation = this->mDevice->GetUniformLocation( this->mCurrentTechnique->Handle(), "gUseTexure" );
		if
			( lShaderLocation == -1 )
		{
			return;
		}

		this->mDevice->Uniform1i( lShaderLocation, lUseTexture );
	}
}

/*****************************************************************************************************/
EffectTechnique* TrailLineEffect::TrailLineTech(const StageDescription& pStagePaths, const std::vector<std::string>& pIncludes)
{
	return new EffectTechnique( this->mDevice, Constants::cTrailLineTechName, pStagePaths, pIncludes );
}

/*****************************************************************************************************/
EffectTechnique* TrailLineEffect::TrailLineTexTech(const StageDescription& pStagePaths, const std::vector<std::string>& pIncludes)
{
	return new EffectTechnique( this->mDevice, Constants::cTrailLineTexTechName, pStagePaths, pIncludes );
}
