#include "Constants.h"

// Namespaces used
using namespace std;

/************************************************************************************************/
const string Constants::cLight1TechName = "Light1Tech";
const string Constants::cLight2TechName = "Light2Tech";
const string Constants::cLight3TechName = "Light3Tech";

const string Constants::cLight0TexTechName = "Light0TexTech";
const string Constants::cLight1TexTechName = "Light1TexTech";
const string Constants::cLight2TexTechName = "Light2TexTech";
const string Constants::cLight3TexTechName = "Light3TexTech";

const string Constants::cLight0TexAlphaClipTechName = "Light0TexAlphaClipTech";
const string Constants::cLight1TexAlphaClipTechName = "Light1TexAlphaClipTech";
const string Constants::cLight2TexAlphaClipTechName = "Light2TexAlphaClipTech";
const string Constants::cLight3TexAlphaClipTechName = "Light3TexAlphaClipTech";

const string Constants::cLight1FogTechName = "Light1FogTech";
const string Constants::cLight2FogTechName = "Light2FogTech";
const string Constants::cLight3FogTechName = "Light3FogTech";

const string Constants::cLight0TexFogTechName = "Light0TexFogTech";
const string Constants::cLight1TexFogTechName = "Light1TexFogTech";
const string Constants::cLight2TexFogTechName = "Light2TexFogTech";
const string Constants::cLight3TexFogTechName = "Light3TexFogTech";

const string Constants::cLight0TexAlphaClipFogTechName = "Light0TexAlphaClipFogTech";
const string Constants::cLight1TexAlphaClipFogTechName = "Light1TexAlphaClipFogTech";
const string Constants::cLight2TexAlphaClipFogTechName = "Light2TexAlphaClipFogTech";
const string Constants::cLight3TexAlphaClipFogTechName = "Light3TexAlphaClipFogTech";

const string Constants::cTrailLineTechName = "TrailLineTech";
const string Constants::cTrailLineTexTechName = "TrailLineTexTech";

const string Constants::cVolumetricTechName = "VolumetricTech";
const string Constants::cVolumetricTexTechName = "VolumetricTexTech";