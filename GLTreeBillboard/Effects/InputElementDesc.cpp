#include "InputElementDesc.h"

// Namespaces used
using namespace std;

/*************************************************************************************************/
InputElementDesc::InputElementDesc() :
mSemanticName(""), mOffset(NULL), mSemanticIndex(0), 
mStride(0), mDataCount(0), mDataType(0), mNeedNormalization(false)
{

}

/*************************************************************************************************/
InputElementDesc::InputElementDesc(const std::string& pSemanticName, const UBIGINT& pSemanticIndex, const UBIGINT& pStride, const USMALLINT& pDataCount, const USMALLINT& pDataType, const VVOID* pOffset, BBOOL pNeedNormalization) :
mSemanticName(pSemanticName), mOffset(pOffset), mSemanticIndex(pSemanticIndex), 
mStride(pStride), mDataCount(pDataCount), mDataType(pDataType), mNeedNormalization(pNeedNormalization)
{

}

/*************************************************************************************************/
InputElementDesc::~InputElementDesc()
{

}
