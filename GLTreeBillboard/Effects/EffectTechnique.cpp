#include "EffectTechnique.h"
#include <fstream>
#include <map>

// Namespaces used
using namespace std;

/********************************************************************************************/
StageDescription::StageDescription() :
mVertexStagePath(""), mHullStagePath(""), mDomainStagePath(""), mGeometryStagePath(""), 
mPixelStagePath(""), mComputeStagePath(""), mHasVertexStage(false), mHasHullStage(false), 
mHasDomainStage(false), mHasGeometryStage(false), mHasPixelStage(false), mHasComputeStage(false)
{

}

/********************************************************************************************/
string StageDescription::VertexStagePath() const
{
	return this->mVertexStagePath;
}

/********************************************************************************************/
VVOID  StageDescription::SetVertexStagePath(const string& pVertexStagePath)
{
	if(pVertexStagePath.empty() == false)
	{
		this->mVertexStagePath = pVertexStagePath;
		this->mHasVertexStage = true;
	}
}

/********************************************************************************************/
string StageDescription::HullStagePath() const
{
	return this->mHullStagePath;
}

/********************************************************************************************/
VVOID  StageDescription::SetHullStagePath(const string& pHullStagePath)
{
	if(pHullStagePath.empty() == false)
	{
		this->mHasHullStage = true;
		this->mHullStagePath = pHullStagePath;
	}
}

/********************************************************************************************/
string StageDescription::DomainStagePath() const
{
	return this->mDomainStagePath;
}

/********************************************************************************************/
VVOID  StageDescription::SetDomainStagePath(const string& pDomainStagePath)
{
	if(pDomainStagePath.empty() == false)
	{
		this->mHasDomainStage = true;
		this->mDomainStagePath = pDomainStagePath;
	}
}

/********************************************************************************************/
string StageDescription::GeometryStagePath() const
{
	return this->mGeometryStagePath;
}

/********************************************************************************************/
VVOID  StageDescription::SetGeometryStagePath(const string& pGeometryStagePath)
{
	if(pGeometryStagePath.empty() == false)
	{
		this->mHasGeometryStage = true;
		this->mGeometryStagePath = pGeometryStagePath;
	}
}

/********************************************************************************************/
string StageDescription::PixelStagePath() const
{
	return this->mPixelStagePath;
}

/********************************************************************************************/
VVOID  StageDescription::SetPixelStagePath(const string& pPixelStagePath)
{
	if(pPixelStagePath.empty() == false)
	{
		this->mHasPixelStage = true;
		this->mPixelStagePath = pPixelStagePath;
	}
}

/********************************************************************************************/
string StageDescription::ComputeStagePath() const
{
	return this->mComputeStagePath;
}

/********************************************************************************************/
VVOID  StageDescription::SetComputeStagePath(const string& pComputeStagePath)
{
	if(pComputeStagePath.empty() == false)
	{
		this->mHasComputeStage = true;
		this->mComputeStagePath = pComputeStagePath;
	}
}

/********************************************************************************************/
BBOOL     StageDescription::HasVertexStage() const
{
	return this->mHasVertexStage;
}

/********************************************************************************************/
BBOOL     StageDescription::HasHullStage() const
{
	return this->mHasHullStage;
}

/********************************************************************************************/
BBOOL     StageDescription::HasDomainStage() const
{
	return this->mHasDomainStage;
}

/********************************************************************************************/
BBOOL     StageDescription::HasGeometryStage() const
{
	return this->mHasGeometryStage;
}

/********************************************************************************************/
BBOOL     StageDescription::HasPixelStage() const
{
	return this->mHasPixelStage;
}

/********************************************************************************************/
BBOOL     StageDescription::HasComputeStage() const
{
	return this->mHasComputeStage;
}

/********************************************************************************************/
string EffectTechnique::LoadShaderFromFile(const string& pShaderPath)
{
	// This function will be in charge of loading
	// string containing shader code from file.
	ifstream lInFile;
	BIGINT lFileSize = 0;
	CCHAR* lBuffer = NULL;
	CCHAR lInput;

	// Open the file.
	lInFile.open(pShaderPath);
	if(lInFile.fail())
		return NULL;

	// Read the first element.
	lInFile.get(lInput);

	// Loop through the file
	// and grab the char count
	while(!lInFile.eof())
	{
		lFileSize++;
		lInFile.get(lInput);
	}

	// Close the file
	lInFile.close();

	// Now the size is known, size the lBuffer
	// and read the file content into.
	lBuffer = new CCHAR[lFileSize + 1]; // +1 for NULL terminating.
	if(!lBuffer)
		return NULL;

	// Re-open the file.
	lInFile.open(pShaderPath);

	// Read the shader code into the lBuffer.
	lInFile.read(lBuffer, lFileSize);

	// Close the file once it is done.
	lInFile.close();

	// NULL terminate the string.
	lBuffer[lFileSize] = '\0';

	string lStr(lBuffer);
	DeletePointer(lBuffer);

	return lStr;
}

/********************************************************************************************/
BBOOL  EffectTechnique::InitializeShaders(const StageDescription& pStagePaths, const vector<string>& pIncludes)
{
	BIGINT lStatus;
	UBIGINT lIncludesCount = (UBIGINT)pIncludes.size();
	const CCHAR* lStageBuffer[2];
	BIGINT lStageBufferLength[2];

	/********************************************** Create the EffectTechnique object ************************************************/
	this->mEffectShader = this->mDevice->CreateProgram();

	/****************************************** Load Includes Files into a buffer *******************************************/
	string lIncludes = "\n#version 430\n\n"; // Set at least the shader version
	for(UBIGINT lCurrInclude = 0; lCurrInclude < lIncludesCount; lCurrInclude++)
	{
		string lIncludeBuffer = this->LoadShaderFromFile(pIncludes[lCurrInclude]);
		if(lIncludeBuffer.empty() == false)
		{
			// Store the buffer of each include
			lIncludes += lIncludeBuffer;
		}
	}
	lStageBuffer[0] = lIncludes.c_str();
	lStageBufferLength[0] = (BIGINT)lIncludes.length();

	/***************************************** Load Each Stage File into a buffer ********************************************/

	if(pStagePaths.HasVertexStage())
	{
		string lVertexStage = this->LoadShaderFromFile(pStagePaths.VertexStagePath());
		if(lVertexStage.empty() == false)
		{
			lStageBuffer[1] = lVertexStage.c_str();
			lStageBufferLength[1] = (BIGINT)lVertexStage.length();

			// Create the shader objects
			this->mVertShaderID = this->mDevice->CreateShader(GL_VERTEX_SHADER);
			// Copy the shader code from strings to objects
			this->mDevice->ShaderSource(this->mVertShaderID, 2, lStageBuffer, lStageBufferLength);
			// Compile Shader stage.
			this->mDevice->CompileShader(this->mVertShaderID);
			// Check for successful compilation.
			this->mDevice->GetShaderIV(this->mVertShaderID, GL_COMPILE_STATUS, &lStatus);
			if(lStatus != 1)
			{
				ShaderErrorReport(this->mVertShaderID, pStagePaths.VertexStagePath());
				return false;
			}
			// Attach them together.
			this->mDevice->AttachShader(this->mEffectShader, this->mVertShaderID);

			GLenum lError = glGetError();
			if
				( lError != GL_NO_ERROR )
			{
				throw exception(" Vertex stage attached shader FAILURE !!! ");
			}
		}
	}

	if(pStagePaths.HasHullStage())
	{
		string lHullStage = this->LoadShaderFromFile(pStagePaths.HullStagePath());
		if(lHullStage.empty() == false)
		{
			lStageBuffer[1] = lHullStage.c_str();
			lStageBufferLength[1] = (BIGINT)lHullStage.length();

			// Create the shader objects
			this->mTessControlShaderID  = this->mDevice->CreateShader(TESS_CONTROL_SHADER);
			// Copy the shader code from strings to objects
			this->mDevice->ShaderSource(this->mTessControlShaderID, 2, lStageBuffer, lStageBufferLength);
			// Compile Shader stage.
			this->mDevice->CompileShader(this->mTessControlShaderID);
			// Check for successful compilation.
			this->mDevice->GetShaderIV(this->mTessControlShaderID, GL_COMPILE_STATUS, &lStatus);
			if(lStatus != 1)
			{
				ShaderErrorReport(this->mTessControlShaderID, pStagePaths.HullStagePath());
				return false;
			}
			// Attach them together.
			this->mDevice->AttachShader(this->mEffectShader, this->mTessControlShaderID);
		}
	}

	if(pStagePaths.HasDomainStage())
	{
		string lDomainStage = this->LoadShaderFromFile(pStagePaths.DomainStagePath());
		if(lDomainStage.empty() == false)
		{
			lStageBuffer[1] = lDomainStage.c_str();
			lStageBufferLength[1] = (BIGINT)lDomainStage.length();

			// Create the shader objects
			this->mTessEvalShaderID  = this->mDevice->CreateShader(TESS_EVALUATION_SHADER);
			// Copy the shader code from strings to objects
			this->mDevice->ShaderSource(this->mTessEvalShaderID, 2, lStageBuffer, lStageBufferLength);
			// Compile Shader stage.
			this->mDevice->CompileShader(this->mTessEvalShaderID);
			// Check for successful compilation.
			this->mDevice->GetShaderIV(this->mTessEvalShaderID, GL_COMPILE_STATUS, &lStatus);
			if(lStatus != 1)
			{
				ShaderErrorReport(this->mTessEvalShaderID, pStagePaths.DomainStagePath());
				return false;
			}
			// Attach them together.
			this->mDevice->AttachShader(this->mEffectShader, this->mTessEvalShaderID);
		}
	}

	if(pStagePaths.HasGeometryStage())
	{
		string lGeometryStage = this->LoadShaderFromFile(pStagePaths.GeometryStagePath());
		if(lGeometryStage.empty() == false)
		{
			lStageBuffer[1] = lGeometryStage.c_str();
			lStageBufferLength[1] = (BIGINT)lGeometryStage.length();

			// Create the shader objects
			this->mGeometryShaderID  = this->mDevice->CreateShader(GL_GEOMETRY_SHADER);
			// Copy the shader code from strings to objects
			this->mDevice->ShaderSource(this->mGeometryShaderID, 2, lStageBuffer, lStageBufferLength);
			// Compile Shader stage.
			this->mDevice->CompileShader(this->mGeometryShaderID);
			// Check for successful compilation.
			this->mDevice->GetShaderIV(this->mGeometryShaderID, GL_COMPILE_STATUS, &lStatus);
			if(lStatus != 1)
			{
				ShaderErrorReport(this->mGeometryShaderID, pStagePaths.GeometryStagePath());
				return false;
			}
			// Attach them together.
			this->mDevice->AttachShader(this->mEffectShader, this->mGeometryShaderID);
		}
	}

	if(pStagePaths.HasPixelStage())
	{
		string lPixelStage = this->LoadShaderFromFile(pStagePaths.PixelStagePath());
		if(lPixelStage.empty() == false)
		{
			lStageBuffer[1] = lPixelStage.c_str();
			lStageBufferLength[1] = (BIGINT)lPixelStage.length();

			// Create the shader objects
			this->mPixShaderID  = this->mDevice->CreateShader(GL_FRAGMENT_SHADER);
			// Copy the shader code from strings to objects
			this->mDevice->ShaderSource(this->mPixShaderID, 2, lStageBuffer, lStageBufferLength);
			// Compile Shader stage.
			this->mDevice->CompileShader(this->mPixShaderID);
			// Check for successful compilation.
			this->mDevice->GetShaderIV(this->mPixShaderID, GL_COMPILE_STATUS, &lStatus);
			if(lStatus != 1)
			{
				ShaderErrorReport(this->mPixShaderID, pStagePaths.PixelStagePath());
				return false;
			}
			// Attach them together.
			this->mDevice->AttachShader(this->mEffectShader, this->mPixShaderID);

			GLenum lError = glGetError();
			if
				( lError != GL_NO_ERROR )
			{
				throw exception(" Pixel stage attached shader FAILURE !!! ");
			}
		}
	}

	if(pStagePaths.HasComputeStage())
	{
		string lComputeStage = this->LoadShaderFromFile(pStagePaths.ComputeStagePath());
		if(lComputeStage.empty() == false)
		{
			lStageBuffer[1] = lComputeStage.c_str();
			lStageBufferLength[1] = (BIGINT)lComputeStage.length();

			// Create the shader objects
			this->mComputeShaderID  = this->mDevice->CreateShader(COMPUTE_SHADER);
			// Copy the shader code from strings to objects
			this->mDevice->ShaderSource(this->mComputeShaderID, 2, lStageBuffer, lStageBufferLength);
			// Compile Shader stage.
			this->mDevice->CompileShader(this->mComputeShaderID);
			// Check for successful compilation.
			this->mDevice->GetShaderIV(this->mComputeShaderID, GL_COMPILE_STATUS, &lStatus);
			if(lStatus != 1)
			{
				ShaderErrorReport(this->mComputeShaderID, pStagePaths.ComputeStagePath());
				return false;
			}
			// Attach them together.
			this->mDevice->AttachShader(this->mEffectShader, this->mComputeShaderID);
		}
	}

	return true;
}

/********************************************************************************************/
BBOOL EffectTechnique::Link()
{
	BIGINT lStatus;

	// Then, link the shader program for terminating 
	// the overall shader creation.
	this->mDevice->LinkProgram(this->mEffectShader);

	// Verify if everything is fine.
	this->mDevice->GetProgramIV(this->mEffectShader, GL_LINK_STATUS, &lStatus);
	if(lStatus != 1)
	{
		LinkerErrorReport(this->mEffectShader);
		return false;
	}

	return true;
}

/********************************************************************************************/
VVOID  EffectTechnique::ReleaseShader()
{
	// First detach all shaders
	this->mDevice->DetachShader(this->mEffectShader, this->mVertShaderID);
	this->mDevice->DetachShader(this->mEffectShader, this->mPixShaderID);

	// Then, delete them
	this->mDevice->DeleteShader(this->mVertShaderID);
	this->mDevice->DeleteShader(this->mPixShaderID);

	// Finally, delete the program
	this->mDevice->DeleteProgram(this->mEffectShader);
}

/********************************************************************************************/
VVOID  EffectTechnique::ShaderErrorReport(USMALLINT pShaderID, const string& pShaderPath)
{
	ofstream lOutFile;
	BIGINT lLogSize = 0;
	CCHAR* lInfoLog = NULL;

	// Grab the size of the string containing the info log 
	// for the failed shader compilation info.
	this->mDevice->GetShaderIV(pShaderID, GL_INFO_LOG_LENGTH, &lLogSize);

	// Add one for NULL terminating the string.
	lLogSize++;

	// Now the size is known, allocate mem for
	// the char to output.
	lInfoLog = new CCHAR[lLogSize];
	if(!lInfoLog)
		return; // Break the process.

	// Now, read the info log into the buffer.
	this->mDevice->GetShaderInfoLog(pShaderID, lLogSize, NULL, lInfoLog);

	// Then, open the output file.
	lOutFile.open("Shader.log");

	// write the info log into.
	for(UBIGINT lCurrChar = 0; lCurrChar < (UBIGINT)lLogSize; lCurrChar++)
	{
		lOutFile << lInfoLog[lCurrChar];
	}

	// Close the file once it is done.
	lOutFile.close();

	// Finally, pop-up a message informing for the failure.
	MessageBoxA(NULL, "Shader compilation error!!! \n See Shader.log file.", pShaderPath.c_str(), MB_OK);
}

/********************************************************************************************/
VVOID  EffectTechnique::LinkerErrorReport(USMALLINT pShaderID)
{
	ofstream lOutFile;
	BIGINT lLogSize = 0;
	CCHAR* lInfoLog = NULL;

	// Grab the size of the string containing the linker info log.
	this->mDevice->GetProgramIV(pShaderID, GL_INFO_LOG_LENGTH, &lLogSize);

	// Add one for NULL terminating.
	lLogSize++;

	// Allocate memory for the string buffer.
	lInfoLog = new CCHAR[lLogSize];
	if(!lInfoLog)
		return;

	// Grab the info log
	this->mDevice->GetProgramInfoLog(pShaderID, lLogSize, NULL, lInfoLog);

	// Open the log file for informing 
	lOutFile.open("Linker.log");

	// Read the info log into 
	for(UBIGINT lCurrChar = 0; lCurrChar < (UBIGINT)lLogSize; lCurrChar++)
	{
		lOutFile << lInfoLog[lCurrChar];
	}

	// Close once it is done.
	lOutFile.close();

	// Pop-up a box informing about the error.
	MessageBoxA(NULL, "Shader Linker error!!! See Linker.log file.", "..", MB_OK);
}

/********************************************************************************************/
EffectTechnique::EffectTechnique(GDevice* pDevice, string pTechniqueName, const StageDescription& pStagePaths, const vector<string>& pIncludes) :
mDevice(pDevice), mTechniqueName(pTechniqueName), mLayout(NULL)
{
	if( this->InitializeShaders(pStagePaths, pIncludes) == false )
	{
		throw exception("Shader Initialization Failed !!!");
	}
}

/********************************************************************************************/
EffectTechnique::~EffectTechnique()
{
	this->ReleaseShader();
}

/********************************************************************************************/
VVOID EffectTechnique::MakeCurrent()
{
	// Inform that this shader will be used as the current one.
	this->mDevice->UseProgram(this->mEffectShader);
}

/********************************************************************************************/
VVOID EffectTechnique::SetLayout(InputLayout* pLayout)
{
	if( pLayout == NULL )
	{
		return;
	}

	this->mLayout = pLayout;
	this->mLayout->BindVertexLayout( this->mEffectShader );
	this->Link();
}

/********************************************************************************************/
string EffectTechnique::Name() const
{
	return this->mTechniqueName;
}

/********************************************************************************************/
UBIGINT EffectTechnique::Handle() const
{
	return this->mEffectShader;
}
