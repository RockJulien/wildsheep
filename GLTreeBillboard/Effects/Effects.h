#ifndef DEF_EFFECTS_H
#define DEF_EFFECTS_H

// Includes
#include "BasicEffect.h"
#include "TreeSpriteEffect.h"
#include "VolumetricEffect.h"
#include "TrailLineEffect.h"

// Namespaces

// Class definition
class Effects
{
private:

	// Attributes
	static BasicEffect*	sBasicFX;
	static TreeSpriteEffect* sTreeSpriteFX;
	static TrailLineEffect*  sTrailLineFX;
	static VolumetricEffect* sVolumetricLineFX;
	
public:

	// Methods
	static VVOID InitializeAll(GDevice* pDevice);
	static VVOID DestroyAll();

	// Shaders handles
	static BasicEffect*		 BasicFX();
	static TreeSpriteEffect* TreeSpriteFX();
	static TrailLineEffect*  TrailLineFX();
	static VolumetricEffect* VolumetricLineFX();
};

#endif