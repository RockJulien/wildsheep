#include "TreeSpriteEffect.h"
#include "Constants.h"
#include <sstream>

// Namespaces used
using namespace std;
using namespace Ocelot;

/************************************************************************************************/
TreeSpriteEffect::TreeSpriteEffect(GDevice* pDevice) :
mCurrentTechnique(NULL), mDevice(pDevice)
{
	// Init the flags map.
	this->mFlagMap.insert(pair<string, USMALLINT>( Constants::cLight3TechName, 0 ));
	this->mFlagMap.insert(pair<string, USMALLINT>( Constants::cLight3TexAlphaClipTechName, Constants::cUseTextureMask | Constants::cAlphaClipMask ));
	this->mFlagMap.insert(pair<string, USMALLINT>( Constants::cLight3TexAlphaClipFogTechName, Constants::cUseTextureMask | Constants::cAlphaClipMask | Constants::cFogEnabledMask ));
}

/************************************************************************************************/
TreeSpriteEffect::~TreeSpriteEffect()
{

}

/************************************************************************************************/
VVOID TreeSpriteEffect::SetViewProj(const Matrix4x4& pMatrix)
{
	UBIGINT lShaderLocation;
	lShaderLocation = this->mDevice->GetUniformLocation(this->mCurrentTechnique->Handle(), "gViewProj");
	if(lShaderLocation == -1)
	{
		return;
	}

	this->mDevice->UniformMat4fv(lShaderLocation, 1, false, pMatrix);
}

/************************************************************************************************/
VVOID TreeSpriteEffect::SetEyePosW(const Vector3& pEyePos)
{
	UBIGINT lShaderLocation;
	lShaderLocation = this->mDevice->GetUniformLocation(this->mCurrentTechnique->Handle(), "gEyePosW");
	if(lShaderLocation == -1)
	{
		return;
	}

	this->mDevice->Uniform3fv( lShaderLocation, 1, pEyePos );
}

/************************************************************************************************/
VVOID TreeSpriteEffect::SetFogColor(const Color& pFogColor)
{
	UBIGINT lShaderLocation;
	lShaderLocation = this->mDevice->GetUniformLocation(this->mCurrentTechnique->Handle(), "gFogColor");
	if(lShaderLocation == -1)
	{
		return;
	}

	this->mDevice->Uniform4fv( lShaderLocation, 1, pFogColor );
}

/************************************************************************************************/
VVOID TreeSpriteEffect::SetFogStart(const FFLOAT& pFogStart)
{
	UBIGINT lShaderLocation;
	lShaderLocation = this->mDevice->GetUniformLocation(this->mCurrentTechnique->Handle(), "gFogStart");
	if(lShaderLocation == -1)
	{
		return;
	}

	this->mDevice->Uniform1f(lShaderLocation, pFogStart);
}

/************************************************************************************************/
VVOID TreeSpriteEffect::SetFogRange(const FFLOAT& pFogRange)
{
	UBIGINT lShaderLocation;
	lShaderLocation = this->mDevice->GetUniformLocation(this->mCurrentTechnique->Handle(), "gFogRange");
	if(lShaderLocation == -1)
	{
		return;
	}

	this->mDevice->Uniform1f(lShaderLocation, pFogRange);
}

/***********************************************************************************************/
VVOID TreeSpriteEffect::SetTexTransform(const Matrix4x4& pMatrix)
{
	UBIGINT lShaderLocation;
	lShaderLocation = this->mDevice->GetUniformLocation(this->mCurrentTechnique->Handle(), "gTexTransform");
	if(lShaderLocation == -1)
	{
		return;
	}

	this->mDevice->UniformMat4fv(lShaderLocation, 1, false, pMatrix);
}

/************************************************************************************************/
VVOID TreeSpriteEffect::SetDirLights(const OcelotParallelLight* pLights, UBIGINT pCount)
{
	if(pLights == NULL)
	{
		return;
	}

	UBIGINT lShaderLocation;
	lShaderLocation = this->mDevice->GetUniformLocation(this->mCurrentTechnique->Handle(), "gLightCount");
	if(lShaderLocation == -1)
	{
		return;
	}

	this->mDevice->Uniform1i(lShaderLocation, pCount);

	for( UBIGINT lCurrLight = 0; lCurrLight < pCount; lCurrLight++ )
	{
		stringstream lAmbientShaderVarName;
		lAmbientShaderVarName << "gDirLights[" << lCurrLight << "].Ambient";
		lShaderLocation = this->mDevice->GetUniformLocation( this->mCurrentTechnique->Handle(), lAmbientShaderVarName.str().c_str() );
		if(lShaderLocation == -1)
		{
			return;
		}

		this->mDevice->Uniform4fv(lShaderLocation, 1, pLights[lCurrLight].Ambient());

		stringstream lDiffuseShaderVarName;
		lDiffuseShaderVarName << "gDirLights[" << lCurrLight << "].Diffuse";
		lShaderLocation = this->mDevice->GetUniformLocation( this->mCurrentTechnique->Handle(), lDiffuseShaderVarName.str().c_str() );
		if(lShaderLocation == -1)
		{
			return;
		}

		this->mDevice->Uniform4fv(lShaderLocation, 1, pLights[lCurrLight].Diffuse());

		stringstream lSpecularShaderVarName;
		lSpecularShaderVarName << "gDirLights[" << lCurrLight << "].Specular";
		lShaderLocation = this->mDevice->GetUniformLocation( this->mCurrentTechnique->Handle(), lSpecularShaderVarName.str().c_str() );
		if(lShaderLocation == -1)
		{
			return;
		}

		this->mDevice->Uniform4fv(lShaderLocation, 1, pLights[lCurrLight].Specular());

		stringstream lDirectionShaderVarName;
		lDirectionShaderVarName << "gDirLights[" << lCurrLight << "].Direction";
		lShaderLocation = this->mDevice->GetUniformLocation(this->mCurrentTechnique->Handle(), lDirectionShaderVarName.str().c_str() );
		if(lShaderLocation == -1)
		{
			return;
		}

		this->mDevice->Uniform3fv(lShaderLocation, 1, pLights[lCurrLight].Direction());

		GLenum lError = glGetError();
		if
			( lError != GL_NO_ERROR )
		{
			throw exception(" Set Effect Light FAILURE !!! ");
		}
	}
}

/************************************************************************************************/
VVOID TreeSpriteEffect::SetMaterial(const Material& pMaterial)
{
	UBIGINT lShaderLocation;

	// Now, pass material object
	// Obviously opengl does not allow to pass an entire struct as DirectX does
	// so, we ll pass elements one by one. It could avoid padding issues that appear
	// in DirectX managing data by one or four. (e.g: vec3 need an extra float as padding).
	lShaderLocation = this->mDevice->GetUniformLocation(this->mCurrentTechnique->Handle(), "gMaterial.Ambient");
	if(lShaderLocation == -1)
	{
		return;
	}

	this->mDevice->Uniform4fv(lShaderLocation, 1, pMaterial.Ambient);

	lShaderLocation = this->mDevice->GetUniformLocation(this->mCurrentTechnique->Handle(), "gMaterial.Diffuse");
	if(lShaderLocation == -1)
	{
		return;
	}

	this->mDevice->Uniform4fv(lShaderLocation, 1, pMaterial.Diffuse);

	lShaderLocation = this->mDevice->GetUniformLocation(this->mCurrentTechnique->Handle(), "gMaterial.Specular");
	if(lShaderLocation == -1)
	{
		return;
	}

	this->mDevice->Uniform4fv(lShaderLocation, 1, pMaterial.Specular);

	lShaderLocation = this->mDevice->GetUniformLocation(this->mCurrentTechnique->Handle(), "gMaterial.Reflect");
	if(lShaderLocation == -1)
	{
		return;
	}

	this->mDevice->Uniform4fv(lShaderLocation, 1, pMaterial.Reflect);

	GLenum lError = glGetError();
	if
		( lError != GL_NO_ERROR )
	{
		throw exception(" Set Material Failure !!! ");
	}
}

/************************************************************************************************/
VVOID TreeSpriteEffect::SetTreeTextureMapArray(const UBIGINT& pTexArray)
{
	UBIGINT lShaderLocation;

	lShaderLocation = this->mDevice->GetUniformLocation(this->mCurrentTechnique->Handle(), "gTreeMapArray");
	if(lShaderLocation == -1)
	{
		return;
	}

	this->mDevice->Uniform1i(lShaderLocation, pTexArray );
}

/************************************************************************************************/
VVOID TreeSpriteEffect::MakeCurrent(EffectTechnique* pCurrent)
{
	if
		( pCurrent == NULL )
	{
		return;
	}

	pCurrent->MakeCurrent();
	this->mCurrentTechnique = pCurrent;

	// Check technique's particularities ans set proper flags
	map<string, USMALLINT>::const_iterator lIter = this->mFlagMap.find( this->mCurrentTechnique->Name() );

	if
		( lIter != this->mFlagMap.end() )
	{
		BBOOL lUseTexture = (lIter->second & Constants::cUseTextureMask) == Constants::cUseTextureMask;
		BBOOL lAlphaClip  = (lIter->second & Constants::cAlphaClipMask)  == Constants::cAlphaClipMask;
		BBOOL lUseFog     = (lIter->second & Constants::cFogEnabledMask) == Constants::cFogEnabledMask;

		UBIGINT lShaderLocation = this->mDevice->GetUniformLocation( this->mCurrentTechnique->Handle(), "gUseTexure" );
		if
			( lShaderLocation == -1 )
		{
			return;
		}

		this->mDevice->Uniform1i( lShaderLocation, lUseTexture );

		lShaderLocation = this->mDevice->GetUniformLocation( this->mCurrentTechnique->Handle(), "gAlphaClip" );
		if
			( lShaderLocation == -1 )
		{
			return;
		}

		this->mDevice->Uniform1i( lShaderLocation, lAlphaClip );

		lShaderLocation = this->mDevice->GetUniformLocation( this->mCurrentTechnique->Handle(), "gFogEnabled" );
		if
			( lShaderLocation == -1 )
		{
			return;
		}

		this->mDevice->Uniform1i( lShaderLocation, lUseFog );
	}
}

/************************************************************************************************/
EffectTechnique* TreeSpriteEffect::Light3Tech(const StageDescription& pStagePaths, const vector<string>& pIncludes)
{
	return new EffectTechnique( this->mDevice, Constants::cLight3TechName, pStagePaths, pIncludes );
}

/************************************************************************************************/
EffectTechnique* TreeSpriteEffect::Light3TexAlphaClipTech(const StageDescription& pStagePaths, const vector<string>& pIncludes)
{
	return new EffectTechnique( this->mDevice, Constants::cLight3TexAlphaClipTechName, pStagePaths, pIncludes );
}

/************************************************************************************************/
EffectTechnique* TreeSpriteEffect::Light3TexAlphaClipFogTech(const StageDescription& pStagePaths, const vector<string>& pIncludes)
{
	return new EffectTechnique( this->mDevice, Constants::cLight3TexAlphaClipFogTechName, pStagePaths, pIncludes );
}
