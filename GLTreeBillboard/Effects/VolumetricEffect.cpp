#include "VolumetricEffect.h"
#include "Constants.h"

// Namespaces used
using namespace std;
using namespace Ocelot;

/*************************************************************************************************/
VolumetricEffect::VolumetricEffect(GDevice* pDevice) :
mCurrentTechnique(NULL), mDevice(pDevice)
{
	this->mFlagMap.insert(pair<string, USMALLINT>( Constants::cVolumetricTechName, 0 ));
	this->mFlagMap.insert(pair<string, USMALLINT>( Constants::cVolumetricTexTechName, Constants::cUseTextureMask ));
}

/*************************************************************************************************/
VolumetricEffect::~VolumetricEffect()
{

}

/*************************************************************************************************/
VVOID VolumetricEffect::SetWorldViewProj(const Matrix4x4& pMatrix)
{
	UBIGINT lShaderLocation;
	lShaderLocation = this->mDevice->GetUniformLocation( this->mCurrentTechnique->Handle(), "gWorldViewProj" );
	if ( lShaderLocation == -1 )
	{
		return;
	}

	this->mDevice->UniformMat4fv( lShaderLocation, 1, false, pMatrix );
}

/*************************************************************************************************/
VVOID VolumetricEffect::SetTexTransform(const Matrix4x4& pMatrix)
{
	UBIGINT lShaderLocation;
	lShaderLocation = this->mDevice->GetUniformLocation( this->mCurrentTechnique->Handle(), "gTexTransform" );
	if ( lShaderLocation == -1 )
	{
		return;
	}

	this->mDevice->UniformMat4fv( lShaderLocation, 1, false, pMatrix );
}

/*************************************************************************************************/
VVOID VolumetricEffect::SetEyePosW(const Vector3& pEyePos)
{
	UBIGINT lShaderLocation;
	lShaderLocation = this->mDevice->GetUniformLocation(this->mCurrentTechnique->Handle(), "gEyePosW");
	if(lShaderLocation == -1)
	{
		return;
	}

	this->mDevice->Uniform3fv( lShaderLocation, 1, pEyePos );
}

/*************************************************************************************************/
VVOID VolumetricEffect::SetRadius(const FFLOAT& pRadius)
{
	UBIGINT lShaderLocation;
	lShaderLocation = this->mDevice->GetUniformLocation( this->mCurrentTechnique->Handle(), "gRadius" );
	if ( lShaderLocation == -1 )
	{
		return;
	}

	this->mDevice->Uniform1f( lShaderLocation, pRadius );
}

/*************************************************************************************************/
VVOID VolumetricEffect::SetInvSrcRatio(const FFLOAT& pRatio)
{
	UBIGINT lShaderLocation;
	lShaderLocation = this->mDevice->GetUniformLocation( this->mCurrentTechnique->Handle(), "gInvScrRatio" );
	if ( lShaderLocation == -1 )
	{
		return;
	}

	this->mDevice->Uniform1f( lShaderLocation, pRatio );
}

/*************************************************************************************************/
VVOID VolumetricEffect::SetDiffuseMap(const UBIGINT& pUnit)
{
	UBIGINT lShaderLocation;

	lShaderLocation = this->mDevice->GetUniformLocation( this->mCurrentTechnique->Handle(), "gDiffuseMap" );
	if ( lShaderLocation == -1 )
	{
		return;
	}

	this->mDevice->Uniform1i( lShaderLocation, pUnit );
}

/*************************************************************************************************/
VVOID VolumetricEffect::MakeCurrent(EffectTechnique* pCurrent)
{
	if ( pCurrent == NULL )
	{
		return;
	}

	pCurrent->MakeCurrent();
	this->mCurrentTechnique = pCurrent;

	// Check technique's particularities and set proper flags
	map<string, USMALLINT>::const_iterator lIter = this->mFlagMap.find( this->mCurrentTechnique->Name() );

	if ( lIter != this->mFlagMap.end() )
	{
		BBOOL lUseTexture = (lIter->second & Constants::cUseTextureMask) == Constants::cUseTextureMask;

		UBIGINT lShaderLocation = this->mDevice->GetUniformLocation( this->mCurrentTechnique->Handle(), "gUseTexure" );
		if
			( lShaderLocation == -1 )
		{
			return;
		}

		this->mDevice->Uniform1i( lShaderLocation, lUseTexture );
	}
}

/*************************************************************************************************/
EffectTechnique* VolumetricEffect::VolumetricTech(const StageDescription& pStagePaths, const std::vector<std::string>& pIncludes)
{
	return new EffectTechnique( this->mDevice, Constants::cVolumetricTechName, pStagePaths, pIncludes );
}

/*************************************************************************************************/
EffectTechnique* VolumetricEffect::VolumetricTexTech(const StageDescription& pStagePaths, const std::vector<std::string>& pIncludes)
{
	return new EffectTechnique( this->mDevice, Constants::cVolumetricTexTechName, pStagePaths, pIncludes );
}