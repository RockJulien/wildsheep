#ifndef DEF_INPUTLAYOUTS_H
#define DEF_INPUTLAYOUTS_H

// Includes
#include "InputLayout.h"

// Namespaces

// Class definition
class InputLayouts
{
private:

	// Attributes
	static InputLayout*	sPosNormalTexLayout;
	static InputLayout* sTreePointSpriteLayout;
	static InputLayout* sTrailLineLayout;
	static InputLayout* sVolumetricLineLayout;

public:

	// Constructor
	static VVOID InitializeAll(GDevice*	pDevice);
	static VVOID DestroyAll();

	// Layout handles
	static InputLayout* PosNormalTexLayout();
	static InputLayout* TreePointSpriteLayout();
	static InputLayout* TrailLineLayout();
	static InputLayout* VolumetricLineLayout();
};

#endif