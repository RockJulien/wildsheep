#include "Effects.h"

// Namespaces used

// Static initialization
BasicEffect*      Effects::sBasicFX = NULL;
TreeSpriteEffect* Effects::sTreeSpriteFX = NULL;
TrailLineEffect*  Effects::sTrailLineFX = NULL;
VolumetricEffect* Effects::sVolumetricLineFX = NULL;

/*****************************************************************************************************/
VVOID Effects::InitializeAll(GDevice* pDevice)
{
	if
		( Effects::sBasicFX == NULL )
	{
		Effects::sBasicFX = new BasicEffect( pDevice );
	}

	if
		( Effects::sTreeSpriteFX == NULL )
	{
		Effects::sTreeSpriteFX = new TreeSpriteEffect( pDevice );
	}

	if
		( Effects::sTrailLineFX == NULL )
	{
		Effects::sTrailLineFX = new TrailLineEffect( pDevice );
	}

	if
		( Effects::sVolumetricLineFX == NULL )
	{
		Effects::sVolumetricLineFX = new VolumetricEffect( pDevice );
	}
}

/*****************************************************************************************************/
VVOID Effects::DestroyAll()
{
	DeletePointer( Effects::sBasicFX );
	DeletePointer( Effects::sTreeSpriteFX );
	DeletePointer( Effects::sTrailLineFX );
	DeletePointer( Effects::sVolumetricLineFX );
}

/*****************************************************************************************************/
BasicEffect*      Effects::BasicFX()
{
	return Effects::sBasicFX;
}

/*****************************************************************************************************/
TreeSpriteEffect* Effects::TreeSpriteFX()
{
	return Effects::sTreeSpriteFX;
}

/*****************************************************************************************************/
TrailLineEffect*  Effects::TrailLineFX()
{
	return Effects::sTrailLineFX;
}

/*****************************************************************************************************/
VolumetricEffect* Effects::VolumetricLineFX()
{
	return Effects::sVolumetricLineFX;
}