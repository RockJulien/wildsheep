#pragma once

// Includes
#include <map>
#include <string>
#include "..\GDevice.h"
#include "..\Materials\Material.h"
#include "..\Maths\Color.h"
#include "..\Maths\Matrix4x4.h"
#include "..\Lights\OcelotLight.h"
#include "EffectTechnique.h"

// Namespaces

// Class declaration
class VolumetricEffect
{
private:

	std::map<std::string, USMALLINT> mFlagMap;

	// Attributes
	EffectTechnique* mCurrentTechnique;
	GDevice*		 mDevice;

public:

	// Constructor & destructor
	VolumetricEffect(GDevice* pDevice);
	~VolumetricEffect();

	// Methods
	VVOID SetWorldViewProj(const Matrix4x4& pMatrix);
	VVOID SetTexTransform(const Matrix4x4& pMatrix);
	VVOID SetEyePosW(const Vector3& pEyePos);
	VVOID SetRadius(const FFLOAT& pRadius);
	VVOID SetInvSrcRatio(const FFLOAT& pRatio);
	VVOID SetDiffuseMap(const UBIGINT& pUnit);

	VVOID MakeCurrent(EffectTechnique* pCurrent);

	// Technique getters
	EffectTechnique* VolumetricTech(const StageDescription& pStagePaths, const std::vector<std::string>& pIncludes);
	EffectTechnique* VolumetricTexTech(const StageDescription& pStagePaths, const std::vector<std::string>& pIncludes);
};