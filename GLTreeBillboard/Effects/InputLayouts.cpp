#include "InputLayouts.h"
#include "..\VertexTypes.h"
#include "..\GDevice.h"

// Namespaces used
using namespace Ocelot;

// Static variables initialization
InputLayout* InputLayouts::sPosNormalTexLayout = NULL;
InputLayout* InputLayouts::sTreePointSpriteLayout = NULL;
InputLayout* InputLayouts::sTrailLineLayout = NULL;
InputLayout* InputLayouts::sVolumetricLineLayout = NULL;

/******************************************************************************************************/
VVOID InputLayouts::InitializeAll(GDevice* pDevice)
{
	if( InputLayouts::sPosNormalTexLayout == NULL )
	{
		UBIGINT lStride = sizeof(SimpleVertex);
		InputLayouts::sPosNormalTexLayout = new InputLayout( pDevice );
		InputLayouts::sPosNormalTexLayout->AddElement( InputElementDesc("PosL",    0, lStride, 3, GL_FLOAT) );
		InputLayouts::sPosNormalTexLayout->AddElement( InputElementDesc("NormalL", 1, lStride, 3, GL_FLOAT, (UCCHAR*)NULL + (3 * sizeof(FFLOAT))) );
		InputLayouts::sPosNormalTexLayout->AddElement( InputElementDesc("TexIn",   2, lStride, 2, GL_FLOAT, (UCCHAR*)NULL + (6 * sizeof(FFLOAT))) );
	}

	if( InputLayouts::sTreePointSpriteLayout == NULL )
	{
		UBIGINT lStride = sizeof(TreePointSprite);
		InputLayouts::sTreePointSpriteLayout = new InputLayout( pDevice );
		InputLayouts::sTreePointSpriteLayout->AddElement( InputElementDesc("PosW",  0, lStride, 3, GL_FLOAT) );
		InputLayouts::sTreePointSpriteLayout->AddElement( InputElementDesc("SizeW", 1, lStride, 2, GL_FLOAT, (UCCHAR*)NULL + (3 * sizeof(FFLOAT))) );
	}

	if ( InputLayouts::sTrailLineLayout == NULL )
	{
		UBIGINT lStride = sizeof(TrailVertex);
		InputLayouts::sTrailLineLayout = new InputLayout( pDevice );
		InputLayouts::sTrailLineLayout->AddElement( InputElementDesc("PosL",		   0, lStride, 3, GL_FLOAT) );
		InputLayouts::sTrailLineLayout->AddElement( InputElementDesc("NormalL",		   1, lStride, 3, GL_FLOAT, (UCCHAR*)NULL + (3 * sizeof(FFLOAT))) );
		InputLayouts::sTrailLineLayout->AddElement( InputElementDesc("TexCoordNIndex", 2, lStride, 3, GL_FLOAT, (UCCHAR*)NULL + (6 * sizeof(FFLOAT))) );
	}

	if ( InputLayouts::sVolumetricLineLayout == NULL )
	{
		UBIGINT lStride = /*sizeof(VolumetricLineVertex);*/ 0; // No stride as each vertex component will come from a different buffer. 
		InputLayouts::sVolumetricLineLayout = new InputLayout( pDevice );
		InputLayouts::sVolumetricLineLayout->AddElement( InputElementDesc("PosL",          0, lStride, 3, GL_FLOAT) );
		InputLayouts::sVolumetricLineLayout->AddElement( InputElementDesc("DuplicatePosL", 1, lStride, 3, GL_FLOAT/*, (UCCHAR*)NULL + (3 * sizeof(FFLOAT))*/) ); // No stride thus no offset.
		InputLayouts::sVolumetricLineLayout->AddElement( InputElementDesc("OffsetUV",      2, lStride, 4, GL_FLOAT/*, (UCCHAR*)NULL + (6 * sizeof(FFLOAT))*/) ); // No stride thus no offset.
	}
}

/******************************************************************************************************/
VVOID InputLayouts::DestroyAll()
{
	DeletePointer( InputLayouts::sPosNormalTexLayout );
	DeletePointer( InputLayouts::sTreePointSpriteLayout );
	DeletePointer( InputLayouts::sTrailLineLayout );
	DeletePointer( InputLayouts::sVolumetricLineLayout );
}

/******************************************************************************************************/
InputLayout* InputLayouts::PosNormalTexLayout()
{
	return InputLayouts::sPosNormalTexLayout;
}

/******************************************************************************************************/
InputLayout* InputLayouts::TreePointSpriteLayout()
{
	return InputLayouts::sTreePointSpriteLayout;
}

/******************************************************************************************************/
InputLayout* InputLayouts::TrailLineLayout()
{
	return InputLayouts::sTrailLineLayout;
}

/******************************************************************************************************/
InputLayout* InputLayouts::VolumetricLineLayout()
{
	return InputLayouts::sVolumetricLineLayout;
}