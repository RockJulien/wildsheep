#ifndef MATRIX3X4_H
#define MATRIX3X4_H

//Spanish cow go "EL MOO"

#include "Vector3.h"
#include "Quaternion.h"

namespace Ocelot
{
	class Matrix3x4
	{
	public:
		Matrix3x4();
		Matrix3x4(const Matrix3x4& m);
		Matrix3x4(const float _11, const float _12, const float _13, const float _14,
			      const float _21, const float _22, const float _23, const float _24,
				  const float _31, const float _32, const float _33, const float _34);

		Vector3 Transform(const Vector3& v) const;
		Vector3 TransformInverse(const Vector3 &v) const;
		Vector3 TransformDirection(const Vector3 &v) const;
		Vector3 TransformDirectionInverse(const Vector3 &v) const;
		void Matrix3x4Multiply(Matrix3x4 &out, const Matrix3x4 &mat);
		void Matrix3x4Inverse(Matrix3x4 &out);
		float GetDeterminant() const;
		void SetOrientationAndPosition(Quaternion &q, const Vector3 &position);
		
		Vector3 GetAxisVector(int i) const;
		
		inline float get11() const { return m_f11; }
		inline float get21() const { return m_f21; }
		inline float get31() const { return m_f31; }
		inline float get12() const { return m_f12; }
		inline float get22() const { return m_f22; }
		inline float get32() const { return m_f32; }
		inline float get13() const { return m_f13; }
		inline float get23() const { return m_f23; }
		inline float get33() const { return m_f33; }
		inline float get14() const { return m_f14; }
		inline float get24() const { return m_f24; }
		inline float get34() const { return m_f34; }

		inline void set11(const float _11) { m_f11 = _11; }
		inline void set12(const float _12) { m_f12 = _12; }
		inline void set13(const float _13) { m_f13 = _13; }
		inline void set14(const float _14) { m_f14 = _14; }
		inline void set21(const float _21) { m_f21 = _21; }
		inline void set22(const float _22) { m_f22 = _22; }
		inline void set23(const float _23) { m_f23 = _23; }
		inline void set24(const float _24) { m_f24 = _24; }
		inline void set31(const float _31) { m_f31 = _31; }
		inline void set32(const float _32) { m_f32 = _32; }
		inline void set33(const float _33) { m_f33 = _33; }
		inline void set34(const float _34) { m_f34 = _34; }
	private:
		float m_f11, m_f12, m_f13, m_f14;
		float m_f21, m_f22, m_f23, m_f24;
		float m_f31, m_f32, m_f33, m_f34;
	};
}

#endif