#include "Plane.h"

using namespace Ocelot;

Plane::Plane() :
m_fA(0.0f), m_fB(0.0f), m_fC(0.0f), m_fD(0.0f)
{

}

Plane::Plane(float a, float b, float c, float d) :
m_fA(a), m_fB(b), m_fC(c), m_fD(d)
{

}

Plane::~Plane()
{

}

void  Plane::PlaneNormalize()
{
	// one of the normals of the plane could be represented
	// by its three components A, B and C
	Vector3 normalToPlane(m_fA, m_fB, m_fC);
	float magnitude = normalToPlane.Vec3Length();

	m_fA /= magnitude;
	m_fB /= magnitude;
	m_fC /= magnitude;
	m_fD /= magnitude;
}

float Plane::PlaneDotCoord(const Vector3& point)
{
	// we assume that w is equal to 1 (so a point allowing translation, not a direction)
	float result;
	result = (m_fA * point.getX()) + (m_fB * point.getY()) + (m_fC * point.getZ()) + m_fD;

	return result;
}

float Plane::PlaneDot(const Vector4& point)
{
	return (m_fA * point.getX()) + (m_fB * point.getY()) + (m_fC * point.getZ()) + (m_fD * point.getW());
}

float Plane::PlaneDotNormal(const Vector3& point)
{
	return (m_fA * point.getX()) + (m_fB * point.getY()) + (m_fC * point.getZ());
}

void  Plane::PlaneRescale(Plane& out, const float& scale)
{
	// rescale the plane
	out.m_fA = m_fA * scale;
	out.m_fB = m_fB * scale;
	out.m_fC = m_fC * scale;
	out.m_fD = m_fD * scale;
}

void  Plane::PlaneCreateFrom3Points(const Vector3& point1, const Vector3& point2, const Vector3& point3)
{
	// Cramer's Rule
	m_fD = 1.0f; // just need to be non-zero

	Matrix3x3 BigD(point1.getX(), point1.getY(), point1.getZ(),
				   point2.getX(), point2.getY(), point2.getZ(),
				   point3.getX(), point3.getY(), point3.getZ());

	Matrix3x3 InvBigD;
	BigD.Matrix3x3Inverse(InvBigD);

	Matrix3x3 matForA(1.0f, point1.getY(), point1.getZ(),
					  1.0f, point2.getY(), point2.getZ(),
					  1.0f, point3.getY(), point3.getZ());

	Matrix3x3 matForB(point1.getX(), 1.0f, point1.getZ(),
					  point2.getX(), 1.0f, point2.getZ(),
					  point3.getX(), 1.0f, point3.getZ());

	Matrix3x3 matForC(point1.getX(), point1.getY(), 1.0f,
					  point2.getX(), point2.getY(), 1.0f,
					  point3.getX(), point3.getY(), 1.0f);

	/*m_fA = -m_fD * InvBigD * matForA;
	m_fB = -m_fD * InvBigD * matForB;
	m_fC = -m_fD * InvBigD * matForC;*/
}

Plane::operator float* ()
{
	float* plan[4];

	plan[0] = &m_fA;
	plan[1] = &m_fB;
	plan[2] = &m_fB;
	plan[3] = &m_fC;

	return plan[0];
}

Plane::operator const float* () const
{
	const float* plan[4];

	plan[0] = &m_fA;
	plan[1] = &m_fB;
	plan[2] = &m_fB;
	plan[3] = &m_fC;

	return plan[0];
}

Plane  Plane::operator + () const
{
	return Plane(+m_fA, +m_fB, +m_fC, +m_fD);
}

Plane  Plane::operator - () const
{
	return Plane(-m_fA, -m_fB, -m_fC, -m_fD);
}

Plane& Plane::operator *= (float val)
{
	m_fA *= val;
	m_fB *= val;
	m_fC *= val;
	m_fD *= val;

	return *this;
}

Plane& Plane::operator /= (float val)
{
	m_fA /= val;
	m_fB /= val;
	m_fC /= val;
	m_fD /= val;

	return *this;
}

Plane  Plane::operator * (float val) const
{
	Plane temp(*this);
	temp *= val;

	return temp;
}

Plane  Plane::operator / (float val) const
{
	Plane temp(*this);
	temp /= val;

	return temp;
}

bool   Plane::operator == (const Plane& plan) const
{
	return (Equal<float>(m_fA, plan.m_fA) && Equal<float>(m_fB, plan.m_fB) && Equal<float>(m_fC, plan.m_fC) && Equal<float>(m_fD, plan.m_fD));
}

bool   Plane::operator != (const Plane& plan) const
{
	return (Different<float>(m_fA, plan.m_fA) || Different<float>(m_fB, plan.m_fB) || Different<float>(m_fC, plan.m_fC) || Different<float>(m_fD, plan.m_fD));
}

Plane operator * (float val, const Plane& plan)
{
	Plane temp(plan);
	temp *= val;

	return temp;
}
