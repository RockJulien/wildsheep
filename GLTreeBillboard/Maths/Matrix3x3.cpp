#include "Matrix3x3.h"

Matrix3x3::Matrix3x3() :
m_f11(0.0f), m_f12(0.0f), m_f13(0.0f),
m_f21(0.0f), m_f22(0.0f), m_f23(0.0f),
m_f31(0.0f), m_f32(0.0f), m_f33(0.0f)
{
	
}

Matrix3x3::Matrix3x3(const Matrix3x3& mat) :
m_f11(mat.m_f11), m_f12(mat.m_f12), m_f13(mat.m_f13),
m_f21(mat.m_f21), m_f22(mat.m_f22), m_f23(mat.m_f23),
m_f31(mat.m_f31), m_f32(mat.m_f32), m_f33(mat.m_f33)
{
	
}

Matrix3x3& Matrix3x3::operator = (const Matrix3x3& mat)
{
	m_f11 = mat.m_f11;
	m_f12 = mat.m_f12;
	m_f13 = mat.m_f13;
	m_f21 = mat.m_f21;
	m_f22 = mat.m_f22;
	m_f23 = mat.m_f23;
	m_f31 = mat.m_f31;
	m_f32 = mat.m_f32;
	m_f33 = mat.m_f33;

	return *this;
}

Matrix3x3::Matrix3x3(float val) :
m_f11(val), m_f12(val), m_f13(val),
m_f21(val), m_f22(val), m_f23(val),
m_f31(val), m_f32(val), m_f33(val)
{

}

Matrix3x3::Matrix3x3(double val) :
m_f11((float)val), m_f12((float)val), m_f13((float)val),
m_f21((float)val), m_f22((float)val), m_f23((float)val),
m_f31((float)val), m_f32((float)val), m_f33((float)val)
{

}

Matrix3x3::Matrix3x3(float _11, float _12, float _13,
			         float _21, float _22, float _23,
			         float _31, float _32, float _33) :
m_f11(_11), m_f12(_12), m_f13(_13),
m_f21(_21), m_f22(_22), m_f23(_23),
m_f31(_31), m_f32(_32), m_f33(_33)
{

}

Matrix3x3::Matrix3x3(double _11, double _12, double _13,
					 double _21, double _22, double _23,
					 double _31, double _32, double _33) :
m_f11((float)_11), m_f12((float)_12), m_f13((float)_13),
m_f21((float)_21), m_f22((float)_22), m_f23((float)_23),
m_f31((float)_31), m_f32((float)_32), m_f33((float)_33)
{

}

Matrix3x3::~Matrix3x3()
{

}

void      Matrix3x3::Matrix3x3Identity()
{
	// initialize as an identity matrix with 1 along the main diagonal.
	m_f11 = 1.0f; m_f12 = 0.0f; m_f13 = 0.0f;
	m_f21 = 0.0f; m_f22 = 1.0f; m_f23 = 0.0f;
	m_f31 = 0.0f; m_f32 = 0.0f; m_f33 = 1.0f;
}

bool      Matrix3x3::Matrix3x3IsIdentity()
{
	// check if the current matrix is an identity one
	Matrix3x3 temp(*this);
	Matrix3x3 temp2;
	temp2.Matrix3x3Identity();

	if(Equal<Matrix3x3>(temp, temp2))
		return true;
	return false;
}

void	  Matrix3x3::Matrix3x3Transpose(Matrix3x3 &mat)
{
	Matrix3x3 temp(*this);

	// transpose the matrix by changing rows with columns
	mat.m_f11 = temp.m_f11; mat.m_f12 = temp.m_f21; mat.m_f13 = temp.m_f31;
	mat.m_f21 = temp.m_f12; mat.m_f22 = temp.m_f22; mat.m_f23 = temp.m_f32;
	mat.m_f31 = temp.m_f13; mat.m_f32 = temp.m_f23; mat.m_f33 = temp.m_f33;
}

float     Matrix3x3::Matrix3x3Determinant()
{
	// compute the determinant of the matrix
	// it could be useful for inversing the matrix
	float result = 0.0f;

	// following the sarrus method
	return result = (m_f11 * m_f22 * m_f33) +
					(m_f21 * m_f32 * m_f13) +
					(m_f31 * m_f12 * m_f23) -
					(m_f13 * m_f22 * m_f31) -
					(m_f23 * m_f32 * m_f11) -
					(m_f33 * m_f12 * m_f21);					
}

void	  Matrix3x3::Matrix3x3Inverse(Matrix3x3 &mat)
{
	// first compute the determinant
	float determinant = this->Matrix3x3Determinant();

	// if the determinant is not equal to zero,
	// the matrix inverse is possible.
	if(Different<float>(determinant, 0.0f))
	{
		// compute Minors for each component of the matrix
		// for each element, eliminate other components on
		// the same row and column and create a new one 
		// with the rest for computing cofactors in a same
		// way that for the determinant.
		/*******************First row************************/
		Matrix2x2 minorMat_11(m_f22, m_f23, m_f32, m_f33);
		float cofactor_11 = minorMat_11.Matrix2x2Determinant();
		Matrix2x2 minorMat_12(m_f21, m_f23, m_f31, m_f33);
		float cofactor_12 = minorMat_12.Matrix2x2Determinant();
		Matrix2x2 minorMat_13(m_f21, m_f22, m_f31, m_f32);
		float cofactor_13 = minorMat_13.Matrix2x2Determinant();
		/*******************Second Row***********************/
		Matrix2x2 minorMat_21(m_f12, m_f13, m_f32, m_f33);
		float cofactor_21 = minorMat_21.Matrix2x2Determinant();
		Matrix2x2 minorMat_22(m_f11, m_f13, m_f31, m_f33);
		float cofactor_22 = minorMat_22.Matrix2x2Determinant();
		Matrix2x2 minorMat_23(m_f11, m_f12, m_f31, m_f32);
		float cofactor_23 = minorMat_23.Matrix2x2Determinant();
		/*******************Third Row************************/
		Matrix2x2 minorMat_31(m_f12, m_f13, m_f22, m_f23);
		float cofactor_31 = minorMat_31.Matrix2x2Determinant();
		Matrix2x2 minorMat_32(m_f11, m_f13, m_f21, m_f23);
		float cofactor_32 = minorMat_32.Matrix2x2Determinant();
		Matrix2x2 minorMat_33(m_f11, m_f12, m_f21, m_f22);
		float cofactor_33 = minorMat_33.Matrix2x2Determinant();

		// Now we can rebuild the new matrix with cofactors without forgetting
		// to inverse values representing the middle diamond of the 3x3 matrix
		// and transpose it.
		// for calculating the sign to put before each cofactor, just use the
		// following calcul : (-1)^(line + col) or (C++ style) pow(-1.0f, (line + col)).
		mat.m_f11 = cofactor_11;  mat.m_f12 = -cofactor_12; mat.m_f13 = cofactor_13;
		mat.m_f21 = -cofactor_21; mat.m_f22 = cofactor_22; mat.m_f23 = -cofactor_23;
		mat.m_f31 = cofactor_31;  mat.m_f32 = -cofactor_32; mat.m_f33 = cofactor_33;

		mat.Matrix3x3Transpose(mat); // invert rows and columns.

		// to end up by dividing by the determinant.
		mat /= determinant;
	}
}

void      Matrix3x3::Matrix3x3Mutliply(Matrix3x3& out, const Matrix3x3& mat)
{
	// Multiplication between two matrices.
	// each row is going to be multiplied by each 
	// element of the column where the value processed is.
	Matrix3x3 temp(*this);

	/*****************first row****************************/
	out.m_f11 = (temp.m_f11 * mat.m_f11) + (temp.m_f12 * mat.m_f21) + (temp.m_f13 * mat.m_f31);
	out.m_f12 = (temp.m_f11 * mat.m_f12) + (temp.m_f12 * mat.m_f22) + (temp.m_f13 * mat.m_f32);
	out.m_f13 = (temp.m_f11 * mat.m_f13) + (temp.m_f12 * mat.m_f23) + (temp.m_f13 * mat.m_f33);
	/*****************second row***************************/
	out.m_f21 = (temp.m_f21 * mat.m_f11) + (temp.m_f22 * mat.m_f21) + (temp.m_f23 * mat.m_f31);
	out.m_f22 = (temp.m_f21 * mat.m_f12) + (temp.m_f22 * mat.m_f22) + (temp.m_f23 * mat.m_f32);
	out.m_f23 = (temp.m_f21 * mat.m_f13) + (temp.m_f22 * mat.m_f23) + (temp.m_f23 * mat.m_f33);
	/*****************third row****************************/
	out.m_f31 = (temp.m_f31 * mat.m_f11) + (temp.m_f32 * mat.m_f21) + (temp.m_f33 * mat.m_f31);
	out.m_f32 = (temp.m_f31 * mat.m_f12) + (temp.m_f32 * mat.m_f22) + (temp.m_f33 * mat.m_f32);
	out.m_f33 = (temp.m_f31 * mat.m_f13) + (temp.m_f32 * mat.m_f23) + (temp.m_f33 * mat.m_f33);
}

void      Matrix3x3::Matrix3x3Mutliply(Matrix3x3& out, const Matrix3x3& mat1, const Matrix3x3& mat2)
{
	// multiply with the first (view for example)
	Matrix3x3Mutliply(out, mat1);
	// then, multiply with the second (projection for example)
	out.Matrix3x3Mutliply(out, mat2);
}

void      Matrix3x3::Matrix3x3MultiplyTranspose(Matrix3x3& out, const Matrix3x3& mat)
{
	// invert result of a multiplication by changing
	// rows with columns.
	Matrix3x3Mutliply(out, mat);

	// just add the transpose fonction into this one.
	out.Matrix3x3Transpose(out);
}

void      Matrix3x3::Matrix3x3MultiplyTranspose(Matrix3x3& out, const Matrix3x3& mat1, const Matrix3x3& mat2)
{
	// same punition :-).
	Matrix3x3Mutliply(out, mat1, mat2);

	out.Matrix3x3Transpose(out);  // magic
}

void      Matrix3x3::Matrix3x3Translation(Matrix3x3& out, const float& Txy)
{
	// Declare a new matrix which will represent the
	// translation to apply.
	// and we will apply by multiply this new one 
	// with the matrix which called the function.
	Matrix3x3 matTranslation;
	matTranslation.Matrix3x3Identity();
	matTranslation.set_31(Txy);
	matTranslation.set_32(Txy);

	// now we apply
	Matrix3x3Mutliply(out, matTranslation);
}

void      Matrix3x3::Matrix3x3Translation(Matrix3x3& out, const double& Txy)
{
	// simple overload for lazy programmers.
	// Let's save some fingers and also the wish
	// to use, for some people a double :-).
	Matrix3x3 matTranslation;
	matTranslation.Matrix3x3Identity();
	matTranslation.set_31((float)Txy); // in all case, it will be a float hahahaha.
	matTranslation.set_32((float)Txy); // I am the devil :-).

	// now we apply
	Matrix3x3Mutliply(out, matTranslation);
}

void      Matrix3x3::Matrix3x3Translation(Matrix3x3& out, const float& Tx, const float& Ty)
{
	// For less lazy guy who want to control every
	// variables in details. Better !!!
	Matrix3x3 matTranslation;
	matTranslation.Matrix3x3Identity();
	matTranslation.set_31(Tx); 
	matTranslation.set_32(Ty); 

	// now we apply
	Matrix3x3Mutliply(out, matTranslation);
}

void      Matrix3x3::Matrix3x3Translation(Matrix3x3& out, const double& Tx, const double& Ty)
{
	// And again an overload with double, sometimes
	// unfortunatly, some person forget the "f" for a float
	// now it is fixed ;-).
	Matrix3x3 matTranslation;
	matTranslation.Matrix3x3Identity();
	matTranslation.set_31((float)Tx); // In all case, it will be a float hahahaha.
	matTranslation.set_32((float)Ty); // I am the devil :-).

	// now we apply
	Matrix3x3Mutliply(out, matTranslation);
}

void      Matrix3x3::Matrix3x3Scaling(Matrix3x3& out, const float& Sxy)
{
	// Same as the translation one, we create
	// a scaling matrix for applying a size change
	// at the matrix which called the function.
	Matrix3x3 matScaling;
	matScaling.Matrix3x3Identity();
	matScaling.set_11(Sxy);
	matScaling.set_22(Sxy);

	// now we apply as previously
	Matrix3x3Mutliply(out, matScaling);
}

void      Matrix3x3::Matrix3x3Scaling(Matrix3x3& out, const double& Sxy)
{
	Matrix3x3 matScaling;
	matScaling.Matrix3x3Identity();
	matScaling.set_11((float)Sxy);
	matScaling.set_22((float)Sxy);

	// now we apply as previously
	Matrix3x3Mutliply(out, matScaling);
}

void      Matrix3x3::Matrix3x3Scaling(Matrix3x3& out, const float& Sx, const float& Sy)
{
	Matrix3x3 matScaling;
	matScaling.Matrix3x3Identity();
	matScaling.set_11(Sx);
	matScaling.set_22(Sy);

	// now we apply as previously
	Matrix3x3Mutliply(out, matScaling);
}

void      Matrix3x3::Matrix3x3Scaling(Matrix3x3& out, const double& Sx, const double& Sy)
{
	Matrix3x3 matScaling;
	matScaling.Matrix3x3Identity();
	matScaling.set_11((float)Sx);
	matScaling.set_22((float)Sy);

	// now we apply as previously
	Matrix3x3Mutliply(out, matScaling);
}

void      Matrix3x3::Matrix3x3RotationX(Matrix3x3& out, const float& Rx)
{
	// compute before for avoiding to compute twice.
	float Cos = cos(Rx);
	float Sin = sin(Rx);

	Matrix3x3 matRotationX;
	matRotationX.Matrix3x3Identity();
	matRotationX.set_22(Cos);
	matRotationX.set_23(Sin);
	matRotationX.set_32(-Sin);
	matRotationX.set_33(Cos);

	Matrix3x3Mutliply(out, matRotationX);
}

void      Matrix3x3::Matrix3x3RotationX(Matrix3x3& out, const double& Rx)
{
	// compute before for avoiding to compute twice.
	float Cos = cos((float)Rx);
	float Sin = sin((float)Rx);

	Matrix3x3 matRotationX;
	matRotationX.Matrix3x3Identity();
	matRotationX.set_22(Cos);
	matRotationX.set_23(Sin);
	matRotationX.set_32(-Sin);
	matRotationX.set_33(Cos);

	Matrix3x3Mutliply(out, matRotationX);
}

void      Matrix3x3::Matrix3x3RotationY(Matrix3x3& out, const float& Ry)
{
	// compute before for avoiding to compute twice.
	float Cos = cos(Ry);
	float Sin = sin(Ry);

	Matrix3x3 matRotationY;
	matRotationY.Matrix3x3Identity();
	matRotationY.set_11(Cos);
	matRotationY.set_13(-Sin);
	matRotationY.set_31(Sin);
	matRotationY.set_33(Cos);

	Matrix3x3Mutliply(out, matRotationY);
}

void      Matrix3x3::Matrix3x3RotationY(Matrix3x3& out, const double& Ry)
{
	// compute before for avoiding to compute twice.
	float Cos = cos((float)Ry);
	float Sin = sin((float)Ry);

	Matrix3x3 matRotationY;
	matRotationY.Matrix3x3Identity();
	matRotationY.set_11(Cos);
	matRotationY.set_13(-Sin);
	matRotationY.set_31(Sin);
	matRotationY.set_33(Cos);

	Matrix3x3Mutliply(out, matRotationY);
}

void      Matrix3x3::Matrix3x3RotationZ(Matrix3x3& out, const float& Rz)
{
	// compute before for avoiding to compute twice.
	float Cos = cos(Rz);
	float Sin = sin(Rz);

	Matrix3x3 matRotationZ;
	matRotationZ.Matrix3x3Identity();
	matRotationZ.set_11(Cos);
	matRotationZ.set_12(Sin);
	matRotationZ.set_21(-Sin);
	matRotationZ.set_22(Cos);

	Matrix3x3Mutliply(out, matRotationZ);
}

void      Matrix3x3::Matrix3x3RotationZ(Matrix3x3& out, const double& Rz)
{
	// compute before for avoiding to compute twice.
	float Cos = cos((float)Rz);
	float Sin = sin((float)Rz);

	Matrix3x3 matRotationZ;
	matRotationZ.Matrix3x3Identity();
	matRotationZ.set_11(Cos);
	matRotationZ.set_12(Sin);
	matRotationZ.set_21(-Sin);
	matRotationZ.set_22(Cos);

	Matrix3x3Mutliply(out, matRotationZ);
}

void      Matrix3x3::Matrix3x3RotationGeneralized(Matrix3x3& out, const float& Rxyz)
{
	// we could either create each different rotation matrix
	// on each axis and multiply them together, or calculate 
	// all in one generalized matrix but means more maths :-).
	// That is why I chose generalized one with maths :-)
	// R(alpha, beta, phi) = Rz(alpha)*Ry(beta)*Rx(phi) = below.
	Matrix3x3 matYawPitchRoll;
	matYawPitchRoll.Matrix3x3Identity();

	// compute cosine and sine result before for avoiding
	// to compute them more than once.
	// Yeah save your PC, not only your fingers!!!
	float CosAlpha = cos(Rxyz);
	float SinAlpha = sin(Rxyz);
	float CosBeta  = cos(Rxyz);
	float SinBeta  = sin(Rxyz);
	float CosPhi   = cos(Rxyz);
	float SinPhi   = sin(Rxyz);

	matYawPitchRoll.set_11((CosAlpha * CosBeta)); matYawPitchRoll.set_12(((CosAlpha * SinBeta * SinPhi) - (SinAlpha * CosPhi))); matYawPitchRoll.set_13(((CosAlpha * SinBeta * CosPhi) + (SinAlpha * SinPhi)));
	matYawPitchRoll.set_21((SinAlpha * CosBeta)); matYawPitchRoll.set_22(((SinAlpha * SinBeta * SinPhi) + (CosAlpha * CosPhi))); matYawPitchRoll.set_23(((SinAlpha * SinBeta * CosPhi) - (CosAlpha * SinPhi)));
	matYawPitchRoll.set_31((-SinBeta));           matYawPitchRoll.set_32((CosBeta * SinPhi));                                    matYawPitchRoll.set_33((CosBeta * CosPhi));

	Matrix3x3Mutliply(out, matYawPitchRoll);
}

void      Matrix3x3::Matrix3x3RotationGeneralized(Matrix3x3& out, const double& Rxyz)
{
	// the order is important. The way you multiply will 
	// determine the way it rotates.
	// DirectX made a mistake by starting with the "yaw"
	// of the "yawPitchRoll" for the X axis. The yaw is 
	// applied to the Z axis (e.g: avionic) and the "roll"
	// must be perform first.
	Matrix3x3 matYawPitchRoll;
	matYawPitchRoll.Matrix3x3Identity();

	// compute cosine and sine result before for avoiding
	// to compute them more than once.
	// Yeah save your PC, not only your fingers!!!
	float CosAlpha = cos((float)Rxyz);
	float SinAlpha = sin((float)Rxyz);
	float CosBeta  = cos((float)Rxyz);
	float SinBeta  = sin((float)Rxyz);
	float CosPhi   = cos((float)Rxyz);
	float SinPhi   = sin((float)Rxyz);

	matYawPitchRoll.set_11((CosAlpha * CosBeta)); matYawPitchRoll.set_12(((CosAlpha * SinBeta * SinPhi) - (SinAlpha * CosPhi))); matYawPitchRoll.set_13(((CosAlpha * SinBeta * CosPhi) + (SinAlpha * SinPhi)));
	matYawPitchRoll.set_21((SinAlpha * CosBeta)); matYawPitchRoll.set_22(((SinAlpha * SinBeta * SinPhi) + (CosAlpha * CosPhi))); matYawPitchRoll.set_23(((SinAlpha * SinBeta * CosPhi) - (CosAlpha * SinPhi)));
	matYawPitchRoll.set_31((-SinBeta));           matYawPitchRoll.set_32((CosBeta * SinPhi));                                    matYawPitchRoll.set_33((CosBeta * CosPhi));

	Matrix3x3Mutliply(out, matYawPitchRoll);
}

void      Matrix3x3::Matrix3x3RotationGeneralized(Matrix3x3& out, const float& Rx, const float& Ry, const float& Rz)
{
	// same overload of method as previously
	// but in this case, be aware of where you 
	// put "x" (Roll), "y" (Pitch) and "z" (Yaw) value.
	Matrix3x3 matYawPitchRoll;
	matYawPitchRoll.Matrix3x3Identity();

	// compute cosine and sine result before for avoiding
	// to compute them more than once.
	// Yeah save your PC, not only your fingers!!!
	float CosAlpha = cos(Rz);  // Yaw (Z)
	float SinAlpha = sin(Rz);  // Yaw (Z)
	float CosBeta  = cos(Ry);  // Pitch (Y)
	float SinBeta  = sin(Ry);  // Pitch (Y)
	float CosPhi   = cos(Rx);  // Roll (X)
	float SinPhi   = sin(Rx);  // Roll (X)

	matYawPitchRoll.set_11((CosAlpha * CosBeta)); matYawPitchRoll.set_12(((CosAlpha * SinBeta * SinPhi) - (SinAlpha * CosPhi))); matYawPitchRoll.set_13(((CosAlpha * SinBeta * CosPhi) + (SinAlpha * SinPhi)));
	matYawPitchRoll.set_21((SinAlpha * CosBeta)); matYawPitchRoll.set_22(((SinAlpha * SinBeta * SinPhi) + (CosAlpha * CosPhi))); matYawPitchRoll.set_23(((SinAlpha * SinBeta * CosPhi) - (CosAlpha * SinPhi)));
	matYawPitchRoll.set_31((-SinBeta));           matYawPitchRoll.set_32((CosBeta * SinPhi));                                    matYawPitchRoll.set_33((CosBeta * CosPhi));

	Matrix3x3Mutliply(out, matYawPitchRoll);
}

void      Matrix3x3::Matrix3x3RotationGeneralized(Matrix3x3& out, const double& Rx, const double& Ry, const double& Rz)
{
	Matrix3x3 matYawPitchRoll;
	matYawPitchRoll.Matrix3x3Identity();

	// compute cosine and sine result before for avoiding
	// to compute them more than once.
	// Yeah save your PC, not only your fingers!!!
	float CosAlpha = cos((float)Rz);  // Yaw (Z)
	float SinAlpha = sin((float)Rz);  // Yaw (Z)
	float CosBeta  = cos((float)Ry);  // Pitch (Y)
	float SinBeta  = sin((float)Ry);  // Pitch (Y)
	float CosPhi   = cos((float)Rx);  // Roll (X)
	float SinPhi   = sin((float)Rx);  // Roll (X)

	matYawPitchRoll.set_11((CosAlpha * CosBeta)); matYawPitchRoll.set_12(((CosAlpha * SinBeta * SinPhi) - (SinAlpha * CosPhi))); matYawPitchRoll.set_13(((CosAlpha * SinBeta * CosPhi) + (SinAlpha * SinPhi)));
	matYawPitchRoll.set_21((SinAlpha * CosBeta)); matYawPitchRoll.set_22(((SinAlpha * SinBeta * SinPhi) + (CosAlpha * CosPhi))); matYawPitchRoll.set_23(((SinAlpha * SinBeta * CosPhi) - (CosAlpha * SinPhi)));
	matYawPitchRoll.set_31((-SinBeta));           matYawPitchRoll.set_32((CosBeta * SinPhi));                                    matYawPitchRoll.set_33((CosBeta * CosPhi));

	Matrix3x3Mutliply(out, matYawPitchRoll);
}

void      Matrix3x3::Matrix3x3RotationQuaternion(Matrix3x3& out, Quaternion q)
{
	// perform a rotation thanks to a quaternion
	// inserted as parameter.
	Matrix3x3 matRotation = q.QuaternionToMatrix3Normalized();

	// apply 
	Matrix3x3Mutliply(out, matRotation);
}

//Similar to above function, but acts on original matrix
void	  Matrix3x3::Matrix3x3SetOrientation(Quaternion& q)
{
	Matrix3x3 orientation = q.QuaternionToMatrix3Normalized();

	*this = orientation;
}

Vector3	  Matrix3x3::Matrix3x3Transform(const Vector3 &v) const
{
	return Vector3(v.getX() * m_f11 + v.getY() * m_f12 + v.getZ() * m_f13,
		           v.getX() * m_f21 + v.getY() * m_f22 + v.getZ() * m_f23,
		           v.getX() * m_f31 + v.getY() * m_f32 + v.getZ() * m_f33);
}

float Matrix3x3::operator () (int line, int col)
{
	Matrix3x3 temp(*this);
	if(line == 1)
	{
		if(col == 1)
		{
			return temp.m_f11;
		}
		else if(col == 2)
		{
			return temp.m_f12;
		}
		else if(col == 3)
		{
			return temp.m_f13;
		}
	}
	else if(line == 2)
	{
		if(col == 1)
		{
			return temp.m_f21;
		}
		else if(col == 2)
		{
			return temp.m_f22;
		}
		else if(col == 3)
		{
			return temp.m_f23;
		}
	}
	else if(line == 3)
	{
		if(col == 1)
		{
			return temp.m_f31;
		}
		else if(col == 2)
		{
			return temp.m_f32;
		}
		else if(col == 3)
		{
			return temp.m_f33;
		}
	}

	return temp.m_f11;    // return the first one in case of bounds error
}

float  Matrix3x3::operator () (int line, int col) const
{
	Matrix3x3 temp(*this);
	if(line == 1)
	{
		if(col == 1)
		{
			return temp.m_f11;
		}
		else if(col == 2)
		{
			return temp.m_f12;
		}
		else if(col == 3)
		{
			return temp.m_f13;
		}
	}
	else if(line == 2)
	{
		if(col == 1)
		{
			return temp.m_f21;
		}
		else if(col == 2)
		{
			return temp.m_f22;
		}
		else if(col == 3)
		{
			return temp.m_f23;
		}
	}
	else if(line == 3)
	{
		if(col == 1)
		{
			return temp.m_f31;
		}
		else if(col == 2)
		{
			return temp.m_f32;
		}
		else if(col == 3)
		{
			return temp.m_f33;
		}
	}

	return temp.m_f11;    // return the first one in case of bounds error
}

Matrix3x3::operator float* ()
{
	float* mat3x3[9];

	mat3x3[0] = &m_f11;
	mat3x3[1] = &m_f12;
	mat3x3[2] = &m_f13;
	mat3x3[3] = &m_f21;
	mat3x3[4] = &m_f22;
	mat3x3[5] = &m_f23;
	mat3x3[6] = &m_f31;
	mat3x3[7] = &m_f32;
	mat3x3[8] = &m_f33;

	return mat3x3[0];
}

Matrix3x3::operator const float* () const
{
	const float* mat3x3[9];

	mat3x3[0] = &m_f11;
	mat3x3[1] = &m_f12;
	mat3x3[2] = &m_f13;
	mat3x3[3] = &m_f21;
	mat3x3[4] = &m_f22;
	mat3x3[5] = &m_f23;
	mat3x3[6] = &m_f31;
	mat3x3[7] = &m_f32;
	mat3x3[8] = &m_f33;

	return mat3x3[0];
}

Matrix3x3 Matrix3x3::operator + () const
{
	return Matrix3x3(+this->m_f11, +this->m_f12, +this->m_f13,
					 +this->m_f21, +this->m_f22, +this->m_f23,
					 +this->m_f31, +this->m_f32, +this->m_f33);
}

Matrix3x3 Matrix3x3::operator - () const
{
	return Matrix3x3(-this->m_f11, -this->m_f12, -this->m_f13,
					 -this->m_f21, -this->m_f22, -this->m_f23,
					 -this->m_f31, -this->m_f32, -this->m_f33);
}

Matrix3x3& Matrix3x3::operator += (const Matrix3x3& mat)
{
	m_f11 += mat.m_f11;
	m_f12 += mat.m_f12;
	m_f13 += mat.m_f13;
	m_f21 += mat.m_f21;
	m_f22 += mat.m_f22;
	m_f23 += mat.m_f23;
	m_f31 += mat.m_f31;
	m_f32 += mat.m_f32;
	m_f33 += mat.m_f33;

	return *this;
}

Matrix3x3& Matrix3x3::operator -= (const Matrix3x3& mat)
{
	m_f11 -= mat.m_f11;
	m_f12 -= mat.m_f12;
	m_f13 -= mat.m_f13;
	m_f21 -= mat.m_f21;
	m_f22 -= mat.m_f22;
	m_f23 -= mat.m_f23;
	m_f31 -= mat.m_f31;
	m_f32 -= mat.m_f32;
	m_f33 -= mat.m_f33;

	return *this;
}

Matrix3x3& Matrix3x3::operator *= (const Matrix3x3& mat)
{
	// same as matrix4x4multiplication
	// Multiplication between two matrices.
	Matrix3x3 temp(*this);

	// each row is going to be multiplied by each 
	// element of the column where the value processed is.
	/*****************first row****************************/
	m_f11 = (temp.m_f11 * mat.m_f11) + (temp.m_f12 * mat.m_f21) + (temp.m_f13 * mat.m_f31);
	m_f12 = (temp.m_f11 * mat.m_f12) + (temp.m_f12 * mat.m_f22) + (temp.m_f13 * mat.m_f32);
	m_f13 = (temp.m_f11 * mat.m_f13) + (temp.m_f12 * mat.m_f23) + (temp.m_f13 * mat.m_f33);
	/*****************second row***************************/
	m_f21 = (temp.m_f21 * mat.m_f11) + (temp.m_f22 * mat.m_f21) + (temp.m_f23 * mat.m_f31);
	m_f22 = (temp.m_f21 * mat.m_f12) + (temp.m_f22 * mat.m_f22) + (temp.m_f23 * mat.m_f32);
	m_f23 = (temp.m_f21 * mat.m_f13) + (temp.m_f22 * mat.m_f23) + (temp.m_f23 * mat.m_f33);
	/*****************third row****************************/
	m_f31 = (temp.m_f31 * mat.m_f11) + (temp.m_f32 * mat.m_f21) + (temp.m_f33 * mat.m_f31);
	m_f32 = (temp.m_f31 * mat.m_f12) + (temp.m_f32 * mat.m_f22) + (temp.m_f33 * mat.m_f32);
	m_f33 = (temp.m_f31 * mat.m_f13) + (temp.m_f32 * mat.m_f23) + (temp.m_f33 * mat.m_f33);

	return *this;
}

Matrix3x3& Matrix3x3::operator *= (const float& val)
{
	m_f11 *= val;
	m_f12 *= val;
	m_f13 *= val;
	m_f21 *= val;
	m_f22 *= val;
	m_f23 *= val;
	m_f31 *= val;
	m_f32 *= val;
	m_f33 *= val;

	return *this;
}

Matrix3x3& Matrix3x3::operator /= (const float& val)
{
	m_f11 /= val;
	m_f12 /= val;
	m_f13 /= val;
	m_f21 /= val;
	m_f22 /= val;
	m_f23 /= val;
	m_f31 /= val;
	m_f32 /= val;
	m_f33 /= val;

	return *this;
}

Matrix3x3 Matrix3x3::operator + (const Matrix3x3& mat) const
{
	Matrix3x3 temp(*this);
	temp += mat;

	return temp;
}

Matrix3x3 Matrix3x3::operator - (const Matrix3x3& mat) const
{
	Matrix3x3 temp(*this);
	temp -= mat;

	return temp;
}

Matrix3x3 Matrix3x3::operator * (const Matrix3x3& mat) const
{
	Matrix3x3 temp(*this);
	temp *= mat;

	return temp;
}

Matrix3x3 Matrix3x3::operator * (const float& val) const
{
	Matrix3x3 temp(*this);
	temp *= val;

	return temp;
}

Matrix3x3 Matrix3x3::operator / (const float& val) const
{
	Matrix3x3 temp(*this);
	temp /= val;

	return temp;
}

bool Matrix3x3::operator == (const Matrix3x3& mat) const
{
	return (Equal<float>(m_f11, mat.m_f11) && Equal<float>(m_f12, mat.m_f12) && Equal<float>(m_f13, mat.m_f13) &&
		    Equal<float>(m_f21, mat.m_f21) && Equal<float>(m_f22, mat.m_f22) && Equal<float>(m_f23, mat.m_f23) &&
			Equal<float>(m_f31, mat.m_f31) && Equal<float>(m_f32, mat.m_f32) && Equal<float>(m_f33, mat.m_f33));
}

bool Matrix3x3::operator != (const Matrix3x3& mat) const
{
	return (Different<float>(m_f11, mat.m_f11) || Different<float>(m_f12, mat.m_f12) || Different<float>(m_f13, mat.m_f13) ||
		    Different<float>(m_f21, mat.m_f21) || Different<float>(m_f22, mat.m_f22) || Different<float>(m_f23, mat.m_f23) ||
			Different<float>(m_f31, mat.m_f31) || Different<float>(m_f32, mat.m_f32) || Different<float>(m_f33, mat.m_f33));
}

Matrix3x3 operator * (float val, const Matrix3x3& mat)
{
	Matrix3x3 temp(mat);
	temp *= val;

	return temp;
}

void Matrix3x3::Matrix3x3SetBasis(const Vector3& one, const Vector3& two, const Vector3& three)
{
	m_f11 = one.getX();
	m_f12 = two.getX();
	m_f13 = three.getX();
	m_f21 = one.getY();
	m_f22 = two.getY();
	m_f23 = three.getY();
	m_f31 = one.getZ();
	m_f32 = two.getZ();
	m_f33 = three.getZ();

	/*
	Transverse order
	m_f11 = one.getX();
	m_f12 = one.getY();
	m_f13 = one.getZ();
	m_f21 = two.getX();
	m_f22 = two.getY();
	m_f23 = two.getZ();
	m_f31 = three.getX();
	m_f32 = three.getY();
	m_f33 = three.getZ();
	*/
}

Vector3 Matrix3x3::Matrix3x3TransformTranspose(const Vector3& v)
{
	return Vector3(v.getX()*m_f11 + v.getY()*m_f21 + v.getZ()*m_f31, v.getX()*m_f12 + v.getY()*m_f22 + v.getZ()*m_f32, v.getX()*m_f13 + v.getY()*m_f23 + v.getZ()*m_f33);
}

void Matrix3x3::Matrix3x3SetSkewSymetric(const Vector3& v)
{
	m_f11 = m_f22 = m_f33 = 0.0f;
	m_f12 = -v.getZ();
	m_f13 = v.getY();
	m_f21 = v.getZ();
	m_f23 = -v.getX();
	m_f31 = -v.getY();
	m_f32 = v.getX();
}