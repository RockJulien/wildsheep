#include "Ray.h"

using namespace Ocelot;

Ray::Ray()
{}

Ray::Ray(const Vector3 &o, const Vector3 &d)
	: origin(o), direction(d)
{}

Ray::Ray(const float oX, const float oY, const float oZ, const float dX, const float dY, const float dZ)
	: origin(oX, oY, oZ), direction(dX, dY, dZ)
{}

Vector3 Ray::operator() (const float t)
{
	return origin + t * direction;
}
