#ifndef DEF_MATRIX2X2_H
#define DEF_MATRIX2X2_H

/****************************************************************************
/ (c) Copyright 2012-2015 OcelotEngine All right reserved.
/
/ File Name:   Matrix2x2.h/.cpp
/
/ Description: This file provide a way to create a 2 by 2 matrix with its
/              basic contents expected for a matrix of this size in Games.
/              It will serve particularly for determinant computation for 
/              inverse calculation of a 3 by 3 matrix (by subdivision in 
/              2 by 2 ones) and could be used as simple matrix as well.
/
/ Author:      Larbi Julien & Carl Mitchell
/ Version:     1.0
/ Date:        05/02/2012
/****************************************************************************/

#include "Utility.h"

class Matrix2x2
{
private:

	// attributes representing the matrix
	float m_f11, m_f12;
	float m_f21, m_f22;

public:

	// Constructors & Destructor
	Matrix2x2();								   // default cst
	Matrix2x2(const Matrix2x2& mat);               // copy cst
	Matrix2x2& operator = (const Matrix2x2&);      // assgm cst
	Matrix2x2(float val);                          // overload cst
	Matrix2x2(double val);						   // overload cst
	Matrix2x2(float _11, float _12,				   // overload cst
			  float _21, float _22);
	Matrix2x2(double _11, double _12,			   // overload cst
			  double _21, double _22);
	~Matrix2x2();

	// Methods
	void      Matrix2x2Identity();					// create an identity matrix
	bool      Matrix2x2IsIdentity();				// check if the matrix is an identity one
	void      Matrix2x2Transpose(Matrix2x2& out);	// transpose the matrix
	void      Matrix2x2Inverse(Matrix2x2& out);	    // compute the inverse of the matrix
	float     Matrix2x2Determinant();				// compute the determinant of the matrix
	void      Matrix2x2Mutliply(Matrix2x2& out, const Matrix2x2& mat);
	void      Matrix2x2Mutliply(Matrix2x2& out, const Matrix2x2& mat1, const Matrix2x2& mat2);

	// Accessors
	inline float get_11() const { return m_f11; }
	inline float get_12() const { return m_f12; }
	inline float get_21() const { return m_f21; }
	inline float get_22() const { return m_f22; }
	inline void  set_11(float _11) { m_f11 = _11; }
	inline void  set_12(float _12) { m_f12 = _12; }
	inline void  set_21(float _21) { m_f21 = _21; }
	inline void  set_22(float _22) { m_f22 = _22; }

	// Operators overload
	/*********************Access Operats**************************/
	float operator () (int line, int col);
	float operator () (int line, int col) const;
	/*********************Cast Operators**************************/
	operator float* ();              // for being able to pass it through the shader as a (float*)
	operator const float* () const;  // constant version
	/*********************Unary Operators*************************/
	Matrix2x2 operator + () const;
	Matrix2x2 operator - () const;
	/*********************Assgm Operators*************************/
	Matrix2x2& operator += (const Matrix2x2&);
	Matrix2x2& operator -= (const Matrix2x2&);
	Matrix2x2& operator *= (const Matrix2x2&);
	Matrix2x2& operator *= (const float& val);
	Matrix2x2& operator /= (const float& val);
	/*********************Binary Operators************************/
	Matrix2x2 operator + (const Matrix2x2&) const;
	Matrix2x2 operator - (const Matrix2x2&) const;
	Matrix2x2 operator * (const Matrix2x2&) const;
	Matrix2x2 operator * (const float& val) const;
	Matrix2x2 operator / (const float& val) const;
	bool operator == (const Matrix2x2&) const;
	bool operator != (const Matrix2x2&) const;
	friend Matrix2x2 operator * (float, const Matrix2x2&);
};

#endif