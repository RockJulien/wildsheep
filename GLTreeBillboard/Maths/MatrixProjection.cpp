#include "MatrixProjection.h"

MatrixProjection::MatrixProjection() :
m_fWidth(0.0f), m_fHeight(0.0f), m_fNearPlaneDist(0.0f), m_fFarPlaneDist(0.0f),
m_fLeftPlaneDist(0.0f), m_fTopPlaneDist(0.0f), m_fRightPlaneDist(0.0f), m_fBottomPlaneDist(0.0f),
m_fFieldOfView(0.0f), m_fRatio(0.0f)
{

}

MatrixProjection::~MatrixProjection()
{

}

MatrixProjection MatrixProjection::MatrixProjectionOrthoLH(const float& width, const float& height, const float& nearPlaneDist, const float& farPlaneDist)
{
	// save data
	m_fWidth         = width;
	m_fHeight        = height;
	m_fNearPlaneDist = nearPlaneDist;
	m_fFarPlaneDist  = farPlaneDist;

	// initialize the matrix
	MatrixProjection projection;
	projection.Matrix4x4Identity();

	// fill the matrix

	// these two first are finally the center of the screen
	projection.set_11(2.0f / m_fWidth);  // scale on X axis (viewPort dependant).
	projection.set_22(2.0f / m_fHeight); // scale on Y axis (viewPort dependant).
	// these two last are for the near and far plane into the screen
	projection.set_33(1.0f / (m_fFarPlaneDist - m_fNearPlaneDist));
	// Note that the three previous are the scale parameter of a matrix.
	projection.set_43(m_fNearPlaneDist / (m_fNearPlaneDist - m_fFarPlaneDist));

	return projection;
}

MatrixProjection MatrixProjection::MatrixProjectionOrthoRH(const float& width, const float& height, const float& nearPlaneDist, const float& farPlaneDist)
{
	// save data
	m_fWidth         = width;
	m_fHeight        = height;
	m_fNearPlaneDist = nearPlaneDist;
	m_fFarPlaneDist  = farPlaneDist;

	// initialize the matrix
	MatrixProjection projection;
	projection.Matrix4x4Identity();

	// fill the matrix

	// these two first are finally the center of the screen
	projection.set_11(2.0f / m_fWidth);  // scale on the X axis (viewPort dependant).
	projection.set_22(2.0f / m_fHeight); // scale on the Y axis (viewPort dependant).
	// these two last are for the near and far plane into the screen
	projection.set_33(1.0f / (m_fNearPlaneDist - m_fFarPlaneDist));   // just inverse the far plane with the near one
	projection.set_43(m_fNearPlaneDist / (m_fNearPlaneDist - m_fFarPlaneDist));

	return projection;
}

MatrixProjection MatrixProjection::MatrixProjectionOrthoExCentredLH(const float& leftDist, const float& topDist, const float& rightDist, const float& bottomDist, const float& nearPlaneDist, const float& farPlaneDist)
{
	// Note: use leftPlaneDist = -width/2, rightPlaneDist = width/2, bottomPlaneDist = -height/2 and topPlaneDist = height/2
	// for passing to a normal OrthoLH.

	// save data
	m_fLeftPlaneDist   = leftDist;
	m_fTopPlaneDist    = topDist;
	m_fRightPlaneDist  = rightDist;
	m_fBottomPlaneDist = bottomDist;
	m_fNearPlaneDist   = nearPlaneDist;
	m_fFarPlaneDist    = farPlaneDist;

	// initialize the matrix
	MatrixProjection projection;
	projection.Matrix4x4Identity();

	// fill the matrix
	projection.set_11(2.0f / (m_fRightPlaneDist - m_fLeftPlaneDist)); // same principle that the previous one
	projection.set_22(2.0f / (m_fTopPlaneDist   - m_fBottomPlaneDist));
	projection.set_33(1.0f / (m_fFarPlaneDist - m_fNearPlaneDist));
	projection.set_43(m_fNearPlaneDist / (m_fNearPlaneDist - m_fFarPlaneDist));

	// but add translation on the X and Y axis amking it ex-centred.
	projection.set_41((m_fLeftPlaneDist + m_fRightPlaneDist) / (m_fLeftPlaneDist - m_fRightPlaneDist));  // translation on X
	projection.set_42((m_fTopPlaneDist + m_fBottomPlaneDist) / (m_fBottomPlaneDist - m_fTopPlaneDist));  // translation on Y

	return projection;
}

MatrixProjection MatrixProjection::MatrixProjectionOrthoExCentredRH(const float& leftDist, const float& topDist, const float& rightDist, const float& bottomDist, const float& nearPlaneDist, const float& farPlaneDist)
{
	// save data
	m_fLeftPlaneDist   = leftDist;
	m_fTopPlaneDist    = topDist;
	m_fRightPlaneDist  = rightDist;
	m_fBottomPlaneDist = bottomDist;
	m_fNearPlaneDist   = nearPlaneDist;
	m_fFarPlaneDist    = farPlaneDist;

	// initialize the matrix
	MatrixProjection projection;
	projection.Matrix4x4Identity();

	// fill the matrix
	projection.set_11(2.0f / (m_fRightPlaneDist - m_fLeftPlaneDist)); // same principle that the previous one
	projection.set_22(2.0f / (m_fTopPlaneDist   - m_fBottomPlaneDist));
	projection.set_33(1.0f / (m_fNearPlaneDist - m_fFarPlaneDist));  // just inverse far and near planes
	projection.set_43(m_fNearPlaneDist / (m_fNearPlaneDist - m_fFarPlaneDist));

	// but add translation on the X and Y axis amking it ex-centred.
	projection.set_41((m_fLeftPlaneDist + m_fRightPlaneDist) / (m_fLeftPlaneDist - m_fRightPlaneDist));  // translation on X
	projection.set_42((m_fTopPlaneDist + m_fBottomPlaneDist) / (m_fBottomPlaneDist - m_fTopPlaneDist));  // translation on Y

	return projection;
}

MatrixProjection MatrixProjection::MatrixProjectionPerspectiveLH(const float& width, const float& height, const float& nearPlaneDist, const float& farPlaneDist)
{
	// save data
	m_fWidth         = width;
	m_fHeight        = height;
	m_fNearPlaneDist = nearPlaneDist;
	m_fFarPlaneDist  = farPlaneDist;

	// initialize the matrix
	MatrixProjection projection; // we do not have to make an identity one for the perspective

	// fill the matrix
	projection.set_11((2.0f * m_fNearPlaneDist) / m_fWidth);  // this time, the scale will depend on the near plane in plus on the X axis for the perspective.
	projection.set_22((2.0f * m_fNearPlaneDist) / m_fHeight); // same as above for the scale on the Y axis.
	projection.set_33(m_fFarPlaneDist / (m_fFarPlaneDist - m_fNearPlaneDist));  // the size is going to change regarding to the distance. (why farPlane as numerator)
	projection.set_43((m_fNearPlaneDist * m_fFarPlaneDist) / (m_fNearPlaneDist - m_fFarPlaneDist)); // increases of the scale between far and near plane.
	projection.set_34(1.0f);

	return projection;
}

MatrixProjection MatrixProjection::MatrixProjectionPerspectiveRH(const float& width, const float& height, const float& nearPlaneDist, const float& farPlaneDist)
{
	// save data
	m_fWidth         = width;
	m_fHeight        = height;
	m_fNearPlaneDist = nearPlaneDist;
	m_fFarPlaneDist  = farPlaneDist;

	// initialize the matrix
	MatrixProjection projection; // we do not have to make an identity one for the perspective

	// fill the matrix
	projection.set_11((2.0f * m_fNearPlaneDist) / m_fWidth);  // this time, the scale will depend on the near plane in plus on the X axis for the perspective.
	projection.set_22((2.0f * m_fNearPlaneDist) / m_fHeight); // same as above for the scale on the Y axis.
	projection.set_33(m_fFarPlaneDist / (m_fNearPlaneDist - m_fFarPlaneDist));  // the size is going to change regarding to the distance. (why farPlane as numerator)
	projection.set_43((m_fNearPlaneDist * m_fFarPlaneDist) / (m_fNearPlaneDist - m_fFarPlaneDist)); // increases of the scale between far and near plane.
	projection.set_34(-1.0f);

	return projection;
}

MatrixProjection MatrixProjection::MatrixProjectionPerspectiveFOVLH(const float& fieldOfView, const float& ratio, const float& nearPlaneDist, const float& farPlaneDist)
{
	// save data
	m_fFieldOfView   = fieldOfView;
	m_fRatio         = ratio;
	m_fNearPlaneDist = nearPlaneDist;
	m_fFarPlaneDist  = farPlaneDist;

	// compute the X and Y scale values
	float scaleY = Cotangent(m_fFieldOfView * 0.5f);  // h = cot(FOV/2)
	float scaleX = scaleY / m_fRatio;                 // w = h / aspectRatio.

	// initialize matrix
	MatrixProjection projection;

	// fill the matrix
	projection.set_11(scaleX);
	projection.set_22(scaleY);
	projection.set_33(m_fFarPlaneDist / (m_fFarPlaneDist - m_fNearPlaneDist));
	projection.set_43((m_fNearPlaneDist * m_fFarPlaneDist) / (m_fNearPlaneDist - m_fFarPlaneDist));
	projection.set_34(1.0f);

	return projection;
}

MatrixProjection MatrixProjection::MatrixProjectionPerspectiveFOVRH(const float& fieldOfView, const float& ratio, const float& nearPlaneDist, const float& farPlaneDist)
{
	// save data
	m_fFieldOfView   = fieldOfView;
	m_fRatio         = ratio;
	m_fNearPlaneDist = nearPlaneDist;
	m_fFarPlaneDist  = farPlaneDist;

	// compute the X and Y scale values
	float scaleY = Cotangent(m_fFieldOfView * 0.5f);  // h = cot(FOV/2)
	float scaleX = scaleY / m_fRatio;				  // w = h / aspectRatio.

	// initialize matrix
	MatrixProjection projection;

	// fill the matrix
	projection.set_11(scaleX);
	projection.set_22(scaleY);
	projection.set_33(m_fFarPlaneDist / (m_fNearPlaneDist - m_fFarPlaneDist));
	projection.set_43((m_fNearPlaneDist * m_fFarPlaneDist) / (m_fNearPlaneDist - m_fFarPlaneDist));
	projection.set_34(-1.0f);

	return projection;
}

MatrixProjection MatrixProjection::MatrixProjectionPerspectiveExCentredLH(const float& leftDist, const float& topDist, const float& rightDist, const float& bottomDist, const float& nearPlaneDist, const float& farPlaneDist)
{
	// save data
	m_fLeftPlaneDist   = leftDist;
	m_fTopPlaneDist    = topDist;
	m_fRightPlaneDist  = rightDist;
	m_fBottomPlaneDist = bottomDist;
	m_fNearPlaneDist   = nearPlaneDist;
	m_fFarPlaneDist    = farPlaneDist;

	// initialize the matrix
	MatrixProjection projection;

	// fill the matrix
	projection.set_11((2.0f * m_fNearPlaneDist) / (m_fRightPlaneDist - m_fLeftPlaneDist));
	projection.set_22((2.0f * m_fNearPlaneDist) / (m_fTopPlaneDist - m_fBottomPlaneDist));
	projection.set_33(m_fFarPlaneDist / (m_fFarPlaneDist - m_fNearPlaneDist));
	projection.set_43((m_fNearPlaneDist * m_fFarPlaneDist) / (m_fNearPlaneDist - m_fFarPlaneDist));
	projection.set_34(1.0f);
	projection.set_31((m_fLeftPlaneDist + m_fRightPlaneDist) / (m_fLeftPlaneDist - m_fRightPlaneDist));
	projection.set_32((m_fTopPlaneDist + m_fBottomPlaneDist) / (m_fBottomPlaneDist - m_fTopPlaneDist));

	return projection;
}

MatrixProjection MatrixProjection::MatrixProjectionPerspectiveExCentredRH(const float& leftDist, const float& topDist, const float& rightDist, const float& bottomDist, const float& nearPlaneDist, const float& farPlaneDist)
{
	// save data
	m_fLeftPlaneDist   = leftDist;
	m_fTopPlaneDist    = topDist;
	m_fRightPlaneDist  = rightDist;
	m_fBottomPlaneDist = bottomDist;
	m_fNearPlaneDist   = nearPlaneDist;
	m_fFarPlaneDist    = farPlaneDist;

	// initialize the matrix
	MatrixProjection projection;

	// fill the matrix
	projection.set_11((2.0f * m_fNearPlaneDist) / (m_fRightPlaneDist - m_fLeftPlaneDist));
	projection.set_22((2.0f * m_fNearPlaneDist) / (m_fTopPlaneDist - m_fBottomPlaneDist));
	projection.set_33(m_fFarPlaneDist / (m_fNearPlaneDist - m_fFarPlaneDist));   // just inverse minus in calc
	projection.set_43((m_fNearPlaneDist * m_fFarPlaneDist) / (m_fNearPlaneDist - m_fFarPlaneDist));
	projection.set_34(-1.0f);  // just oppose
	projection.set_31((m_fLeftPlaneDist + m_fRightPlaneDist) / (m_fRightPlaneDist - m_fLeftPlaneDist));  // just inverse minus in calc
	projection.set_32((m_fTopPlaneDist + m_fBottomPlaneDist) / (m_fTopPlaneDist - m_fBottomPlaneDist));  // just inverse minus in calc

	return projection;
}