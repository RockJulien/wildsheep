#include "GDevice.h"

GDevice::GDevice() :
m_deviceContext(NULL), m_renderContext(NULL)
{

}

GDevice::~GDevice()
{
	
}

BBOOL GDevice::LoadExtension()
{
	// Init opengl and extension(s)
	if ( gl3wInit() )
	{
		return false;
	}

	// Load all the OpenGL extensions needed.
	wglChoosePixFmtARB = (PFNWGLCHOOSEPIXELFORMATARBPROC)wglGetProcAddress("wglChoosePixelFormatARB");
	if(!wglChoosePixFmtARB)
		return false;

	wglCreateContextAttrARB = (PFNWGLCREATECONTEXTATTRIBSARBPROC)wglGetProcAddress("wglCreateContextAttribsARB");
	if(!wglCreateContextAttrARB)
		return false;

	wglSwapIntervalExt = (PFNWGLSWAPINTERVALEXTPROC)wglGetProcAddress("wglSwapIntervalEXT");
	if(!wglSwapIntervalExt)
		return false;

	AttachShader = glAttachShader;
	if(!AttachShader)
		return false;

	BindBuffer = glBindBuffer;
	if(!BindBuffer)
		return false;

	BindVertArray = glBindVertexArray;
	if(!BindVertArray)
		return false;

	BufferData = glBufferData;
	if(!BufferData)
		return false;

	CompileShader = glCompileShader;
	if(!CompileShader)
		return false;

	CreateProgram = glCreateProgram;
	if(!CreateProgram)
		return false;

	CreateShader = glCreateShader;
	if(!CreateShader)
		return false;

	DeleteBuffers = glDeleteBuffers;
	if(!DeleteBuffers)
		return false;

	DeleteProgram = glDeleteProgram;
	if(!DeleteProgram)
		return false;

	DeleteShader = glDeleteShader;
	if(!DeleteShader)
		return false;

	DeleteVertArrays = glDeleteVertexArrays;
	if(!DeleteVertArrays)
		return false;

	DetachShader = glDetachShader;
	if(!DetachShader)
		return false;

	EnableVertAttrArrays = glEnableVertexAttribArray;
	if(!EnableVertAttrArrays)
		return false;

	GenBuffers = glGenBuffers;
	if(!GenBuffers)
		return false;

	GenVertArrays = glGenVertexArrays;
	if(!GenVertArrays)
		return false;

	GetAttrLocation = glGetAttribLocation;
	if(!GetAttrLocation)
		return false;

	GetProgramInfoLog = glGetProgramInfoLog;
	if(!GetProgramInfoLog)
		return false;

	GetProgramIV = glGetProgramiv;
	if(!GetProgramIV)
		return false;

	GetShaderInfoLog = glGetShaderInfoLog;
	if(!GetShaderInfoLog)
		return false;

	GetShaderIV = glGetShaderiv;
	if(!GetShaderIV)
		return false;

	LinkProgram = glLinkProgram;
	if(!LinkProgram)
		return false;

	ShaderSource = glShaderSource;
	if(!ShaderSource)
		return false;

	UseProgram = glUseProgram;
	if(!UseProgram)
		return false;

	VertAttrPointer = glVertexAttribPointer;
	if(!VertAttrPointer)
		return false;

	BindAttrLocation = glBindAttribLocation;
	if(!BindAttrLocation)
		return false;

	GetUniformLocation = glGetUniformLocation;
	if(!GetUniformLocation)
		return false;

	UniformMat4fv = glUniformMatrix4fv;
	if(!UniformMat4fv)
		return false;

	ActiveTexture = glActiveTexture;
	if(!ActiveTexture)
		return false;

	CompressTextImg2D = glCompressedTexImage2D;
	if(!CompressTextImg2D)
		return false;

	TexImage3D = glTexImage3D;
	if(!TexImage3D)
		return false;

	CompressTextImg3D = glCompressedTexImage3D;
	if(!CompressTextImg3D)
		return false;

	Uniform1i = glUniform1i;
	if(!Uniform1i)
		return false;

	GenerateMipMap = glGenerateMipmap;
	if(!GenerateMipMap)
		return false;

	DisableVertAttrArray = glDisableVertexAttribArray;
	if(!DisableVertAttrArray)
		return false;

	Uniform1f = glUniform1f;
	if(!Uniform1f)
		return false;

	Uniform3fv = glUniform3fv;
	if(!Uniform3fv)
		return false;

	Uniform4fv = glUniform4fv;
	if(!Uniform4fv)
		return false;

	DrawRangeElements = glDrawRangeElements;
	if(!DrawRangeElements)
	{
		return false;
	}

	BindRenderbuffer = glBindRenderbuffer;
	if(!BindRenderbuffer)
	{
		return false;
	}

	RenderbufferStorage = glRenderbufferStorage;
	if(!RenderbufferStorage)
	{
		return false;
	}

	FramebufferRenderbuffer = glFramebufferRenderbuffer;
	if(!FramebufferRenderbuffer)
	{
		return false;
	}

	GenFrameBuffers = glGenFramebuffers;
	if(!GenFrameBuffers)
	{
		return false;
	}

	GenRenderBuffers = glGenRenderbuffers;
	if(!GenRenderBuffers)
	{
		return false;
	}

	BindFramebuffer = glBindFramebuffer;
	if(!BindFramebuffer)
	{
		return false;
	}

	FrameBufferTexture = glFramebufferTexture;
	if(!FrameBufferTexture)
	{
		return false;
	}

	FrameBufferTexture1D = glFramebufferTexture1D;
	if(!FrameBufferTexture1D)
	{
		return false;
	}

	FrameBufferTexture2D = glFramebufferTexture2D;
	if(!FrameBufferTexture2D)
	{
		return false;
	}

	FrameBufferTexture3D = glFramebufferTexture3D;
	if(!FrameBufferTexture3D)
	{
		return false;
	}

	TexSubImage3D = glTexSubImage3D;
	if(!TexSubImage3D)
	{
		return false;
	}

	DeleteFrameBuffers = glDeleteFramebuffers;
	if(!DeleteFrameBuffers)
	{
		return false;
	}

	DeleteRenderBuffers = glDeleteRenderbuffers;
	if(!DeleteRenderBuffers)
	{
		return false;
	}

	DrawBuffers = glDrawBuffers;
	if(!DrawBuffers)
	{
		return false;
	}

	DrawElementsBaseVertex = glDrawElementsBaseVertex;
	if(!DrawElementsBaseVertex)
	{
		return false;
	}

	CheckFrameBufferStatus = glCheckFramebufferStatus;
	if(!CheckFrameBufferStatus)
	{
		return false;
	}

	ClearBufferiv  = glClearBufferiv;
	if(!ClearBufferiv)
	{
		return false;
	}

	ClearBufferuiv = glClearBufferuiv;
	if(!ClearBufferuiv)
	{
		return false;
	}

	ClearBufferfv  = glClearBufferfv;
	if(!ClearBufferfv)
	{
		return false;
	}

	ClearBufferfi  = glClearBufferfi;
	if(!ClearBufferfi)
	{
		return false;
	}

	MapBuffer = glMapBuffer;
	if(!MapBuffer)
	{
		return false;
	}

	UnmapBuffer = glUnmapBuffer;
	if(!UnmapBuffer)
	{
		return false;
	}

	BlendEquationSeparate = glBlendEquationSeparate;
	if(!BlendEquationSeparate)
	{
		return false;
	}

	BlendFuncSeparate = glBlendFuncSeparate;
	if(!BlendFuncSeparate)
	{
		return false;
	}

	BlendColor = glBlendColor;
	if(!BlendColor)
	{
		return false;
	}

	return true;
}

BBOOL  GDevice::InitializeExtensions(HWND winInst)
{
	HDC deviceCont;
	HGLRC renderCont;
	PIXELFORMATDESCRIPTOR pixFmt;
	BIGINT error;
	BBOOL hr;

	// Get the device context.
	deviceCont = GetDC(winInst);
	if(!deviceCont)
		return false;

	// Set temporary pixFmt
	error = SetPixelFormat(deviceCont, 1, &pixFmt);
	if(error != 1)
		return false;

	// Set temporary render context.
	renderCont = wglCreateContext(deviceCont);
	if(!renderCont)
		return false;

	// Set the render context as the one for the current win.
	error = wglMakeCurrent(deviceCont, renderCont);
	if(error != 1)
		return false;

	// Initialize the xtensions.
	hr = LoadExtension();
	if(!hr)
		return false;

	// Release the render context.
	wglMakeCurrent(NULL, NULL);
	wglDeleteContext(renderCont);
	renderCont = NULL;

	// Release the device context.
	ReleaseDC(winInst, deviceCont);
	deviceCont = NULL;

	return true;
}

BBOOL  GDevice::InitializeDevice(HWND winInst, BIGINT width, BIGINT height, FFLOAT fFar, FFLOAT fNear, BBOOL vsync)
{
	BIGINT  attrList19[19];
	BIGINT  pixFmt[1];
	UBIGINT fmtCount;
	BIGINT  hr;
	PIXELFORMATDESCRIPTOR pixFmtDesc;
	BIGINT  attrList5[5];
	CCHAR*  GPUName, *APIName;

	// Get the device context.
	m_deviceContext = GetDC(winInst);
	if(!m_deviceContext)
		return false;

	GLenum lError = glGetError();

	// Prepare the attr list for support checking.
	// OpenGL support.
	attrList19[0] = WGL_SUPPORT_OPENGL_ARB;
	attrList19[1] = TRUE;

	// Render to window support.
	attrList19[2] = WGL_DRAW_TO_WINDOW_ARB;
	attrList19[3] = TRUE;

	// Graphic acceleration support
	attrList19[4] = WGL_ACCELERATION_ARB;
	attrList19[5] = WGL_FULL_ACCELERATION_ARB;

	// 24Bits Color support.
	attrList19[6] = WGL_COLOR_BITS_ARB;
	attrList19[7] = 24;

	// 24Bits Depth support.
	attrList19[8] = WGL_DEPTH_BITS_ARB;
	attrList19[9] = 24;

	// Double buffer support.
	attrList19[10] = WGL_DOUBLE_BUFFER_ARB;
	attrList19[11] = TRUE;

	// Swapping chain support.
	attrList19[12] = WGL_SWAP_METHOD_ARB;
	attrList19[13] = WGL_SWAP_EXCHANGE_ARB;

	// Alpha support
	attrList19[14] = WGL_PIXEL_TYPE_ARB;
	attrList19[15] = WGL_TYPE_RGBA_ARB;

	// 8Bits Stencil buffer support.
	attrList19[16] = WGL_STENCIL_BITS_ARB;
	attrList19[17] = 8;

	// Null Terminate the list.
	attrList19[18] = NULL;

	// Check for an appropriate pixel format.
	hr = wglChoosePixFmtARB(m_deviceContext, attrList19, NULL, 1, pixFmt, &fmtCount);
	if(hr != 1)
		return false;

	lError = glGetError();

	// If support the desired pixel fmt, set as current.
	hr = SetPixelFormat(m_deviceContext, pixFmt[0], &pixFmtDesc);
	if(hr != 1)
		return false;

	lError = glGetError();

	// Then, Set the version of openGL.
	attrList5[0] = WGL_CONTEXT_MAJOR_VERSION_ARB;
	attrList5[1] = 4; // 4.0 and up to 4.last version.
	attrList5[2] = WGL_CONTEXT_MINOR_VERSION_ARB;
	attrList5[3] = 0;

	// Null Terminate the list.
	attrList5[4] = NULL;

	// Create the OpenGL render context.
	m_renderContext = wglCreateContextAttrARB(m_deviceContext, NULL, attrList5);
	if(!m_renderContext)
		return false;

	lError = glGetError();

	// Set the current render context
	hr = wglMakeCurrent(m_deviceContext, m_renderContext);
	if(hr != 1)
		return false;

	lError = glGetError();

	// Clear the depth buffer.
	glClearDepth(1.0f);

	// Enable depth testing
	glEnable(GL_DEPTH_TEST);

	// Set the polygon winding as clockwise for left-handed system.
	glFrontFace(GL_CW);

	// Enable back face culling.
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);

	// Grab the GPU and device names.
	GPUName = (CCHAR*)glGetString(GL_VENDOR);
	APIName = (CCHAR*)glGetString(GL_RENDERER);

	// Store above info in the class array
	strcpy_s(m_cGPUDesc, GPUName);
	strcat_s(m_cGPUDesc, "-");
	strcat_s(m_cGPUDesc, APIName);

	// Turn on or not the vertical synchronisation
	if(vsync)
		hr = wglSwapIntervalExt(1); // 60.
	else
		hr = wglSwapIntervalExt(0); // all cycles you can give me.

	lError = glGetError();

	// Check vsync.
	if(hr != 1)
		return false;

	return true;
}

BBOOL GDevice::StartRender(BBOOL clearPix, BBOOL clearDepthStencil)
{
	// Set the clear color for clearing buffers.
	glClearColor( 0.0f, 0.0f, 0.0f, 1.0f ); // Black
	glClearDepth( 1.0f );

	if
		( clearPix )
	{
		glClear(GL_COLOR_BUFFER_BIT);
	}

	if
		( clearDepthStencil )
	{
		glClear(GL_DEPTH_BUFFER_BIT);
	}

	return true;
}

VVOID  GDevice::StopRender()
{
	// Present the back buffer to the screen.
	SwapBuffers(m_deviceContext); // Equal to Present( vsync, ..) in DirectX
}

VVOID  GDevice::Release(HWND winInst)
{
	// Release the render context.
	if(m_renderContext)
	{
		wglMakeCurrent(NULL, NULL);
		wglDeleteContext(m_renderContext);
		m_renderContext = 0;
	}

	// Release the device context.
	if(m_deviceContext)
	{
		ReleaseDC(winInst, m_deviceContext);
		m_deviceContext = NULL;
	}
}
